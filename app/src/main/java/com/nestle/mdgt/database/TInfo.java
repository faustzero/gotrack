package com.nestle.mdgt.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TINFO".
 */
@Entity
public class TInfo {

    @Id
    private Long id;
    private Integer InfoId;
    private String InfoName;
    private String StartDate;
    private String EndDate;
    private String ImagePath;

    @Generated
    public TInfo() {
    }

    public TInfo(Long id) {
        this.id = id;
    }

    @Generated
    public TInfo(Long id, Integer InfoId, String InfoName, String StartDate, String EndDate, String ImagePath) {
        this.id = id;
        this.InfoId = InfoId;
        this.InfoName = InfoName;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.ImagePath = ImagePath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInfoId() {
        return InfoId;
    }

    public void setInfoId(Integer InfoId) {
        this.InfoId = InfoId;
    }

    public String getInfoName() {
        return InfoName;
    }

    public void setInfoName(String InfoName) {
        this.InfoName = InfoName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String ImagePath) {
        this.ImagePath = ImagePath;
    }

}
