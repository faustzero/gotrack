package com.nestle.mdgt.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TREPORT_COMPETITOR".
*/
public class TReportCompetitorDao extends AbstractDao<TReportCompetitor, Long> {

    public static final String TABLENAME = "TREPORT_COMPETITOR";

    /**
     * Properties of entity TReportCompetitor.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property VisitId = new Property(1, String.class, "VisitId", false, "VISIT_ID");
        public final static Property SurveyorId = new Property(2, Integer.class, "SurveyorId", false, "SURVEYOR_ID");
        public final static Property StoreId = new Property(3, String.class, "StoreId", false, "STORE_ID");
        public final static Property ActivityTypeId = new Property(4, int.class, "ActivityTypeId", false, "ACTIVITY_TYPE_ID");
        public final static Property ActivityTypeName = new Property(5, String.class, "ActivityTypeName", false, "ACTIVITY_TYPE_NAME");
        public final static Property Harga = new Property(6, int.class, "Harga", false, "HARGA");
        public final static Property ProductId = new Property(7, int.class, "ProductId", false, "PRODUCT_ID");
        public final static Property ProductName = new Property(8, String.class, "ProductName", false, "PRODUCT_NAME");
        public final static Property CategoryId = new Property(9, int.class, "CategoryId", false, "CATEGORY_ID");
        public final static Property CategoryName = new Property(10, String.class, "CategoryName", false, "CATEGORY_NAME");
        public final static Property StartDate = new Property(11, String.class, "StartDate", false, "START_DATE");
        public final static Property EndDate = new Property(12, String.class, "EndDate", false, "END_DATE");
        public final static Property Description = new Property(13, String.class, "Description", false, "DESCRIPTION");
        public final static Property PhotoPath = new Property(14, String.class, "PhotoPath", false, "PHOTO_PATH");
        public final static Property IsDone = new Property(15, Boolean.class, "IsDone", false, "IS_DONE");
    }


    public TReportCompetitorDao(DaoConfig config) {
        super(config);
    }
    
    public TReportCompetitorDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TREPORT_COMPETITOR\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"VISIT_ID\" TEXT," + // 1: VisitId
                "\"SURVEYOR_ID\" INTEGER," + // 2: SurveyorId
                "\"STORE_ID\" TEXT," + // 3: StoreId
                "\"ACTIVITY_TYPE_ID\" INTEGER NOT NULL ," + // 4: ActivityTypeId
                "\"ACTIVITY_TYPE_NAME\" TEXT NOT NULL ," + // 5: ActivityTypeName
                "\"HARGA\" INTEGER NOT NULL ," + // 6: Harga
                "\"PRODUCT_ID\" INTEGER NOT NULL ," + // 7: ProductId
                "\"PRODUCT_NAME\" TEXT NOT NULL ," + // 8: ProductName
                "\"CATEGORY_ID\" INTEGER NOT NULL ," + // 9: CategoryId
                "\"CATEGORY_NAME\" TEXT NOT NULL ," + // 10: CategoryName
                "\"START_DATE\" TEXT NOT NULL ," + // 11: StartDate
                "\"END_DATE\" TEXT NOT NULL ," + // 12: EndDate
                "\"DESCRIPTION\" TEXT," + // 13: Description
                "\"PHOTO_PATH\" TEXT," + // 14: PhotoPath
                "\"IS_DONE\" INTEGER);"); // 15: IsDone
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TREPORT_COMPETITOR\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TReportCompetitor entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String VisitId = entity.getVisitId();
        if (VisitId != null) {
            stmt.bindString(2, VisitId);
        }
 
        Integer SurveyorId = entity.getSurveyorId();
        if (SurveyorId != null) {
            stmt.bindLong(3, SurveyorId);
        }
 
        String StoreId = entity.getStoreId();
        if (StoreId != null) {
            stmt.bindString(4, StoreId);
        }
        stmt.bindLong(5, entity.getActivityTypeId());
        stmt.bindString(6, entity.getActivityTypeName());
        stmt.bindLong(7, entity.getHarga());
        stmt.bindLong(8, entity.getProductId());
        stmt.bindString(9, entity.getProductName());
        stmt.bindLong(10, entity.getCategoryId());
        stmt.bindString(11, entity.getCategoryName());
        stmt.bindString(12, entity.getStartDate());
        stmt.bindString(13, entity.getEndDate());
 
        String Description = entity.getDescription();
        if (Description != null) {
            stmt.bindString(14, Description);
        }
 
        String PhotoPath = entity.getPhotoPath();
        if (PhotoPath != null) {
            stmt.bindString(15, PhotoPath);
        }
 
        Boolean IsDone = entity.getIsDone();
        if (IsDone != null) {
            stmt.bindLong(16, IsDone ? 1L: 0L);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TReportCompetitor entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String VisitId = entity.getVisitId();
        if (VisitId != null) {
            stmt.bindString(2, VisitId);
        }
 
        Integer SurveyorId = entity.getSurveyorId();
        if (SurveyorId != null) {
            stmt.bindLong(3, SurveyorId);
        }
 
        String StoreId = entity.getStoreId();
        if (StoreId != null) {
            stmt.bindString(4, StoreId);
        }
        stmt.bindLong(5, entity.getActivityTypeId());
        stmt.bindString(6, entity.getActivityTypeName());
        stmt.bindLong(7, entity.getHarga());
        stmt.bindLong(8, entity.getProductId());
        stmt.bindString(9, entity.getProductName());
        stmt.bindLong(10, entity.getCategoryId());
        stmt.bindString(11, entity.getCategoryName());
        stmt.bindString(12, entity.getStartDate());
        stmt.bindString(13, entity.getEndDate());
 
        String Description = entity.getDescription();
        if (Description != null) {
            stmt.bindString(14, Description);
        }
 
        String PhotoPath = entity.getPhotoPath();
        if (PhotoPath != null) {
            stmt.bindString(15, PhotoPath);
        }
 
        Boolean IsDone = entity.getIsDone();
        if (IsDone != null) {
            stmt.bindLong(16, IsDone ? 1L: 0L);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public TReportCompetitor readEntity(Cursor cursor, int offset) {
        TReportCompetitor entity = new TReportCompetitor( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // VisitId
            cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2), // SurveyorId
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // StoreId
            cursor.getInt(offset + 4), // ActivityTypeId
            cursor.getString(offset + 5), // ActivityTypeName
            cursor.getInt(offset + 6), // Harga
            cursor.getInt(offset + 7), // ProductId
            cursor.getString(offset + 8), // ProductName
            cursor.getInt(offset + 9), // CategoryId
            cursor.getString(offset + 10), // CategoryName
            cursor.getString(offset + 11), // StartDate
            cursor.getString(offset + 12), // EndDate
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13), // Description
            cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14), // PhotoPath
            cursor.isNull(offset + 15) ? null : cursor.getShort(offset + 15) != 0 // IsDone
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TReportCompetitor entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setVisitId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setSurveyorId(cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2));
        entity.setStoreId(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setActivityTypeId(cursor.getInt(offset + 4));
        entity.setActivityTypeName(cursor.getString(offset + 5));
        entity.setHarga(cursor.getInt(offset + 6));
        entity.setProductId(cursor.getInt(offset + 7));
        entity.setProductName(cursor.getString(offset + 8));
        entity.setCategoryId(cursor.getInt(offset + 9));
        entity.setCategoryName(cursor.getString(offset + 10));
        entity.setStartDate(cursor.getString(offset + 11));
        entity.setEndDate(cursor.getString(offset + 12));
        entity.setDescription(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
        entity.setPhotoPath(cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14));
        entity.setIsDone(cursor.isNull(offset + 15) ? null : cursor.getShort(offset + 15) != 0);
     }
    
    @Override
    protected final Long updateKeyAfterInsert(TReportCompetitor entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(TReportCompetitor entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TReportCompetitor entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
