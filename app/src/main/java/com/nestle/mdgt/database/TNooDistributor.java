package com.nestle.mdgt.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TNOO_DISTRIBUTOR".
 */
@Entity
public class TNooDistributor {

    @Id
    private Long id;
    private Integer DistributorId;
    private String DistributorName;
    private Integer AreaId;

    @Generated
    public TNooDistributor() {
    }

    public TNooDistributor(Long id) {
        this.id = id;
    }

    @Generated
    public TNooDistributor(Long id, Integer DistributorId, String DistributorName, Integer AreaId) {
        this.id = id;
        this.DistributorId = DistributorId;
        this.DistributorName = DistributorName;
        this.AreaId = AreaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDistributorId() {
        return DistributorId;
    }

    public void setDistributorId(Integer DistributorId) {
        this.DistributorId = DistributorId;
    }

    public String getDistributorName() {
        return DistributorName;
    }

    public void setDistributorName(String DistributorName) {
        this.DistributorName = DistributorName;
    }

    public Integer getAreaId() {
        return AreaId;
    }

    public void setAreaId(Integer AreaId) {
        this.AreaId = AreaId;
    }
    @Override
    public String toString() {
        return DistributorName;
    }
}
