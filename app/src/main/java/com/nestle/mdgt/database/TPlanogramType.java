package com.nestle.mdgt.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TPLANOGRAM_TYPE".
 */
@Entity
public class TPlanogramType {

    @Id
    private Long id;
    private Integer PlanogramTypeId;
    private String PlanogramTypeName;
    private Integer ProductCategoryId;
    private String ProductCategoryName;
    private String GuidelinePhotoPath;

    @Generated
    public TPlanogramType() {
    }

    public TPlanogramType(Long id) {
        this.id = id;
    }

    @Generated
    public TPlanogramType(Long id, Integer PlanogramTypeId, String PlanogramTypeName, Integer ProductCategoryId, String ProductCategoryName, String GuidelinePhotoPath) {
        this.id = id;
        this.PlanogramTypeId = PlanogramTypeId;
        this.PlanogramTypeName = PlanogramTypeName;
        this.ProductCategoryId = ProductCategoryId;
        this.ProductCategoryName = ProductCategoryName;
        this.GuidelinePhotoPath = GuidelinePhotoPath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPlanogramTypeId() {
        return PlanogramTypeId;
    }

    public void setPlanogramTypeId(Integer PlanogramTypeId) {
        this.PlanogramTypeId = PlanogramTypeId;
    }

    public String getPlanogramTypeName() {
        return PlanogramTypeName;
    }

    public void setPlanogramTypeName(String PlanogramTypeName) {
        this.PlanogramTypeName = PlanogramTypeName;
    }

    public Integer getProductCategoryId() {
        return ProductCategoryId;
    }

    public void setProductCategoryId(Integer ProductCategoryId) {
        this.ProductCategoryId = ProductCategoryId;
    }

    public String getProductCategoryName() {
        return ProductCategoryName;
    }

    public void setProductCategoryName(String ProductCategoryName) {
        this.ProductCategoryName = ProductCategoryName;
    }

    public String getGuidelinePhotoPath() {
        return GuidelinePhotoPath;
    }

    public void setGuidelinePhotoPath(String GuidelinePhotoPath) {
        this.GuidelinePhotoPath = GuidelinePhotoPath;
    }

    @Override
    public String toString() {
        return PlanogramTypeName;
    }
}
