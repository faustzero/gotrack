package com.nestle.mdgt.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TKABUPATEN".
*/
public class TKabupatenDao extends AbstractDao<TKabupaten, Long> {

    public static final String TABLENAME = "TKABUPATEN";

    /**
     * Properties of entity TKabupaten.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property KabupatenId = new Property(1, Integer.class, "KabupatenId", false, "KABUPATEN_ID");
        public final static Property KabupatenName = new Property(2, String.class, "KabupatenName", false, "KABUPATEN_NAME");
        public final static Property AreaId = new Property(3, Integer.class, "AreaId", false, "AREA_ID");
    }


    public TKabupatenDao(DaoConfig config) {
        super(config);
    }
    
    public TKabupatenDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TKABUPATEN\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"KABUPATEN_ID\" INTEGER," + // 1: KabupatenId
                "\"KABUPATEN_NAME\" TEXT," + // 2: KabupatenName
                "\"AREA_ID\" INTEGER);"); // 3: AreaId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TKABUPATEN\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TKabupaten entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer KabupatenId = entity.getKabupatenId();
        if (KabupatenId != null) {
            stmt.bindLong(2, KabupatenId);
        }
 
        String KabupatenName = entity.getKabupatenName();
        if (KabupatenName != null) {
            stmt.bindString(3, KabupatenName);
        }
 
        Integer AreaId = entity.getAreaId();
        if (AreaId != null) {
            stmt.bindLong(4, AreaId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TKabupaten entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer KabupatenId = entity.getKabupatenId();
        if (KabupatenId != null) {
            stmt.bindLong(2, KabupatenId);
        }
 
        String KabupatenName = entity.getKabupatenName();
        if (KabupatenName != null) {
            stmt.bindString(3, KabupatenName);
        }
 
        Integer AreaId = entity.getAreaId();
        if (AreaId != null) {
            stmt.bindLong(4, AreaId);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public TKabupaten readEntity(Cursor cursor, int offset) {
        TKabupaten entity = new TKabupaten( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1), // KabupatenId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // KabupatenName
            cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3) // AreaId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TKabupaten entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setKabupatenId(cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1));
        entity.setKabupatenName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setAreaId(cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(TKabupaten entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(TKabupaten entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TKabupaten entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
