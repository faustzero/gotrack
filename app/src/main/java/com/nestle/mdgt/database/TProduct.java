package com.nestle.mdgt.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TPRODUCT".
 */
@Entity
public class TProduct {

    @Id
    private Long id;
    private int ProductRegionId;
    private int RegionId;

    @NotNull
    private String RegionName;
    private int ProductId;

    @NotNull
    private String ProductCode;

    @NotNull
    private String ProductName;
    private int ProductCategoryId;

    @NotNull
    private String ProductCategoryName;
    private int BrandId;

    @NotNull
    private String BrandName;
    private int ProdusenId;

    @NotNull
    private String ProdusenName;
    private int IsCompetitor;
    private int UnitId1;

    @NotNull
    private String UnitName1;
    private int Price1;
    private int UnitId2;

    @NotNull
    private String UnitName2;
    private int Price2;

    @Generated
    public TProduct() {
    }

    public TProduct(Long id) {
        this.id = id;
    }

    @Generated
    public TProduct(Long id, int ProductRegionId, int RegionId, String RegionName, int ProductId, String ProductCode, String ProductName, int ProductCategoryId, String ProductCategoryName, int BrandId, String BrandName, int ProdusenId, String ProdusenName, int IsCompetitor, int UnitId1, String UnitName1, int Price1, int UnitId2, String UnitName2, int Price2) {
        this.id = id;
        this.ProductRegionId = ProductRegionId;
        this.RegionId = RegionId;
        this.RegionName = RegionName;
        this.ProductId = ProductId;
        this.ProductCode = ProductCode;
        this.ProductName = ProductName;
        this.ProductCategoryId = ProductCategoryId;
        this.ProductCategoryName = ProductCategoryName;
        this.BrandId = BrandId;
        this.BrandName = BrandName;
        this.ProdusenId = ProdusenId;
        this.ProdusenName = ProdusenName;
        this.IsCompetitor = IsCompetitor;
        this.UnitId1 = UnitId1;
        this.UnitName1 = UnitName1;
        this.Price1 = Price1;
        this.UnitId2 = UnitId2;
        this.UnitName2 = UnitName2;
        this.Price2 = Price2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getProductRegionId() {
        return ProductRegionId;
    }

    public void setProductRegionId(int ProductRegionId) {
        this.ProductRegionId = ProductRegionId;
    }

    public int getRegionId() {
        return RegionId;
    }

    public void setRegionId(int RegionId) {
        this.RegionId = RegionId;
    }

    @NotNull
    public String getRegionName() {
        return RegionName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setRegionName(@NotNull String RegionName) {
        this.RegionName = RegionName;
    }

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int ProductId) {
        this.ProductId = ProductId;
    }

    @NotNull
    public String getProductCode() {
        return ProductCode;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setProductCode(@NotNull String ProductCode) {
        this.ProductCode = ProductCode;
    }

    @NotNull
    public String getProductName() {
        return ProductName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setProductName(@NotNull String ProductName) {
        this.ProductName = ProductName;
    }

    public int getProductCategoryId() {
        return ProductCategoryId;
    }

    public void setProductCategoryId(int ProductCategoryId) {
        this.ProductCategoryId = ProductCategoryId;
    }

    @NotNull
    public String getProductCategoryName() {
        return ProductCategoryName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setProductCategoryName(@NotNull String ProductCategoryName) {
        this.ProductCategoryName = ProductCategoryName;
    }

    public int getBrandId() {
        return BrandId;
    }

    public void setBrandId(int BrandId) {
        this.BrandId = BrandId;
    }

    @NotNull
    public String getBrandName() {
        return BrandName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setBrandName(@NotNull String BrandName) {
        this.BrandName = BrandName;
    }

    public int getProdusenId() {
        return ProdusenId;
    }

    public void setProdusenId(int ProdusenId) {
        this.ProdusenId = ProdusenId;
    }

    @NotNull
    public String getProdusenName() {
        return ProdusenName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setProdusenName(@NotNull String ProdusenName) {
        this.ProdusenName = ProdusenName;
    }

    public int getIsCompetitor() {
        return IsCompetitor;
    }

    public void setIsCompetitor(int IsCompetitor) {
        this.IsCompetitor = IsCompetitor;
    }

    public int getUnitId1() {
        return UnitId1;
    }

    public void setUnitId1(int UnitId1) {
        this.UnitId1 = UnitId1;
    }

    @NotNull
    public String getUnitName1() {
        return UnitName1;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setUnitName1(@NotNull String UnitName1) {
        this.UnitName1 = UnitName1;
    }

    public int getPrice1() {
        return Price1;
    }

    public void setPrice1(int Price1) {
        this.Price1 = Price1;
    }

    public int getUnitId2() {
        return UnitId2;
    }

    public void setUnitId2(int UnitId2) {
        this.UnitId2 = UnitId2;
    }

    @NotNull
    public String getUnitName2() {
        return UnitName2;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setUnitName2(@NotNull String UnitName2) {
        this.UnitName2 = UnitName2;
    }

    public int getPrice2() {
        return Price2;
    }

    public void setPrice2(int Price2) {
        this.Price2 = Price2;
    }

}
