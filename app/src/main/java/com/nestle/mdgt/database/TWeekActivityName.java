package com.nestle.mdgt.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TWEEK_ACTIVITY_NAME".
 */
@Entity
public class TWeekActivityName {

    @Id
    private Long id;
    private int WeeklyNameId;

    @NotNull
    private String WeeklyName;

    @Generated
    public TWeekActivityName() {
    }

    public TWeekActivityName(Long id) {
        this.id = id;
    }

    @Generated
    public TWeekActivityName(Long id, int WeeklyNameId, String WeeklyName) {
        this.id = id;
        this.WeeklyNameId = WeeklyNameId;
        this.WeeklyName = WeeklyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWeeklyNameId() {
        return WeeklyNameId;
    }

    public void setWeeklyNameId(int WeeklyNameId) {
        this.WeeklyNameId = WeeklyNameId;
    }

    @NotNull
    public String getWeeklyName() {
        return WeeklyName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setWeeklyName(@NotNull String WeeklyName) {
        this.WeeklyName = WeeklyName;
    }

    @Override
    public String toString() {
        return WeeklyName;
    }

}
