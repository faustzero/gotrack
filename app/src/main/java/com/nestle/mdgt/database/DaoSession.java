package com.nestle.mdgt.database;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.nestle.mdgt.database.TTransmissionHistory;
import com.nestle.mdgt.database.TInfo;
import com.nestle.mdgt.database.TReasonNoVisit;
import com.nestle.mdgt.database.TActivityType;
import com.nestle.mdgt.database.TStoreType;
import com.nestle.mdgt.database.TDistributor;
import com.nestle.mdgt.database.TArea;
import com.nestle.mdgt.database.TKabupaten;
import com.nestle.mdgt.database.TKecamatan;
import com.nestle.mdgt.database.TPlanogramType;
import com.nestle.mdgt.database.TPosmType;
import com.nestle.mdgt.database.TActivity;
import com.nestle.mdgt.database.TMarket;
import com.nestle.mdgt.database.TUnit;
import com.nestle.mdgt.database.TStore;
import com.nestle.mdgt.database.TProduct;
import com.nestle.mdgt.database.TProductCompetitor;
import com.nestle.mdgt.database.TBrandCompetitor;
import com.nestle.mdgt.database.TPlanogram;
import com.nestle.mdgt.database.TPosm;
import com.nestle.mdgt.database.TVisitMarket;
import com.nestle.mdgt.database.TVisitStore;
import com.nestle.mdgt.database.TReportMsl;
import com.nestle.mdgt.database.TReseller;
import com.nestle.mdgt.database.TResellerType;
import com.nestle.mdgt.database.TCampaign;
import com.nestle.mdgt.database.TCampaignPosm;
import com.nestle.mdgt.database.TReportCampaignHeader;
import com.nestle.mdgt.database.TReportCampaignDetail;
import com.nestle.mdgt.database.TReportTopup;
import com.nestle.mdgt.database.TCategoryCompetitor;
import com.nestle.mdgt.database.TBanner;
import com.nestle.mdgt.database.TReportPlanogram;
import com.nestle.mdgt.database.TReportPenjualan;
import com.nestle.mdgt.database.TReportPosm;
import com.nestle.mdgt.database.TReportActivity;
import com.nestle.mdgt.database.TReportCompetitor;
import com.nestle.mdgt.database.TReasonCampaign;
import com.nestle.mdgt.database.TReportPullProgram;
import com.nestle.mdgt.database.TReportActivityHeader;
import com.nestle.mdgt.database.TReportActivityDetail;
import com.nestle.mdgt.database.TLocalActivityType;
import com.nestle.mdgt.database.TReportSurveyPrice;
import com.nestle.mdgt.database.TProductMSL;
import com.nestle.mdgt.database.TProductMSLRegion;
import com.nestle.mdgt.database.TReportWeeklyHeader;
import com.nestle.mdgt.database.TReportWeeklyDetail;
import com.nestle.mdgt.database.TDistributorArea;
import com.nestle.mdgt.database.TReportProductPlanogram;
import com.nestle.mdgt.database.TDistributorStore;
import com.nestle.mdgt.database.TLocalActivityName;
import com.nestle.mdgt.database.TLocalAcitivityLocation;
import com.nestle.mdgt.database.TWeekActivityName;
import com.nestle.mdgt.database.TRole;
import com.nestle.mdgt.database.TNooRegion;
import com.nestle.mdgt.database.TNooArea;
import com.nestle.mdgt.database.TNooDistributor;
import com.nestle.mdgt.database.TCampaignCompliance;
import com.nestle.mdgt.database.TReasonCampaignPosm;
import com.nestle.mdgt.database.TReportCompliance;

import com.nestle.mdgt.database.TTransmissionHistoryDao;
import com.nestle.mdgt.database.TInfoDao;
import com.nestle.mdgt.database.TReasonNoVisitDao;
import com.nestle.mdgt.database.TActivityTypeDao;
import com.nestle.mdgt.database.TStoreTypeDao;
import com.nestle.mdgt.database.TDistributorDao;
import com.nestle.mdgt.database.TAreaDao;
import com.nestle.mdgt.database.TKabupatenDao;
import com.nestle.mdgt.database.TKecamatanDao;
import com.nestle.mdgt.database.TPlanogramTypeDao;
import com.nestle.mdgt.database.TPosmTypeDao;
import com.nestle.mdgt.database.TActivityDao;
import com.nestle.mdgt.database.TMarketDao;
import com.nestle.mdgt.database.TUnitDao;
import com.nestle.mdgt.database.TStoreDao;
import com.nestle.mdgt.database.TProductDao;
import com.nestle.mdgt.database.TProductCompetitorDao;
import com.nestle.mdgt.database.TBrandCompetitorDao;
import com.nestle.mdgt.database.TPlanogramDao;
import com.nestle.mdgt.database.TPosmDao;
import com.nestle.mdgt.database.TVisitMarketDao;
import com.nestle.mdgt.database.TVisitStoreDao;
import com.nestle.mdgt.database.TReportMslDao;
import com.nestle.mdgt.database.TResellerDao;
import com.nestle.mdgt.database.TResellerTypeDao;
import com.nestle.mdgt.database.TCampaignDao;
import com.nestle.mdgt.database.TCampaignPosmDao;
import com.nestle.mdgt.database.TReportCampaignHeaderDao;
import com.nestle.mdgt.database.TReportCampaignDetailDao;
import com.nestle.mdgt.database.TReportTopupDao;
import com.nestle.mdgt.database.TCategoryCompetitorDao;
import com.nestle.mdgt.database.TBannerDao;
import com.nestle.mdgt.database.TReportPlanogramDao;
import com.nestle.mdgt.database.TReportPenjualanDao;
import com.nestle.mdgt.database.TReportPosmDao;
import com.nestle.mdgt.database.TReportActivityDao;
import com.nestle.mdgt.database.TReportCompetitorDao;
import com.nestle.mdgt.database.TReasonCampaignDao;
import com.nestle.mdgt.database.TReportPullProgramDao;
import com.nestle.mdgt.database.TReportActivityHeaderDao;
import com.nestle.mdgt.database.TReportActivityDetailDao;
import com.nestle.mdgt.database.TLocalActivityTypeDao;
import com.nestle.mdgt.database.TReportSurveyPriceDao;
import com.nestle.mdgt.database.TProductMSLDao;
import com.nestle.mdgt.database.TProductMSLRegionDao;
import com.nestle.mdgt.database.TReportWeeklyHeaderDao;
import com.nestle.mdgt.database.TReportWeeklyDetailDao;
import com.nestle.mdgt.database.TDistributorAreaDao;
import com.nestle.mdgt.database.TReportProductPlanogramDao;
import com.nestle.mdgt.database.TDistributorStoreDao;
import com.nestle.mdgt.database.TLocalActivityNameDao;
import com.nestle.mdgt.database.TLocalAcitivityLocationDao;
import com.nestle.mdgt.database.TWeekActivityNameDao;
import com.nestle.mdgt.database.TRoleDao;
import com.nestle.mdgt.database.TNooRegionDao;
import com.nestle.mdgt.database.TNooAreaDao;
import com.nestle.mdgt.database.TNooDistributorDao;
import com.nestle.mdgt.database.TCampaignComplianceDao;
import com.nestle.mdgt.database.TReasonCampaignPosmDao;
import com.nestle.mdgt.database.TReportComplianceDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig tTransmissionHistoryDaoConfig;
    private final DaoConfig tInfoDaoConfig;
    private final DaoConfig tReasonNoVisitDaoConfig;
    private final DaoConfig tActivityTypeDaoConfig;
    private final DaoConfig tStoreTypeDaoConfig;
    private final DaoConfig tDistributorDaoConfig;
    private final DaoConfig tAreaDaoConfig;
    private final DaoConfig tKabupatenDaoConfig;
    private final DaoConfig tKecamatanDaoConfig;
    private final DaoConfig tPlanogramTypeDaoConfig;
    private final DaoConfig tPosmTypeDaoConfig;
    private final DaoConfig tActivityDaoConfig;
    private final DaoConfig tMarketDaoConfig;
    private final DaoConfig tUnitDaoConfig;
    private final DaoConfig tStoreDaoConfig;
    private final DaoConfig tProductDaoConfig;
    private final DaoConfig tProductCompetitorDaoConfig;
    private final DaoConfig tBrandCompetitorDaoConfig;
    private final DaoConfig tPlanogramDaoConfig;
    private final DaoConfig tPosmDaoConfig;
    private final DaoConfig tVisitMarketDaoConfig;
    private final DaoConfig tVisitStoreDaoConfig;
    private final DaoConfig tReportMslDaoConfig;
    private final DaoConfig tResellerDaoConfig;
    private final DaoConfig tResellerTypeDaoConfig;
    private final DaoConfig tCampaignDaoConfig;
    private final DaoConfig tCampaignPosmDaoConfig;
    private final DaoConfig tReportCampaignHeaderDaoConfig;
    private final DaoConfig tReportCampaignDetailDaoConfig;
    private final DaoConfig tReportTopupDaoConfig;
    private final DaoConfig tCategoryCompetitorDaoConfig;
    private final DaoConfig tBannerDaoConfig;
    private final DaoConfig tReportPlanogramDaoConfig;
    private final DaoConfig tReportPenjualanDaoConfig;
    private final DaoConfig tReportPosmDaoConfig;
    private final DaoConfig tReportActivityDaoConfig;
    private final DaoConfig tReportCompetitorDaoConfig;
    private final DaoConfig tReasonCampaignDaoConfig;
    private final DaoConfig tReportPullProgramDaoConfig;
    private final DaoConfig tReportActivityHeaderDaoConfig;
    private final DaoConfig tReportActivityDetailDaoConfig;
    private final DaoConfig tLocalActivityTypeDaoConfig;
    private final DaoConfig tReportSurveyPriceDaoConfig;
    private final DaoConfig tProductMSLDaoConfig;
    private final DaoConfig tProductMSLRegionDaoConfig;
    private final DaoConfig tReportWeeklyHeaderDaoConfig;
    private final DaoConfig tReportWeeklyDetailDaoConfig;
    private final DaoConfig tDistributorAreaDaoConfig;
    private final DaoConfig tReportProductPlanogramDaoConfig;
    private final DaoConfig tDistributorStoreDaoConfig;
    private final DaoConfig tLocalActivityNameDaoConfig;
    private final DaoConfig tLocalAcitivityLocationDaoConfig;
    private final DaoConfig tWeekActivityNameDaoConfig;
    private final DaoConfig tRoleDaoConfig;
    private final DaoConfig tNooRegionDaoConfig;
    private final DaoConfig tNooAreaDaoConfig;
    private final DaoConfig tNooDistributorDaoConfig;
    private final DaoConfig tCampaignComplianceDaoConfig;
    private final DaoConfig tReasonCampaignPosmDaoConfig;
    private final DaoConfig tReportComplianceDaoConfig;

    private final TTransmissionHistoryDao tTransmissionHistoryDao;
    private final TInfoDao tInfoDao;
    private final TReasonNoVisitDao tReasonNoVisitDao;
    private final TActivityTypeDao tActivityTypeDao;
    private final TStoreTypeDao tStoreTypeDao;
    private final TDistributorDao tDistributorDao;
    private final TAreaDao tAreaDao;
    private final TKabupatenDao tKabupatenDao;
    private final TKecamatanDao tKecamatanDao;
    private final TPlanogramTypeDao tPlanogramTypeDao;
    private final TPosmTypeDao tPosmTypeDao;
    private final TActivityDao tActivityDao;
    private final TMarketDao tMarketDao;
    private final TUnitDao tUnitDao;
    private final TStoreDao tStoreDao;
    private final TProductDao tProductDao;
    private final TProductCompetitorDao tProductCompetitorDao;
    private final TBrandCompetitorDao tBrandCompetitorDao;
    private final TPlanogramDao tPlanogramDao;
    private final TPosmDao tPosmDao;
    private final TVisitMarketDao tVisitMarketDao;
    private final TVisitStoreDao tVisitStoreDao;
    private final TReportMslDao tReportMslDao;
    private final TResellerDao tResellerDao;
    private final TResellerTypeDao tResellerTypeDao;
    private final TCampaignDao tCampaignDao;
    private final TCampaignPosmDao tCampaignPosmDao;
    private final TReportCampaignHeaderDao tReportCampaignHeaderDao;
    private final TReportCampaignDetailDao tReportCampaignDetailDao;
    private final TReportTopupDao tReportTopupDao;
    private final TCategoryCompetitorDao tCategoryCompetitorDao;
    private final TBannerDao tBannerDao;
    private final TReportPlanogramDao tReportPlanogramDao;
    private final TReportPenjualanDao tReportPenjualanDao;
    private final TReportPosmDao tReportPosmDao;
    private final TReportActivityDao tReportActivityDao;
    private final TReportCompetitorDao tReportCompetitorDao;
    private final TReasonCampaignDao tReasonCampaignDao;
    private final TReportPullProgramDao tReportPullProgramDao;
    private final TReportActivityHeaderDao tReportActivityHeaderDao;
    private final TReportActivityDetailDao tReportActivityDetailDao;
    private final TLocalActivityTypeDao tLocalActivityTypeDao;
    private final TReportSurveyPriceDao tReportSurveyPriceDao;
    private final TProductMSLDao tProductMSLDao;
    private final TProductMSLRegionDao tProductMSLRegionDao;
    private final TReportWeeklyHeaderDao tReportWeeklyHeaderDao;
    private final TReportWeeklyDetailDao tReportWeeklyDetailDao;
    private final TDistributorAreaDao tDistributorAreaDao;
    private final TReportProductPlanogramDao tReportProductPlanogramDao;
    private final TDistributorStoreDao tDistributorStoreDao;
    private final TLocalActivityNameDao tLocalActivityNameDao;
    private final TLocalAcitivityLocationDao tLocalAcitivityLocationDao;
    private final TWeekActivityNameDao tWeekActivityNameDao;
    private final TRoleDao tRoleDao;
    private final TNooRegionDao tNooRegionDao;
    private final TNooAreaDao tNooAreaDao;
    private final TNooDistributorDao tNooDistributorDao;
    private final TCampaignComplianceDao tCampaignComplianceDao;
    private final TReasonCampaignPosmDao tReasonCampaignPosmDao;
    private final TReportComplianceDao tReportComplianceDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        tTransmissionHistoryDaoConfig = daoConfigMap.get(TTransmissionHistoryDao.class).clone();
        tTransmissionHistoryDaoConfig.initIdentityScope(type);

        tInfoDaoConfig = daoConfigMap.get(TInfoDao.class).clone();
        tInfoDaoConfig.initIdentityScope(type);

        tReasonNoVisitDaoConfig = daoConfigMap.get(TReasonNoVisitDao.class).clone();
        tReasonNoVisitDaoConfig.initIdentityScope(type);

        tActivityTypeDaoConfig = daoConfigMap.get(TActivityTypeDao.class).clone();
        tActivityTypeDaoConfig.initIdentityScope(type);

        tStoreTypeDaoConfig = daoConfigMap.get(TStoreTypeDao.class).clone();
        tStoreTypeDaoConfig.initIdentityScope(type);

        tDistributorDaoConfig = daoConfigMap.get(TDistributorDao.class).clone();
        tDistributorDaoConfig.initIdentityScope(type);

        tAreaDaoConfig = daoConfigMap.get(TAreaDao.class).clone();
        tAreaDaoConfig.initIdentityScope(type);

        tKabupatenDaoConfig = daoConfigMap.get(TKabupatenDao.class).clone();
        tKabupatenDaoConfig.initIdentityScope(type);

        tKecamatanDaoConfig = daoConfigMap.get(TKecamatanDao.class).clone();
        tKecamatanDaoConfig.initIdentityScope(type);

        tPlanogramTypeDaoConfig = daoConfigMap.get(TPlanogramTypeDao.class).clone();
        tPlanogramTypeDaoConfig.initIdentityScope(type);

        tPosmTypeDaoConfig = daoConfigMap.get(TPosmTypeDao.class).clone();
        tPosmTypeDaoConfig.initIdentityScope(type);

        tActivityDaoConfig = daoConfigMap.get(TActivityDao.class).clone();
        tActivityDaoConfig.initIdentityScope(type);

        tMarketDaoConfig = daoConfigMap.get(TMarketDao.class).clone();
        tMarketDaoConfig.initIdentityScope(type);

        tUnitDaoConfig = daoConfigMap.get(TUnitDao.class).clone();
        tUnitDaoConfig.initIdentityScope(type);

        tStoreDaoConfig = daoConfigMap.get(TStoreDao.class).clone();
        tStoreDaoConfig.initIdentityScope(type);

        tProductDaoConfig = daoConfigMap.get(TProductDao.class).clone();
        tProductDaoConfig.initIdentityScope(type);

        tProductCompetitorDaoConfig = daoConfigMap.get(TProductCompetitorDao.class).clone();
        tProductCompetitorDaoConfig.initIdentityScope(type);

        tBrandCompetitorDaoConfig = daoConfigMap.get(TBrandCompetitorDao.class).clone();
        tBrandCompetitorDaoConfig.initIdentityScope(type);

        tPlanogramDaoConfig = daoConfigMap.get(TPlanogramDao.class).clone();
        tPlanogramDaoConfig.initIdentityScope(type);

        tPosmDaoConfig = daoConfigMap.get(TPosmDao.class).clone();
        tPosmDaoConfig.initIdentityScope(type);

        tVisitMarketDaoConfig = daoConfigMap.get(TVisitMarketDao.class).clone();
        tVisitMarketDaoConfig.initIdentityScope(type);

        tVisitStoreDaoConfig = daoConfigMap.get(TVisitStoreDao.class).clone();
        tVisitStoreDaoConfig.initIdentityScope(type);

        tReportMslDaoConfig = daoConfigMap.get(TReportMslDao.class).clone();
        tReportMslDaoConfig.initIdentityScope(type);

        tResellerDaoConfig = daoConfigMap.get(TResellerDao.class).clone();
        tResellerDaoConfig.initIdentityScope(type);

        tResellerTypeDaoConfig = daoConfigMap.get(TResellerTypeDao.class).clone();
        tResellerTypeDaoConfig.initIdentityScope(type);

        tCampaignDaoConfig = daoConfigMap.get(TCampaignDao.class).clone();
        tCampaignDaoConfig.initIdentityScope(type);

        tCampaignPosmDaoConfig = daoConfigMap.get(TCampaignPosmDao.class).clone();
        tCampaignPosmDaoConfig.initIdentityScope(type);

        tReportCampaignHeaderDaoConfig = daoConfigMap.get(TReportCampaignHeaderDao.class).clone();
        tReportCampaignHeaderDaoConfig.initIdentityScope(type);

        tReportCampaignDetailDaoConfig = daoConfigMap.get(TReportCampaignDetailDao.class).clone();
        tReportCampaignDetailDaoConfig.initIdentityScope(type);

        tReportTopupDaoConfig = daoConfigMap.get(TReportTopupDao.class).clone();
        tReportTopupDaoConfig.initIdentityScope(type);

        tCategoryCompetitorDaoConfig = daoConfigMap.get(TCategoryCompetitorDao.class).clone();
        tCategoryCompetitorDaoConfig.initIdentityScope(type);

        tBannerDaoConfig = daoConfigMap.get(TBannerDao.class).clone();
        tBannerDaoConfig.initIdentityScope(type);

        tReportPlanogramDaoConfig = daoConfigMap.get(TReportPlanogramDao.class).clone();
        tReportPlanogramDaoConfig.initIdentityScope(type);

        tReportPenjualanDaoConfig = daoConfigMap.get(TReportPenjualanDao.class).clone();
        tReportPenjualanDaoConfig.initIdentityScope(type);

        tReportPosmDaoConfig = daoConfigMap.get(TReportPosmDao.class).clone();
        tReportPosmDaoConfig.initIdentityScope(type);

        tReportActivityDaoConfig = daoConfigMap.get(TReportActivityDao.class).clone();
        tReportActivityDaoConfig.initIdentityScope(type);

        tReportCompetitorDaoConfig = daoConfigMap.get(TReportCompetitorDao.class).clone();
        tReportCompetitorDaoConfig.initIdentityScope(type);

        tReasonCampaignDaoConfig = daoConfigMap.get(TReasonCampaignDao.class).clone();
        tReasonCampaignDaoConfig.initIdentityScope(type);

        tReportPullProgramDaoConfig = daoConfigMap.get(TReportPullProgramDao.class).clone();
        tReportPullProgramDaoConfig.initIdentityScope(type);

        tReportActivityHeaderDaoConfig = daoConfigMap.get(TReportActivityHeaderDao.class).clone();
        tReportActivityHeaderDaoConfig.initIdentityScope(type);

        tReportActivityDetailDaoConfig = daoConfigMap.get(TReportActivityDetailDao.class).clone();
        tReportActivityDetailDaoConfig.initIdentityScope(type);

        tLocalActivityTypeDaoConfig = daoConfigMap.get(TLocalActivityTypeDao.class).clone();
        tLocalActivityTypeDaoConfig.initIdentityScope(type);

        tReportSurveyPriceDaoConfig = daoConfigMap.get(TReportSurveyPriceDao.class).clone();
        tReportSurveyPriceDaoConfig.initIdentityScope(type);

        tProductMSLDaoConfig = daoConfigMap.get(TProductMSLDao.class).clone();
        tProductMSLDaoConfig.initIdentityScope(type);

        tProductMSLRegionDaoConfig = daoConfigMap.get(TProductMSLRegionDao.class).clone();
        tProductMSLRegionDaoConfig.initIdentityScope(type);

        tReportWeeklyHeaderDaoConfig = daoConfigMap.get(TReportWeeklyHeaderDao.class).clone();
        tReportWeeklyHeaderDaoConfig.initIdentityScope(type);

        tReportWeeklyDetailDaoConfig = daoConfigMap.get(TReportWeeklyDetailDao.class).clone();
        tReportWeeklyDetailDaoConfig.initIdentityScope(type);

        tDistributorAreaDaoConfig = daoConfigMap.get(TDistributorAreaDao.class).clone();
        tDistributorAreaDaoConfig.initIdentityScope(type);

        tReportProductPlanogramDaoConfig = daoConfigMap.get(TReportProductPlanogramDao.class).clone();
        tReportProductPlanogramDaoConfig.initIdentityScope(type);

        tDistributorStoreDaoConfig = daoConfigMap.get(TDistributorStoreDao.class).clone();
        tDistributorStoreDaoConfig.initIdentityScope(type);

        tLocalActivityNameDaoConfig = daoConfigMap.get(TLocalActivityNameDao.class).clone();
        tLocalActivityNameDaoConfig.initIdentityScope(type);

        tLocalAcitivityLocationDaoConfig = daoConfigMap.get(TLocalAcitivityLocationDao.class).clone();
        tLocalAcitivityLocationDaoConfig.initIdentityScope(type);

        tWeekActivityNameDaoConfig = daoConfigMap.get(TWeekActivityNameDao.class).clone();
        tWeekActivityNameDaoConfig.initIdentityScope(type);

        tRoleDaoConfig = daoConfigMap.get(TRoleDao.class).clone();
        tRoleDaoConfig.initIdentityScope(type);

        tNooRegionDaoConfig = daoConfigMap.get(TNooRegionDao.class).clone();
        tNooRegionDaoConfig.initIdentityScope(type);

        tNooAreaDaoConfig = daoConfigMap.get(TNooAreaDao.class).clone();
        tNooAreaDaoConfig.initIdentityScope(type);

        tNooDistributorDaoConfig = daoConfigMap.get(TNooDistributorDao.class).clone();
        tNooDistributorDaoConfig.initIdentityScope(type);

        tCampaignComplianceDaoConfig = daoConfigMap.get(TCampaignComplianceDao.class).clone();
        tCampaignComplianceDaoConfig.initIdentityScope(type);

        tReasonCampaignPosmDaoConfig = daoConfigMap.get(TReasonCampaignPosmDao.class).clone();
        tReasonCampaignPosmDaoConfig.initIdentityScope(type);

        tReportComplianceDaoConfig = daoConfigMap.get(TReportComplianceDao.class).clone();
        tReportComplianceDaoConfig.initIdentityScope(type);

        tTransmissionHistoryDao = new TTransmissionHistoryDao(tTransmissionHistoryDaoConfig, this);
        tInfoDao = new TInfoDao(tInfoDaoConfig, this);
        tReasonNoVisitDao = new TReasonNoVisitDao(tReasonNoVisitDaoConfig, this);
        tActivityTypeDao = new TActivityTypeDao(tActivityTypeDaoConfig, this);
        tStoreTypeDao = new TStoreTypeDao(tStoreTypeDaoConfig, this);
        tDistributorDao = new TDistributorDao(tDistributorDaoConfig, this);
        tAreaDao = new TAreaDao(tAreaDaoConfig, this);
        tKabupatenDao = new TKabupatenDao(tKabupatenDaoConfig, this);
        tKecamatanDao = new TKecamatanDao(tKecamatanDaoConfig, this);
        tPlanogramTypeDao = new TPlanogramTypeDao(tPlanogramTypeDaoConfig, this);
        tPosmTypeDao = new TPosmTypeDao(tPosmTypeDaoConfig, this);
        tActivityDao = new TActivityDao(tActivityDaoConfig, this);
        tMarketDao = new TMarketDao(tMarketDaoConfig, this);
        tUnitDao = new TUnitDao(tUnitDaoConfig, this);
        tStoreDao = new TStoreDao(tStoreDaoConfig, this);
        tProductDao = new TProductDao(tProductDaoConfig, this);
        tProductCompetitorDao = new TProductCompetitorDao(tProductCompetitorDaoConfig, this);
        tBrandCompetitorDao = new TBrandCompetitorDao(tBrandCompetitorDaoConfig, this);
        tPlanogramDao = new TPlanogramDao(tPlanogramDaoConfig, this);
        tPosmDao = new TPosmDao(tPosmDaoConfig, this);
        tVisitMarketDao = new TVisitMarketDao(tVisitMarketDaoConfig, this);
        tVisitStoreDao = new TVisitStoreDao(tVisitStoreDaoConfig, this);
        tReportMslDao = new TReportMslDao(tReportMslDaoConfig, this);
        tResellerDao = new TResellerDao(tResellerDaoConfig, this);
        tResellerTypeDao = new TResellerTypeDao(tResellerTypeDaoConfig, this);
        tCampaignDao = new TCampaignDao(tCampaignDaoConfig, this);
        tCampaignPosmDao = new TCampaignPosmDao(tCampaignPosmDaoConfig, this);
        tReportCampaignHeaderDao = new TReportCampaignHeaderDao(tReportCampaignHeaderDaoConfig, this);
        tReportCampaignDetailDao = new TReportCampaignDetailDao(tReportCampaignDetailDaoConfig, this);
        tReportTopupDao = new TReportTopupDao(tReportTopupDaoConfig, this);
        tCategoryCompetitorDao = new TCategoryCompetitorDao(tCategoryCompetitorDaoConfig, this);
        tBannerDao = new TBannerDao(tBannerDaoConfig, this);
        tReportPlanogramDao = new TReportPlanogramDao(tReportPlanogramDaoConfig, this);
        tReportPenjualanDao = new TReportPenjualanDao(tReportPenjualanDaoConfig, this);
        tReportPosmDao = new TReportPosmDao(tReportPosmDaoConfig, this);
        tReportActivityDao = new TReportActivityDao(tReportActivityDaoConfig, this);
        tReportCompetitorDao = new TReportCompetitorDao(tReportCompetitorDaoConfig, this);
        tReasonCampaignDao = new TReasonCampaignDao(tReasonCampaignDaoConfig, this);
        tReportPullProgramDao = new TReportPullProgramDao(tReportPullProgramDaoConfig, this);
        tReportActivityHeaderDao = new TReportActivityHeaderDao(tReportActivityHeaderDaoConfig, this);
        tReportActivityDetailDao = new TReportActivityDetailDao(tReportActivityDetailDaoConfig, this);
        tLocalActivityTypeDao = new TLocalActivityTypeDao(tLocalActivityTypeDaoConfig, this);
        tReportSurveyPriceDao = new TReportSurveyPriceDao(tReportSurveyPriceDaoConfig, this);
        tProductMSLDao = new TProductMSLDao(tProductMSLDaoConfig, this);
        tProductMSLRegionDao = new TProductMSLRegionDao(tProductMSLRegionDaoConfig, this);
        tReportWeeklyHeaderDao = new TReportWeeklyHeaderDao(tReportWeeklyHeaderDaoConfig, this);
        tReportWeeklyDetailDao = new TReportWeeklyDetailDao(tReportWeeklyDetailDaoConfig, this);
        tDistributorAreaDao = new TDistributorAreaDao(tDistributorAreaDaoConfig, this);
        tReportProductPlanogramDao = new TReportProductPlanogramDao(tReportProductPlanogramDaoConfig, this);
        tDistributorStoreDao = new TDistributorStoreDao(tDistributorStoreDaoConfig, this);
        tLocalActivityNameDao = new TLocalActivityNameDao(tLocalActivityNameDaoConfig, this);
        tLocalAcitivityLocationDao = new TLocalAcitivityLocationDao(tLocalAcitivityLocationDaoConfig, this);
        tWeekActivityNameDao = new TWeekActivityNameDao(tWeekActivityNameDaoConfig, this);
        tRoleDao = new TRoleDao(tRoleDaoConfig, this);
        tNooRegionDao = new TNooRegionDao(tNooRegionDaoConfig, this);
        tNooAreaDao = new TNooAreaDao(tNooAreaDaoConfig, this);
        tNooDistributorDao = new TNooDistributorDao(tNooDistributorDaoConfig, this);
        tCampaignComplianceDao = new TCampaignComplianceDao(tCampaignComplianceDaoConfig, this);
        tReasonCampaignPosmDao = new TReasonCampaignPosmDao(tReasonCampaignPosmDaoConfig, this);
        tReportComplianceDao = new TReportComplianceDao(tReportComplianceDaoConfig, this);

        registerDao(TTransmissionHistory.class, tTransmissionHistoryDao);
        registerDao(TInfo.class, tInfoDao);
        registerDao(TReasonNoVisit.class, tReasonNoVisitDao);
        registerDao(TActivityType.class, tActivityTypeDao);
        registerDao(TStoreType.class, tStoreTypeDao);
        registerDao(TDistributor.class, tDistributorDao);
        registerDao(TArea.class, tAreaDao);
        registerDao(TKabupaten.class, tKabupatenDao);
        registerDao(TKecamatan.class, tKecamatanDao);
        registerDao(TPlanogramType.class, tPlanogramTypeDao);
        registerDao(TPosmType.class, tPosmTypeDao);
        registerDao(TActivity.class, tActivityDao);
        registerDao(TMarket.class, tMarketDao);
        registerDao(TUnit.class, tUnitDao);
        registerDao(TStore.class, tStoreDao);
        registerDao(TProduct.class, tProductDao);
        registerDao(TProductCompetitor.class, tProductCompetitorDao);
        registerDao(TBrandCompetitor.class, tBrandCompetitorDao);
        registerDao(TPlanogram.class, tPlanogramDao);
        registerDao(TPosm.class, tPosmDao);
        registerDao(TVisitMarket.class, tVisitMarketDao);
        registerDao(TVisitStore.class, tVisitStoreDao);
        registerDao(TReportMsl.class, tReportMslDao);
        registerDao(TReseller.class, tResellerDao);
        registerDao(TResellerType.class, tResellerTypeDao);
        registerDao(TCampaign.class, tCampaignDao);
        registerDao(TCampaignPosm.class, tCampaignPosmDao);
        registerDao(TReportCampaignHeader.class, tReportCampaignHeaderDao);
        registerDao(TReportCampaignDetail.class, tReportCampaignDetailDao);
        registerDao(TReportTopup.class, tReportTopupDao);
        registerDao(TCategoryCompetitor.class, tCategoryCompetitorDao);
        registerDao(TBanner.class, tBannerDao);
        registerDao(TReportPlanogram.class, tReportPlanogramDao);
        registerDao(TReportPenjualan.class, tReportPenjualanDao);
        registerDao(TReportPosm.class, tReportPosmDao);
        registerDao(TReportActivity.class, tReportActivityDao);
        registerDao(TReportCompetitor.class, tReportCompetitorDao);
        registerDao(TReasonCampaign.class, tReasonCampaignDao);
        registerDao(TReportPullProgram.class, tReportPullProgramDao);
        registerDao(TReportActivityHeader.class, tReportActivityHeaderDao);
        registerDao(TReportActivityDetail.class, tReportActivityDetailDao);
        registerDao(TLocalActivityType.class, tLocalActivityTypeDao);
        registerDao(TReportSurveyPrice.class, tReportSurveyPriceDao);
        registerDao(TProductMSL.class, tProductMSLDao);
        registerDao(TProductMSLRegion.class, tProductMSLRegionDao);
        registerDao(TReportWeeklyHeader.class, tReportWeeklyHeaderDao);
        registerDao(TReportWeeklyDetail.class, tReportWeeklyDetailDao);
        registerDao(TDistributorArea.class, tDistributorAreaDao);
        registerDao(TReportProductPlanogram.class, tReportProductPlanogramDao);
        registerDao(TDistributorStore.class, tDistributorStoreDao);
        registerDao(TLocalActivityName.class, tLocalActivityNameDao);
        registerDao(TLocalAcitivityLocation.class, tLocalAcitivityLocationDao);
        registerDao(TWeekActivityName.class, tWeekActivityNameDao);
        registerDao(TRole.class, tRoleDao);
        registerDao(TNooRegion.class, tNooRegionDao);
        registerDao(TNooArea.class, tNooAreaDao);
        registerDao(TNooDistributor.class, tNooDistributorDao);
        registerDao(TCampaignCompliance.class, tCampaignComplianceDao);
        registerDao(TReasonCampaignPosm.class, tReasonCampaignPosmDao);
        registerDao(TReportCompliance.class, tReportComplianceDao);
    }
    
    public void clear() {
        tTransmissionHistoryDaoConfig.clearIdentityScope();
        tInfoDaoConfig.clearIdentityScope();
        tReasonNoVisitDaoConfig.clearIdentityScope();
        tActivityTypeDaoConfig.clearIdentityScope();
        tStoreTypeDaoConfig.clearIdentityScope();
        tDistributorDaoConfig.clearIdentityScope();
        tAreaDaoConfig.clearIdentityScope();
        tKabupatenDaoConfig.clearIdentityScope();
        tKecamatanDaoConfig.clearIdentityScope();
        tPlanogramTypeDaoConfig.clearIdentityScope();
        tPosmTypeDaoConfig.clearIdentityScope();
        tActivityDaoConfig.clearIdentityScope();
        tMarketDaoConfig.clearIdentityScope();
        tUnitDaoConfig.clearIdentityScope();
        tStoreDaoConfig.clearIdentityScope();
        tProductDaoConfig.clearIdentityScope();
        tProductCompetitorDaoConfig.clearIdentityScope();
        tBrandCompetitorDaoConfig.clearIdentityScope();
        tPlanogramDaoConfig.clearIdentityScope();
        tPosmDaoConfig.clearIdentityScope();
        tVisitMarketDaoConfig.clearIdentityScope();
        tVisitStoreDaoConfig.clearIdentityScope();
        tReportMslDaoConfig.clearIdentityScope();
        tResellerDaoConfig.clearIdentityScope();
        tResellerTypeDaoConfig.clearIdentityScope();
        tCampaignDaoConfig.clearIdentityScope();
        tCampaignPosmDaoConfig.clearIdentityScope();
        tReportCampaignHeaderDaoConfig.clearIdentityScope();
        tReportCampaignDetailDaoConfig.clearIdentityScope();
        tReportTopupDaoConfig.clearIdentityScope();
        tCategoryCompetitorDaoConfig.clearIdentityScope();
        tBannerDaoConfig.clearIdentityScope();
        tReportPlanogramDaoConfig.clearIdentityScope();
        tReportPenjualanDaoConfig.clearIdentityScope();
        tReportPosmDaoConfig.clearIdentityScope();
        tReportActivityDaoConfig.clearIdentityScope();
        tReportCompetitorDaoConfig.clearIdentityScope();
        tReasonCampaignDaoConfig.clearIdentityScope();
        tReportPullProgramDaoConfig.clearIdentityScope();
        tReportActivityHeaderDaoConfig.clearIdentityScope();
        tReportActivityDetailDaoConfig.clearIdentityScope();
        tLocalActivityTypeDaoConfig.clearIdentityScope();
        tReportSurveyPriceDaoConfig.clearIdentityScope();
        tProductMSLDaoConfig.clearIdentityScope();
        tProductMSLRegionDaoConfig.clearIdentityScope();
        tReportWeeklyHeaderDaoConfig.clearIdentityScope();
        tReportWeeklyDetailDaoConfig.clearIdentityScope();
        tDistributorAreaDaoConfig.clearIdentityScope();
        tReportProductPlanogramDaoConfig.clearIdentityScope();
        tDistributorStoreDaoConfig.clearIdentityScope();
        tLocalActivityNameDaoConfig.clearIdentityScope();
        tLocalAcitivityLocationDaoConfig.clearIdentityScope();
        tWeekActivityNameDaoConfig.clearIdentityScope();
        tRoleDaoConfig.clearIdentityScope();
        tNooRegionDaoConfig.clearIdentityScope();
        tNooAreaDaoConfig.clearIdentityScope();
        tNooDistributorDaoConfig.clearIdentityScope();
        tCampaignComplianceDaoConfig.clearIdentityScope();
        tReasonCampaignPosmDaoConfig.clearIdentityScope();
        tReportComplianceDaoConfig.clearIdentityScope();
    }

    public TTransmissionHistoryDao getTTransmissionHistoryDao() {
        return tTransmissionHistoryDao;
    }

    public TInfoDao getTInfoDao() {
        return tInfoDao;
    }

    public TReasonNoVisitDao getTReasonNoVisitDao() {
        return tReasonNoVisitDao;
    }

    public TActivityTypeDao getTActivityTypeDao() {
        return tActivityTypeDao;
    }

    public TStoreTypeDao getTStoreTypeDao() {
        return tStoreTypeDao;
    }

    public TDistributorDao getTDistributorDao() {
        return tDistributorDao;
    }

    public TAreaDao getTAreaDao() {
        return tAreaDao;
    }

    public TKabupatenDao getTKabupatenDao() {
        return tKabupatenDao;
    }

    public TKecamatanDao getTKecamatanDao() {
        return tKecamatanDao;
    }

    public TPlanogramTypeDao getTPlanogramTypeDao() {
        return tPlanogramTypeDao;
    }

    public TPosmTypeDao getTPosmTypeDao() {
        return tPosmTypeDao;
    }

    public TActivityDao getTActivityDao() {
        return tActivityDao;
    }

    public TMarketDao getTMarketDao() {
        return tMarketDao;
    }

    public TUnitDao getTUnitDao() {
        return tUnitDao;
    }

    public TStoreDao getTStoreDao() {
        return tStoreDao;
    }

    public TProductDao getTProductDao() {
        return tProductDao;
    }

    public TProductCompetitorDao getTProductCompetitorDao() {
        return tProductCompetitorDao;
    }

    public TBrandCompetitorDao getTBrandCompetitorDao() {
        return tBrandCompetitorDao;
    }

    public TPlanogramDao getTPlanogramDao() {
        return tPlanogramDao;
    }

    public TPosmDao getTPosmDao() {
        return tPosmDao;
    }

    public TVisitMarketDao getTVisitMarketDao() {
        return tVisitMarketDao;
    }

    public TVisitStoreDao getTVisitStoreDao() {
        return tVisitStoreDao;
    }

    public TReportMslDao getTReportMslDao() {
        return tReportMslDao;
    }

    public TResellerDao getTResellerDao() {
        return tResellerDao;
    }

    public TResellerTypeDao getTResellerTypeDao() {
        return tResellerTypeDao;
    }

    public TCampaignDao getTCampaignDao() {
        return tCampaignDao;
    }

    public TCampaignPosmDao getTCampaignPosmDao() {
        return tCampaignPosmDao;
    }

    public TReportCampaignHeaderDao getTReportCampaignHeaderDao() {
        return tReportCampaignHeaderDao;
    }

    public TReportCampaignDetailDao getTReportCampaignDetailDao() {
        return tReportCampaignDetailDao;
    }

    public TReportTopupDao getTReportTopupDao() {
        return tReportTopupDao;
    }

    public TCategoryCompetitorDao getTCategoryCompetitorDao() {
        return tCategoryCompetitorDao;
    }

    public TBannerDao getTBannerDao() {
        return tBannerDao;
    }

    public TReportPlanogramDao getTReportPlanogramDao() {
        return tReportPlanogramDao;
    }

    public TReportPenjualanDao getTReportPenjualanDao() {
        return tReportPenjualanDao;
    }

    public TReportPosmDao getTReportPosmDao() {
        return tReportPosmDao;
    }

    public TReportActivityDao getTReportActivityDao() {
        return tReportActivityDao;
    }

    public TReportCompetitorDao getTReportCompetitorDao() {
        return tReportCompetitorDao;
    }

    public TReasonCampaignDao getTReasonCampaignDao() {
        return tReasonCampaignDao;
    }

    public TReportPullProgramDao getTReportPullProgramDao() {
        return tReportPullProgramDao;
    }

    public TReportActivityHeaderDao getTReportActivityHeaderDao() {
        return tReportActivityHeaderDao;
    }

    public TReportActivityDetailDao getTReportActivityDetailDao() {
        return tReportActivityDetailDao;
    }

    public TLocalActivityTypeDao getTLocalActivityTypeDao() {
        return tLocalActivityTypeDao;
    }

    public TReportSurveyPriceDao getTReportSurveyPriceDao() {
        return tReportSurveyPriceDao;
    }

    public TProductMSLDao getTProductMSLDao() {
        return tProductMSLDao;
    }

    public TProductMSLRegionDao getTProductMSLRegionDao() {
        return tProductMSLRegionDao;
    }

    public TReportWeeklyHeaderDao getTReportWeeklyHeaderDao() {
        return tReportWeeklyHeaderDao;
    }

    public TReportWeeklyDetailDao getTReportWeeklyDetailDao() {
        return tReportWeeklyDetailDao;
    }

    public TDistributorAreaDao getTDistributorAreaDao() {
        return tDistributorAreaDao;
    }

    public TReportProductPlanogramDao getTReportProductPlanogramDao() {
        return tReportProductPlanogramDao;
    }

    public TDistributorStoreDao getTDistributorStoreDao() {
        return tDistributorStoreDao;
    }

    public TLocalActivityNameDao getTLocalActivityNameDao() {
        return tLocalActivityNameDao;
    }

    public TLocalAcitivityLocationDao getTLocalAcitivityLocationDao() {
        return tLocalAcitivityLocationDao;
    }

    public TWeekActivityNameDao getTWeekActivityNameDao() {
        return tWeekActivityNameDao;
    }

    public TRoleDao getTRoleDao() {
        return tRoleDao;
    }

    public TNooRegionDao getTNooRegionDao() {
        return tNooRegionDao;
    }

    public TNooAreaDao getTNooAreaDao() {
        return tNooAreaDao;
    }

    public TNooDistributorDao getTNooDistributorDao() {
        return tNooDistributorDao;
    }

    public TCampaignComplianceDao getTCampaignComplianceDao() {
        return tCampaignComplianceDao;
    }

    public TReasonCampaignPosmDao getTReasonCampaignPosmDao() {
        return tReasonCampaignPosmDao;
    }

    public TReportComplianceDao getTReportComplianceDao() {
        return tReportComplianceDao;
    }

}
