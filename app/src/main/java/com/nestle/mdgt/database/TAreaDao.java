package com.nestle.mdgt.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TAREA".
*/
public class TAreaDao extends AbstractDao<TArea, Long> {

    public static final String TABLENAME = "TAREA";

    /**
     * Properties of entity TArea.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property AreaId = new Property(1, Integer.class, "AreaId", false, "AREA_ID");
        public final static Property AreaName = new Property(2, String.class, "AreaName", false, "AREA_NAME");
        public final static Property RegionId = new Property(3, Integer.class, "RegionId", false, "REGION_ID");
    }


    public TAreaDao(DaoConfig config) {
        super(config);
    }
    
    public TAreaDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TAREA\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"AREA_ID\" INTEGER," + // 1: AreaId
                "\"AREA_NAME\" TEXT," + // 2: AreaName
                "\"REGION_ID\" INTEGER);"); // 3: RegionId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TAREA\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TArea entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer AreaId = entity.getAreaId();
        if (AreaId != null) {
            stmt.bindLong(2, AreaId);
        }
 
        String AreaName = entity.getAreaName();
        if (AreaName != null) {
            stmt.bindString(3, AreaName);
        }
 
        Integer RegionId = entity.getRegionId();
        if (RegionId != null) {
            stmt.bindLong(4, RegionId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TArea entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer AreaId = entity.getAreaId();
        if (AreaId != null) {
            stmt.bindLong(2, AreaId);
        }
 
        String AreaName = entity.getAreaName();
        if (AreaName != null) {
            stmt.bindString(3, AreaName);
        }
 
        Integer RegionId = entity.getRegionId();
        if (RegionId != null) {
            stmt.bindLong(4, RegionId);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public TArea readEntity(Cursor cursor, int offset) {
        TArea entity = new TArea( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1), // AreaId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // AreaName
            cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3) // RegionId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TArea entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setAreaId(cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1));
        entity.setAreaName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setRegionId(cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(TArea entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(TArea entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TArea entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
