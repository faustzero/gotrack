package com.nestle.mdgt.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TREPORT_ACTIVITY_DETAIL".
 */
@Entity
public class TReportActivityDetail {

    @Id
    private Long id;

    @NotNull
    private String ReportDetailId;

    @NotNull
    private String ReportHeaderId;
    private int ActivityTypeId;
    private int UserId;

    @NotNull
    private String PhotoPath;
    private int ColumnNumber;
    private int RowNumber;
    private int LeftPosition;
    private int TopPosition;
    private int RightPosition;
    private int BottomPosition;

    @NotNull
    private String InfoPhoto;

    @NotNull
    private String CreatedDate;

    @NotNull
    private String Created;

    @NotNull
    private String modified;

    @Generated
    public TReportActivityDetail() {
    }

    public TReportActivityDetail(Long id) {
        this.id = id;
    }

    @Generated
    public TReportActivityDetail(Long id, String ReportDetailId, String ReportHeaderId, int ActivityTypeId, int UserId, String PhotoPath, int ColumnNumber, int RowNumber, int LeftPosition, int TopPosition, int RightPosition, int BottomPosition, String InfoPhoto, String CreatedDate, String Created, String modified) {
        this.id = id;
        this.ReportDetailId = ReportDetailId;
        this.ReportHeaderId = ReportHeaderId;
        this.ActivityTypeId = ActivityTypeId;
        this.UserId = UserId;
        this.PhotoPath = PhotoPath;
        this.ColumnNumber = ColumnNumber;
        this.RowNumber = RowNumber;
        this.LeftPosition = LeftPosition;
        this.TopPosition = TopPosition;
        this.RightPosition = RightPosition;
        this.BottomPosition = BottomPosition;
        this.InfoPhoto = InfoPhoto;
        this.CreatedDate = CreatedDate;
        this.Created = Created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getReportDetailId() {
        return ReportDetailId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setReportDetailId(@NotNull String ReportDetailId) {
        this.ReportDetailId = ReportDetailId;
    }

    @NotNull
    public String getReportHeaderId() {
        return ReportHeaderId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setReportHeaderId(@NotNull String ReportHeaderId) {
        this.ReportHeaderId = ReportHeaderId;
    }

    public int getActivityTypeId() {
        return ActivityTypeId;
    }

    public void setActivityTypeId(int ActivityTypeId) {
        this.ActivityTypeId = ActivityTypeId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    @NotNull
    public String getPhotoPath() {
        return PhotoPath;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setPhotoPath(@NotNull String PhotoPath) {
        this.PhotoPath = PhotoPath;
    }

    public int getColumnNumber() {
        return ColumnNumber;
    }

    public void setColumnNumber(int ColumnNumber) {
        this.ColumnNumber = ColumnNumber;
    }

    public int getRowNumber() {
        return RowNumber;
    }

    public void setRowNumber(int RowNumber) {
        this.RowNumber = RowNumber;
    }

    public int getLeftPosition() {
        return LeftPosition;
    }

    public void setLeftPosition(int LeftPosition) {
        this.LeftPosition = LeftPosition;
    }

    public int getTopPosition() {
        return TopPosition;
    }

    public void setTopPosition(int TopPosition) {
        this.TopPosition = TopPosition;
    }

    public int getRightPosition() {
        return RightPosition;
    }

    public void setRightPosition(int RightPosition) {
        this.RightPosition = RightPosition;
    }

    public int getBottomPosition() {
        return BottomPosition;
    }

    public void setBottomPosition(int BottomPosition) {
        this.BottomPosition = BottomPosition;
    }

    @NotNull
    public String getInfoPhoto() {
        return InfoPhoto;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setInfoPhoto(@NotNull String InfoPhoto) {
        this.InfoPhoto = InfoPhoto;
    }

    @NotNull
    public String getCreatedDate() {
        return CreatedDate;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCreatedDate(@NotNull String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    @NotNull
    public String getCreated() {
        return Created;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCreated(@NotNull String Created) {
        this.Created = Created;
    }

    @NotNull
    public String getModified() {
        return modified;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setModified(@NotNull String modified) {
        this.modified = modified;
    }

}
