package com.nestle.mdgt.rests

import com.nestle.mdgt.models.DailyDashboardResponse
import com.nestle.mdgt.models.LoginResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface DashboardApiInterface {
    @FormUrlEncoded
    @POST("GotrackDashboard/getDailyAchievement")
    fun getDailyAchievement(
            @Field("tanggal") tanggal: String,
            @Field("surveyor_id") surveyorId: Int): Call<DailyDashboardResponse>
}