package com.nestle.mdgt.rests

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

object ApiClient {
    private val BASE_URL = "https://gotrack-api.pitjarus.co/index.php/"
    private val BASE_URL2 = "https://api.openweathermap.org/data/2.5/"
    private var retrofit: Retrofit? = null

    val client: Retrofit?
        get() {
//            if (retrofit == null) {
                val okHttpClient = OkHttpClient.Builder()
                        .readTimeout(120, TimeUnit.SECONDS)
                        .connectTimeout(120, TimeUnit.SECONDS)
                        .build()
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build()
//            }

            return retrofit
        }


    val client2: Retrofit?
        get() {
            //    if (retrofit == null) {
            val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .build()
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL2)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
            //  }

            return retrofit
        }



    val hostUrl: String
        get() = "https://gotrack-api.pitjarus.co/"

    fun getUpdateStoreLocationUrl(): String {
        return BASE_URL + "GotrackReport/updateStoreLocation"
    }

    fun getUpdateMarketLocationUrl(): String {
        return BASE_URL + "GotrackReport/updateMarketLocation"
    }

    fun getInsertVisitMarketUrl(): String {
        return BASE_URL + "GotrackReport/checkInVisitMarket"
    }

    fun getUpdateVisitOutMarketUrl(): String {
        return BASE_URL + "GotrackReport/checkOutVisitMarket"
    }

    fun getInsertVisitUrl(): String {
        return BASE_URL + "GotrackReport/checkInVisit"
    }

    fun getUpdateVisitOutUrl(): String {
        return BASE_URL + "GotrackReport/checkOutVisit"
    }

    fun getInsertMslUrl(): String {
        return BASE_URL + "GotrackReport/insertMsl"
    }

    fun getInsertTopupUrl(): String {
        return BASE_URL + "GotrackReport/insertTopup"
    }

    fun getInsertPlanogramUrl(): String {
        return BASE_URL + "GotrackReport/insertPlanogram4"
    }

    fun getInsertPosmUrl(): String {
        return BASE_URL + "GotrackReport/insertPosm"
    }

    fun getInsertCompetitorUrl(): String {
        return BASE_URL + "GotrackReport/insertCompetitor"
    }

    fun getInsertActivityUrl(): String {
        return BASE_URL + "GotrackReport/insertActivity"
    }

    fun getInsertCampaignHeader(): String {
        return BASE_URL + "GotrackReport/insertCampaignHeader2"
    }

    fun getInsertCampaignDetail(): String {
        return BASE_URL + "GotrackReport/insertCampaignDetail_4"
    }

    fun getInsertStoreUrl(): String {
        return BASE_URL + "GotrackReport/insertStore"
    }

    fun getUpdateResellerLocation(): String {
        return BASE_URL + "GotrackGotrackReport/updateResellerLocation"
    }

    fun getInsertReseller(): String {
        return BASE_URL + "GotrackReport/insertReseller2"
    }

    fun getUpdateReseller(): String {
        return BASE_URL + "GotrackReport/updateReseller"
    }

    fun getResetStore(): String {
        return BASE_URL + "GotrackReport/sendResetLokasi"
    }

    fun getInsertPenjualan(): String {
        return BASE_URL + "GotrackReport/insertPenjualan"
    }

    fun getInsertPullProgram(): String {
        return BASE_URL + "GotrackReport/insertPullProgram"
    }

    fun getInsertReportSurveyPrice(): String {
        return BASE_URL + "GotrackReport/insertSurveyPrice"
    }

    fun getInsertGeneralReport(): String {
        return BASE_URL + "GotrackReport/insertReportLocalActivity5"
    }

    fun getInsertPhotoDetail(): String {
        return BASE_URL + "GotrackReport/insertActivityDetailPhoto"
    }

    fun getupdateActivityDetailPhoto(): String {
        return BASE_URL + "GotrackReport/updateActivityDetailPhoto"
    }

    fun getInsertReportWeeklyOperation(): String {
        return BASE_URL + "GotrackReport/insertReportWeeklyOperation"
    }

    fun getInsertWeeklyOperationDetailPhoto(): String {
        return BASE_URL + "GotrackReport/insertWeeklyOperationDetailPhoto"
    }

    fun getInsertCompliance(): String {
        return BASE_URL + "GotrackReport/insertCompliance"
    }

    fun getInsertCompliancePhoto(): String {
        return BASE_URL + "GotrackReport/insertCompliancePhoto"
    }

    val hostUrlUpdate: String
        get() = "http://gotrack-api.pitjarus.co/"



}
