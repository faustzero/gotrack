package com.mpi.storecheckin.rests

import com.nestle.mdgt.models.ReportResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ReportApiInterface {
//
//    @Multipart
//    @POST("report/checkInVisitWith3Photo")
//    fun visitIn(
//            @Part("visit_id") visitId: RequestBody,
//            @Part("store_id") storeId: RequestBody,
//            @Part("surveyor_id") employeeId: RequestBody,
//            @Part("is_visit") isVisit: RequestBody,
//            @Part("reason_no_visit") reasonNoVisit: RequestBody,
//            @Part("start_datetime_utc") startDatetimeUtc: RequestBody,
//            @Part("end_datetime_utc") endDatetimeUtc: RequestBody,
//            @Part file: MultipartBody.Part,
//            @Part fileVisual: MultipartBody.Part,
//            @Part filePOSM: MultipartBody.Part
//    ): Call<ReportResponse>
//
//    @FormUrlEncoded
//    @POST("report/checkOutVisit")
//    fun visitOut(
//            @Field("visit_id") visitId: String,
//            @Field("check_out_datetime_utc") endDatetimeUtc: Long
//    ): Call<ReportResponse>
//
//    @FormUrlEncoded
//    @POST("report/updateStoreLocation")
//    fun updateStoreLocation(
//            @Field("store_id") storeId: Int,
//            @Field("latitude") latitude: Double,
//            @Field("longitude") longitude: Double
//    ): Call<ReportResponse>
//
//    @FormUrlEncoded
//    @POST("report/updateResellerLocation")
//    fun updateResellerLocation(
//            @Field("reseller_id") resellerId: String,
//            @Field("latitude") latitude: Double,
//            @Field("longitude") longitude: Double
//    ): Call<ReportResponse>
//
//    @FormUrlEncoded
//    @POST("report/insertReseller")
//    fun insertReseller(
//            @Field("reseller_id") resellerId: String,
//            @Field("reseller_name") resellerName: String,
//            @Field("reseller_type_id") resellerTypeId: Int,
//            @Field("phone") phone: String,
//            @Field("address") address: String,
//            @Field("kecamatan_id") kecamatanId: Int,
//            @Field("register_date") registerDate: String,
//            @Field("store_id") storeId: Int,
//            @Field("surveyor_id") surveyorId: Int
//    ): Call<ReportResponse>
//
//    @Multipart
//    @POST("report/insertCompetitorActivity")
//    fun insertCompetitorActivity(
//            @Part("visit_id") visitId: RequestBody,
//            @Part("activity_id") activityId: RequestBody,
//            @Part("product_id") productId: RequestBody,
//            @Part("description") description: RequestBody,
//            @Part("start_date") startDate: RequestBody,
//            @Part("end_date") endDate: RequestBody,
//            @Part file: MultipartBody.Part
//    ): Call<ReportResponse>
//
//    @FormUrlEncoded
//    @POST("report/insertPullProgram")
//    fun insertPullProgram(
//            @Field("visit_id") visitId: String,
//            @Field("surveyor_id") surveyorId: Int,
//            @Field("reseller_id") resellerId: String,
//            @Field("values") values: String
//    ): Call<ReportResponse>
//
//    @FormUrlEncoded
//    @POST("report/insertPenjualan")
//    fun insertPenjualan(
//            @Field("visit_id") visitId: String,
//            @Field("surveyor_id") surveyorId: Int,
//            @Field("reseller_id") resellerId: String,
//            @Field("values") posmTypeId: String
//    ): Call<ReportResponse>
//
//    @Multipart
//    @POST("report/updateProfilePhoto")
//    fun updateProfilePhoto(
//            @Part("surveyor_id") surveyorId: RequestBody,
//            @Part file: MultipartBody.Part
//    ): Call<ReportResponse>
}
