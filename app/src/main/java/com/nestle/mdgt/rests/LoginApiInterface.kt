package com.nestle.mdgt.rests

import com.nestle.mdgt.models.*

import retrofit2.Call
import retrofit2.http.*

interface LoginApiInterface {
    @FormUrlEncoded
    @POST("GotrackLogin/login11")
    fun getLoginData(
            @Field("username") username: String,
            @Field("password") password: String,
            @Field("android_id") androidId: String,
            @Field("app_version") appVersion: String,
            @Field("device_data") deviceData: String): Call<LoginResponse>

    @GET("weather")
    fun getWeather(
            @Query("lat") lat: Double,
            @Query("lon") lon: Double,
            @Query("appid") appId: String,
            @Query("units") units: String): Call<WeatherResponse>

    @GET("forecast")
    fun getWeatherNext(
            @Query("lat") lat: Double,
            @Query("lon") lon: Double,
            @Query("appid") appId: String,
            @Query("units") units: String): Call<ListWeather>

    @FormUrlEncoded
    @POST("GotrackReport/deleteActivity")
    fun deleteList(
            @Field("report_header_id") reportheader: String) : Call<DetailList>

    @FormUrlEncoded
    @POST("GotrackLogin/createDataNoo")
    fun insertNoo(
            @Field("store_code") storeCode: String,
            @Field("store_name") storeName: String,
            @Field("store_address") storeAddress: String,
            @Field("store_phone") storePhone: String,
            @Field("distributor_id") distributorId: Int,
            @Field("store_type_id") storeTypeId: Int,
            @Field("role_id") roleId: Int) : Call<NooDataResponse>




}