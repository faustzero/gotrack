package com.nestle.mdgt.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.opengl.Visibility
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder
import com.github.dhaval2404.imagepicker.ImagePicker

import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoLandscapeActivity
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_local_activity.*
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class CampaignPosmAdapter(private var daoSession: DaoSession,private  val context: Activity, private val models: List<TReportCampaignDetail>, private val rowLayout: Int,
                          private var reasonPosmAdapter: ArrayAdapter<TReasonCampaignPosm>
                          , private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<CampaignPosmAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TReportCampaignDetail)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {

        var posmName: TextView = v.findViewById(R.id.posmName)
        var btnPhoto: FloatingActionButton = v.findViewById(R.id.btnPhoto)

        var btnQr: FloatingActionButton = v.findViewById(R.id.btnQr)
        var posmImage: ImageView = v.findViewById(R.id.posmImage)

        var pilihanName:TextView = v.findViewById(R.id.picos_avaibility)

        var pilihanRadio:RadioGroup = v.findViewById(R.id.row_rgAvailable_picos)
        var tombolYa:RadioButton = v.findViewById(R.id.radioTombol_1)
        var tombolTidak:RadioButton = v.findViewById(R.id.radioTombol_2)
        var spinnerReason:Spinner = v.findViewById(R.id.spinReasonCampaign_perPOSM)
        var cardPosmImage:CardView = v.findViewById(R.id.cardPosmImage)





        fun bind(item: TReportCampaignDetail, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }
//    private lateinit var reasonAdapter: ArrayAdapter<TReasonCampaign>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        var tersedia = -1

        holder.spinnerReason.adapter=reasonPosmAdapter

        holder.spinnerReason.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long) {
                var reason=parent.getItemAtPosition(position) as TReasonCampaignPosm
                model.alasan=reason.reasonId
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }
        tersedia = model.status
        if(tersedia == 1){
            (holder.pilihanRadio.getChildAt(0) as RadioButton).isChecked = true
            holder.posmName.visibility = View.VISIBLE
            holder.cardPosmImage.visibility = View.VISIBLE
            if(model.isQrMandatory == 1) {
                holder.btnPhoto.hide()
                holder.btnQr.show()
                if (model.isScanned == 1) {
                    holder.btnPhoto.show()
                    holder.btnQr.hide()
                }
            }else{
                holder.btnPhoto.show()
                holder.btnQr.hide()
            }
            holder.spinnerReason.visibility=View.GONE
            model.status = 1
        }else if(tersedia == 0){
            (holder.pilihanRadio.getChildAt(1) as RadioButton).isChecked = true
            holder.posmName.visibility = View.GONE
            holder.cardPosmImage.visibility = View.GONE
            holder.btnPhoto.hide()
            holder.btnQr.hide()
            if(Login.getEmployeeRoleId(context)==12 || Login.getEmployeeRoleId(context)==14 || Login.getEmployeeRoleId(context)==15){
                holder.spinnerReason.visibility=View.VISIBLE
            }
            model.status = 0
        }else{
            holder.posmName.visibility = View.GONE
            holder.cardPosmImage.visibility = View.GONE
            holder.btnPhoto.hide()
            holder.btnQr.hide()
            holder.spinnerReason.visibility=View.GONE
            model.status = -1
            holder.pilihanRadio.clearCheck()
        }

//        reasonAdapter = ArrayAdapter(context,
//                android.R.layout.simple_spinner_dropdown_item, daoSession.tReasonCampaignDao.loadAll())
//        reasonAdapter.insert(TReasonCampaign(null, 0, ""), 0)

        if(model.alasan>0) {
            val reason = reasonPosmAdapter.getPosition(daoSession.tReasonCampaignPosmDao.queryBuilder().where(TReasonCampaignPosmDao.Properties.ReasonId.eq(model.alasan)).limit(1).unique())
            holder.spinnerReason.setSelection(reason)
        }

        holder.pilihanRadio.setOnCheckedChangeListener { group, checkedId ->
            val rs = group.findViewById<View>(checkedId) as RadioButton?
            if(rs != null){
                tersedia = Integer.parseInt(rs.contentDescription.toString())
                if(tersedia == 1){
                    holder.posmName.visibility = View.VISIBLE
                    holder.cardPosmImage.visibility = View.VISIBLE
                    holder.btnPhoto.hide()
                    holder.btnQr.show()
                    if(model.isScanned==1) {
                        holder.btnPhoto.show()
                        holder.btnQr.hide()
                    }
                    holder.spinnerReason.visibility=View.GONE
                    model.status = 1
                }else if(tersedia == 0){
                    holder.posmName.visibility = View.GONE
                    holder.cardPosmImage.visibility = View.GONE
                    holder.btnPhoto.hide()
                    holder.btnQr.hide()

                    if(Login.getEmployeeRoleId(context)==12|| Login.getEmployeeRoleId(context)==14 || Login.getEmployeeRoleId(context)==15){
                        holder.spinnerReason.visibility=View.VISIBLE
                    }
                    model.status = 0
                }else{
                    holder.posmName.visibility = View.GONE
                    holder.cardPosmImage.visibility = View.GONE
                    holder.btnPhoto.hide()
                    holder.btnQr.hide()
                    holder.spinnerReason.visibility=View.GONE
                    model.status = -1
                }

            }
        }
        holder.posmName.text = model.posmName
        holder.pilihanName.text = "Apakah ${model.posmName} Tersedia"
        Log.i("path",model.photoPath)



        if (!model.photoPath.equals("") && model.status == 1) {
            holder.posmImage.setScaleType(ImageView.ScaleType.FIT_XY)
            holder.posmImage.setImageBitmap(BitmapFactory.decodeFile(model.photoPath))

            holder.tombolYa.isChecked= true


        }else if(model.photoPath.equals("") && model.status == 0){
            holder.tombolTidak.isChecked= true
        }

        holder.btnPhoto.setOnClickListener {

            val builder = android.app.AlertDialog.Builder(context)
            builder.setTitle("Perhatian!")
            builder.setMessage("Harap mengambil foto secara Landscape")
            builder.setPositiveButton("Ya") { _, _ -> takePhoto(position) }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()

        }
        holder.posmImage.setOnClickListener {
            if (!model.photoPath.equals("") && model.status == 1) {
                val intent = Intent(context, FullImageActivity::class.java)
                intent.putExtra("ImageUrl", model.photoPath)
                intent.putExtra("Rotation", 90)

                holder.tombolYa.isChecked= true


                context.startActivity(intent)
            }
            else if(model.photoPath.equals("") && model.status == 0){
                holder.tombolTidak.isChecked= true
            }

        }

        holder.btnQr.setOnClickListener {
            try{

                if(Login.getEmployeeRoleId(context)==14) {
                    val items = arrayOf<CharSequence>("Scan QR", "Gallery")
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("Add Photo!")
                    builder.setItems(items) { dialog, item ->
                        //                    if(taskEditText.text.toString() != "") {
                        if (items[item] == "Scan QR") {
                            val materialBarcodeScanner = MaterialBarcodeScannerBuilder()
                                    .withActivity(context)
                                    .withEnableAutoFocus(true)
                                    .withBleepEnabled(true)
                                    .withBackfacingCamera()
                                    .withCenterTracker()
                                    .withText("Scanning...")
                                    .withResultListener { barcode ->
                                        var barcodeResult = barcode
                                        Log.i("var-md5hasil scan", "." + md5(barcode.rawValue) + ".")
                                        Log.i("var-md5Qr", "." + md5(model.encode) + ".")
//                            Toast.makeText(context, md5(barcode.rawValue), Toast.LENGTH_SHORT).show()

                                        if (model.encode.equals(md5(barcode.rawValue))) {
                                            Toast.makeText(context, "Qr code sesuai, Silahkan lanjutkan untuk foto", Toast.LENGTH_SHORT).show()
                                            holder.btnPhoto.show()
                                            holder.btnQr.hide()
                                            model.isScanned = 1
                                        } else {
//                                Toast.makeText(context, barcode.rawValue, Toast.LENGTH_SHORT).show()
                                            Toast.makeText(context, "Qr code tidak sesuai", Toast.LENGTH_SHORT).show()

                                            holder.btnPhoto.hide()
                                            holder.btnQr.show()
                                        }

                                    }
                                    .build()
                            materialBarcodeScanner.startScan()
                        } else if (items[item] == "Gallery") {
                            ImagePicker.with(context).galleryOnly().start()

//                            val task = taskEditText.text.toString()
//                          model.infoPhoto = task
                            SharedPrefsUtils.setStringPreference(context, Config.POSM_ID, model.posmId.toString())
                            SharedPrefsUtils.setStringPreference(context, Config.COMPLIANCE_ID, "x")
//                            val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                    val intent = Intent(context, CampaignAddActivity::class.java)
//                    context.startActivityForResult(intent, 104)

                        }


                    }
                    builder.show()
                }else{
                    val materialBarcodeScanner = MaterialBarcodeScannerBuilder()
                            .withActivity(context)
                            .withEnableAutoFocus(true)
                            .withBleepEnabled(true)
                            .withBackfacingCamera()
                            .withCenterTracker()
                            .withText("Scanning...")
                            .withResultListener { barcode ->
                                var barcodeResult = barcode
                                Log.i("var-md5hasil scan raw", "." + barcode.rawValue + ".")
                                Log.i("var-md5hasil scan", "." + md5(barcode.rawValue) + ".")
                                Log.i("var-md5Qr", "." + model.encode + ".")
//                            Toast.makeText(context, md5(barcode.rawValue), Toast.LENGTH_SHORT).show()

                                if (model.encode.equals(md5(barcode.rawValue))) {
                                    Toast.makeText(context, "Qr code sesuai, Silahkan lanjutkan untuk foto", Toast.LENGTH_SHORT).show()
                                    holder.btnPhoto.show()
                                    holder.btnQr.hide()
                                    model.isScanned = 1
                                } else {
//                                Toast.makeText(context, barcode.rawValue, Toast.LENGTH_SHORT).show()
                                    Toast.makeText(context, "Qr code tidak sesuai", Toast.LENGTH_SHORT).show()

                                    holder.btnPhoto.hide()
                                    holder.btnQr.show()
                                }

                            }
                            .build()
                    materialBarcodeScanner.startScan()
                }


            } catch (e: Exception) {
                Toast.makeText(context, "Terjadi kesalahan scan qrcode. Silahkan coba lagi.", Toast.LENGTH_SHORT).show()
            }

        }


        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    fun md5(s: String): String? {
        val MD5 = "MD5"
        try {
            // Create MD5 Hash
            val digest = MessageDigest
                    .getInstance(MD5)
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2) h = "0$h"
                hexString.append(h)
            }
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    fun takePhoto(position: Int) {

        val model = models[position]

        if(Login.getEmployeeRoleId(context)==14){
            val items = arrayOf<CharSequence>("Camera", "Gallery")
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Add Photo!")
            builder.setItems(items) { dialog, item ->
                //                    if(taskEditText.text.toString() != "") {
                if (items[item] == "Camera") {
//                            val task = taskEditText.text.toString()
//                            model.infoPhoto = task
                    val intent = Intent(context, PhotoLandscapeActivity::class.java)
                    intent.putExtra("nama_file", "campaign#" + model.campaignId+ "#" + DateTime.now().toString("yyyyMMddHHmmssSSS")+ "#" + model.posmId  + "%" + position)
                    intent.putExtra("photo_path_left", "")
                    intent.putExtra("photo_path_top", "")
                    intent.putExtra("photo_path_right", "")
                    intent.putExtra("photo_path_bottom", "")
                    context.startActivityForResult(intent, 101)

                } else if (items[item] == "Gallery") {
                    ImagePicker.with(context).galleryOnly().start()

//                            val task = taskEditText.text.toString()
//                          model.infoPhoto = task
                    SharedPrefsUtils.setStringPreference(context, Config.POSM_ID, model.posmId.toString())
//                            val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                    val intent = Intent(context, CampaignAddActivity::class.java)
//                    context.startActivityForResult(intent, 104)

                }


            }
            builder.show()
        }else{
            val intent = Intent(context, PhotoLandscapeActivity::class.java)
            intent.putExtra("nama_file", "campaign#" + model.campaignId+ "#" + DateTime.now().toString("yyyyMMddHHmmssSSS")+ "#" + model.posmId  + "%" + position)
            intent.putExtra("photo_path_left", "")
            intent.putExtra("photo_path_top", "")
            intent.putExtra("photo_path_right", "")
            intent.putExtra("photo_path_bottom", "")
            context.startActivityForResult(intent, 101)
        }
    }


//    fun md5(s: String): String? {
//        try {
//            // Create MD5 Hash
//            val digest = MessageDigest.getInstance("MD5")
//            digest.update(s.toByteArray())
//            val messageDigest = digest.digest()
//
//            // Create Hex String
//            val hexString = StringBuffer()
//            for (i in messageDigest.indices)
//                hexString.append(Integer.toHexString(0xFF and messageDigest[i].toInt())
//                )
//            return hexString.toString()
//        } catch (e: NoSuchAlgorithmException) {
//            e.printStackTrace()
//        }
//        return ""
//    }

}
