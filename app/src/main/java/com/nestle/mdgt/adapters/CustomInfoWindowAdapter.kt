package com.nestle.mdgt.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.nestle.mdgt.R

class CustomInfoWindowAdapter(context: Context) : GoogleMap.InfoWindowAdapter {
    private val mContents: View

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mContents = inflater.inflate(R.layout.custom_info_contents, null)
    }

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View? {
        if (marker.snippet == "") {
            return null
        } else {
            render(marker, mContents)
            return mContents
        }
    }

    private fun render(marker: Marker, view: View) {
        val lblStoreName = view.findViewById(R.id.infoContent_marketName) as TextView
        val lblStoreCode = view.findViewById(R.id.infoContent_marketCode) as TextView
        val lblLocation = view.findViewById(R.id.infoContent_description) as TextView

        lblStoreName.text = marker.title
        if (marker.snippet != "") {
            val snippet = marker.snippet.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            lblStoreCode.text = snippet[1]
            lblLocation.text = "${snippet[2]}, ${snippet[3]}"
        }
    }
}