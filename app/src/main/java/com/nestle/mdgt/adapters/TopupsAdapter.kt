package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.nestle.mdgt.R
import com.nestle.mdgt.database.TReportTopup

class TopupsAdapter(private val models: List<TReportTopup>, private val rowLayout: Int)
    : androidx.recyclerview.widget.RecyclerView.Adapter<TopupsAdapter.ViewHolder>() {

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var productName: TextView = v.findViewById(R.id.row_lblProductName)
        var description: TextView = v.findViewById(R.id.row_lblDescription)
        var unit1: TextView = v.findViewById(R.id.row_lblUnit1)
        var qty1: EditText = v.findViewById(R.id.row_txtQty1)
        var unit2: TextView = v.findViewById(R.id.row_lblUnit2)
        var qty2: EditText = v.findViewById(R.id.row_txtQty2)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.productName.text = model.productName
        holder.description.text = model.brandName
        holder.unit1.text = model.unitName1
        holder.qty1.setText(model.quantity1.toString())
        holder.unit2.text = model.unitName2
        holder.qty2.setText(model.quantity2.toString())

        holder.qty1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                val enteredString = charSequence.toString()
                if (enteredString != "0") {
                    if (enteredString.startsWith("0") && enteredString.isNotEmpty()) {
                        holder.qty1.setText(enteredString.substring(1))
                    }
                }

                if (charSequence.toString().trim { it <= ' ' } != "") {
                    model.quantity1 = Integer.parseInt(charSequence.toString().trim { it <= ' ' })
                } else {
                    model.quantity1 = 0
                }
            }
            override fun afterTextChanged(editable: Editable) {
            }
        })
        holder.qty2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                val enteredString = charSequence.toString()
                if (enteredString != "0") {
                    if (enteredString.startsWith("0") && enteredString.isNotEmpty()) {
                        holder.qty2.setText(enteredString.substring(1))
                    }
                }

                if (charSequence.toString().trim { it <= ' ' } != "") {
                    model.quantity2 = Integer.parseInt(charSequence.toString().trim { it <= ' ' })
                } else {
                    model.quantity2 = 0
                }
            }
            override fun afterTextChanged(editable: Editable) {
            }
        })
    }


    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
