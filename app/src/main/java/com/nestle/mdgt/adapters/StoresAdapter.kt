package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.nestle.mdgt.R
import com.nestle.mdgt.database.TStore
import com.nestle.mdgt.utils.Config

class StoresAdapter(private val models: List<TStore>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<StoresAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TStore)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var description: TextView = v.findViewById(R.id.row_description)
        var storeCode: TextView = v.findViewById(R.id.row_storeCode)
        var storeName: TextView = v.findViewById(R.id.row_storeName)
        var imgChecked: ImageView = v.findViewById(R.id.row_imgChecked)

        fun bind(item: TStore, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.description.text = "${model.address}, ${model.kecamatanName}, ${model.kabupatenName}, ${model.areaName}"
        holder.storeCode.text = "${model.storeCode}, ${model.storeTypeName}"
        holder.storeName.text = model.storeName

        if (model.isVisited == Config.YES_CODE) {
            holder.imgChecked.visibility = View.VISIBLE
        } else {
            holder.imgChecked.visibility = View.GONE
        }

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
