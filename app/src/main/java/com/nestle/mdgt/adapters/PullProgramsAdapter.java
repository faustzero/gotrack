package com.nestle.mdgt.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nestle.mdgt.R;
import com.nestle.mdgt.database.TReportPullProgram;

import java.util.List;

public class PullProgramsAdapter extends RecyclerView.Adapter<PullProgramsAdapter.ViewHolder> {
    private final OnItemClickListener onItemClickListener;
    private final List<TReportPullProgram> models;
    private int rowLayout;

    public interface OnItemClickListener {
        void onItemClick(TReportPullProgram item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView sticker, qty;

        ViewHolder(View v) {
            super(v);
            sticker = (TextView) v.findViewById(R.id.pullprogramlistrow_lblStickerName);
            qty = (TextView) v.findViewById(R.id.pullprogramlistrow_lblQty);
        }

        void bind(final TReportPullProgram item, final OnItemClickListener onItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(item);
                }
            });
        }
    }

    public PullProgramsAdapter(List<TReportPullProgram> models, int rowLayout, OnItemClickListener listener) {
        this.models = models;
        this.rowLayout = rowLayout;
        this.onItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TReportPullProgram model = models.get(position);
        holder.sticker.setText(model.getStickerName());
        holder.qty.setText(String.valueOf(model.getQuantity()));
        holder.bind(model, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
