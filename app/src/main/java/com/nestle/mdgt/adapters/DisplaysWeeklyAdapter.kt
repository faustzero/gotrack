package com.nestle.mdgt.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide

import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageIrActivity
import com.nestle.mdgt.activities.FullImageIrInfoActivity
import com.nestle.mdgt.activities.PhotoLandscapeActivity
import com.nestle.mdgt.activities.PhotoLandscapeInfoActivity
import com.nestle.mdgt.database.TReportWeeklyDetail
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils

import org.joda.time.DateTime
import java.io.File

class DisplaysWeeklyAdapter(private val context: Activity, private val models: List<TReportWeeklyDetail>?, private val rowLayout: Int) : androidx.recyclerview.widget.RecyclerView.Adapter<DisplaysWeeklyAdapter.ViewHolder>() {

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var image: ImageView
        var btnPhoto: Button
        var lblNumber: TextView

        init {
            image = v.findViewById(R.id.row_img)
            btnPhoto = v.findViewById(R.id.row_btnPhoto)
            lblNumber = v.findViewById(R.id.row_imgNumber)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, _position: Int) {
        val position = holder.adapterPosition
        val model = models!![position]

        if(model.photoPath!=null && model.photoPath!=""){
            if (File(model.photoPath).exists()) {
                holder.image.setImageBitmap(BitmapFactory.decodeFile(model.photoPath))
                holder.image.setScaleType(ImageView.ScaleType.FIT_XY)
//                holder.lblNumber.text = model.infoPhoto
            } else {
                Glide.with(context).asBitmap().load(ApiClient.hostUrl + model.photoPath).into(holder.image)
                holder.image.setScaleType(ImageView.ScaleType.FIT_XY)
//                holder.lblNumber.text = model.infoPhoto
            }
        }

        holder.image.setOnClickListener {
            if (model.photoPath != "") {
                Log.i("path", model.photoPath)
                val intent = Intent(context, FullImageIrActivity::class.java)
                intent.putExtra("ImageUrl", model.photoPath)
//                intent.putExtra("ImageInfo", model.infoPhoto)
                context.startActivity(intent)
            }
        }

        holder.btnPhoto.setOnClickListener {
//            val taskEditText = EditText(context)
            val items = arrayOf<CharSequence>("Camera", "Gallery")
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Add Photo!")
//            builder.setView(taskEditText)
//            taskEditText.setHint("Info Foto")
//            taskEditText.setAllCaps(true)

            builder.setItems(items) { dialog, item ->
//                if(taskEditText.text.toString() != "") {
                    if (items[item] == "Camera") {
//                        val task = taskEditText.text.toString()
//                        model.infoPhoto = task
                        val intent = Intent(context, PhotoLandscapeInfoActivity::class.java)

                        val left = if (model.leftPosition == 0) "" else models[model.leftPosition - 1].photoPath
                        val top = if (model.topPosition == 0) "" else models[model.topPosition - 1].photoPath
                        val right = if (model.rightPosition == 0) "" else models[model.rightPosition - 1].photoPath
                        val bottom = if (model.bottomPosition == 0) "" else models[model.bottomPosition - 1].photoPath

                        intent.putExtra("nama_file", "WO" + model.userId + "_" + model.distId + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS") + "_" + model.reportDetailId + ":" + position)
                        intent.putExtra("photo_path_left", left)
                        intent.putExtra("photo_path_top", top)
                        intent.putExtra("photo_path_right", right)
                        intent.putExtra("photo_path_bottom", bottom)

                        context.startActivityForResult(intent, 33)
                    } else if (items[item] == "Gallery") {
//                        val task = taskEditText.text.toString()
//                        model.infoPhoto = task
                        SharedPrefsUtils.setStringPreference(context, Config.DETAIL_ID, model.reportDetailId)
                        val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        context.startActivityForResult(galleryIntent, 34)
                    }
//                }
//                else{
//                    Toast.makeText(context, "Isi info foto terlebih dahulu", Toast.LENGTH_SHORT).show()
//                }

                Log.i("modelfoto",model.infoPhoto)
            }
            builder.show()
        }

        if (model.rowNumber == 1 && model.columnNumber == 1) {
            holder.lblNumber.visibility = View.VISIBLE
        } else {
            holder.lblNumber.visibility = View.GONE
        }

    }


    override fun getItemCount(): Int {
        return models?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}
