package com.nestle.mdgt.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.nestle.mdgt.R
import com.nestle.mdgt.database.TReportCampaignHeader

class CampaignAdapter( private val models: List<TReportCampaignHeader>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<CampaignAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TReportCampaignHeader)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var CampaignName: TextView = v.findViewById(R.id.row_lblCampaignName)
        var description: TextView = v.findViewById(R.id.row_lblDescription)
        var imgChecked: ImageView = v.findViewById(R.id.row_imgChecked)

        fun bind(item: TReportCampaignHeader, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.CampaignName.text = model.campaignName

        if (model.isDone == true) {
            holder.imgChecked.visibility = View.VISIBLE
        } else {
            holder.imgChecked.visibility = View.GONE
        }

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
