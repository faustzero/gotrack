package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.nestle.mdgt.R
import com.nestle.mdgt.database.TReportMsl

class MslsAdapter(private val models: List<TReportMsl>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<MslsAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TReportMsl)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var productName: TextView = v.findViewById(R.id.row_lblProductName)
        var description: TextView = v.findViewById(R.id.row_lblDescription)
        var imgChecked: ImageView = v.findViewById(R.id.row_imgChecked)

        fun bind(item: TReportMsl, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.productName.text = model.productName
        holder.description.text = model.brandName

        if (model.isDone == true) {
            holder.imgChecked.visibility = View.VISIBLE
        } else {
            holder.imgChecked.visibility = View.GONE
        }

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
