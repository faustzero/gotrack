package com.nestle.mdgt.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.mdgt.R;
import com.nestle.mdgt.database.TReseller;

import java.util.List;

public class ResellersAdapter extends RecyclerView.Adapter<ResellersAdapter.ViewHolder> {
    private final OnItemClickListener onItemClickListener;
    private final List<TReseller> models;
    private int rowLayout;

    public interface OnItemClickListener {
        void onItemClick(TReseller item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView resellerType, resellerId, resellerName;
        ImageView imgChecked;

        ViewHolder(View v) {
            super(v);
            resellerType = (TextView) v.findViewById(R.id.resellerlistrow_type);
            resellerId = (TextView) v.findViewById(R.id.resellerlistrow_resellerId);
            resellerName = (TextView) v.findViewById(R.id.resellerlistrow_resellerName);
            imgChecked = (ImageView) v.findViewById(R.id.resellerlistrow_imgChecked);
        }

        void bind(final TReseller item, final OnItemClickListener onItemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(item);
                }
            });
        }
    }

    public ResellersAdapter(List<TReseller> models, int rowLayout, OnItemClickListener listener) {
        this.models = models;
        this.rowLayout = rowLayout;
        this.onItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TReseller model = models.get(position);
        holder.resellerType.setText(model.getResellerTypeName());
        holder.resellerId.setText(model.getResellerId() + "\n" + model.getAddress());
        holder.resellerName.setText(model.getResellerName());

        if(model.getIsDonePenjualan()) {
            holder.imgChecked.setVisibility(View.VISIBLE);
        } else {
            holder.imgChecked.setVisibility(View.GONE);
        }

        holder.bind(models.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
