package com.nestle.mdgt.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.nestle.mdgt.R
import com.nestle.mdgt.database.TReportActivity
import com.nestle.mdgt.utils.DateTimeUtils

class ActivitiesAdapter(private val context: Context, private val models: List<TReportActivity>, private val rowLayout: Int)
    : androidx.recyclerview.widget.RecyclerView.Adapter<ActivitiesAdapter.ViewHolder>() {

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var activityName: TextView = v.findViewById(R.id.row_lblActivityName)
        var productCategory: TextView = v.findViewById(R.id.row_lblProductCategory)
        var description: TextView = v.findViewById(R.id.row_lblDescription)
        var available: RadioGroup = v.findViewById(R.id.row_rgAvailable)
//        var imgPhoto: ImageView = v.findViewById(R.id.row_imgPhoto)
//        var btnPhoto: ImageView = v.findViewById(R.id.row_btnPhoto)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        val startDate = DateTimeUtils.formatDateString(model.startDate, "yyyy-MM-dd", "d MMMM yyyy")
        val endDate = DateTimeUtils.formatDateString(model.endDate, "yyyy-MM-dd", "d MMMM yyyy")
        holder.activityName.text = model.activityName
        holder.productCategory.text = model.productCategoryName
        holder.description.text = "$startDate s/d $endDate"

        if (model.isAvailable > -1) {
            if (model.isAvailable == 1) {
                (holder.available.getChildAt(0) as RadioButton).isChecked = true
            } else if (model.isAvailable == 0) {
                (holder.available.getChildAt(1) as RadioButton).isChecked = true
            }
        }
        holder.available.setOnCheckedChangeListener({ radioGroup, i ->
            val rb = radioGroup.findViewById<View>(i) as RadioButton
            if (rb != null) {
                model.isAvailable = rb.contentDescription.toString().toInt()
            }
        })
//
//        holder.imgPhoto.setOnClickListener({
//            if (model.photoPath != "") {
//                val intent = Intent(context, FullImageActivity::class.java)
//                intent.putExtra("ImageUrl", model.photoPath)
//                context.startActivity(intent)
//            }
//        })
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
