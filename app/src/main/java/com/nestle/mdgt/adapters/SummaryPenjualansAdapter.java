package com.nestle.mdgt.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nestle.mdgt.R;
import com.nestle.mdgt.database.TReportPenjualan;

import java.util.List;

public class SummaryPenjualansAdapter extends RecyclerView.Adapter<SummaryPenjualansAdapter.ViewHolder> {
    private final List<TReportPenjualan> models;
    private int rowLayout;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView productName, description, unitName1, unitName2, qty1, qty2;

        ViewHolder(View v) {
            super(v);
            productName = (TextView) v.findViewById(R.id.productsummarylistrow_lblProductName);
            description = (TextView) v.findViewById(R.id.productsummarylistrow_lblDescription);
            unitName1 = (TextView) v.findViewById(R.id.productsummarylistrow_lblUnit1);
            unitName2 = (TextView) v.findViewById(R.id.productsummarylistrow_lblUnit2);
            qty1 = (TextView) v.findViewById(R.id.productsummarylistrow_txtQty1);
            qty2 = (TextView) v.findViewById(R.id.productsummarylistrow_txtQty2);
        }
    }

    public SummaryPenjualansAdapter(List<TReportPenjualan> models, int rowLayout) {
        this.models = models;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TReportPenjualan model = models.get(position);
        holder.productName.setText(model.getProductName());
        holder.description.setText(model.getBrandName());
        holder.unitName1.setText(model.getUnitName1());
        holder.unitName2.setText(model.getUnitName2());
        holder.qty1.setText(String.valueOf(model.getQuantity1()));
        holder.qty2.setText(String.valueOf(model.getQuantity2()));
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
