package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.nestle.mdgt.R
import com.nestle.mdgt.database.TProductMSLRegion

class ProductMslAdapter(private val models: List<TProductMSLRegion>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<ProductMslAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TProductMSLRegion)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var txtProductName: TextView = v.findViewById(R.id.productName)
        var txtProductChecked: CheckBox = v.findViewById(R.id.productChecked)

        fun bind(item: TProductMSLRegion, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.txtProductName.text = model.productName
        model.isChoosed=0

        holder.txtProductChecked.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                model.isChoosed=1
            }
            else{
                model.isChoosed=0
            }
        }

//        holder.description.text = model.headerId

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}