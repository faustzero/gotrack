package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView

import com.nestle.mdgt.R
import com.nestle.mdgt.database.TReportSurveyPrice
import com.nestle.mdgt.utils.NumberTextWatcherForThousand

class SurveyPriceAdapter(private val models: List<TReportSurveyPrice>, private val rowLayout: Int)
    : androidx.recyclerview.widget.RecyclerView.Adapter<SurveyPriceAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TReportSurveyPrice)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var skuName: TextView = v.findViewById(R.id.surveylistrow_lblSkuName)
        var rowTxtJual: EditText = v.findViewById(R.id.row_txtJual1)
        var rowTxtBeli: EditText = v.findViewById(R.id.row_txtBeli1)

        fun bind(item: TReportSurveyPrice, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.skuName.text = model.productName
        if(model.beli>-1){
            holder.rowTxtBeli.setText(model.beli.toInt().toString())
        }
        if(model.jual>-1){
            holder.rowTxtJual.setText(model.jual.toInt().toString())
        }
        holder.rowTxtBeli.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                val enteredString = charSequence.toString()
                val str = holder.rowTxtBeli.text.toString().replace(",".toRegex(), "")
                if (enteredString != "0") {
                    if (enteredString.startsWith("0") && enteredString.length > 0) {
                        holder.rowTxtBeli.setText(enteredString.substring(1))
//                        holder.rowTxtBeli.setText(NumberTextWatcherForThousand.getDecimalFormattedString(str))
                    }
                }

                if (charSequence.toString().trim { it <= ' ' } != "") {
                    model.beli=charSequence.toString().trim { it <= ' ' }.toDouble()
                } else {
                    model.beli= (-1).toDouble()
                }
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })
        holder.rowTxtJual.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                val enteredString = charSequence.toString()
                val str = holder.rowTxtJual.text.toString().replace(",".toRegex(), "")
                if (enteredString != "0") {
                    if (enteredString.startsWith("0") && enteredString.length > 0) {
                        holder.rowTxtJual.setText(enteredString.substring(1))
//                        holder.rowTxtJual.setText(NumberTextWatcherForThousand.getDecimalFormattedString(str))
                    }
//                    val str = holder.rowTxtBeli.text.toString().replace(",".toRegex(), "")
//                    if (enteredString != "")
//                        holder.rowTxtBeli.setText(NumberTextWatcherForThousand.getDecimalFormattedString(str))
//                    holder.rowTxtBeli.setSelection(holder.rowTxtBeli.text.toString().length)
                }


                if (charSequence.toString().trim { it <= ' ' } != "") {
                    model.jual=charSequence.toString().trim { it <= ' ' }.toDouble()
                } else {
                    model.jual= (-1).toDouble()
                }
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })


    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
