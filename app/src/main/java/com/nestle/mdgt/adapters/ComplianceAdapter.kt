package com.nestle.mdgt.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.nestle.mdgt.database.TReportCompliance
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoLandscapeActivity
import com.nestle.mdgt.activities.reguler.CampaignAddActivity
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import org.joda.time.DateTime

class ComplianceAdapter(

    models: List<TReportCompliance>?,
    rowLayout: Int,
    listener: OnItemClickListener,
    context:Activity
) : androidx.recyclerview.widget.RecyclerView.Adapter<ComplianceAdapter.ViewHolder>() {
    private val onItemClickListener: OnItemClickListener
    private val models: List<TReportCompliance>?
    private val rowLayout: Int
    private val context: Activity

    interface OnItemClickListener {
        fun onItemClick(item: TReportCompliance?)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var compliance: TextView
        var rgComply: RadioGroup
        var photo: TextView
        var btnPhoto: FloatingActionButton
        var image: ImageView
        var cardImage:CardView
        fun bind(item: TReportCompliance?, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }

        init {
            compliance = v.findViewById<View>(R.id.compliance) as TextView
            rgComply = v.findViewById<View>(R.id.rgComply) as RadioGroup
            photo= v.findViewById(R.id.photo)
            btnPhoto= v.findViewById(R.id.btnPhoto)
            image= v.findViewById(R.id.image)
            cardImage=v.findViewById(R.id.cardImage)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val model: TReportCompliance = models!![position]
        holder.compliance.setText(model.compliance)
        holder.photo.visibility = View.GONE
        holder.cardImage.visibility = View.GONE
        holder.btnPhoto.hide()

        if (model.isComply == 1) {
            (holder.rgComply.getChildAt(0) as RadioButton).isChecked = true
        } else if (model.isComply == 0) {
            (holder.rgComply.getChildAt(1) as RadioButton).isChecked = true
        } else {
            holder.rgComply.clearCheck()
        }

        var isComply = model.isComply
        if(isComply == 1){
            holder.photo.visibility = View.VISIBLE
            holder.cardImage.visibility = View.VISIBLE
            holder.btnPhoto.show()
        }else if(isComply == 0){
            holder.photo.visibility = View.GONE
            holder.cardImage.visibility = View.GONE
            holder.btnPhoto.hide()
        }else{
            holder.photo.visibility = View.GONE
            holder.cardImage.visibility = View.GONE
            holder.btnPhoto.hide()
        }



        holder.rgComply.setOnCheckedChangeListener { group, checkedId ->
            val rb = group.findViewById<View>(checkedId) as RadioButton?
            if(rb != null) {
                model.isComply=Integer.parseInt(rb.contentDescription.toString())
                var isComply = model.isComply
                if(isComply == 1){
                    holder.photo.visibility = View.VISIBLE
                    holder.cardImage.visibility = View.VISIBLE
                    holder.btnPhoto.show()
                }else if(isComply == 0){
                    holder.photo.visibility = View.GONE
                    holder.cardImage.visibility = View.GONE
                    holder.btnPhoto.hide()
                }else{
                    holder.photo.visibility = View.GONE
                    holder.cardImage.visibility = View.GONE
                    holder.btnPhoto.hide()
                }
                model.isDone=true
            }
            else{

            }
        }

        if (!model.photoPath.equals("")) {
            holder.image.setScaleType(ImageView.ScaleType.FIT_XY)
            holder.image.setImageBitmap(BitmapFactory.decodeFile(model.photoPath))
        }

        holder.btnPhoto.setOnClickListener {

            val builder = android.app.AlertDialog.Builder(context)
            builder.setTitle("Perhatian!")
            builder.setMessage("Harap mengambil foto secara Landscape")
            builder.setPositiveButton("Ya") { _, _ -> takePhoto(position) }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()

        }
        holder.image.setOnClickListener {
            if (!model.photoPath.equals("")) {
                val intent = Intent(context, FullImageActivity::class.java)
                intent.putExtra("ImageUrl", model.photoPath)
                intent.putExtra("Rotation", 90)
                context.startActivity(intent)
            }

        }



        holder.bind(model, onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    init {
        this.models = models
        this.rowLayout = rowLayout
        onItemClickListener = listener
        this.context=context
    }

    fun takePhoto(position: Int) {

        val model = models!![position]

        if(Login.getEmployeeRoleId(context)==14) {

            val items = arrayOf<CharSequence>("Camera", "Gallery")
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Add Photo!")
            builder.setItems(items) { dialog, item ->
                //                    if(taskEditText.text.toString() != "") {
                if (items[item] == "Camera") {
//                            val task = taskEditText.text.toString()
//                            model.infoPhoto = task
                    val intent = Intent(context, PhotoLandscapeActivity::class.java)
                    intent.putExtra("nama_file", "compliance#" + model.campaignId + "#" + DateTime.now().toString("yyyyMMddHHmmssSSS") + "#" + model.complianceId + "%" + position)
                    intent.putExtra("photo_path_left", "")
                    intent.putExtra("photo_path_top", "")
                    intent.putExtra("photo_path_right", "")
                    intent.putExtra("photo_path_bottom", "")
                    context.startActivityForResult(intent, 102)

                } else if (items[item] == "Gallery") {
                    ImagePicker.with(context).galleryOnly().start()

//                            val task = taskEditText.text.toString()
//                          model.infoPhoto = task
                    SharedPrefsUtils.setStringPreference(context, Config.COMPLIANCE_ID, model.complianceId.toString())
                    SharedPrefsUtils.setStringPreference(context, Config.POSM_ID, "x")

//                            val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                    val intent = Intent(context, CampaignAddActivity::class.java)
//                    context.startActivityForResult(intent, 104)

                }


            }
            builder.show()
        }else{
            val intent = Intent(context, PhotoLandscapeActivity::class.java)
            intent.putExtra("nama_file", "compliance#" + model.campaignId + "#" + DateTime.now().toString("yyyyMMddHHmmssSSS") + "#" + model.complianceId + "%" + position)
            intent.putExtra("photo_path_left", "")
            intent.putExtra("photo_path_top", "")
            intent.putExtra("photo_path_right", "")
            intent.putExtra("photo_path_bottom", "")
            context.startActivityForResult(intent, 102)
        }
    }
}