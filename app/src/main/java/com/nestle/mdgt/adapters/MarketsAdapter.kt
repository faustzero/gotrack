package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.nestle.mdgt.R
import com.nestle.mdgt.models.MarketViewModel
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.Helper

class MarketsAdapter (private val models: List<MarketViewModel>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<MarketsAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: MarketViewModel)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var description: TextView = v.findViewById(R.id.row_description)
        var marketCode: TextView = v.findViewById(R.id.row_marketCode)
        var marketName: TextView = v.findViewById(R.id.row_marketName)
        var distance: TextView = v.findViewById(R.id.row_distance)
        var imgChecked: ImageView = v.findViewById(R.id.row_imgChecked)
        var imgMarker: ImageView = v.findViewById(R.id.row_imgMarker)

        fun bind(item: MarketViewModel, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        val market = model.market
        holder.description.text = "${market!!.address}, ${market.kecamatanName},\n${market.kabupatenName}, ${market.areaName}"
        holder.marketCode.text = market.marketCode
        holder.marketName.text = market.marketName
        holder.distance.text = Helper.convertMeterToKm(model.distance)

        if (model.market!!.latitude == 0.toDouble() && model.market!!.longitude == 0.toDouble()) {
            holder.imgMarker.setImageResource(R.drawable.ic_marker_grey)
            holder.distance.text = "N/A"
        } else {
            when {
                model.distance <= model.market!!.radiusVerificationLimit - 20 -> holder.imgMarker.setImageResource(R.drawable.ic_marker_green)
                model.distance <= model.market!!.radiusVerificationLimit -> holder.imgMarker.setImageResource(R.drawable.ic_marker_yellow)
                else -> holder.imgMarker.setImageResource(R.drawable.ic_marker_red)
            }
        }

        if (market.isVisited == Config.YES_CODE) {
            holder.imgChecked.visibility = View.VISIBLE
        } else {
            holder.imgChecked.visibility = View.GONE
        }

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
