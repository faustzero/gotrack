package com.nestle.mdgt.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.reguler.LocalAddActivity
import com.nestle.mdgt.database.TReportActivityHeader
import com.nestle.mdgt.models.DetailList
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.rests.LoginApiInterface
import com.nestle.mdgt.utils.Config
import kotlinx.android.synthetic.main.activity_activity_list.view.*
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback
import com.nestle.mdgt.activities.reguler.ActivityListActivity
import com.nestle.mdgt.database.DaoSession
import java.security.cert.TrustAnchor


class LocalAdapter(private val daoSession: DaoSession, private var context: Context, private val models: MutableList<TReportActivityHeader>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<LocalAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TReportActivityHeader)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {

        var localName: TextView = v.findViewById(R.id.row_lblLocalName)
        var description: TextView = v.findViewById(R.id.row_lblDescription)
        var lokasi: TextView = v.findViewById(R.id.row_lblLokasi)
        var store: TextView = v.findViewById(R.id.row_lblStore)

        var periode: TextView = v.findViewById(R.id.row_lblPeriode)
        var type: TextView = v.findViewById(R.id.row_lblType)
        var imgViewRemoveIcon: ImageView = v.findViewById(R.id.imgView)

        fun bind(item: TReportActivityHeader, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val model = models[position]
        holder.localName.text = model.activityName
        holder.description.text = model.headerId
//        holder.store.text = model.storeName
        holder.periode.text = model.periodeStart //model.headerId
        holder.type.text = model.activityTypeName
        holder.lokasi.text = model.locationName
        holder.store.text = model.storeName
        holder.bind(models[position], onItemClickListener)
        holder.imgViewRemoveIcon.setOnClickListener {
        deleteList(holder,position)
            model.isDeleted =true
            daoSession.tReportActivityHeaderDao.update(model)
        }
    }
    fun deleteList (holder: ViewHolder, position: Int) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Konfirmasi")
        builder.setMessage("Apakah yakin ingin dihapus ?")
        builder.setPositiveButton("YES") { _, i ->

        val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
        val call  = apiService.deleteList(models[position].headerId)
        Log.i("log delete"," reportheader")
        call.enqueue(object : retrofit2.Callback<DetailList> {
            override fun onResponse(call: Call<DetailList>, response: Response<DetailList>) {
                try {
                    if (response.isSuccessful) {
                        Log.i("log",models[position].headerId)
                        if (response.body().status == Config.STATUS_SUCCESS) {
                            models.removeAt(position)

                            notifyItemRemoved(position)
                        } else if (response.body().status == Config.STATUS_FAILURE) {
//                            Toast.makeText(this@LocalAdapter, response.body().message, Toast.LENGTH_SHORT).show()
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<DetailList>, t: Throwable) {
                try {

//                    Toast.makeText(this@Content_Dashboard, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

        }
        builder.setNegativeButton("NO") { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }


    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}


