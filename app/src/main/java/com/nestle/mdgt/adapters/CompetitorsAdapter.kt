package com.nestle.mdgt.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.nestle.mdgt.R
import com.nestle.mdgt.database.TReportCompetitor

class CompetitorsAdapter(private val models: List<TReportCompetitor>, private val rowLayout: Int, private val onItemClickListener: OnItemClickListener)
    : androidx.recyclerview.widget.RecyclerView.Adapter<CompetitorsAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TReportCompetitor)
    }

    class ViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var competitorName: TextView = v.findViewById(R.id.row_lblCompetitorName)
        var description: TextView = v.findViewById(R.id.row_lblDescription)

        fun bind(item: TReportCompetitor, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        holder.competitorName.text = model.activityTypeName
        holder.description.text = model.description

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}