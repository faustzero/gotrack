package com.nestle.mdgt.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.nestle.mdgt.R;
import com.nestle.mdgt.database.TReportPenjualan;

import java.util.List;

public class PenjualansAdapter extends RecyclerView.Adapter<PenjualansAdapter.ViewHolder> {
    private final List<TReportPenjualan> models;
    private int rowLayout;

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView productName, description, amount, unitName1, unitName2;
        EditText qty1, qty2;

        ViewHolder(View v) {
            super(v);
            productName = (TextView) v.findViewById(R.id.productlistrow_lblProductName);
            description = (TextView) v.findViewById(R.id.productlistrow_lblDescription);
            amount = (TextView) v.findViewById(R.id.productlistrow_lblAmount);
            unitName1 = (TextView) v.findViewById(R.id.productlistrow_lblUnit1);
            unitName2 = (TextView) v.findViewById(R.id.productlistrow_lblUnit2);
            qty1 = (EditText) v.findViewById(R.id.productlistrow_txtQty1);
            qty2 = (EditText) v.findViewById(R.id.productlistrow_txtQty2);
        }
    }

    public PenjualansAdapter(List<TReportPenjualan> models, int rowLayout) {
        this.models = models;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TReportPenjualan model = models.get(position);
        holder.productName.setText(model.getProductName());
        holder.description.setText(model.getBrandName());
        holder.unitName1.setText(model.getUnitName1());
        holder.unitName2.setText(model.getUnitName2());
        holder.qty1.setText(String.valueOf(model.getQuantity1()));
        holder.qty2.setText(String.valueOf(model.getQuantity2()));

        holder.qty1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String enteredString = charSequence.toString();
                if (!enteredString.equals("0")) {
                    if (enteredString.startsWith("0") && enteredString.length() > 0) {
                        holder.qty1.setText(enteredString.substring(1));
                    }
                }

                if(!charSequence.toString().trim().equals("")) {
                    model.setQuantity1(Integer.parseInt(charSequence.toString().trim()));
                } else {
                    model.setQuantity1(0);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.qty2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String enteredString = charSequence.toString();
                if (!enteredString.equals("0")) {
                    if (enteredString.startsWith("0") && enteredString.length() > 0) {
                        holder.qty2.setText(enteredString.substring(1));
                    }
                }
                if(!charSequence.toString().trim().equals("")) {
                    model.setQuantity2(Integer.parseInt(charSequence.toString().trim()));
                } else {
                    model.setQuantity2(0);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
