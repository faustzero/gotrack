package com.nestle.mdgt.utils

import android.app.Activity
import android.graphics.Color
import com.google.android.material.textfield.TextInputLayout
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView

object FormValidation {
    fun validateRequiredText(activity: Activity, editText: EditText, errorMessageId: Int): Boolean {
        if (editText.text.toString().trim { it <= ' ' }.isEmpty()) {
            editText.error = activity.getString(errorMessageId)
            requestFocus(activity, editText)
            return false
        } else {
            editText.error = null
        }
        return true
    }

    fun validateRequiredText(activity: Activity, editText: EditText, errorMessage: String): Boolean {
        if (editText.text.toString().trim { it <= ' ' }.isEmpty()) {
            editText.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            editText.error = null
        }
        return true
    }

    fun validateRequiredText(activity: Activity, editText: EditText,
                             inputLayout: TextInputLayout, errorMessageId: Int): Boolean {
        if (editText.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputLayout.error = activity.getString(errorMessageId)
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateRequiredText(activity: Activity, editText: EditText,
                             inputLayout: TextInputLayout, errorMessage: String): Boolean {
        if (editText.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputLayout.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateNotZeroText(activity: Activity, editText: EditText,
                            inputLayout: TextInputLayout, errorMessage: String): Boolean {
        if (Integer.parseInt(Helper.removeComma(editText.text.toString().trim { it <= ' ' })) == 0) {
            inputLayout.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateRangePriceRoomText(activity: Activity, editText: EditText,
                                   inputLayout: TextInputLayout, errorMessage: String): Boolean {
        val value = Integer.parseInt(Helper.removeComma(editText.text.toString().trim { it <= ' ' }))
        if (value < 100000 || value > 9999999) {
            inputLayout.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validatePersentaseText(activity: Activity, editText: EditText,
                               inputLayout: TextInputLayout, errorMessage: String): Boolean {
        val value = Integer.parseInt(Helper.removeComma(editText.text.toString().trim { it <= ' ' }))
        if (value > 100) {
            inputLayout.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateRangeText(activity: Activity, editText: EditText,
                          inputLayout: TextInputLayout, errorMessage: String, startLimit: Int, endLimit: Int): Boolean {
        val value = Integer.parseInt(Helper.removeComma(editText.text.toString().trim { it <= ' ' }))
        if (value < startLimit || value > endLimit) {
            inputLayout.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateMustBeLowerOrEqualText(activity: Activity, editText: EditText,
                                       inputLayout: TextInputLayout, limit: Int, errorMessage: String): Boolean {
        if (Integer.parseInt(Helper.removeComma(editText.text.toString().trim { it <= ' ' })) > limit) {
            inputLayout.error = errorMessage
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateRequiredEmailText(activity: Activity, editText: EditText,
                                  inputLayout: TextInputLayout, errorMessageId: Int): Boolean {
        val email = editText.text.toString().trim { it <= ' ' }
        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayout.error = activity.getString(errorMessageId)
            requestFocus(activity, editText)
            return false
        } else {
            inputLayout.error = null
            inputLayout.isErrorEnabled = false
        }
        return true
    }

    fun validateRequiredSpinner(activity: Activity, spinner: Spinner, errorMessageId: Int): Boolean {
        if (spinner.selectedItem.toString().trim { it <= ' ' }.isEmpty()) {
            val errorText = spinner.selectedView as TextView
            errorText.error = ""
            errorText.setTextColor(Color.RED)
            errorText.text = activity.getString(errorMessageId)
            return false
        }
        return true
    }

    fun validateRequiredSpinner(activity: Activity, spinner: Spinner, errorMessage: String): Boolean {
        if (spinner.selectedItem.toString().trim { it <= ' ' }.isEmpty()) {
            val errorText = spinner.selectedView as TextView
            errorText.error = ""
            errorText.setTextColor(Color.RED)
            errorText.text = errorMessage
            return false
        }
        return true
    }

    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun requestFocus(activity: Activity, view: View) {
        if (view.requestFocus()) {
            activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }
}
