package com.nestle.mdgt.utils

import android.app.Activity
import android.content.Intent
import androidx.core.content.FileProvider
import com.nestle.mdgt.BuildConfig
import java.io.File

object EmailUtils {
    fun sendEmail(activity: Activity, mailto: Array<String>, subject: String, message: String, attachment: File?) {
        val emailIntent = Intent(Intent.ACTION_SEND)

        // set the type to 'email'
        emailIntent.type = "vnd.android.cursor.dir/email"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, mailto)

        // the attachment
        if (attachment != null) {
            val path = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", attachment)
            emailIntent.putExtra(Intent.EXTRA_STREAM, path)
        }


        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)

        activity.startActivity(Intent.createChooser(emailIntent, message))
    }
}