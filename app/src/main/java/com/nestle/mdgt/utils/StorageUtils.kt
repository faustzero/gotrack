package com.nestle.mdgt.utils

import android.os.Environment

import java.io.File

object StorageUtils {
    val DIRECTORY_ROOT = 1
    val DIRECTORY_CACHE = 2
    val DIRECTORY_IMAGE = 3
    val DIRECTORY_REPORT_LOG = 4
    val DIRECTORY_DATABASE = 5

    fun getDirectory(directory: Int): String {
        when (directory) {
            DIRECTORY_ROOT -> return Environment.getExternalStorageDirectory().toString() + "/" + Config.DIRECTORY_ROOT_NAME
            DIRECTORY_CACHE -> return Environment.getExternalStorageDirectory().toString() + "/" + Config.DIRECTORY_ROOT_NAME + "/Caches"
            DIRECTORY_IMAGE -> return Environment.getExternalStorageDirectory().toString() + "/" + Config.DIRECTORY_ROOT_NAME + "/Images"
            DIRECTORY_REPORT_LOG -> return Environment.getExternalStorageDirectory().toString() + "/" + Config.DIRECTORY_ROOT_NAME + "/log_report.sys"
            DIRECTORY_DATABASE -> return Environment.getExternalStorageDirectory().toString() + "/" + Config.DIRECTORY_ROOT_NAME + "/Databases"
            else -> return ""
        }
    }

    fun createDirectories() {
        val mainDirectory = File(getDirectory(DIRECTORY_ROOT))
        val cacheDirectory = File(getDirectory(DIRECTORY_CACHE))
        val imageDirectory = File(getDirectory(DIRECTORY_IMAGE))
        val databaseDirectory = File(getDirectory(DIRECTORY_DATABASE))

        if (!mainDirectory.exists()) {
            mainDirectory.mkdir()
        }
        if (!cacheDirectory.exists()) {
            cacheDirectory.mkdir()
        }
        if (!imageDirectory.exists()) {
            imageDirectory.mkdir()
        }
        if (!databaseDirectory.exists()) {
            databaseDirectory.mkdir()
        }
    }
}