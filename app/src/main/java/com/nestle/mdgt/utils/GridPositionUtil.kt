package com.nestle.mdgt.utils

object GridPositionUtil {

    fun getPositionByDirection(direction: String, position: Int, rowSize: Int, colSize: Int, row: Int, column: Int): Int {
        when (direction) {
            "left" -> return getLeftPosition(position, column)
            "top" -> return getTopPosition(position, colSize, row)
            "right" -> return getRightPosition(position, colSize, column)
            "bottom" -> return getBottomPosition(position, rowSize, colSize, row)
        }
        return 0
    }

    fun getLeftPosition(position: Int, column: Int): Int {
        return if (column == 1) {
            0
        } else position - 1
    }

    fun getTopPosition(position: Int, colSize: Int, row: Int): Int {
        return if (row == 1) {
            0
        } else position - colSize
    }

    fun getRightPosition(position: Int, colSize: Int, column: Int): Int {
        return if (column == colSize) {
            0
        } else position + 1
    }

    fun getBottomPosition(position: Int, rowSize: Int, colSize: Int, row: Int): Int {
        return if (row == rowSize) {
            0
        } else position + colSize
    }
}