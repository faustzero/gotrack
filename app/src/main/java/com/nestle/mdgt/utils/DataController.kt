package com.nestle.mdgt.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.RectF
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.StrictMode
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.FileProvider
import com.nestle.mdgt.BuildConfig
import com.nestle.mdgt.reports.DatabaseReport

//import org.joda.time.LocalDateTime

import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.zip.CRC32

object DataController {
    val PROJECT_NAME = "Gotrack"

    enum class FileType {
        mainDir, cacheDir, photoDir, log_report, database, PDFDir, database_report
    }

    const val RADIUS = 50.0
    const val ACCURACY = 50.0
    const val LOGIN_PREFERENCES = "Login"

    fun getDirectory(directory: FileType): String {
        return getDirectory(null, directory)
    }

    fun getDirectory(context: Context?, directory: FileType): String {
        if (context != null) {
            return when (directory) {
                FileType.mainDir -> context.filesDir.toString() + "/" + PROJECT_NAME
                FileType.cacheDir -> context.filesDir.toString() + "/" + PROJECT_NAME + "/Cache"
                FileType.photoDir -> context.filesDir.toString() + "/" + PROJECT_NAME + "/Image"
                FileType.log_report -> context.filesDir.toString() + "/" + PROJECT_NAME + "/log_report.pjr"
                FileType.database -> context.filesDir.toString() + "/" + PROJECT_NAME + "/database.db"
                FileType.PDFDir -> context.filesDir.toString() + "/" + PROJECT_NAME + "/pdf"
                FileType.database_report -> context.filesDir.toString() + "/" + PROJECT_NAME + "/database_report.db"
                else -> ""
            }
        } else {
            return when (directory) {
                FileType.mainDir -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME
                FileType.cacheDir -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME + "/Cache"
                FileType.photoDir -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME + "/Image"
                FileType.log_report -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME + "/log_report.pjr"
                FileType.database -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME + "/database.db"
                FileType.PDFDir -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME + "/pdf"
                FileType.database_report -> Environment.getExternalStorageDirectory().toString() + "/" + PROJECT_NAME + "/database_report.db"
                else -> ""
            }
        }
    }

    fun checkDirectory() {
        checkDirectory(null)
    }

    private fun checkDirectory(context: Context?) {
        val maindir = File(getDirectory(context, FileType.mainDir))
        val cacheDir = File(getDirectory(context, FileType.cacheDir))
        val photoDir = File(getDirectory(context, FileType.photoDir))
        val pdfDir = File(getDirectory(context, FileType.PDFDir))

        if (!maindir.exists())
            maindir.mkdir()
        if (!cacheDir.exists())
            cacheDir.mkdir()
        if (!photoDir.exists())
            photoDir.mkdir()
        if (!pdfDir.exists())
            pdfDir.mkdir()
    }

    fun deleteFilesNotInReport(path: String, context: Context) {
        val dir = File(path)
        if (dir.exists() && dir.isDirectory) {
            val listFiles = dir.listFiles()
            val dbReport = DatabaseReport.getDatabase(context)
            for (aFile in listFiles!!) {
                val namaFile = aFile.name
                if (!dbReport.isExistOnNotSentReport(namaFile)) {
                    aFile.delete()
                }
                Log.i("Foto \${successDelete}", aFile.absolutePath)
            }
        }
    }

    fun saveStringToShared(ctx: Context, prefName: String, name: String, data: String) {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        val ed = sp.edit().putString(name, data)
        ed.commit()
    }

    fun saveLongToShared(ctx: Context, prefName: String, name: String, data: Long) {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        val ed = sp.edit().putLong(name, data)
        ed.commit()
    }

    fun saveBooleanToShared(ctx: Context, prefName: String, name: String, data: Boolean) {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        val ed = sp.edit().putBoolean(name, data)
        ed.commit()
    }

    fun getStringFromShared(ctx: Context, prefName: String, name: String, defReturn: String?): String? {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        return sp.getString(name, defReturn)
    }

    fun getLongFromShared(ctx: Context, prefName: String, name: String): Long {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        return sp.getLong(name, -1)
    }

    fun getBooleanFromShared(ctx: Context, prefName: String, name: String): Boolean {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        return sp.getBoolean(name, false)
    }

    fun clearShared(ctx: Context, prefName: String) {
        val sp = ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE)
        val ed = sp.edit()
        ed.clear()
        ed.commit()
    }


    //		String tes= c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
    val waktuForReport: String
        get() {
            val c = Calendar.getInstance()
            val d = c.time
            val tes = (d.time / 1000).toString()
            return tes
        }

    //		String tes= c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
    val waktu: String
        get() {
            val c = Calendar.getInstance()
            val d = c.time
            val tes = d.time.toString()
            return tes
        }

    //		String tes= c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
    val waktuInLong: Long
        get() {
            val c = Calendar.getInstance()
            val d = c.time

            return d.time
        }


    fun generateArray(size: Int): Array<Int> {
        val arr = Array<Int>(size,{i->0})

        for (i in 0..size - 1) {
            arr[i] = i + 1
        }

        return arr
    }

    fun <T> getStringArray(list: List<T>): Array<String> {
        val arr = Array<String>(list.size,{i->""})

        for (i in list.indices) {
            arr[i] = (i + 1).toString() + ". " + list[i].toString()
        }

        return arr
    }

    fun hashCRC32(text: String): String {
        return hashCRC32(text.toByteArray())

    }


    fun hashCRC32(bytes: ByteArray): String {
        val crc: String

        val crc32 = CRC32()
        crc32.update(bytes)
        val value = crc32.value
        crc = java.lang.Long.toHexString(value)

        return crc

    }

    fun setScreen(activity: Activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //		activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //                              WindowManager.LayoutParams.FLAG_FULLSCREEN);

        val size = activity.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
        if (size == Configuration.SCREENLAYOUT_SIZE_LARGE) {

            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
        } else if (size == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

    }

    fun transformTime(time: Long): String {
        val sdf = SimpleDateFormat("HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        val tz = cal.timeZone
        sdf.timeZone = tz

        val localTime = sdf.format(cal.time)

        return localTime
    }

    fun transformTime(time: Long, format: String): String {
        val sdf = SimpleDateFormat(format)
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        val tz = cal.timeZone
        sdf.timeZone = tz

        val localTime = sdf.format(cal.time)

        return localTime
    }

    fun transformDate(time: Long): String {
        val sdf = SimpleDateFormat("yyMMdd")
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        val tz = cal.timeZone
        sdf.timeZone = tz

        val localTime = sdf.format(cal.time)

        return localTime
    }

    fun transformFullTime(time: Long): String {
        val sdf = SimpleDateFormat("d, MMM yyyy HH:mm:ss")
        val cal = Calendar.getInstance()
        cal.timeInMillis = time
        val tz = cal.timeZone
        sdf.timeZone = tz

        val localTime = sdf.format(cal.time)

        return localTime
    }

//    fun generateVisitID(karyawan_id: String, store_id: String, waktu: String): String {
//        val time = Integer.parseInt(waktu)
//        //		byte[]arr_time = ByteBuffer.allocate(4).putInt(time).array();
//        val calendar = Calendar.getInstance()
//        calendar.timeInMillis = time * 1000L
//        val dateTime = LocalDateTime.fromCalendarFields(calendar)
//        val visit_id = karyawan_id + store_id + dateTime.toString("yyMMddHHmmss")
//
//        return visit_id
//
//    }


    fun readpdf(c: Activity, title: String) {

        val pdfFile = File(getDirectory(FileType.PDFDir) + "/" + title + ".pdf")

        if (pdfFile.exists()) {

            val path = Uri.fromFile(pdfFile)
            val pdfIntent = Intent(Intent.ACTION_VIEW)
            pdfIntent.setDataAndType(path, "application/pdf")
            pdfIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

            try {
                c.startActivity(pdfIntent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(c, "No Application available to view pdf", Toast.LENGTH_LONG).show()
            }

        }
    }


    fun getPxFromSP(ctx: Context, sp: Int): Float {
        val metrics = ctx.resources.displayMetrics

        return sp * metrics.scaledDensity
    }


    fun getVersion(context: Context): String {
        var v = ""
        try {
            v = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return v
    }

     fun findBinary(binaryName: String): Boolean {
        var found = false
        if (!found) {
            val places = arrayOf("/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/")
            for (where in places) {
                if (File(where + binaryName).exists()) {
                    found = true

                    break
                }
            }
        }
        return found
    }

    val isRooted: Boolean
        get() = findBinary("su")

    fun getAndroidID(context: Context): String {
        val android_id = Settings.Secure.getString(context.contentResolver,
                Settings.Secure.ANDROID_ID)

        return android_id
    }

    fun getDeviceData(context: Context): String {
        val osVersion = System.getProperty("os.version") // OS version
        val sdk = android.os.Build.VERSION.SDK     // API Level
        val device = android.os.Build.DEVICE         // Device
        val model = android.os.Build.MODEL          // Model
        val product = android.os.Build.PRODUCT
        val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val imei = ""//telephonyManager.deviceId
        return "$osVersion,$sdk,$device,$model,$product,$imei"
    }

    fun getEmptyView(activity: Activity): View {
        val tv = TextView(activity)
        tv.text = "Tidak Ada Data"
        //        activity.addContentView(tv,new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return tv
    }

    fun setGroupEnabled(grup: RadioGroup, enable: Boolean) {
        for (i in 0..grup.childCount - 1) {
            grup.getChildAt(i).isEnabled = enable
        }
    }


    fun getString(number: Int): String {
        if (number == 0)
            return "00"
        else if (number > 0 && number < 10)
            return "0" + number
        else
            return number.toString()
    }

    fun getPopup(a: Activity, title: String, text: String): AlertDialog.Builder {
        val alert = AlertDialog.Builder(a)
        alert.setTitle(title)
        alert.setMessage(text)

        alert.setNegativeButton("Batal") { dialog, which ->
            // TODO Auto-generated method stub
            dialog.dismiss()
        }
        return alert
    }

    fun finishButton(button: Button, activity: Activity) {
        button.paintFlags = button.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        button.setTextColor(activity.resources.getColor(android.R.color.holo_green_dark))
    }


//    @Throws(Exception::class)
//    fun zipDir(zipFileName: String, dir: String) {
//        val appZip = DataHelper.AppZip(dir, zipFileName)
//        appZip.run()
//    }

    fun sendEmail(activity: Activity, mailto: Array<String>, subject: String, message: String, attachment: File?) {


        val emailIntent = Intent(Intent.ACTION_SEND)
        // set the type to 'email'
        emailIntent.type = "vnd.android.cursor.dir/email"
        val to = mailto
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
        // the attachment
        if (attachment != null) {
            val filelocation = attachment
            val path = Uri.fromFile(filelocation)
            emailIntent.putExtra(Intent.EXTRA_STREAM, path)
        }

        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)

        activity.startActivity(Intent.createChooser(emailIntent, message))
    }


//    fun addDataLoginToShared(context: Context, dataLogin: DataHelper.DataLogin) {
//        saveBooleanToShared(context, LOGIN_PREFERENCES, "login", true)
//
//        val sp = context.getSharedPreferences(LOGIN_PREFERENCES, Context.MODE_PRIVATE)
//
//        var ed = sp.edit().putString("id_karyawan", dataLogin.id_karyawan)
//        ed.commit()
//        ed = sp.edit().putString("username", dataLogin.username)
//        ed.commit()
//        ed = sp.edit().putString("nama_karyawan", dataLogin.nama_karyawan)
//        ed.commit()
//        ed = sp.edit().putString("nama_role", dataLogin.nama_role)
//        ed.commit()
//        ed = sp.edit().putString("nama_tl", dataLogin.nama_tl)
//        ed.commit()
//        ed = sp.edit().putString("hari_ke", dataLogin.hari_ke)
//        ed.commit()
//        ed = sp.edit().putString("tanggal_login", dataLogin.tanggal_login)
//        ed.commit()
//        ed = sp.edit().putString("role_id", dataLogin.role_id)
//        ed.commit()
//
//    }

    fun getDataLoginFromShared(context: Context): DataHelper.DataLogin? {
        var dataLogin: DataHelper.DataLogin? = null
        if (getBooleanFromShared(context, LOGIN_PREFERENCES, "login")) {
            val sp = context.getSharedPreferences(LOGIN_PREFERENCES, Context.MODE_PRIVATE)

            val id_karyawan = sp.getString("id_karyawan", "")
            val username = sp.getString("username", "")
            val nama_karyawan = sp.getString("nama_karyawan", "")
            val nama_role = sp.getString("nama_role", "")
            val nama_tl = sp.getString("nama_tl","")
            val hari_ke = sp.getString("hari_ke","")
            val tanggal = sp.getString("tanggal_login","")
            val role_id = sp.getString("role_id","")

            dataLogin = DataHelper.DataLogin(id_karyawan, username, nama_karyawan, nama_role, nama_tl, hari_ke, tanggal, role_id)
        }

        return dataLogin
    }
//
//    fun checkIsSaturday(tanggal:String) : Boolean
//    {
//        val localDate = LocalDate.parse(tanggal)
//        if(localDate.dayOfWeek == DateTimeConstants.SATURDAY)
//            return true
//        else
//            return false
//    }

    fun updateAPK(activity: Activity, path: String) {
        val toInstall =  File(path)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
                val apkUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", toInstall)
                val intent = Intent(Intent.ACTION_INSTALL_PACKAGE)
                intent.data = apkUri
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                Intent.FLAG_GRANT_READ_URI_PERMISSION
                activity.startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            val apkUri = Uri.fromFile(toInstall)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive")
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
        }
    }


    fun calculateDistance(latitude1: Double, longitude1: Double, latitude2: Double, longitude2: Double): Int {
        val lat1 = latitude1
        val lng1 = longitude1
        val lat2 = latitude2
        val lng2 = longitude2

        val earthRadius = 6371000.0
        val dLat = Math.toRadians(lat2 - lat1)
        val dLng = Math.toRadians(lng2 - lng1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                Math.sin(dLng / 2) * Math.sin(dLng / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        val dist = earthRadius * c

        //	    Log.d("Get distance form "+lng1+","+lat1, "to "+lng2+","+lat2);

        return Math.round(dist).toInt()
    }

    fun getRoundedCornerBitmap(bitmap: Bitmap?, pixels: Int): Bitmap? {
        var output: Bitmap? = null
        if (bitmap != null)
            output = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        else
            return null

        if (output != null) {
            val canvas = Canvas(output)

            val color = 0xff424242.toInt()
            val paint = Paint()
            val rect = Rect(0, 0, bitmap.width, bitmap.height)
            val rectF = RectF(rect)
            val roundPx = pixels.toFloat()

            paint.isAntiAlias = true
            canvas.drawARGB(0, 0, 0, 0)
            paint.color = color
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint)

            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(bitmap, rect, rect, paint)
        } else {
            return bitmap
        }

        return output
    }

    fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                  filter: Boolean): Bitmap {
        val ratio = Math.min(
                maxImageSize.toFloat() / realImage.width,
                maxImageSize.toFloat() / realImage.height)
        val width = Math.round(ratio.toFloat() * realImage.width)
        val height = Math.round(ratio.toFloat() * realImage.height)

        val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter)
        return newBitmap
    }

//    val tanggalNow: String
//        get() = LocalDate.now().toString("yyyy-MM-dd")
//
//    	fun addStoreToShared(context : Context, store:Store)
//    	{
//    //		public String id_store,account_id, nama_account,kode_store,nama_store,alamat_store;
//    //		public double latitude,longitude;
//    //		public int done;
//    		saveBooleanToShared(context,STORE_PREFERENCES,"store",true);
//            val sp = context.getSharedPreferences(STORE_PREFERENCES, Context.MODE_PRIVATE);
//
//    		var ed = sp.edit().putString("id_store", store.id_store); ed.commit()
//    		ed = sp.edit().putString("account_id", store.account_id);	ed.commit()
//    		ed = sp.edit().putString("nama_account", store.nama_account);	ed.commit()
//    		ed = sp.edit().putString("kode_store", store.kode_store);	ed.commit()
//    		ed = sp.edit().putString("nama_store", store.nama_store);	ed.commit()
//    		ed = sp.edit().putString("alamat_store", store.alamat_store);	ed.commit()
//    		ed = sp.edit().putString("latitude", store.latitude.toString());	ed.commit()
//    		ed = sp.edit().putString("longitude", store.longitude.toString());	ed.commit()
//    		ed = sp.edit().putInt("done", store.done);	ed.commit();
//    	}


//    fun getStoreFromShared(context : Context) : Store?
//    {
//        var store:Store? = null
//
//        if(getBooleanFromShared(context, STORE_PREFERENCES,"store"))
//        {
//            val sp = context.getSharedPreferences(STORE_PREFERENCES,Context.MODE_PRIVATE)
//
//            val id_store = sp.getString("id_store","")
//            val account_id = sp.getString("account_id","")
//            val nama_account = sp.getString("nama_account","")
//            val kode_store = sp.getString("kode_store","")
//            val nama_store = sp.getString("nama_store","")
//            val alamat_store = sp.getString("alamat_store","")
//            val latitude = sp.getString("latitude", "-1000.0").toDouble()
//            val longitude = sp.getString("longitude", "-1000.0").toDouble()
//            val done = sp.getInt("done",0)
//            store = Store(id_store,account_id,nama_account,kode_store,nama_store,alamat_store,latitude,longitude,done)
//
//        }
//
//        return store
//
//    }
//
//    fun addSubbrandToShared(context : Context, subbrand : ProductSubbrand)
//    {
//        saveBooleanToShared(context, SUBBRAND_PREFERENCES,"subbrand",true)
//        val sp = context.getSharedPreferences(SUBBRAND_PREFERENCES,Context.MODE_PRIVATE)
//        var ed = sp.edit().putString("id_subbrand",subbrand.id_subbrand); ed.commit()
//         ed = sp.edit().putString("kode_subbrand",subbrand.kode_subbrand); ed.commit()
//        ed = sp.edit().putString("nama_subbrand",subbrand.nama_subbrand); ed.commit()
//        ed = sp.edit().putBoolean("done",subbrand.done); ed.commit()
//    }
//
//    fun getSubbrandFromShared(context: Context) : ProductSubbrand?
//    {
//        var subbrand: ProductSubbrand? = null
//
//        if(getBooleanFromShared(context, SUBBRAND_PREFERENCES,"subbrand"))
//        {
//            val sp = context.getSharedPreferences(SUBBRAND_PREFERENCES,Context.MODE_PRIVATE)
//            val id_subbrand =sp.getString("id_subbrand", "")
//            val kode_subbrand =sp.getString("kode_subbrand", "")
//            val nama_subbrand =sp.getString("nama_subbrand", "")
//            val done=sp.getBoolean("done", false)
//
//            subbrand = ProductSubbrand(id_subbrand,kode_subbrand,nama_subbrand,done)
//
//        }
//
//        return subbrand
//    }


    //
    //	public static void addCategorytoShared(Context context, ProductCategory category)
    //	{
    //		saveBooleanToShared(context,CATEGORY_PREFERENCES,"category",true);
    //		SharedPreferences sp = context.getSharedPreferences(CATEGORY_PREFERENCES, Context.MODE_PRIVATE);
    //
    //		Editor ed = sp.edit().putString("id_category", category.id_category); ed.commit();
    //		ed = sp.edit().putString("nama_category", category.nama_category);	ed.commit();
    //		ed = sp.edit().putBoolean("done", category.done);	ed.commit();
    //	}
    //
    //	public static ProductCategory getCategoryFromShared(Context context)
    //	{
    //		ProductCategory category= null;
    //		if(getBooleanFromShared(context,CATEGORY_PREFERENCES,"category"))
    //		{
    //			SharedPreferences sp = context.getSharedPreferences(CATEGORY_PREFERENCES, Context.MODE_PRIVATE);
    //
    //			String id_category,nama_category;
    //			boolean done;
    //			id_category =sp.getString("id_category", "");
    //			nama_category =sp.getString("nama_category", "");
    //			done=sp.getBoolean("done", false);
    //			category = new ProductCategory(id_category,nama_category,done);
    //		}
    //
    //		return category;
    //	}


//    fun openListSubbrand(applicationContext: Context, store : Store): List<ProductSubbrand>
//    {
//        val db = Database.getDatabase(applicationContext)
//        val list_subbrand = db.getSubbrandOnStore(store)
//
//        return list_subbrand
//    }

    //	public static List<ProductCategory> openListCategory(Context context, Store store)
    //	{
    //        List<ProductCategory> list_category = new ArrayList<>();
    //		String path_category = DataController.getDirectory(null,DataController.cacheDire)+"/listcategory";
    //		File f = new File(path_category);
    //		if(f.exists())
    //		{
    //			try {
    //				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
    //				list_category = (List<ProductCategory>) ois.readObject();
    //			} catch (IOException e) {
    //				e.printStackTrace();
    //			} catch (ClassNotFoundException e) {
    //				e.printStackTrace();
    //			}
    //
    //			if(list_category == null )
    //			{
    //				Database db = Database.getDatabase(context);
    //				list_category = db.getCategoryOnStore(store);
    ////				db.closeDB();
    //			}
    //			else if( list_category.size()==0)
    //			{
    //				Database db = Database.getDatabase(context);
    //				list_category = db.getCategoryOnStore(store);
    ////				db.closeDB();
    //			}
    //
    ////            return listca;
    //
    //		}
    //		else
    //		{
    //			Database db = Database.getDatabase(context);
    //			list_category = db.getCategoryOnStore(store);
    ////			db.closeDB();
    //
    ////            return list;
    //		}
    //
    //		return list_category;
    //	}
    //
    //	public static void saveListCategory(List<ProductCategory> list_category)
    //	{
    //		String path_category = DataController.getDirectory(null,DataController.cacheDire)+"/listcategory";
    //		File f = new File(path_category);
    //
    //		try {
    //			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
    //			oos.writeObject(list_category);
    //		} catch (IOException e) {
    //			e.printStackTrace();
    //		}
    //	}
    //
    //	public static boolean clearCategory()
    //	{
    //		String path_category = DataController.getDirectory(null,DataController.cacheDire)+"/listcategory";
    //		File f = new File(path_category);
    //		return f.delete();
    //	}
    //
    //
    //	public static List<Product> openListSKU(Context context, Store store, ProductCategory category)
    //	{
    //		List<Product> listProduct = new ArrayList<>();
    //		String path_category = DataController.getDirectory(null,DataController.cacheDire)+"/listsku";
    //		File f = new File(path_category);
    //		if(f.exists())
    //		{
    //			try {
    //				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
    //				listProduct = (List<Product>) ois.readObject();
    //			} catch (IOException e) {
    //				e.printStackTrace();
    //			} catch (ClassNotFoundException e) {
    //				e.printStackTrace();
    //			}
    //
    //			if(listProduct == null )
    //			{
    //				Database db = Database.getDatabase(context);
    //				listProduct = db.getProductByCategoryOnStore(category,store);
    ////				db.closeDB();
    //			}
    //			else if( listProduct.size()==0)
    //			{
    //				Database db = Database.getDatabase(context);
    //				listProduct = db.getProductByCategoryOnStore(category,store);
    ////				db.closeDB();
    //			}
    //
    //		}
    //		else
    //		{
    //			Database db = Database.getDatabase(context);
    //			listProduct = db.getProductByCategoryOnStore(category,store);
    ////			db.closeDB();
    //
    ////            return list;
    //		}
    //
    //		return listProduct;
    //	}

    //	public static void saveListSKU(List<Product> listProduct)
    //	{
    //		String path_category = DataController.getDirectory(null,DataController.cacheDire)+"/listsku";
    //		File f = new File(path_category);
    //
    //		try {
    //			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
    //			oos.writeObject(listProduct);
    //		} catch (IOException e) {
    //			e.printStackTrace();
    //		}
    //	}

    //	public static boolean clearListSKU()
    //	{
    //		String path_category = DataController.getDirectory(null,DataController.cacheDire)+"/listsku";
    //		File f = new File(path_category);
    //		return f.delete();
    //	}


//    fun openListPromoCategory(context: Context, store: Store, category: ProductCategory):List<Promo>
//    {
//        var listPromo : List<Promo> = ArrayList()
//
//        val path_category = getDirectory(null,FileType.cacheDir)+"/listpromo"
//        val f:File = File(path_category)
//        if(f.exists())
//        {
//            try{
//                val ois:ObjectInputStream = ObjectInputStream(FileInputStream(f))
//                @Suppress("UNCHECKED_CAST")
//                listPromo = ois.readObject() as ArrayList<Promo>
//            }
//            catch (e: IOException)
//            {
//                e.printStackTrace()
//            }
//            catch (e:ClassNotFoundException)
//            {
//                e.printStackTrace()
//            }
//
//            if( listPromo.size==0)
//            {
//                val db:Database = Database.getDatabase(context);
//                listPromo = db.getPromoByCategoryOnStore(category,store);
//            }
//
//        }
//        else
//        {
//            val db:Database = Database.getDatabase(context);
//            listPromo = db.getPromoByCategoryOnStore(category,store);
//        }
//
//
//        return listPromo
//    }

//    	public static List<Promo> openListPromoCategory(Context context, Store store, ProductCategory category)
//    	{
//
//    		List<Promo> listPromo = new ArrayList<>();
//    		String path_category = DataController.getDirectory(null,DataController.FileType.cacheDir)+"/listpromo";
//    		File f = new File(path_category);
//    		if(f.exists())
//    		{
//    			try {
//    				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
//    				listPromo = (List<Promo>) ois.readObject();
//    			} catch (IOException e) {
//    				e.printStackTrace();
//    			} catch (ClassNotFoundException e) {
//    				e.printStackTrace();
//    			}
//
//    			if(listPromo == null )
//    			{
//    				Database db = Database.getDatabase(context);
//    				listPromo = db.getPromoByCategoryOnStore(category,store);
//    //				db.closeDB();
//    			}
//    			else if( listPromo.size()==0)
//    			{
//    				Database db = Database.getDatabase(context);
//    				listPromo = db.getPromoByCategoryOnStore(category,store);
//    //				db.closeDB();
//    			}
//
//    		}
//    		else
//    		{
//    			Database db = Database.getDatabase(context);
//    			listPromo = db.getPromoByCategoryOnStore(category,store);
//    //			db.closeDB();
//
//    //            return list;
//    		}
//
//    		return listPromo;
//
//
//    	}
    //
    //	public static void saveListPromo(List<Promo> listPromo)
    //	{
    //		String path_category = DataController.getDirectory(null,DataController.FileType.cacheDir)+"/listpromo";
    //		File f = new File(path_category);
    //
    //		try {
    //			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
    //			oos.writeObject(listPromo);
    //		} catch (IOException e) {
    //			e.printStackTrace();
    //		}
    //	}
    //

//    fun saveListPromo(listPromo : List<Promo>)
//    {
//        val path_category = getDirectory(null,FileType.cacheDir)+"/listpromo"
//        val f:File = File(path_category)
//
//        try{
//            val oos:ObjectOutputStream = ObjectOutputStream(FileOutputStream(f))
//            oos.writeObject(listPromo)
//        }
//        catch (e : IOException)
//        {
//            e.printStackTrace()
//        }
//
//    }

    fun clearListPromo(): Boolean
    {
        val path_category = getDirectory(null, FileType.cacheDir) +"/listpromo"
        val f:File = File(path_category)
        return f.delete()
    }

    //	public static boolean clearListPromo()
    //	{
    //		String path_category = DataController.getDirectory(null,DataController.FileType.cacheDir)+"/listpromo";
    //		File f = new File(path_category);
    //		return f.delete();
    //	}
    //
    //	public static String getTitle(Context context, String nama_client)
    //	{
    //		String title = "GELDIS untuk "+nama_client +" "+ getVersion(context);
    //		return title;
    //	}




}
