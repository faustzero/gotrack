package com.nestle.mdgt.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.provider.Settings
import android.widget.Spinner
import com.nestle.mdgt.database.TTransmissionHistory
import org.joda.time.DateTime
import java.io.File
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object Helper {
    val isRooted: Boolean
        get() = findBinary("su")

    val deviceData: String
        get() {
            val osVersion = System.getProperty("os.version")
            val sdk = Build.VERSION.SDK_INT
            val device = Build.DEVICE
            val model = Build.MODEL
            val product = Build.PRODUCT
            return "$osVersion,$sdk,$device,$model,$product"
        }

    fun setSpinnerValue(context: Context, arrayId: Int, spinner: Spinner, value: String) {
        val arr = context.resources.getStringArray(arrayId)
        val index = Arrays.asList(*arr).indexOf(value)
        spinner.setSelection(index)
    }

    fun convertZeroToEmpty(intValue: Int, isCompleted: Boolean): String {
        var value = NumberFormat.getNumberInstance(Locale.US).format(intValue.toLong())
        if (!isCompleted) {
            value = if (intValue == 0) "" else value
        }
        return value
    }

    fun convertZeroToEmpty(intValue: Int): String {
        return if (intValue == 0) "" else intValue.toString()
    }

    fun convertYesNoToNumber(value: String): String {
        return when {
            value.toLowerCase() == "yes" -> "1"
            value.toLowerCase() == "no" -> "0"
            else -> value
        }
    }

    fun generateVisitIdMarket(surveyorId: Int, marketId: Int): String {
        return "VM.$surveyorId.$marketId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateVisitIdStore(surveyorId: Int, storeId: String): String {
        return "VS.$surveyorId.$storeId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }
    fun generateStoreId(marketId: Int, storeTypeId: Int, kecamatanId: Int): String {
        return "S.$marketId.$storeTypeId.$kecamatanId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }
    fun generateStoreCode(marketId: Int, storeTypeId: Int, kecamatanId: Int): String {
        return "SC.$marketId.$storeTypeId.$kecamatanId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateStoreCodeNoo(surveyor: Int, storeTypeId: Int, distributorId: Int): String {
        return "NOO.$surveyor.$storeTypeId.$distributorId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateResellerId(storeId: Int, resellerTypeId: Int, kecamatanId: Int): String {
        return "R" + String.format("%06d", storeId) + String.format("%02d", resellerTypeId) + String.format("%04d", kecamatanId) + DateTime.now().toString("yyMMddHHmmssSS")
    }

    fun generatePlanogramId(planogramTypeId: Int, regionId: Int): String {
        return "PL.$planogramTypeId.$regionId"
    }

    fun generateStickerName(): String {
        return "STICKER-" + DateTime.now().toString("yyMMddHHmmssSS")
    }

    fun generatePosmId(posmTypeId: Int, planogramRegionId: String): String {
        return "PO.$posmTypeId.$planogramRegionId"
    }

    fun getDistance(location: Location, longitude: Double, latitude: Double): Double {
        return getDistance(location.longitude, location.latitude, longitude, latitude)
    }

    fun getDistance(longitude1: Double, latitude1: Double, longitude2: Double, latitude2: Double): Double {
        val earthRadius = 6371000.0
        val dLat = Math.toRadians(latitude2 - latitude1)
        val dLng = Math.toRadians(longitude2 - longitude1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(latitude1)) * Math.cos(Math.toRadians(latitude2)) *
                Math.sin(dLng / 2) * Math.sin(dLng / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return earthRadius * c
    }

    fun <T> getStringArray(list: List<T>): Array<String?> {
        val arr = arrayOfNulls<String>(list.size)

        for (i in list.indices) {
            arr[i] = (i + 1).toString() + ". " + (list[i] as TTransmissionHistory).getDescription()
        }

        return arr
    }

    fun getVersion(context: Context): String {
        var v = ""
        try {
            v = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return v
    }

    fun getAndroidId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    private fun findBinary(binaryName: String): Boolean {
        var found = false
        if (!found) {
            val places = arrayOf("/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/")
            for (where in places) {
                if (File(where + binaryName).exists()) {
                    found = true

                    break
                }
            }
        }
        return found
    }

    fun checkPermissionExternalStorage(activity: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    fun removeComma(string: String): String {
        return if (string.contains(",")) {
            string.replace(",", "")
        } else {
            string
        }
    }

    fun formatYesNoToCode(value: String): Int {
        return if (value == Config.YES) {
            1
        } else if (value == Config.NO) {
            0
        } else {
            -1
        }
    }

    fun convertMeterToKm(distance: Float): String {
        val df1 = DecimalFormat("#")
        val df2 = DecimalFormat("#.##")
        return if (distance > 1000) {
            df2.format((distance / 1000).toDouble()) + " km"
        } else {
            df1.format(distance.toDouble()) + " meter"
        }
    }
}
