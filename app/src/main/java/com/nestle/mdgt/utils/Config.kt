package com.nestle.mdgt.utils

object Config {
    var DIRECTORY_ROOT_NAME = "GoTrack"

    var DATABASE_NAME = "GoTrack181034-db"
    var DATABASE_NAME_ENCRYPTED = "GoTrack181034-db-encrypted"

    // Shared Preference
    const val PREF_NAME = "nestlesmdgt_pref"
    const val KEY_IS_LOGGED_IN = "KEY_IS_LOGGED_IN"
    const val KEY_RADIUS_LIMIT = "KEY_RADIUS_LIMIT"
    const val KEY_EMPLOYEE_ID = "KEY_EMPLOYEE_ID"
    const val KEY_EMPLOYEE_USERNAME = "KEY_EMPLOYEE_USERNAME"
    const val KEY_EMPLOYEE_NAME = "KEY_EMPLOYEE_NAME"
    const val KEY_EMPLOYEE_ROLE_ID = "KEY_EMPLOYEE_ROLE_ID"
    const val KEY_EMPLOYEE_ROLE_NAME = "KEY_EMPLOYEE_ROLE_NAME"
    const val KEY_EMPLOYEE_RADIUS_VERIFICATION_ID = "KEY_EMPLOYEE_RADIUS_VERIFICATION_ID"
    const val KEY_EMPLOYEE_REGION_ID = "KEY_EMPLOYEE_REGION_ID"
    const val KEY_EMPLOYEE_REGION_NAME = "KEY_EMPLOYEE_REGION_NAME"
    const val KEY_VISIT_ID_MARKET = "KEY_VISIT_ID_MARKET"
    const val KEY_VISIT_ID_STORE = "KEY_VISIT_ID_STORE"
    const val KEY_LOCAL_ID = "KEY_LOCAL_ID"
    const val KEY_PLANOGRAM_REGION_ID = "KEY_PLANOGRAM_REGION_ID"
    const val KEY_EMPLOYEE_LOGIN_TIME = "KEY_EMPLOYEE_LOGIN_TIME"
    const val KEY_EMPLOYEE_FIRST_TIME_IN = "KEY_EMPLOYEE_FIRST_TIME_IN"
    const val KEY_IN_REPORT_RESELLER = "KEY_IN_REPORT_RESELLER"
    const val KEY_RESELLER_ID = "KEY_RESELLER_ID"
    const val KEY_RESELLER_NAME = "KEY_RESELLER_NAME"
    const val KEY_EMPLOYEE_ROW = "KEY_EMPLOYEE_ROW"
    const val KEY_EMPLOYEE_ROW2 = "KEY_EMPLOYEE_ROW2"
    const val KEY_EMPLOYEE_COLUMN = "KEY_EMPLOYEE_COLUMN"
    const val KEY_EMPLOYEE_COLUMN2 = "KEY_EMPLOYEE_COLUMN2"
    const val KEY_SURVEY_ID="KEY_SURVEY_ID"
    const val KEY_DISTRIBUTOR_ID="KEY_DISTRIBUTOR_ID"
    const val KEY_DISTRIBUTOR_NAME="KEY_DISTRIBUTOR_NAME"
    const val KEY_EMPLOYEE_AREA_ID="KEY_EMPLOYEE_AREA_ID"



    // Market & Store
    const val KEY_STORE_ID = "KEY_STORE_ID"
    const val KEY_STORE_CODE = "KEY_STORE_CODE"
    const val KEY_STORE_NAME = "KEY_STORE_NAME"
    const val KEY_MARKET_ID = "KEY_MARKET_ID"
    const val KEY_MARKET_NAME = "KEY_MARKET_NAME"

    // Visit
    const val VISIT_CODE = 1
    const val NO_VISIT_CODE = 0
    const val VISIT_STORE_LIMIT = 25

    // Status api response
    const val STATUS_SUCCESS = "success"
    const val STATUS_FAILURE = "failure"
    const val STATUS_ERROR = "error"

    // Network
    const val INTERVAL = (1000 * 10).toLong()
    const val FASTEST_INTERVAL = (1000 * 5).toLong()

    // Location
    const val ACCURACY_LIMIT = 50f
    const val ACCURACY_SMALL_LIMIT = 30f
    const val REQUEST_LOCATION_LIMIT = 10
    const val RADIUS_VERIFICATION_LOCATION = 300

    // Report
    const val RESEND_REPORT_IN_SECONDS = 45
    const val KEY_CHECKED_REPORT_MSL = "report_msl"
    const val KEY_CHECKED_REPORT_TOPUP = "report_topup"
    const val KEY_CHECKED_REPORT_POSM = "report_posm"
    const val KEY_CHECKED_REPORT_PLANOGRAM = "report_planogram"
    const val KEY_CHECKED_REPORT_CAMPAIGN = "report_campaign"

    const val KEY_CHECKED_REPORT_ACTIVITY = "report_activity"
    const val KEY_CHECKED_REPORT_COMPETITOR = "report_competitor"
    const val KEY_IN_REPORT = "in_report"
    const val KEY_CHECKED_REPORT_RESELLER = "report_reseller"
    const val KEY_CHECKED_REPORT_RESELLER_PENJUALAN = "report_reseller_penjualan"
    const val KEY_CHECKED_REPORT_RESELLER_PULLPROGGRAM = "report_reseller_pull_program"
    const val KEY_CHECKED_REPORT_LOCAL_ACTIVITY = "report_local_activity"
    const val KEY_CHECKED_REPORT_WEEKLY_REPORT = "report_weekly_report"

    const val DETAIL_ID = "detail_id"
    const val COMPLIANCE_ID = "compliance_id"
    const val POSM_ID = "posm_id"





    const val ITEM_AVAILABLE = 1
    const val ITEM_NOT_AVAILABLE = 0

    // Crash Report
    const val CRASH_REPORT_URI = "http://103.7.226.22/crash_report/index.php/apis/crash/create"

    // Date Format
    const val DATE_FORMAT_DATABASE = "y-M-d H:m:s"
    val DATE_FORMAT_DATABASE_WEATHER = "yyyy-MM-dd HH:mm:ss"
    const val DATE_FORMAT_yMdHms = "yMdHms"
    const val DATE_FORMAT_yMdHmsSSS = "yMdHmsSSS"
    const val DATE_FORMAT_PATH = "yMMdd"


    // Storage
    var REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 10

    // Camera
    const val REQUEST_CODE_CAMERA = 20

    // Yes/No
    const val YES_CODE = 1
    const val NO_CODE = 0
    const val YES = "Yes"
    const val NO = "No"
}
