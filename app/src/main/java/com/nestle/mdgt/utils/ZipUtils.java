package com.nestle.mdgt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
    public static class AppZip {
        List<String> fileList;
        private String OUTPUT_ZIP_FILE;
        private String SOURCE_FOLDER;

        public AppZip(String source, String output) {
            fileList = new ArrayList<>();
            OUTPUT_ZIP_FILE = output;
            SOURCE_FOLDER = source;
        }

        public void run() {
            generateFileList(new File(SOURCE_FOLDER));
            zipIt(OUTPUT_ZIP_FILE);
        }

        void zipIt(String zipFile) {

            byte[] buffer = new byte[1024];

            try {

                FileOutputStream fos = new FileOutputStream(zipFile);
                ZipOutputStream zos = new ZipOutputStream(fos);

                for (String file : this.fileList) {
                    ZipEntry ze = new ZipEntry(file);
                    zos.putNextEntry(ze);
                    FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);

                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                    in.close();
                }
                zos.closeEntry();
                zos.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        void generateFileList(File node) {
            if (node.isFile() && !node.getAbsolutePath().endsWith(".apk")) {
                fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
            }

            if (node.isDirectory()) {
                String[] subNote = node.list();
                for (String filename : subNote) {
                    generateFileList(new File(node, filename));
                }
            }
        }

        private String generateZipEntry(String file) {
            return file.substring(SOURCE_FOLDER.length() + 1, file.length());
        }
    }
}
