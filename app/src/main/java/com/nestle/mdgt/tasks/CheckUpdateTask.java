package com.nestle.mdgt.tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.nestle.mdgt.activities.LoginActivity;
import com.nestle.mdgt.rests.ApiClient;
import com.nestle.mdgt.utils.DataController;
import com.nestle.mdgt.utils.Helper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ikhwan on 18-Sep-16.
 */
public class CheckUpdateTask extends AsyncTask<Void,String,Integer> {

    String returnString;
    LoginActivity activity;
    String path;

    public CheckUpdateTask(LoginActivity act)
    {
        activity = act;
    }

    public static final int  MOST_UPDATED= -1,SUCCESS= 0, ERROR_API=1,ERROR_DOWNLOAD = 2;

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        activity.setLoading(true,"Loading",values[0]);
    }

    @Override
    protected Integer doInBackground(Void... voids) {

//        activity.setLoading(true,"Loading","Memeriksa Versi Aplikasi");
        publishProgress("Memeriksa Versi Aplikasi");
        try {
            DefaultHttpClient client = new DefaultHttpClient();

            HttpPost login = new HttpPost("https://gotrack-api.pitjarus.co/index.php/GotrackLogin/checkVersionFf");
            //            HttpPost login = new HttpPost("http://dev.pitjarus.co/api/sariroti/dms/index.php/Agent/Login/checkVersionAgent");

            MultipartEntity me =  new MultipartEntity();


//            me.addPart("nik", new StringBody(karayawan_id));
//            me.addPart("absensi", new StringBody(ABSENSI));
//          me.addPart("TIME", new StringBody(timeForReport));

            login.setEntity(me);
            client.getConnectionManager().getSchemeRegistry().register(
                    new Scheme("https", SSLSocketFactory.getSocketFactory(), 443)
            );

            HttpResponse response = client.execute(login);
            HttpEntity responseEntity = response.getEntity();

            returnString = EntityUtils.toString(responseEntity);

            Log.i("debug", returnString);

            JSONObject jsonreturn = new JSONObject(returnString);

            String status = jsonreturn.getString("status");

            if(status.equals("success"))
            {

                JSONObject data_object = jsonreturn.getJSONObject("data");
                Log.i("data",data_object.toString());

                String version = data_object.getString("latest_version");
                publishProgress("Proses Versi Aplikasi");
                if(checkVersion(version) != 0)
                {
                    String url = data_object.getString("url_link");
                    String nama_file = "time.apk";

                    if(url ==null)
                        return ERROR_DOWNLOAD;


                    publishProgress("Download Aplikasi");
                    path = downloadFile(url,nama_file);
                    if(path !=null)

                        return SUCCESS;

                    else
                        return ERROR_DOWNLOAD;
                }
                else
                {
                    return MOST_UPDATED;
                }

            }
            else
            {
                return ERROR_API;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return ERROR_API;
    }

    int checkVersion(String version) {
        String cur_ver = Helper.INSTANCE.getVersion(activity);
        Log.d("debug", cur_ver + " , " + version);
        Log.d("debug", version.compareTo(cur_ver) + "");
        return version.compareTo(cur_ver);
    }

    String downloadFile(String surl, String nama_file) {
        String path_download = DataController.INSTANCE.getDirectory(DataController.FileType.mainDir) + "/" + nama_file;

        // String path_download = DataController.getDirectory(DataController.FileType.mainDir)+"/"+nama_file;
        File perfectstore2file = new File(path_download);
        if (perfectstore2file.exists())
            perfectstore2file.delete();
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(surl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
            }

            int fileLength = connection.getContentLength();

            input = connection.getInputStream();
            output = new FileOutputStream(path_download);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress("Downloading application " + (int) (total * 100 / fileLength) + " % complete");
                output.write(data, 0, count);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
                ignored.printStackTrace();
                return null;
            }

            if (connection != null)
                connection.disconnect();
        }
        return path_download;
    }


    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        activity.setLoading(false,"","");
        if(integer == SUCCESS)
        {
            DataController.INSTANCE.updateAPK(activity,path);
        }
        else if(integer == MOST_UPDATED)
        {
            Toast.makeText(activity,"Aplikasi sekarang paling baru", Toast.LENGTH_LONG).show();
        }
        else if(integer == ERROR_API)
        {
            Toast.makeText(activity,"Autoupdate error", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(activity,"Error Update", Toast.LENGTH_LONG).show();
        }
    }
}
