package com.nestle.mdgt.tasks

import android.os.AsyncTask
import com.nestle.mdgt.utils.ZipUtils

class ZippingTask(internal var activity: Loadable, sourcePath: String, outputPath: String) : AsyncTask<Void, String, Boolean>() {
    private var appZip: ZipUtils.AppZip = ZipUtils.AppZip(sourcePath, outputPath)

    override fun onPreExecute() {
        super.onPreExecute()
        activity.setLoading(true, "Loading", "Zipping folder")
    }

    override fun onProgressUpdate(vararg values: String) {
        super.onProgressUpdate(*values)
        activity.setLoading(true, "Loading", values[0])
    }

    override fun doInBackground(vararg params: Void): Boolean? {
        try {
            appZip.run()

        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

        return true
    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)
        activity.setLoading(false, "", "")

        if (result!!) {
            activity.setFinish(result, "")
        } else {
            activity.setFinish(result, "Zip failed")
        }
    }
}
