package com.nestle.mdgt.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.core.app.NotificationCompat
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.nestle.mdgt.R
import com.nestle.mdgt.models.Login

class NotifyWorker(_context: Context, params: WorkerParameters) : Worker(_context, params) {
    private val title = "SMD App."
    private val text = "Anda belum logout aplikasi SMD hari ini, silahkan logout."
    private val context = _context

    override fun doWork(): Result {
        if(Login.isLoggedIn(context)) {
            notify(context)
        }
        return Result.success()
    }

    private fun notify(context: Context) {
        try {
//        val intent = Intent(context, LoginActivity::class.java)
//        intent.putExtra("FromNotification", true)
//        intent.putExtra("Message", text)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val channelId = "ChannelId101"
                val channelName = "ChannelName101"
                val importance = NotificationManager.IMPORTANCE_HIGH
                val nChannel = NotificationChannel(channelId, channelName, importance)
                nChannel.enableLights(true)
                nChannel.lightColor = Color.BLUE
                nChannel.enableVibration(true)
                nChannel.vibrationPattern = longArrayOf(250, 250, 250, 250)
                nManager.createNotificationChannel(nChannel)

                val mBuilder = NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                    .setContentIntent(pendingIntent)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                        .setAutoCancel(true)
                nManager.notify(1, mBuilder.build())
            } else {
                val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val mBuilder = NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setSound(defaultSoundUri)
//                    .setContentIntent(pendingIntent)
                        .setVibrate(longArrayOf(250, 250, 250, 250))
                        .setLights(Color.BLUE, 1000, 1000)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                        .setAutoCancel(true)
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(1, mBuilder.build())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showToast(context: Context) {
        Handler(Looper.getMainLooper()).post {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }
    }
}