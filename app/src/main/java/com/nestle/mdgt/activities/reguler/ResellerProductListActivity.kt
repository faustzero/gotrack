package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast

import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.adapters.PenjualansAdapter
import com.nestle.mdgt.adapters.SummaryPenjualansAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TProduct
import com.nestle.mdgt.database.TProductDao
import com.nestle.mdgt.database.TReportPenjualan
import com.nestle.mdgt.database.TReportPenjualanDao
import com.nestle.mdgt.database.TReseller
import com.nestle.mdgt.database.TResellerDao
import com.nestle.mdgt.database.TUnit
import com.nestle.mdgt.database.TUnitDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import org.joda.time.LocalDate
import java.util.*

class ResellerProductListActivity : AppCompatActivity(),Loadable {
    private var lblNoData: TextView? = null
    private var txtSearch: EditText? = null
    private var recycler: androidx.recyclerview.widget.RecyclerView? = null
    private var mPopupWindow: PopupWindow? = null

    private var mVisitId: String? = null
    private var mResellerId: String? = null
    private var mResellerName: String? = null

    private var daoSession: DaoSession? = null
    private var reportPenjualanDao: TReportPenjualanDao? = null
    private var unitDao: TUnitDao? = null
    private var reportPenjualans: List<TReportPenjualan>? = null
    private var progressDialog: ProgressDialog? = null
    private var listreportPenjualan: MutableList<TReportPenjualan>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reseller_product_list)

        val subTitle = Login.getUsername(this) + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "") + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_NAME, "")
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_penjualan)
        supportActionBar!!.subtitle = subTitle

        initData()
        initView()
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mResellerId = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_ID, "")
        mResellerName = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_NAME, "")
        progressDialog = ProgressDialog(this)
        daoSession = (application as GlobalApp).daoSession!!
        val productByRoleDao = daoSession!!.tProductDao
        reportPenjualanDao = daoSession!!.tReportPenjualanDao
        unitDao = daoSession!!.tUnitDao

        reportPenjualans = reportPenjualanDao!!.queryBuilder()
                .where(TReportPenjualanDao.Properties.VisitId.eq(mVisitId),
                        TReportPenjualanDao.Properties.ResellerId.eq(mResellerId))
                .orderAsc(TReportPenjualanDao.Properties.ProductName)
                .list()

        Log.i("report penjualan size", reportPenjualans!!.size.toString())

        if (reportPenjualans!!.size == 0) {
            val productByRoles = productByRoleDao.queryBuilder()
                    .where(TProductDao.Properties.IsCompetitor.eq(Config.NO_CODE))
                    .list()
            val productByRolesSize = productByRoles.size
            if (productByRolesSize > 0) {
                var i = 0
                val products = arrayOfNulls<TReportPenjualan>(productByRolesSize)
                var product: TReportPenjualan
                for (model in productByRoles) {
                    product = TReportPenjualan()
                    product.visitId = mVisitId
                    product.resellerId = mResellerId
                    product.resellerName = mResellerName
                    product.brandId = model.brandId
                    product.brandName = model.brandName
                    product.productId = model.productId
                    product.productName = model.productName
                    product.unitId1 = model.unitId1
                    product.unitName1 = model.unitName1
                    product.quantity1 = 0
                    product.price1 = model.price1
                    product.unitId2 = model.unitId2
                    product.unitName2 = model.unitName2
                    product.quantity2 = 0
                    product.price2 = model.price2

                    //Unit
//                    val units = unitDao!!.queryBuilder()
//                            .where(TUnitDao.Properties.ProductId.eq(model.productId))
//                            .orderAsc(TUnitDao.Properties.UnitName).list()
//                    if (units.size > 0) {
//                        var y = 1
//                        for (u in units) {
//                            if (y == 1) {
//                                product.unitId1 = u.unitId
//                                product.unitName1 = u.unitName
//                                product.quantity1 = 0
//                                product.price1 = u.price
//                            }
//                            if (y == 2) {
//                                product.unitId2 = u.unitId
//                                product.unitName2 = u.unitName
//                                product.quantity2 = 0
//                                product.price2 = u.price
//                            }
//                            y++
//                        }
//                    }

                    products[i] = product
                    i++
                }
                reportPenjualanDao!!.insertInTx(*products)
                reportPenjualans = reportPenjualanDao!!.queryBuilder()
                        .where(TReportPenjualanDao.Properties.VisitId.eq(mVisitId),
                                TReportPenjualanDao.Properties.ResellerId.eq(mResellerId))
                        .orderAsc(TReportPenjualanDao.Properties.ProductName)
                        .list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById<View>(R.id.productList_lblNoData) as TextView
        val btnSimpan = findViewById<View>(R.id.productList_btnSelesai) as Button
        recycler = findViewById<View>(R.id.productList_recycler) as androidx.recyclerview.widget.RecyclerView
        txtSearch = findViewById<View>(R.id.productList_txtSearch) as EditText

        lblNoData!!.visibility = View.GONE
        if (reportPenjualans!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }

        val productsAdapter = PenjualansAdapter(reportPenjualans, R.layout.activity_reseller_product_list_row)
        recycler!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler!!.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler!!.adapter = productsAdapter
        productsAdapter.notifyDataSetChanged()

        btnSimpan.setOnClickListener { btnSelesaiClicked() }

        txtSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                searchStore()
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })
    }

    private fun searchStore() {
        if (txtSearch!!.text.toString().trim { it <= ' ' } == "") {
            reportPenjualans = reportPenjualanDao!!.queryBuilder()
                    .where(TReportPenjualanDao.Properties.VisitId.eq(mVisitId),
                            TReportPenjualanDao.Properties.ResellerId.eq(mResellerId))
                    .orderAsc(TReportPenjualanDao.Properties.ProductName)
                    .list()
        } else {
            val searchValue = "%" + txtSearch!!.text.toString().trim { it <= ' ' } + "%"
            reportPenjualans = reportPenjualanDao!!.queryBuilder()
                    .where(TReportPenjualanDao.Properties.VisitId.eq(mVisitId),
                            TReportPenjualanDao.Properties.ResellerId.eq(mResellerId),
                            TReportPenjualanDao.Properties.ProductName.like(searchValue))
                    .orderAsc(TReportPenjualanDao.Properties.ProductName)
                    .list()
        }

        val productsAdapter = PenjualansAdapter(reportPenjualans, R.layout.activity_reseller_product_list_row)
        recycler!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler!!.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler!!.adapter = productsAdapter
        productsAdapter.notifyDataSetChanged()

        lblNoData!!.visibility = View.GONE
        if (reportPenjualans!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }
    }

    private fun btnSelesaiClicked() {
        //region validation
        if (!validateForm()) {
            Toast.makeText(this, getString(R.string.info_msg_required_fields), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion
        showPopupSummary()

        // Close soft keyboard
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun submitForm() {
        for (o in reportPenjualans!!) {
            reportPenjualanDao!!.update(o)
        }
        val resellerDao = daoSession!!.tResellerDao
        val reseller = resellerDao.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()
        reseller.isDonePenjualan = true
        daoSession!!.update(reseller)
        finish()

    }

    private fun showPopupSummary() {
        val summary = ArrayList<TReportPenjualan>()
        txtSearch!!.setText("")
        searchStore()

        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layoutPopup = inflater.inflate(R.layout.popup_summary_penjualan2, findViewById(R.id.popup_summary_penjualan2) as ViewGroup?)
        mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
        val btnClose = layoutPopup.findViewById(R.id.penjualanPopup_btnClose) as ImageButton
        val btnSimpan = layoutPopup.findViewById(R.id.penjualanPopup_btnSimpan) as Button
        val recyclerSum = layoutPopup.findViewById(R.id.penjualanPopup_recycle) as androidx.recyclerview.widget.RecyclerView

        for (o in reportPenjualans!!) {
            if (o.quantity1 > 0 || o.quantity2 > 0) {
                summary.add(o)
            }
        }

        val sumProductsAdapter = SummaryPenjualansAdapter(summary, R.layout.activity_product_summary_list_row)
        recyclerSum.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerSum.addItemDecoration(SimpleDividerItemDecoration(this))
        recyclerSum.adapter = sumProductsAdapter
        sumProductsAdapter.notifyDataSetChanged()

        mPopupWindow!!.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)

        btnSimpan.setOnClickListener {
            val builder = AlertDialog.Builder(this@ResellerProductListActivity)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_msg_save))
            builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i ->
                submitForm()
                mPopupWindow!!.dismiss()
            }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }

        btnClose.setOnClickListener { mPopupWindow!!.dismiss() }
    }


    private fun validateForm(): Boolean {
        if (reportPenjualans == null) {
            return true
        }
        return if (reportPenjualans!!.size == 0) {
            true
        } else true

    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i ->
            if (reportPenjualans != null) {
                for (o in reportPenjualans!!) {
                    reportPenjualanDao!!.refresh(o)
                }
            }
            finish()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

    private fun addReportsToQueue() {
        val surveyorId = Login.getUserId(this)
        val reportPenjualanDao = daoSession!!.getTReportPenjualanDao()
        //ReportSender sender;
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentUtc = DateTimeUtils.currentUtc

        //BACKGROUND SENDING VERSI 2
        val listReport = ArrayList<GenericReport>()
        listreportPenjualan = reportPenjualanDao.queryBuilder()
                .where(TReportPenjualanDao.Properties.VisitId.eq(mVisitId),
                        TReportPenjualanDao.Properties.ResellerId.eq(mResellerId))
                .whereOr(TReportPenjualanDao.Properties.Quantity1.gt(0),
                        TReportPenjualanDao.Properties.Quantity2.gt(0)).list()



        if (listreportPenjualan!!.size > 0) {
            var values = ""
            for (item in listreportPenjualan!!) {
                if (item.getQuantity1() > 0) {
                    values += String.format(Locale.US, "%d,%d,%d,%d;", item.getProductId(), item.getUnitId1(), item.getQuantity1(), item.getPrice1())
                }
                if (item.getQuantity2() > 0) {
                    values += String.format(Locale.US, "%d,%d,%d,%d;", item.getProductId(), item.getUnitId2(), item.getQuantity2(), item.getPrice2())
                }
            }

            if (values != "") {
                values = values.substring(0, values.length - 1)
            }

            val id_report = DataController.waktuForReport + mVisitId + "penjualan" + surveyorId.toString() + mResellerId//+category.id_category+"-"+c.id_product_heinz+"-"+c.competitor_id;

            val parameters = ArrayList<ReportParameter>()
            parameters.add(ReportParameter("1", id_report, "visit_id", mVisitId!!, ReportParameter.TEXT))
            parameters.add(ReportParameter("2", id_report, "surveyor_id", surveyorId.toString(), ReportParameter.TEXT))
            parameters.add(ReportParameter("3", id_report, "reseller_id", mResellerId!!, ReportParameter.TEXT))
            parameters.add(ReportParameter("4", id_report, "values", values, ReportParameter.TEXT))


            val report = GenericReport(id_report,
                    surveyorId.toString(),
                    "Report Penjualan",
                    "Report Penjualan : " + mVisitId + ", " + surveyorId.toString() + ", " + mResellerId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                    ApiClient.getInsertPenjualan(),
                    currentDate, Config.NO_CODE, currentUtc, parameters)
            listReport.add(report)

        }
        if (listReport.size != 0) {
            val task = TambahReportTask(this, listReport)
            task.execute()
        } else {
            Log.i("kemari","gak")

            finish()
        }

    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog!!.setTitle(title)
        progressDialog!!.setMessage(message)
        progressDialog!!.setCancelable(false)
        if (show) {
            progressDialog!!.show()
            //selesai_button.setEnabled(false);
        } else {
            progressDialog!!.dismiss()
            // selesai_button.setEnabled(true);
        }
    }

    override fun setFinish(result: Boolean, message: String) {

        // selesai_button.setEnabled(true);
        if (result) {
            Log.i("kemari","ya")
            val resellerDao = daoSession!!.tResellerDao
            val reseller = resellerDao.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()
            reseller.isDonePenjualan = true
//        if (SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ROLE_ID, 0) != 3 && SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ROLE_ID, 0) != 1) {
//            reseller.isDonePullProgram = true
//        }
            daoSession!!.update(reseller)
            finish()
        }

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()

    }
}
