package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.StoresAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_store_list.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.ArrayList

class StoreListActivity : AppCompatActivity(), Loadable {
    private lateinit var daoSession: DaoSession
    private lateinit var storeDao: TStoreDao
    private lateinit var mStores: List<TStore>
    private lateinit var mMarket: TMarket
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView
    private lateinit var storesAdapter: StoresAdapter
    private var mMarketId: Int = 0
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_daftar_store)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mMarketId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_MARKET_ID, 0)
        daoSession = (application as GlobalApp).daoSession!!
        storeDao = daoSession.tStoreDao
        //mStores = storeDao.queryBuilder().where(TStoreDao.Properties.MarketId.eq(mMarketId)).list()
        mMarket = daoSession.tMarketDao.queryBuilder().where(TMarketDao.Properties.MarketId.eq(mMarketId)).limit(1).unique()
        progressDialog = ProgressDialog(this)
    }

    private fun initView() {
        recycler = findViewById(R.id.store_recycler)
        store_lblMarketName.text = mMarket.marketName
        store_lblAddress.text = mMarket.address

        store_btnSelesai.setOnClickListener {
            validateForm()
        }
    }


    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun validateForm() {
        if(!isAllStoresVisited()) {
            if(mStores.size >= Config.VISIT_STORE_LIMIT) {
                Toast.makeText(this, getString(R.string.info_msg_required_visit_limit_store, Config.VISIT_STORE_LIMIT), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, getString(R.string.info_msg_required_visit_all_store), Toast.LENGTH_SHORT).show()
            }
            return
        }

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_visit_out))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, i ->
            submitForm()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val reportId = "VOM" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_MARKET, "")

        //Add report visit out to queue
        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "visit_id", mVisitId!!, ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "check_out_datetime_utc",currentUtc.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "check_out_datetime", currentDateTime, ReportParameter.TEXT))

        val report = GenericReport(reportId,
                Login.getUserId(this).toString(), "CheckOut Market",
                "CheckOut Market: " + mVisitId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getUpdateVisitOutMarketUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)

        val task = TambahReportTask(this)
        task.execute(report)
    }

    private fun isAllStoresVisited(): Boolean {
        val stores = storeDao.queryBuilder()
                    .where(TStoreDao.Properties.MarketId.eq(mMarketId))
                    .list()

        return if(stores.size >= Config.VISIT_STORE_LIMIT) {
            storeDao.queryBuilder()
                    .where(TStoreDao.Properties.MarketId.eq(mMarketId),
                            TStoreDao.Properties.IsVisited.eq(Config.YES_CODE))
                    .list().size >= Config.VISIT_STORE_LIMIT
        } else {
            storeDao.queryBuilder()
                    .where(TStoreDao.Properties.MarketId.eq(mMarketId),
                            TStoreDao.Properties.IsVisited.eq(Config.YES_CODE))
                    .list().size == mStores.size
        }
    }

    override fun onResume() {
        super.onResume()
        mStores = storeDao.queryBuilder().where(TStoreDao.Properties.MarketId.eq(mMarketId)).list()
        storesAdapter = StoresAdapter(mStores, R.layout.activity_store_list_row, object : StoresAdapter.OnItemClickListener {
            override fun onItemClick(item: TStore) {
                if (item.isVisited == Config.NO_CODE) {
                    val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                    intent.putExtra("StoreId", item.storeId)
                    startActivity(intent)
                } else {
                    Toast.makeText(this@StoreListActivity, item.storeName + " sudah Anda kunjungi", Toast.LENGTH_SHORT).show();
                }
            }
        })

        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = storesAdapter
        storesAdapter.notifyDataSetChanged()
        store_lblNoData.visibility = View.GONE
        if(mStores.isEmpty()) {
            store_lblNoData.visibility = View.VISIBLE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater

        inflater.inflate(R.menu.menu_store_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_add) {
            val intent = Intent(this, StoreAddActivity::class.java)
            startActivity(intent)
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            val marketDao = daoSession.tMarketDao
            val market = marketDao.queryBuilder().where(TMarketDao.Properties.MarketId.eq(mMarketId)).limit(1).unique()
            market.isVisited = Config.YES_CODE
            marketDao.update(market)

            Login.clearSharedPrefVisitMarket(this@StoreListActivity)
            val intent = Intent(this@StoreListActivity, MarketListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}
