package com.nestle.mdgt.activities.reguler

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.PosmsAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.mtoolbar.*

class PosmListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null
    private var mStoreId: String? = null
    private var mSurveyorId: Int? = 0
    private var mPlanogramRegionId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportPosmDao
    private lateinit var items: MutableList<TReportPosm>
    private var adapter: PosmsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posm_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_daftar_posm)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mStoreId = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_ID, "")
        mSurveyorId = Login.getUserId(this)
        mPlanogramRegionId = SharedPrefsUtils.getStringPreference(this, Config.KEY_PLANOGRAM_REGION_ID, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportPosmDao
        val posmDao = daoSession.tPosmDao

        items = itemDao.queryBuilder()
                .where(TReportPosmDao.Properties.VisitId.eq(mVisitId),
                        TReportPosmDao.Properties.PlanogramRegionId.eq(mPlanogramRegionId)).list()

        if (items.isEmpty()) {
            val posms = posmDao.queryBuilder().where(TPosmDao.Properties.PlanogramRegionId.eq(mPlanogramRegionId)).list()
            val posmsSize = posms.size
            if (posmsSize > 0) {
                val objects = arrayOfNulls<TReportPosm>(posmsSize)
                var o: TReportPosm
                for ((i, model) in posms.withIndex()) {
                    o = TReportPosm()
                    o.visitId = mVisitId
                    o.storeId = mStoreId
                    o.surveyorId = mSurveyorId
                    o.planogramRegionId = model.planogramRegionId
                    o.posmPlanogramId = model.posmPlanogramId
                    o.planogramTypeId = model.planogramTypeId
                    o.planogramTypeName = model.planogramTypeName
                    o.posmTypeId = model.posmTypeId
                    o.posmTypeName = model.posmTypeName
                    o.ketersediaan = -1
                    o.penempatan = -1
                    o.kondisi = -1
                    o.photoPath = ""
                    o.isDone = false
                    o.isNew = Config.NO
                    objects[i] = o
                }
                itemDao.insertInTx(objects.toList())
                items = itemDao.queryBuilder()
                        .where(TReportPosmDao.Properties.VisitId.eq(mVisitId),
                                TReportPosmDao.Properties.PlanogramRegionId.eq(mPlanogramRegionId)).list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById(R.id.posm_lblNoData)
        recycler = findViewById(R.id.posm_recycler)
        val btnLanjut = findViewById<Button>(R.id.posm_btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }
    }


    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun reloadList() {
        items = itemDao.queryBuilder()
                .where(TReportPosmDao.Properties.VisitId.eq(mVisitId),
                        TReportPosmDao.Properties.PlanogramRegionId.eq(mPlanogramRegionId)).list()
        lblNoData.visibility = View.GONE
        if (items.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }

        adapter = PosmsAdapter(items, R.layout.activity_posm_list_row, object : PosmsAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportPosm) {
                val intent = Intent(this@PosmListActivity, PosmAddActivity::class.java)
                intent.putExtra("Id", item.id)
                startActivity(intent)
            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun validateForm() {
        //region validation
        if (!isAllItemsDone()) {
            Toast.makeText(this, getString(R.string.info_msg_required_check_all_report_item, "posm"), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion

        submitForm()
    }

    private fun submitForm() {
        val planogram = daoSession.tReportPlanogramDao.queryBuilder().
                where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId),
                        TReportPlanogramDao.Properties.PlanogramRegionId.eq(mPlanogramRegionId))
                .limit(1).unique()
        if(planogram != null) {
            planogram.isAllPosmsDone = true
            daoSession.tReportPlanogramDao.update(planogram)
        }

        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_POSM, true)
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "planogram")
        val intent = Intent(this, PlanogramListActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun isAllItemsDone(): Boolean {
        return if (items.isEmpty()) {
            true
        } else {
            itemDao.queryBuilder()
                    .where(TReportPosmDao.Properties.VisitId.eq(mVisitId),
                            TReportPosmDao.Properties.PlanogramRegionId.eq(mPlanogramRegionId),
                            TReportPosmDao.Properties.IsDone.eq(Config.YES_CODE))
                    .list().size == items.size
        }
    }

    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    private fun back() {
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "planogram")
        val intent = Intent(this, PlanogramListActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_posm_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            back()
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        if (id == R.id.action_add) {
            val intent = Intent(this, PosmAddActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        back()
    }
}
