package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TNooDistributor
import com.nestle.mdgt.database.TStoreType
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.*
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.rests.LoginApiInterface
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_add_noo.*
import kotlinx.android.synthetic.main.activity_store_add.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NooAddActivity : AppCompatActivity() {


    private var progressDialog: ProgressDialog? = null
    private var mVisitId: String? = ""
    private var mSurveyorId = 0
    private lateinit var daoSession: DaoSession
    private lateinit var regionAdapter : ArrayAdapter<TNooRegion>
    private lateinit var areaAdapter : ArrayAdapter<TNooArea>
    private lateinit var distributorAdapter : ArrayAdapter<TNooDistributor>
    private lateinit var storeTypeAdapter: ArrayAdapter<TStoreType>

    private var storeCode=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_noo)
        val subTitle = Login.getUsername(this)

        val mToolbar = findViewById<View>(R.id.mtoolbar) as androidx.appcompat.widget.Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_noo)
        supportActionBar!!.subtitle = subTitle

        initData()
        initView()
    }

    private fun initData() {
        mSurveyorId= Login.getUserId(this)
        progressDialog = ProgressDialog(this)
        daoSession = (application as GlobalApp).daoSession!!

    }

    private fun initView() {

        txtStoreName.filters = txtStoreName.filters + InputFilter.AllCaps()
        txtAlamatToko.filters = txtAlamatToko.filters + InputFilter.AllCaps()

        regionAdapter = ArrayAdapter(this@NooAddActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tNooRegionDao.queryBuilder().list())
        regionAdapter.insert(TNooRegion(null, 0,"", "" ), 0)
        spinnerRegion.adapter = regionAdapter

        storeTypeAdapter = ArrayAdapter(this@NooAddActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tStoreTypeDao.queryBuilder().list())
        storeTypeAdapter.insert(TStoreType(null, 0,"" ), 0)
        spinnerStoreType.adapter = storeTypeAdapter



        spinnerRegion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val region = regionAdapter.getItem(position) as TNooRegion
        areaAdapter = ArrayAdapter(this@NooAddActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tNooAreaDao.queryBuilder()
                        .where(TNooAreaDao.Properties.RegionId.eq(region.regionId)).list())
                areaAdapter!!.insert(TNooArea(null,0,"",0),0)
        spinnerArea.adapter =areaAdapter
            }

        }

        spinnerArea.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val area = areaAdapter.getItem(position) as TNooArea
                distributorAdapter = ArrayAdapter(this@NooAddActivity, android.R.layout.simple_spinner_dropdown_item,
                        daoSession.tNooDistributorDao.queryBuilder()
                                .where(TNooDistributorDao.Properties.AreaId.eq(area.areaId)).list())
                distributorAdapter!!.insert(TNooDistributor(null, 0, "", 0), 0)
                spinnerDistributor.adapter = distributorAdapter

            }
        }

        val btnSimpan = findViewById<View>(R.id.btnSimpan) as Button
        btnSimpan.setOnClickListener { btnSimpanClicked() }

    }

    private fun validateForm() {

        if (!FormValidation.validateRequiredText(
                        this, txtStoreName, String.format(getString(R.string.info_msg_required_field_text), "Nama Toko"))) {
            return
        }
        if(Login.getEmployeeRoleId(this) != 14) {
            if (!FormValidation.validateRequiredText(
                            this, txtAlamatToko, String.format(getString(R.string.info_msg_required_field_text), "Alamat"))) {
                return
            }
        }
        if (!FormValidation.validateRequiredSpinner(this, spinnerRegion, String.format(getString(R.string.info_msg_required_field_spinner), "Region"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, spinnerArea, String.format(getString(R.string.info_msg_required_field_spinner), "Area"))) {
            return
        }

        if (!FormValidation.validateRequiredSpinner(this, spinnerDistributor, String.format(getString(R.string.info_msg_required_field_spinner), "Depo"))) {
            return
        }

        if (!FormValidation.validateRequiredSpinner(this, spinnerStoreType, String.format(getString(R.string.info_msg_required_field_spinner), "Tipe Toko"))) {
            return
        }
        if(Login.getEmployeeRoleId(this) != 14) {
            if (!FormValidation.validateRequiredText(this, txtPhone, String.format(getString(R.string.info_msg_required_field_text), "Nomor Telepon Toko"))) {
                return
            }
        }

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
            val connectionDetectorUtil = ConnectionDetectorUtil(this@NooAddActivity)
            if (connectionDetectorUtil.isConnectingToInternet) {
                submitForm()
                btnSimpan.isClickable = false
            } else {
                Toast.makeText(this, getString(R.string.info_msg_no_network_connection), Toast.LENGTH_SHORT).show()
                btnSimpan.isClickable = true
            }
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun btnSimpanClicked() { //region Validation
        validateForm()
    }

    private fun submitForm() {

        progressBar.visibility = View.VISIBLE
//        val area = spinnerArea.selectedItem as TNooArea
//        val region = spinnerRegion.selectedItem as TNooRegion
        val distributor = spinnerDistributor.selectedItem as TNooDistributor
        val storeType = spinnerStoreType.selectedItem as TStoreType

        storeCode = Helper.generateStoreCodeNoo(Login.getUserId(this), storeType.storeTypeId, distributor.distributorId)


        val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
        val call = apiService.insertNoo(storeCode,txtStoreName.text.toString(),txtAlamatToko.text.toString(),txtPhone.text.toString(),
                distributor.distributorId,storeType.storeTypeId,Login.getEmployeeRoleId(this))
        call.enqueue(object : Callback<NooDataResponse> {
            override fun onResponse(call: Call<NooDataResponse>, response: Response<NooDataResponse>) {
                if (response.isSuccessful) {
                    if (response.body().status == Config.STATUS_SUCCESS) {
                        if(response.body().store != null) {
                            var model=response.body().store!!
                            Log.i("varStoreName",model.storeName)
                            var storeSize=daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.StoreCode.eq(storeCode)).list().size
                            Log.i("varStoreCode",storeCode)
                            Log.i("varStoreSize",storeSize.toString())

                            if(storeSize==0){
                                var store: TStore

                                store = TStore()
                                store.sId = model.sId
                                store.marketId = model.marketId
                                store.marketName = model.marketName
                                store.storeId = model.storeId
                                store.storeCode = model.storeCode
                                store.storeName = model.storeName
                                store.address = model.address
                                store.phone = model.phone
                                store.storeTypeId = model.storeTypeId
                                store.storeTypeName = model.storeTypeName
                                store.distributorId = model.distributorId
                                store.distributorName = model.distributorName
                                store.kecamatanId = model.kecamatanId
                                store.kecamatanName = model.kecamatanName
                                store.kabupatenId = model.kabupatenId
                                store.kabupatenName = model.kabupatenName
                                store.areaId = model.areaId
                                store.areaName = model.areaName
                                store.regionId = model.regionId
                                store.regionName = model.regionName
                                store.latitude = model.latitude
                                store.longitude = model.longitude
                                store.lastVisited = model.lastVisited
                                store.lastVisitedUtc = model.lastVisitedUtc
                                store.photoPath = model.photoPath
                                store.isVisited = Config.NO_CODE
                                store.radiusVerificationLimit = model.radiusVerificationLimit

                                daoSession.tStoreDao.insert(store)
                            }


                        //Planogram
                        if(response.body().planograms != null) {
                            val planogramSize = response.body().planograms!!.size
                            if (planogramSize > 0) {
                                val planograms = arrayOfNulls<TPlanogram>(planogramSize)
                                var planogram: TPlanogram
                                for ((i, model) in response.body().planograms!!.withIndex()) {
                                    planogram = TPlanogram()
                                    planogram.planogramRegionId = model.planogramRegionId
                                    planogram.storeId = model.storeId
                                    planogram.storeName = model.storeName
                                    planogram.planogramTypeId = model.planogramTypeId
                                    planogram.planogramTypeName = model.planogramTypeName
                                    planogram.productCategoryId = model.productCategoryId
                                    planogram.productCategoryName = model.productCategoryName
                                    planogram.guidelinePhotoPath = model.guidelinePhotoPath
                                    planogram.isMsl = model.isMsl

                                    planograms[i] = planogram
                                }
                                daoSession.tPlanogramDao.insertInTx(planograms.toList())
                            }
                        }

                        //region Campaign
                        if (response.body().campaigns != null) {
                            val campaignSize = response.body().campaigns!!.size
                            if (campaignSize > 0) {
                                var i = 0
                                val campaigns = arrayOfNulls<TCampaign>(campaignSize)
                                var campaign: TCampaign
                                for (model in response.body().campaigns!!) {
                                    campaign = TCampaign()
                                    campaign.storeId=model.storeId
                                    campaign.campaignId=model.campaignId
                                    campaign.surveyorTypeId=model.surveyorTypeId
                                    campaign.campaignName=model.campaignName
                                    campaign.startDate=model.startDate
                                    campaign.endDate=model.endDate
                                    campaigns[i] = campaign
                                    i++

                                    var posms=daoSession.tCampaignPosmDao.queryBuilder().where(TCampaignPosmDao.Properties.CampaignId.eq(model.campaignId)).list()
                                    daoSession.tCampaignPosmDao.deleteInTx(posms)

                                }
                                daoSession!!.tCampaignDao.insertInTx(campaigns.toList())
                            }
                        }
                        //endregion

                        //region Campaign Posm
                        if (response.body().campaignPosms != null) {
                            val campaignPosmSize = response.body().campaignPosms!!.size
                            if (campaignPosmSize > 0) {
                                var i = 0
                                val campaignPosms = arrayOfNulls<TCampaignPosm>(campaignPosmSize)
                                var campaignPosm: TCampaignPosm
                                for (model in response.body().campaignPosms!!) {
                                    campaignPosm = TCampaignPosm()
                                    campaignPosm.posmId=model.posmId
                                    campaignPosm.posmName=model.posmName
                                    campaignPosm.campaignId=model.campaignId
                                    campaignPosm.qrCode=model.qrCode
                                    campaignPosm.encode=model.encode
                                    campaignPosms[i] = campaignPosm

                                    i++
                                }
                                daoSession!!.tCampaignPosmDao.insertInTx(campaignPosms.toList())
                            }
                        }
                        //endregion

                            val intent = Intent(this@NooAddActivity, StoreDetailActivity2::class.java)
                            intent.putExtra("StoreId", model.storeId)
                            intent.putExtra("locationStatus", 0)
                            startActivity(intent)
                            finish()

                        }
                        else{
                            Toast.makeText(this@NooAddActivity,"Gagal dalam pembuatan NOO",Toast.LENGTH_SHORT).show()
                        }
                    }
                    else if (response.body().status == Config.STATUS_FAILURE) {
                        Toast.makeText(this@NooAddActivity, response.body().message, Toast.LENGTH_SHORT).show()
                    }
                    progressBar.visibility = View.GONE
                }
                btnSimpan.isClickable = false
            }

            override fun onFailure(call: Call<NooDataResponse>, t: Throwable) {
                btnSimpan.isClickable = true
                progressBar.visibility = View.GONE
                Toast.makeText(this@NooAddActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })

    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }



        return super.onOptionsItemSelected(item)
    }



    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
            finish()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

}
