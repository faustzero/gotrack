package com.nestle.mdgt.activities.reguler

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.CampaignAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TCampaignDao
import com.nestle.mdgt.database.TReportCampaignHeader
import com.nestle.mdgt.database.TReportCampaignHeaderDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.mtoolbar.*

class CampaignListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportCampaignHeaderDao
    private lateinit var items: MutableList<TReportCampaignHeader>
    private var adapter: CampaignAdapter? = null
    private var storeId:Int=0
    private var storeName:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_campaign_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_picos)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportCampaignHeaderDao
        val campaignDao = daoSession.tCampaignDao
        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        storeName= SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")!!

        items = itemDao.queryBuilder().where(TReportCampaignHeaderDao.Properties.VisitId.eq(mVisitId)).list()

        if (items.isEmpty()) {
            val campaigns = campaignDao.queryBuilder().where(TCampaignDao.Properties.StoreId.eq(storeId),
                    TCampaignDao.Properties.SurveyorTypeId.eq(Login.getEmployeeRoleId(this@CampaignListActivity))).list()
            val campaignsSize = campaigns.size
            Log.i("campaignsSize",campaignsSize.toString())

            if (campaignsSize > 0) {
                val objects = arrayOfNulls<TReportCampaignHeader>(campaignsSize)
                var o: TReportCampaignHeader
                for ((i, model) in campaigns.withIndex()) {
                    o = TReportCampaignHeader()
                    o.visitId = mVisitId
                    o.storeId = storeId
                    o.campaignId = model.campaignId
                    o.campaignName = model.campaignName
                    o.startDate = model.startDate
                    o.endDate = model.endDate
                    o.isTersedia = Config.NO_CODE
                    o.reasonId = Config.NO_CODE
                    o.isDone = false
                    objects[i] = o
                }
                itemDao.insertInTx(objects.toList())
                this.items = itemDao.queryBuilder().where(TReportCampaignHeaderDao.Properties.VisitId.eq(mVisitId)).list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById(R.id.lblNoData)
        recycler = findViewById(R.id.recycler)
        val btnLanjut = findViewById<Button>(R.id.btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }
    }

    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post { mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun reloadList() {
        items = itemDao.queryBuilder().where(TReportCampaignHeaderDao.Properties.VisitId.eq(mVisitId)).list()
        lblNoData.visibility = View.GONE
        if (items.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }

        adapter = CampaignAdapter(items, R.layout.activity_campaign_list_row, object : CampaignAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportCampaignHeader) {
                if(item.isDone){
                    Toast.makeText(this@CampaignListActivity,"Picos ${item.campaignName} sudah dikerjakan",Toast.LENGTH_SHORT).show()
                }

                else{

                        val intent = Intent(this@CampaignListActivity, CampaignAddActivity::class.java)
                        intent.putExtra("Id", item.id)
                        startActivity(intent)

                }
            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }


    private fun isValidForm(): Boolean {
        for(o in items){
            if(!o.isDone){
                return false
            }
        }
        return true
    }

    private fun validateForm() {

        if (!isValidForm()) { //TAMBAHAN CODINGAN AKTIFKAN MANDATORY
            Toast.makeText(this, "Anda harus mengisi report picos", Toast.LENGTH_SHORT).show()
            return
        }

        submitForm()
    }

    private fun submitForm() {
        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_CAMPAIGN, true)
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "competitor")
        var intent = Intent(this, CompetitorListActivity::class.java)
        if(Login.getEmployeeSurveyId(this@CampaignListActivity)>0){
            SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "surveyprice")

            intent = Intent(this, SurveyPriceListActivity::class.java)
        }
        startActivity(intent)
        finish()
    }


    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    private fun back() {
        if(Login.getEmployeeRoleId(this)!=12 || Login.getEmployeeRoleId(this)!=14 || Login.getEmployeeRoleId(this)!=15){
            SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "planogram")
            val intent = Intent(this, PlanogramListActivity::class.java)
            startActivity(intent)
            finish()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_planogram_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            back()
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if(Login.getEmployeeRoleId(this)==12 || Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
            val menu1 = menu.findItem(R.id.action_dashboard)
            menu1.isVisible = false
        }
        return true
    }

    override fun onBackPressed() {
        back()
    }
}
