package com.nestle.mdgt.activities

import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView

import com.bumptech.glide.Glide
import com.nestle.mdgt.R
import com.nestle.mdgt.rests.ApiClient

import java.io.File

class FullImageActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            val mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        setContentView(R.layout.activity_full_image)
        val image = findViewById<ImageView>(R.id.fullImage)
        val imageUrl = intent.getStringExtra("ImageUrl")
        val rotation = intent.getIntExtra("Rotation", 0)
        if (rotation == 90 || rotation == -90) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        if (File(imageUrl).exists()) {
            image.setImageBitmap(BitmapFactory.decodeFile(imageUrl))
        } else {
            Glide.with(this).asBitmap().load(imageUrl).into(image)
        }
    }
}
