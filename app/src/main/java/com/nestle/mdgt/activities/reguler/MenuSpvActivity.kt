package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.LoginActivity
import com.nestle.mdgt.activities.TransmissionActivity
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_menu_spv.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.io.File

class MenuSpvActivity : AppCompatActivity(), Loadable {

    private var daoSession: DaoSession? = null
    internal var progressDialog: ProgressDialog? = null
    private  var userId=0
    private var doneLocalActivity=false
    private var doneWeeklOperation=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_spv)

        val subTitle = Login.getUsername(this)
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setTitle(getString(R.string.label_menu_spv))
        supportActionBar!!.setSubtitle(subTitle)

        initData()
        initView()
    }

    private fun initData() {
        userId=Login.getUserId(this@MenuSpvActivity)
        daoSession = (application as GlobalApp).daoSession

    }

    private fun initView() {

        progressDialog = ProgressDialog(this)



        val onClickListener = View.OnClickListener { view -> onClicked(view.id) }

        btnLocalActivity.setOnClickListener(onClickListener)
        btnWeeklyOperation.setOnClickListener(onClickListener)
        btnSelesai.setOnClickListener(onClickListener)
    }

//    private fun initCheckedVisibility() {
//        doneLocalActivity=SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_LOCAL_ACTIVITY, false)
//        doneWeeklOperation=SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_WEEKLY_REPORT, false)
//
//        if(doneLocalActivity){
//            imgCheckedLocalActivity!!.visibility = View.VISIBLE
//        }
//        else{
//            imgCheckedLocalActivity!!.visibility = View.GONE
//        }
//
//        if(doneWeeklOperation){
//            imgCheckedWeeklyOperation!!.visibility = View.VISIBLE
//        }
//        else{
//            imgCheckedWeeklyOperation!!.visibility = View.GONE
//        }
//    }

    private fun onClicked(id: Int) {
        when (id) {
            R.id.btnLocalActivity -> {
                val iProductList = Intent(this, LocalActivityListActivity::class.java)
                startActivity(iProductList)
            }
            R.id.btnWeeklyOperation -> {
                val iWeeklyOperation = Intent(this, WeeklyOperationActivity::class.java)

                startActivity(iWeeklyOperation)
            }
            R.id.btnSelesai -> {
//                if (doneLocalActivity&&doneWeeklOperation) {
                    btnSelesaiClicked()
//                } else {
//                    Toast.makeText(this, "Anda harus menyelesaikan report", Toast.LENGTH_SHORT).show()
//                }
            }
        }
    }

    private fun btnSelesaiClicked() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_finish))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
        //        reseller.setIsDonePenjualan(false);
        //        reseller.setIsDonePullProgram(false);
        //        resellerDao.update(reseller);
        //        finish();

    }

    private fun submitForm() {
        logout()
    }

    override fun onResume() {
        super.onResume()
//        initCheckedVisibility()
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog!!.setTitle(title)
        progressDialog!!.setMessage(message)
        progressDialog!!.setCancelable(false)
        if (show) {
            progressDialog!!.show()
            //selesai_button.setEnabled(false);
        } else {
            progressDialog!!.dismiss()
            // selesai_button.setEnabled(true);
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_spv, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_transmission) {
            val intent = Intent(this, TransmissionActivity::class.java)
            startActivity(intent)
        }

        if (id == R.id.action_logout) {
                val builder = android.app.AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.label_confirm))
                builder.setMessage(getString(R.string.confirm_msg_logout))
                builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> logout() }
                builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
                val alertDialog = builder.create()
                alertDialog.setCanceledOnTouchOutside(false)
                alertDialog.show()

        }



        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        Thread {
            try {
                runOnUiThread {
                    setLoading(true, "Loading", "Logging Out")
                }
                Login.logout(this)
                val imagePath = StorageUtils.getDirectory(StorageUtils.DIRECTORY_IMAGE)
                DataController.deleteFilesNotInReport(imagePath, this)

                runOnUiThread {
                    setLoading(false, "Loading", "Logging Out")
                    val iLogin = Intent(this, LoginActivity::class.java)
                    iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(iLogin)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }.start()
    }

    override fun setFinish(result: Boolean, message: String) {

        if (result) {
            logout()
        }

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()

    }
}
