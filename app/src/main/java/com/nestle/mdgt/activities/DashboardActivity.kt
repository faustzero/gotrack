package com.nestle.mdgt.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.util.Log
import android.view.*
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.reguler.MarketListActivity
import com.nestle.mdgt.models.DailyDashboardResponse
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.rests.DashboardApiInterface
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.mtoolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import android.view.Gravity
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.activities.reguler.StoreListActivity2
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.utils.DataController
import com.nestle.mdgt.utils.StorageUtils

class DashboardActivity : AppCompatActivity(), Loadable {
    override fun setLoading(show: Boolean, title: String, message: String) {
    }

    override fun setFinish(result: Boolean, message: String) {
    }

    private lateinit var daoSession: DaoSession
    private var isVisiting: Boolean = false
    private var isFirstTimeIn: Boolean = false
    private var isFromLogin: Boolean = false
    private var surveyorId = 0
    private var loginTime: String? = null
    private lateinit var inflater: LayoutInflater
    private lateinit var mPopupWindow: PopupWindow
    private lateinit var layoutPopup: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        isVisiting = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_MARKET, "") != ""
        isFirstTimeIn = SharedPrefsUtils.getBooleanPreference(this, Config.KEY_EMPLOYEE_FIRST_TIME_IN, false)
        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(isVisiting || !isFirstTimeIn)
        supportActionBar!!.setDisplayShowHomeEnabled(isVisiting || !isFirstTimeIn)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Dashboard"

        initData()
        initView()
    }

    private fun initData() {
        isVisiting = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_MARKET, "") != ""
        surveyorId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ID, 0)
        loginTime = SharedPrefsUtils.getStringPreference(this, Config.KEY_EMPLOYEE_LOGIN_TIME, defaultValue = "")
        isFromLogin = intent.getBooleanExtra("IsFromLogin", false)
        daoSession = (application as GlobalApp).daoSession!!

        getAchievement()
    }

    private fun initView() {
        initPopupReminderTools()
        dashboard_lblSurveyorName.text = Login.getEmployeeName(this)
        dashboard_lblSurveyorType.text = Login.getEmployeeRoleName(this)
        dashboard_lblLoginTime.text = loginTime

        if (isVisiting || !isFirstTimeIn) {
            dashboard_btnDaftarPasar.visibility = View.GONE
        } else {
            dashboard_btnDaftarPasar.setOnClickListener {
                val intent = Intent(this, StoreListActivity2::class.java)
                startActivity(intent)
                finish()
            }
        }

        if(isFirstTimeIn) {
            if(!mPopupWindow.isShowing) {
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
            SharedPrefsUtils.setBooleanPreference(this, Config.KEY_EMPLOYEE_FIRST_TIME_IN, false)
        }

        dashboard_btnReminderTools.setOnClickListener {
            if(!mPopupWindow.isShowing) {
                initPopupReminderTools()
                mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)
            }
        }
    }

    private fun getAchievement() {
        try {
            dashboard_progressBar.visibility = View.VISIBLE
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
            val apiService = ApiClient.client!!.create(DashboardApiInterface::class.java)
            val call = apiService.getDailyAchievement(date, surveyorId)
            call.enqueue(object : Callback<DailyDashboardResponse> {
                override fun onResponse(call: Call<DailyDashboardResponse>, response: Response<DailyDashboardResponse>) {
                    if (response.isSuccessful) {
                        if (response.body().status == Config.STATUS_SUCCESS) {
                            try {
                                val ach = response.body().dailyAchievement
                                dashboard_lblPenjualan.text = ach!!.selling
                                dashboard_lblStoreVisited.text = ach.persenVisit
                                dashboard_lblPlanKunjungan.text = ach.planKunjungan
                                dashboard_lblActualVisit.text = ach.actualVisit
                                dashboard_lblPlanogramStore.text = ach.planogramStore
                                dashboard_lblPicos.text = ach.picos
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                    dashboard_progressBar.visibility = View.GONE
                }

                override fun onFailure(call: Call<DailyDashboardResponse>, t: Throwable) {
                    dashboard_progressBar.visibility = View.GONE
                    Toast.makeText(this@DashboardActivity, t.message, Toast.LENGTH_SHORT).show()
                }
            })
        } catch (e: Exception) {
            dashboard_progressBar.visibility = View.GONE
            e.printStackTrace()
        }
    }

    private fun logout() {
        Thread({
            try {
                runOnUiThread {
                    setLoading(true, "Loading", "Logging Out...")
                }
                val imagePath = StorageUtils.getDirectory(StorageUtils.DIRECTORY_IMAGE)
                DataController.deleteFilesNotInReport(imagePath, applicationContext)

                runOnUiThread {
                    setLoading(false, "Loading", "Logging Out...")
                    Login.logout(this)
                    val iLogin = Intent(this, LoginActivity::class.java)
                    iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(iLogin)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

    private fun initPopupReminderTools() {
        try {
            inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            layoutPopup = inflater.inflate(R.layout.popup_reminder_tools, findViewById(R.id.popup_reminder_tools))
            mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
            val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
            btnClose.setOnClickListener {
                mPopupWindow.dismiss()

                if (isFromLogin) {
                    popupInfo()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun popupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)
                btnClose.setOnClickListener { mPopupWindow.dismiss() }

                mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)
                isFromLogin = false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_dashboard, menu)
        if (isVisiting) {
            menu.getItem(2).isVisible = false
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (isVisiting || !isFirstTimeIn) {
            if (id == android.R.id.home) {
                finish()
            }
        }

        if (id == R.id.action_transmission) {
            startActivity(Intent(this, TransmissionActivity::class.java))
        }
        if (id == R.id.action_refresh) {
            getAchievement()
        }

        return super.onOptionsItemSelected(item)
    }
}
