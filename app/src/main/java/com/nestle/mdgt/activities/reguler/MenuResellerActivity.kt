package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast

import com.bumptech.glide.Glide
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.reguler.PullProgramListActivity
import com.nestle.mdgt.activities.reguler.ResellerListActivity
import com.nestle.mdgt.activities.reguler.ResellerProductListActivity
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TReportPenjualan
import com.nestle.mdgt.database.TReportPenjualanDao
import com.nestle.mdgt.database.TReportPullProgram
import com.nestle.mdgt.database.TReportPullProgramDao
import com.nestle.mdgt.database.TReseller
import com.nestle.mdgt.database.TResellerDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.DataController
import com.nestle.mdgt.utils.DateTimeUtils
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import kotlinx.android.synthetic.main.activity_menu_reseller.*

import org.joda.time.LocalDate

import java.util.ArrayList
import java.util.Locale

class MenuResellerActivity : AppCompatActivity(), Loadable {
    private var imgCheckedPenjualan: ImageView? = null
    private var imgCheckedPullProgram: ImageView? = null
    private val imgCheckedLocationAdd: ImageView? = null
    private val imgCheckedUpdateReseller: ImageView? = null

    private var mStoreName: String? = null
    private var mResellerId: String? = null

    private var daoSession: DaoSession? = null
    private var resellerDao: TResellerDao? = null
    private var pullProgramDao: TReportPullProgramDao? = null
    private var penjualanDao: TReportPenjualanDao? = null
    private var reportPullPrograms: List<TReportPullProgram>? = null
    private var listreportPenjualan: List<TReportPenjualan>? = null
    private var resellerId: String? = null
    private var visitId: String? = null
    private var reseller: TReseller? = null


    internal var progressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_reseller)

        val subTitle = Login.getUsername(this) + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "") + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_NAME, "")
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setTitle(getString(R.string.label_menu_reseller))
        supportActionBar!!.setSubtitle(subTitle)

        initData()
        //initDrawer(mToolbar);
        initView()
    }

    private fun initData() {
        resellerId = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_ID, "")
        visitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mResellerId = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_ID, "")
        mStoreName = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")
        daoSession = (application as GlobalApp).daoSession
        pullProgramDao = daoSession!!.tReportPullProgramDao
        penjualanDao = daoSession!!.tReportPenjualanDao
        resellerDao = daoSession!!.tResellerDao
        reseller = resellerDao!!.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()

    }

    private fun initView() {
        if(Login.getEmployeeRoleId(this)==13){
            menuReseller_btnPullProgram.visibility=View.GONE
        }
        val btnPenjualan = findViewById<View>(R.id.menuReseller_btnPenjualan) as RelativeLayout
        val btnPullProgram = findViewById<View>(R.id.menuReseller_btnPullProgram) as RelativeLayout

        val btnSelesai = findViewById<View>(R.id.menuReseller_btnSelesai) as Button
        imgCheckedPenjualan = findViewById<View>(R.id.menuReseller_imgCheckedPenjualan) as ImageView
        imgCheckedPullProgram = findViewById<View>(R.id.menuReseller_imgCheckedPullProgram) as ImageView

        progressDialog = ProgressDialog(this)


        initCheckedVisibility()

        val onClickListener = View.OnClickListener { view -> onClicked(view.id) }

        btnPenjualan.setOnClickListener(onClickListener)
        btnPullProgram.setOnClickListener(onClickListener)
        btnSelesai.setOnClickListener(onClickListener)
    }

    private fun initCheckedVisibility() {
        val reseller = resellerDao!!.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()
        if (reseller != null) {
            if (reseller.isDonePenjualan) {
                imgCheckedPenjualan!!.visibility = View.VISIBLE
            }
            if (reseller.isDonePullProgram) {
                imgCheckedPullProgram!!.visibility = View.VISIBLE
            }
        }

        //        if (SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_PENJUALAN, false)) {
        //            imgCheckedPenjualan.setVisibility(View.VISIBLE);
        //        }
        //        if (SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_PULLPROGGRAM, false)) {
        //            imgCheckedPullProgram.setVisibility(View.VISIBLE);
        //        }
        //        if (SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_LOCATION, false)) {
        //            imgCheckedLocationAdd.setVisibility(View.VISIBLE);
        //        }
    }

    private fun onClicked(id: Int) {
        when (id) {
            R.id.menuReseller_btnPenjualan -> {
                val iProductList = Intent(this, ResellerProductListActivity::class.java)
                startActivity(iProductList)
            }
            R.id.menuReseller_btnPullProgram -> {
                val iPullProgramList = Intent(this, PullProgramListActivity::class.java)
                startActivity(iPullProgramList)
            }
            R.id.menuReseller_btnSelesai -> {
                //                boolean pullProgramChecked = true;
                //                String messageReport = getString(R.string.info_msg_required_report_reseller_penjualan);
                //                if(Login.getEmployeeRoleId(this) == 3) { // Jika role bandar maka munculkan report pullprogram
                //                    pullProgramChecked = SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_PULLPROGGRAM, false);
                //                    messageReport = getString(R.string.info_msg_required_report_reseller_mandatory);
                //                }

                val reseller = resellerDao!!.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()
                if (reseller != null) {
                    if(Login.getEmployeeRoleId(this)==13){
                        if (reseller.isDonePenjualan) {
                            btnSelesaiClicked()
                        } else {
                            Toast.makeText(this, "Anda harus menyelesaikan report", Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        if (reseller.isDonePenjualan && reseller.isDonePullProgram) {
                            btnSelesaiClicked()
                        } else {
                            Toast.makeText(this, "Anda harus menyelesaikan report", Toast.LENGTH_SHORT).show()
                        }
                    }

                } else {
                    btnSelesaiClicked()
                }
            }
        }//                if (SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_PENJUALAN, false) && pullProgramChecked) {
        //                    btnSelesaiClicked();
        //                } else {
        //                    Toast.makeText(this, messageReport, Toast.LENmdgtH_SHORT).show();
        //                }
    }

    private fun btnSelesaiClicked() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_finish))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> addReportsToQueue() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
        //        reseller.setIsDonePenjualan(false);
        //        reseller.setIsDonePullProgram(false);
        //        resellerDao.update(reseller);
        //        finish();

    }

    private fun addReportsToQueue() {
        val surveyorId = Login.getUserId(this)
        val daoSession = (application as GlobalApp).daoSession
        val reportPenjualanDao = daoSession!!.tReportPenjualanDao
        val reportPullProgramDao = daoSession.tReportPullProgramDao
        //ReportSender sender;
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentUtc = DateTimeUtils.currentUtc

        //BACKGROUND SENDING VERSI 2
        val listReport = ArrayList<GenericReport>()
        listreportPenjualan = reportPenjualanDao.queryBuilder()
                .where(TReportPenjualanDao.Properties.VisitId.eq(visitId),
                        TReportPenjualanDao.Properties.ResellerId.eq(resellerId))
                .whereOr(TReportPenjualanDao.Properties.Quantity1.gt(0),
                        TReportPenjualanDao.Properties.Quantity2.gt(0)).list()



        if (listreportPenjualan!!.size > 0) {
            var values = ""
            for (item in listreportPenjualan!!) {
                if (item.quantity1 > 0) {
                    values += String.format(Locale.US, "%d,%d,%d,%d;", item.productId, item.unitId1, item.quantity1, item.price1)
                }
                if (item.quantity2 > 0) {
                    values += String.format(Locale.US, "%d,%d,%d,%d;", item.productId, item.unitId2, item.quantity2, item.price2)
                }
            }

            if (values != "") {
                values = values.substring(0, values.length - 1)
            }

            val id_report = DataController.waktuForReport + visitId + "penjualan" + surveyorId.toString() + resellerId//+category.id_category+"-"+c.id_product_heinz+"-"+c.competitor_id;

            val parameters = ArrayList<ReportParameter>()
            parameters.add(ReportParameter("1", id_report, "visit_id", visitId!!, ReportParameter.TEXT))
            parameters.add(ReportParameter("2", id_report, "surveyor_id", surveyorId.toString(), ReportParameter.TEXT))
            parameters.add(ReportParameter("3", id_report, "reseller_id", resellerId!!, ReportParameter.TEXT))
            parameters.add(ReportParameter("4", id_report, "values", values, ReportParameter.TEXT))


            val report = GenericReport(id_report,
                    Login.getUsername(this@MenuResellerActivity)!!,
                    "Report Penjualan",
                    "Report Penjualan : " + visitId + ", " + surveyorId.toString() + ", " + resellerId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                    ApiClient.getInsertPenjualan(),
                    currentDate, Config.NO_CODE, currentUtc, parameters)
            listReport.add(report)

        }

        reportPullPrograms = reportPullProgramDao.queryBuilder().where(TReportPullProgramDao.Properties.VisitId.eq(visitId), TReportPullProgramDao.Properties.ResellerId.eq(mResellerId)).list()

        if (reportPullPrograms!!.size > 0) {
            var values = ""
            for (item in reportPullPrograms!!) {
                values += String.format(Locale.US, "%s,%d;", item.stickerName, item.quantity)
            }

            if (values != "") {
                values = values.substring(0, values.length - 1)
            }

            val id_report = DataController.waktuForReport + visitId + "pull program" + surveyorId.toString() + resellerId//+category.id_category+"-"+c.id_product_heinz+"-"+c.competitor_id;

            val parameters = ArrayList<ReportParameter>()
            parameters.add(ReportParameter("1", id_report, "visit_id", visitId!!, ReportParameter.TEXT))
            parameters.add(ReportParameter("2", id_report, "surveyor_id", surveyorId.toString(), ReportParameter.TEXT))
            parameters.add(ReportParameter("3", id_report, "reseller_id", resellerId!!, ReportParameter.TEXT))
            parameters.add(ReportParameter("4", id_report, "values", values, ReportParameter.TEXT))


            val report = GenericReport(id_report,
                    Login.getUsername(this@MenuResellerActivity)!!,
                    "Report Pull Program",
                    "Report Pull Program : " + visitId + ", " + surveyorId.toString() + ", " + resellerId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                    ApiClient.getInsertPullProgram(),
                    currentDate, Config.NO_CODE, currentUtc, parameters)
            listReport.add(report)

            //            TambahReportTask task =new  TambahReportTask(this, listReport);
            //            task.execute();

        }
        if (listReport.size != 0) {
            val tambahReportTask = TambahReportTask(this, listReport)

            tambahReportTask.execute()
        } else {
            Login.checkoutReseller(this@MenuResellerActivity)
            finish()
        }

    }

    override fun onResume() {
        super.onResume()
        initCheckedVisibility()
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog!!.setTitle(title)
        progressDialog!!.setMessage(message)
        progressDialog!!.setCancelable(false)
        if (show) {
            progressDialog!!.show()
            //selesai_button.setEnabled(false);
        } else {
            progressDialog!!.dismiss()
            // selesai_button.setEnabled(true);
        }
    }

    override fun setFinish(result: Boolean, message: String) {

        if (result) {
            Login.checkoutReseller(this@MenuResellerActivity)
            listreportPenjualan = penjualanDao!!.queryBuilder()
                    .where(TReportPenjualanDao.Properties.VisitId.eq(visitId),
                            TReportPenjualanDao.Properties.ResellerId.eq(resellerId)).list()
             pullProgramDao!!.deleteInTx(reportPullPrograms);
            penjualanDao!!.deleteInTx(listreportPenjualan);
            Log.i("size pull program", pullProgramDao!!.loadAll().size.toString())
            Log.i("size penjualan", penjualanDao!!.loadAll().size.toString())

            finish()
        }

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()

    }
}
