package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner

import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TReportPullProgram
import com.nestle.mdgt.database.TReportPullProgramDao
import com.nestle.mdgt.database.TReseller
import com.nestle.mdgt.database.TResellerDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.FormValidation
import com.nestle.mdgt.utils.Helper
import com.nestle.mdgt.utils.SharedPrefsUtils

class PullProgramAddActivity : AppCompatActivity() {
    private var spinQuantity: Spinner? = null

    private var mVisitId: String? = null
    private var mResellerId: String? = null
    private var mResellerName: String? = null
    private var mId: Long = 0

    private var daoSession: DaoSession? = null
    private var reportPullProgramDao: TReportPullProgramDao? = null
    private var mPullProgram: TReportPullProgram? = null
    private var quantityAdapter: ArrayAdapter<Int>? = null
    private val arrQuantity = arrayOf(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_program_add)

        val subTitle = Login.getUsername(this) + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "") + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_NAME, "")
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_tambah_stiker)
        supportActionBar!!.subtitle = subTitle

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {
        mId = intent.getLongExtra("Id", 0)
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mResellerId = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_ID, "")
        mResellerName = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_NAME, "")
        daoSession = (application as GlobalApp).daoSession
        reportPullProgramDao = daoSession!!.tReportPullProgramDao
        mPullProgram = reportPullProgramDao!!.load(mId)
    }

    private fun initView() {
        val btnSimpan = findViewById<View>(R.id.pullprogramAdd_btSimpan) as Button
        spinQuantity = findViewById<View>(R.id.pullprogramAdd_spinQuantity) as Spinner

        quantityAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, arrQuantity)
        spinQuantity!!.adapter = quantityAdapter

        btnSimpan.setOnClickListener { btnSimpanClicked() }
    }

    private fun initValuesToViews() {
        if (mPullProgram != null) {
            supportActionBar!!.title = getString(R.string.label_ubah_stiker)

            val position = quantityAdapter!!.getPosition(mPullProgram!!.quantity)
            spinQuantity!!.setSelection(position)
        }
    }

    private fun btnSimpanClicked() {
        //region Validation
        if (!FormValidation.validateRequiredSpinner(this, spinQuantity!!, String.format(getString(R.string.info_msg_required_field_spinner), "quantity"))) {
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        if (mPullProgram == null) {
            mPullProgram = TReportPullProgram()
            mPullProgram!!.visitId = mVisitId
            mPullProgram!!.resellerId = mResellerId
            mPullProgram!!.resellerName = mResellerName
            mPullProgram!!.stickerName = Helper.generateStickerName()
            mPullProgram!!.quantity = Integer.parseInt(spinQuantity!!.selectedItem.toString())
            reportPullProgramDao!!.insert(mPullProgram)
        } else {
            mPullProgram!!.quantity = Integer.parseInt(spinQuantity!!.selectedItem.toString())
            reportPullProgramDao!!.update(mPullProgram)
        }

        //        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_PULLPROGGRAM, true);

        val resellerDao = daoSession!!.tResellerDao
        val reseller = resellerDao.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()
        reseller.isDonePullProgram = true
        daoSession!!.update(reseller)
        finish()
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }
}
