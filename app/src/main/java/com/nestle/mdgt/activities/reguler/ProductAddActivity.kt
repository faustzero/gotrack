package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TReportMsl
import com.nestle.mdgt.database.TReportMslDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.FormValidation
import com.nestle.mdgt.utils.Helper
import com.nestle.mdgt.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_product_add.*
import kotlinx.android.synthetic.main.mtoolbar.*

class ProductAddActivity : AppCompatActivity() {
    private var mId: Long = 0

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportMslDao
    private var item: TReportMsl? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_add)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_product_detail)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {
        mId = intent.getLongExtra("Id", 0)
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportMslDao
        item = itemDao.load(mId)
    }

    private fun initView() {
        product_btnSimpan.setOnClickListener { validateForm() }
    }

    private fun initValuesToViews() {
        if (item != null) {
            //supportActionBar!!.title = item!!.productName
            product_lblProductName.text = item!!.productName
            product_lblDescription.text = item!!.brandName
            product_lblStock1.text = "Stok (${item!!.unitName1})"
            product_lblPrice1.text = "Harga (${item!!.unitName1})"
            product_txtStock1.setText(Helper.convertZeroToEmpty(item!!.stock1))
            product_txtPrice1.setText(Helper.convertZeroToEmpty(item!!.price1))
            product_lblStock2.text = "Stok (${item!!.unitName2})"
            product_lblPrice2.text = "Harga (${item!!.unitName2})"
            product_txtStock2.setText(Helper.convertZeroToEmpty(item!!.stock2))
            product_txtPrice2.setText(Helper.convertZeroToEmpty(item!!.price2))
        }
    }

    private fun validateForm() {
        //region Validation
        if (!FormValidation.validateRequiredText(this, product_txtStock1, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, product_txtPrice1, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, product_txtStock2, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, product_txtPrice2, getString(R.string.info_msg_required_field_text))) {
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        if (item == null) {
            item = TReportMsl()
            item!!.isDone = true
            itemDao.insert(item)
        } else {
            item!!.stock1 = product_txtStock1.text.toString().toInt()
            item!!.price1 = product_txtPrice1.text.toString().toInt()
            item!!.stock2 = product_txtStock2.text.toString().toInt()
            item!!.price2 = product_txtPrice2.text.toString().toInt()
            item!!.isDone = true
            itemDao.update(item)
        }
        finish()
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }
}
