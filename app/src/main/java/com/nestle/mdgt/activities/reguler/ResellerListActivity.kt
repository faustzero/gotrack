package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*


import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.TransmissionActivity
import com.nestle.mdgt.adapters.ResellersAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.DateTimeUtils
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.activity_reseller_list.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.ArrayList


class ResellerListActivity : AppCompatActivity(),Loadable {
    private var recycler: androidx.recyclerview.widget.RecyclerView? = null
    private var lblNoData: TextView? = null
    private var txtSearch: EditText? = null
    private var mVisitId: String? = null


    private var mStoreId: Int = 0
    private var mStoreName: String? = null

    private var daoSession: DaoSession? = null
    private var resellerDao: TResellerDao? = null
    private var resellers: MutableList<TReseller>? = null
    private var resellersAdapter: ResellersAdapter? = null
    private lateinit var mStore:TStore
    private var radiusId=1
    private var progressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reseller_list)

        val subTitle = Login.getUsername(this) + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setTitle(getString(R.string.label_daftar_reseller))
        supportActionBar!!.setSubtitle(subTitle)

        initData()
        initView()
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")

        mStoreId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID,0)
        mStoreName = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")
        daoSession = (application as GlobalApp).daoSession
        mStore = daoSession!!.tStoreDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(mStoreId)).limit(1).unique()

        resellerDao = daoSession!!.tResellerDao
        resellers = resellerDao!!.queryBuilder()
                .where(TResellerDao.Properties.StoreId.eq(mStore.storeId))
                .orderAsc(TResellerDao.Properties.ResellerName)
                .list()
        progressDialog = ProgressDialog(this)


    }
    private fun initView() {
        val btnAdd = findViewById<View>(R.id.resellerList_fabAdd) as FloatingActionButton
        lblNoData = findViewById<View>(R.id.resellerList_lblNoData) as TextView
        val btnSelesai = findViewById<View>(R.id.resellerList_btnSelesai) as Button
        recycler = findViewById<View>(R.id.resellerList_recycler) as androidx.recyclerview.widget.RecyclerView
        txtSearch = findViewById<View>(R.id.resellerList_txtSearch) as EditText

        main_tabTop.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
//                onTabSubTopSelected(approval_tabTop.selectedTabPosition, tab.position)
                callGetInventorys()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })

        lblNoData!!.visibility = View.GONE
        if (resellers!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }

        resellersAdapter = ResellersAdapter(resellers, R.layout.activity_reseller_list_row, ResellersAdapter.OnItemClickListener { item -> resellerClicked(item) })
        recycler!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler!!.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler!!.adapter = resellersAdapter
        resellersAdapter!!.notifyDataSetChanged()

        btnAdd.setOnClickListener {
            val intent = Intent(this@ResellerListActivity, ResellerAddActivity::class.java)
            intent.putExtra("radiusId",radiusId)
            if(radiusId==1){
                intent.putExtra("radiusName","OUTSTORE")
            }
            else{
                intent.putExtra("radiusName","INSTORE")
            }

            startActivity(intent)
        }

        btnSelesai.setOnClickListener { btnSelesaiClicked() }

        txtSearch!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                searchReseller(radiusId)
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })

    }

    private fun callGetInventorys() {

        when (main_tabTop.selectedTabPosition) {
            0 -> {
                radiusId=1
                refreshResellerList()
                txtSearch!!.setText("")

            }
            1 -> {
                radiusId=2
                refreshResellerList()
                txtSearch!!.setText("")
            }
        }
    }

    private fun searchReseller(radiusId:Int) {
        if (txtSearch!!.text.toString().trim { it <= ' ' } == "") {
            resellers = resellerDao!!.queryBuilder()
                    .where(TResellerDao.Properties.StoreId.eq(mStore.storeId))
                    .where(TResellerDao.Properties.RadiusId.eq(radiusId))
                    .orderAsc(TResellerDao.Properties.ResellerName)
                    .list()
        } else {
            val searchValue = "%" + txtSearch!!.text.toString().trim { it <= ' ' } + "%"
            resellers = resellerDao!!.queryBuilder()
                    .where(TResellerDao.Properties.StoreId.eq(mStore.storeId), TResellerDao.Properties.ResellerName.like(searchValue),
                            TResellerDao.Properties.RadiusId.eq(radiusId))
                    .orderAsc(TResellerDao.Properties.ResellerName)
                    .list()
        }


        resellersAdapter = ResellersAdapter(resellers, R.layout.activity_reseller_list_row, ResellersAdapter.OnItemClickListener { item -> resellerClicked(item) })
        recycler!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler!!.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler!!.adapter = resellersAdapter
        resellersAdapter!!.notifyDataSetChanged()

        lblNoData!!.visibility = View.GONE
        if (resellers!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }
    }

    private fun resellerClicked(r: TReseller) {
        if(r.isDonePenjualan){
            Toast.makeText(this@ResellerListActivity,"Reseller ${r.resellerName} sudah melakukan penjualan",Toast.LENGTH_SHORT).show()
        }
        else{
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_msg_click_reseller))
            builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i ->
                SharedPrefsUtils.setStringPreference(this@ResellerListActivity, Config.KEY_RESELLER_ID, r.resellerId)
                SharedPrefsUtils.setStringPreference(this@ResellerListActivity, Config.KEY_RESELLER_NAME, r.resellerName)
                val intent = Intent(this@ResellerListActivity, MenuResellerActivity::class.java)
                //                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }


    }

    private fun btnSelesaiClicked() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_finish))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

//    private fun submitForm() {
//        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER, true)
//        val storeDao = daoSession!!.tStoreDao
//        val store = storeDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(mStoreId)).limit(1).unique()
//        store.isVisited = Config.YES_CODE
//        storeDao.update(store)
//
//        Login.clearSharedPrefVisitStore(this)
//        val intent = Intent(this, StoreListActivity2::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        startActivity(intent)
//        finish()
//    }

    private fun submitForm() {
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val reports = ArrayList<GenericReport>()

        //region add report visit out to queue
        val reportId = "VOS$mVisitId${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "visit_id", mVisitId!!, ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "check_out_datetime_utc",currentUtc.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "check_out_datetime", currentDateTime, ReportParameter.TEXT))

        val rVisit = GenericReport(reportId,
                Login.getUserId(this).toString(), "CheckOut",
                "CheckOut: $mVisitId (${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                ApiClient.getUpdateVisitOutUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)
        reports.add(rVisit)
        //endregion

        //execute task
        val task = TambahReportTask(this, reports)
        task.execute()

    }

    private fun refreshResellerList() {
        resellers!!.clear()
        resellers!!.addAll(resellerDao!!.queryBuilder()
                .where(TResellerDao.Properties.StoreId.eq(mStore.storeId))
                .where(TResellerDao.Properties.RadiusId.eq(radiusId))
                .list())
        resellersAdapter!!.notifyDataSetChanged()
        lblNoData!!.visibility = View.GONE
        if (resellers!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }
    }

    public override fun onResume() {
        super.onResume()
        refreshResellerList()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_trans, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_trans) {
            val intent = Intent(this, TransmissionActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER, true)
            val storeDao = daoSession!!.tStoreDao
            val store = storeDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(mStoreId)).limit(1).unique()
            store.isVisited = Config.YES_CODE
            storeDao.update(store)

            Login.clearSharedPrefVisitStore(this)
            val intent = Intent(this, StoreListActivity2::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()

        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
