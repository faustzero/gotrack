package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.Toast
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoActivity
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.FormValidation
import com.nestle.mdgt.utils.Helper
import com.nestle.mdgt.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_posm_add.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import java.io.File

class PosmAddActivity : AppCompatActivity() {
    private var mId: Long = 0
    private val requestCodeCamera = 10
    private var mPhotoPath: String? = null
    private var mPhotoType: Int = 0
    private var isAvailablePhoto: Boolean = false

    private var mVisitId: String? = null
    private var mPlanogramRegionId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportPosmDao
    private lateinit var posmTypeDao: TPosmTypeDao
    private var item: TReportPosm? = null
    private var posmTypeAdapter: ArrayAdapter<TPosmType>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posm_add)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_tambah_posm)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {
        mId = intent.getLongExtra("Id", 0)
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mPlanogramRegionId = SharedPrefsUtils.getStringPreference(this, Config.KEY_PLANOGRAM_REGION_ID, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportPosmDao
        posmTypeDao = daoSession.tPosmTypeDao
        item = itemDao.load(mId)
    }

    private fun initView() {
//        Glide.with(this).load(ApiClient.hostUrl + item.guidelinePhotoPath).asBitmap().into(posm_imgGuideline)

        posm_imgPhoto.setOnClickListener {
            if (mPhotoPath != "") {
                val intent = Intent(this, FullImageActivity::class.java)
                intent.putExtra("ImageUrl", mPhotoPath)
                startActivity(intent)
            }
        }

        posmTypeAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item,
                posmTypeDao.queryBuilder().list())
        posmTypeAdapter!!.insert(TPosmType(null, 0, ""), 0)
        posm_spinType.adapter = posmTypeAdapter

        posm_btnPhoto.setOnClickListener {
            mPhotoType = requestCodeCamera
            startCamera(mPhotoType)
        }

        posm_btnSimpan.setOnClickListener { validateForm() }
    }

    private fun initValuesToViews() {
        if (item != null) {
            supportActionBar!!.title = item!!.posmTypeName
            val planoTypePosition = posmTypeAdapter!!.getPosition(posmTypeDao.queryBuilder()
                    .where(TPosmTypeDao.Properties.PosmTypeId.eq(item!!.posmTypeId))
                    .limit(1).unique())
            posm_spinType.setSelection(planoTypePosition)
            posm_spinType.isEnabled = false
            posm_spinType.setBackgroundResource(R.drawable.form_spinner_style_disabled)

            if (item!!.ketersediaan == Config.YES_CODE) {
                (posm_rgKetersediaan.getChildAt(0) as RadioButton).isChecked = true
            } else if (item!!.ketersediaan == Config.NO_CODE) {
                (posm_rgKetersediaan.getChildAt(1) as RadioButton).isChecked = true
            }

            if (item!!.penempatan == Config.YES_CODE) {
                (posm_rgPenempatan.getChildAt(0) as RadioButton).isChecked = true
            } else if (item!!.penempatan == Config.NO_CODE) {
                (posm_rgPenempatan.getChildAt(1) as RadioButton).isChecked = true
            }
            if (item!!.kondisi == Config.YES_CODE) {
                (posm_rgKondisi.getChildAt(0) as RadioButton).isChecked = true
            } else if (item!!.kondisi == Config.NO_CODE) {
                (posm_rgKondisi.getChildAt(1) as RadioButton).isChecked = true
            }

            mPhotoPath = item!!.photoPath
            val fileBefore = File(mPhotoPath)
            if (fileBefore.exists()) {
                posm_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath))
                posm_imgPhoto.scaleType = ImageView.ScaleType.CENTER
                isAvailablePhoto = true
            } else {
                posm_imgPhoto.setImageResource(R.drawable.img_no_picture)
                posm_imgPhoto.scaleType = ImageView.ScaleType.FIT_CENTER
                isAvailablePhoto = false
                mPhotoPath = ""
            }
        }
    }

    private fun validateForm() {
        //region Validation
        if(item == null) {
            if (!FormValidation.validateRequiredSpinner(this, posm_spinType, String.format(getString(R.string.info_msg_required_field_spinner), "tipe posm"))) {
                return
            }
            val posmTypeSelected = posm_spinType.selectedItem as TPosmType
            val mPlanogramRegionId = SharedPrefsUtils.getStringPreference(this, Config.KEY_PLANOGRAM_REGION_ID, "")
            if(isExistPosm(posmTypeSelected.posmTypeId, mPlanogramRegionId!!)) {
                Toast.makeText(this, "${posmTypeSelected.posmTypeName} sudah ada, silahkan pilih tipe posm lainnya", Toast.LENGTH_SHORT).show()
                return
            }
        }
        if (posm_rgKetersediaan.checkedRadioButtonId == -1) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_param, "ketersediaan")), Toast.LENGTH_SHORT).show()
            return
        }
        if (posm_rgPenempatan.checkedRadioButtonId == -1) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_param, "penempatan")), Toast.LENGTH_SHORT).show()
            return
        }
        if (posm_rgKondisi.checkedRadioButtonId == -1) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_param, "kondisi")), Toast.LENGTH_SHORT).show()
            return
        }
        if (!isAvailablePhoto) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "posm"), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        val ketersediaan = Integer.parseInt(findViewById<View>(posm_rgKetersediaan.checkedRadioButtonId).contentDescription.toString())
        val penempatan = Integer.parseInt(findViewById<View>(posm_rgPenempatan.checkedRadioButtonId).contentDescription.toString())
        val kondisi = Integer.parseInt(findViewById<View>(posm_rgKondisi.checkedRadioButtonId).contentDescription.toString())

        if (item == null) {
            val posmTypeSelected = posm_spinType.selectedItem as TPosmType
            if(!isExistPosm(posmTypeSelected.posmTypeId, mPlanogramRegionId!!)) {
                item = TReportPosm()
                item!!.visitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
                item!!.storeId = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_ID, "")
                item!!.surveyorId = Login.getUserId(this)
                item!!.planogramRegionId = mPlanogramRegionId
                item!!.posmPlanogramId = Helper.generatePosmId(posmTypeSelected.posmTypeId, mPlanogramRegionId!!)
                item!!.posmTypeId = posmTypeSelected.posmTypeId
                item!!.posmTypeName = posmTypeSelected.posmTypeName
                item!!.planogramTypeId = 0
                item!!.planogramTypeName = ""
                item!!.ketersediaan = ketersediaan
                item!!.penempatan = penempatan
                item!!.kondisi = kondisi
                item!!.photoPath = mPhotoPath
                item!!.isNew = Config.YES
                item!!.isDone = true
                itemDao.insert(item)
            }
        } else {
            item!!.ketersediaan = ketersediaan
            item!!.penempatan = penempatan
            item!!.kondisi = kondisi
            item!!.photoPath = mPhotoPath
            item!!.isDone = true
            itemDao.update(item)
        }
        finish()
    }

    private fun isExistPosm(posmTypeId: Int, planogramRegionId: String): Boolean {
        return itemDao.queryBuilder()
                .where(TReportPosmDao.Properties.PosmTypeId.eq(posmTypeId),
                        TReportPosmDao.Properties.PlanogramRegionId.eq(planogramRegionId),
                        TReportPosmDao.Properties.VisitId.eq(mVisitId)).list().size > 0
    }

    private fun callIntentCamera() {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("nama_file", "posm_" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))
        startActivityForResult(intent, mPhotoType)
    }

    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
            callIntentCamera()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent ?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == requestCodeCamera && resultCode == Activity.RESULT_OK) {
                mPhotoPath = data?.extras!!.getString("path")
                posm_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath))
                posm_imgPhoto.scaleType = ImageView.ScaleType.CENTER
                isAvailablePhoto = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isAvailablePhoto = false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }
}
