package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.CompetitorsAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.io.File
import java.util.*

class CompetitorListActivity : AppCompatActivity(), Loadable {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportCompetitorDao
    private lateinit var items: MutableList<TReportCompetitor>
    private var adapter: CompetitorsAdapter? = null
    private var progressDialog: ProgressDialog? = null
    private lateinit var planograms:MutableList<TReportPlanogram>
    private lateinit var planogramProducts:MutableList<TReportProductPlanogram>
    private lateinit var planogramsize: MutableList<TReportPlanogram>
    private lateinit var campaigns:MutableList<TReportCampaignHeader>
    private lateinit var compliances:MutableList<TReportCompliance>

    private var surveyId:Int=0
    private var storeId:Int=0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_competitor_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_daftar_competitor)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        surveyId=Login.getEmployeeSurveyId(this)
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportCompetitorDao
        items = itemDao.queryBuilder().where(TReportCompetitorDao.Properties.VisitId.eq(mVisitId)).list()
        progressDialog = ProgressDialog(this)

        if(Login.getEmployeeRoleId(this@CompetitorListActivity) !=12 || Login.getEmployeeRoleId(this)!=14 || Login.getEmployeeRoleId(this) != 15){
            planograms = daoSession.tReportPlanogramDao.queryBuilder()
                    .where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId),
                            TReportPlanogramDao.Properties.IsPilih.eq(Config.YES_CODE)).list()

            planogramsize = daoSession.tReportPlanogramDao.queryBuilder()
                    .where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId),
                            TReportPlanogramDao.Properties.IsDone.eq(false)).list()
        }

        campaigns = daoSession.tReportCampaignHeaderDao.queryBuilder()
                .where(TReportCampaignHeaderDao.Properties.VisitId.eq(mVisitId),
                        TReportCampaignHeaderDao.Properties.StoreId.eq(storeId),
                        TReportCampaignHeaderDao.Properties.IsDone.eq(true)).list()

        compliances = daoSession.tReportComplianceDao.queryBuilder()
                .where(TReportComplianceDao.Properties.VisitId.eq(mVisitId),
                        TReportComplianceDao.Properties.IsDone.eq(true)).list()


    }

    private fun initView() {
        if(Login.getEmployeeRoleId(this)==12 || Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
            supportActionBar!!.title = "Selesai Toko"
        }
        lblNoData = findViewById(R.id.competitor_lblNoData)
        recycler = findViewById(R.id.competitor_recycler)
        val btnLanjut = findViewById<Button>(R.id.competitor_btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }
    }


    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun reloadList() {
        items = itemDao.queryBuilder().where(TReportCompetitorDao.Properties.VisitId.eq(mVisitId)).list()
        lblNoData.visibility = View.GONE
        if (items.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }

        adapter = CompetitorsAdapter(items, R.layout.activity_competitor_list_row, object : CompetitorsAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportCompetitor) {
                val intent = Intent(this@CompetitorListActivity, CompetitorAddActivity::class.java)
                intent.putExtra("Id", item.id)
                startActivity(intent)
            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun validateForm() {
        val planogramsMaster = daoSession.tPlanogramDao.queryBuilder().where(TPlanogramDao.Properties.StoreId.eq(storeId)).list()
        val campaignsMaster = daoSession.tCampaignDao.queryBuilder().where(TCampaignDao.Properties.StoreId.eq(storeId),
                TCampaignDao.Properties.SurveyorTypeId.eq(Login.getEmployeeRoleId(this@CompetitorListActivity))).list()

        if(planogramsMaster.size>0){
            if (planogramsize.size>0) {
                Toast.makeText(this,"harap mengerjakan semua report display",Toast.LENGTH_SHORT).show()
                return
            }
        }
//        if(campaignsMaster.size>0) {
//            if (campaigns.size == 0) {
//                Toast.makeText(this, "Anda belum mengerjakan report picos", Toast.LENGTH_SHORT).show()
//                return
//            }
//        }

        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, i ->
            submitForm()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {

        try {


        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val reports = ArrayList<GenericReport>()

        //region add report planogram to queue

        Log.i("complianceSize",compliances.size.toString())


        if(Login.getEmployeeRoleId(this@CompetitorListActivity) !=12 || Login.getEmployeeRoleId(this)!=14 || Login.getEmployeeRoleId(this)!=15){
            if(planograms.size > 0) {
                for (o in planograms){
                    planogramProducts=daoSession.tReportProductPlanogramDao.queryBuilder()
                            .where(TReportProductPlanogramDao.Properties.VisitId.eq(mVisitId),
                                    TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(o.planogramRegionId)).list()

                    var planogramProductValues=""
                    for (x in planogramProducts){
                        planogramProductValues += String.format(Locale.US, "%d,%d,%d,%s,%d;", x.posisi, x.productId,x.planogramTypeId,x.planogramStoreId,x.isListing)

                    }
                    if (planogramProductValues != "") {
                        planogramProductValues = planogramProductValues.substring(0, planogramProductValues.length - 1)
                    }


                    val reportPlanoId = "PLN${o.planogramRegionId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                    val params = ArrayList<ReportParameter>()
                    params.add(ReportParameter("1", reportPlanoId, "visit_id", o.visitId, ReportParameter.TEXT))
                    params.add(ReportParameter("2", reportPlanoId, "planogram_id", o.planogramRegionId, ReportParameter.TEXT))
                    params.add(ReportParameter("3", reportPlanoId, "is_comply", o.isComply.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("4", reportPlanoId, "is_new", o.isNew, ReportParameter.TEXT))
                    params.add(ReportParameter("5", reportPlanoId, "planogram_type_id", o.planogramTypeId.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("6", reportPlanoId, "store_id", o.storeId.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("7", reportPlanoId, "category_id", o.productCategoryId.toString(), ReportParameter.TEXT))
                    if(o.beforePhotoPath != "") params.add(ReportParameter("8", reportPlanoId, "before_photo_file", o.beforePhotoPath, ReportParameter.FILE))
                    if(o.afterPhotoPath != "") params.add(ReportParameter("9", reportPlanoId, "after_photo_file", o.afterPhotoPath, ReportParameter.FILE))
                    if(o.photoPath3 != "") params.add(ReportParameter("10", reportPlanoId, "photo_file3", o.photoPath3, ReportParameter.FILE))
                    if(o.photoPath4 != "") params.add(ReportParameter("11", reportPlanoId, "photo_file4", o.photoPath4, ReportParameter.FILE))
                    if(o.photoPath5 != "") params.add(ReportParameter("12", reportPlanoId, "photo_file5", o.photoPath5, ReportParameter.FILE))
                    if(o.photoPath6 != "") params.add(ReportParameter("13", reportPlanoId, "photo_file6", o.photoPath6, ReportParameter.FILE))
                    if(o.photoPath7 != "") params.add(ReportParameter("14", reportPlanoId, "photo_file7", o.photoPath7, ReportParameter.FILE))
                    if(o.photoPath8 != "") params.add(ReportParameter("15", reportPlanoId, "photo_file8", o.photoPath8, ReportParameter.FILE))
                    params.add(ReportParameter("16", reportPlanoId, "product_values", planogramProductValues, ReportParameter.TEXT))

                    val report = GenericReport(reportPlanoId,
                            Login.getUserId(this).toString(),
                            "Display",
                            "Display: $mVisitId-${o.planogramRegionId} (${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                            ApiClient.getInsertPlanogramUrl(),
                            currentDate, Config.NO_CODE, currentUtc, params)
                    reports.add(report)

                    //region add report posm to queue
                    val posms = daoSession.tReportPosmDao.queryBuilder()
                            .where(TReportPosmDao.Properties.VisitId.eq(mVisitId),
                                    TReportPosmDao.Properties.PlanogramRegionId.eq(o.planogramRegionId),
                                    TReportPosmDao.Properties.IsDone.eq(Config.YES_CODE)).list()
                    if(posms.size > 0) {
                        for (p in posms){
                            val reportIdPosm = "PSM${p.posmPlanogramId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                            val paramsPosm = ArrayList<ReportParameter>()
                            paramsPosm.add(ReportParameter("1", reportIdPosm, "visit_id", p.visitId, ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("2", reportIdPosm, "posm_id", p.posmPlanogramId, ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("3", reportIdPosm, "ketersediaan", p.ketersediaan.toString(), ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("4", reportIdPosm, "penempatan", p.penempatan.toString(), ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("5", reportIdPosm, "kondisi", p.kondisi.toString(), ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("6", reportIdPosm, "is_new", p.isNew, ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("7", reportIdPosm, "posm_type_id", p.posmTypeId.toString(), ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("8", reportIdPosm, "planogram_id", p.planogramTypeId.toString(), ReportParameter.TEXT))
                            paramsPosm.add(ReportParameter("9", reportIdPosm, "photo_file", p.photoPath, ReportParameter.FILE))
                            val reportPosm = GenericReport(reportIdPosm,
                                    Login.getUserId(this).toString(),
                                    "POSM",
                                    "POSM: $mVisitId-${p.posmPlanogramId} (${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                                    ApiClient.getInsertPosmUrl(),
                                    currentDate, Config.NO_CODE, currentUtc, paramsPosm)
                            reports.add(reportPosm)
                        }
                    }
                    //endregion
                }
            }
            //endregion

            val surveyPrice = daoSession.tReportSurveyPriceDao.queryBuilder()
                    .where(TReportSurveyPriceDao.Properties.Beli.gt(-1),
                            TReportSurveyPriceDao.Properties.Jual.gt(-1),
                            TReportSurveyPriceDao.Properties.VisitId.eq(mVisitId)
                    ).list()

            if(surveyPrice.size > 0) {
                var values = ""
                for (item in surveyPrice!!) {
                    values += String.format(Locale.US, "%d,%f,%f;", item.productId, item.beli,item.jual)
                }

                if (values != "") {
                    values = values.substring(0, values.length - 1)
                }
                Log.i("dataValue",values)
                val reportSurveyId = "SurveyPrice${mVisitId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                val params = ArrayList<ReportParameter>()
                params.add(ReportParameter("1", reportSurveyId, "visit_id", mVisitId!!, ReportParameter.TEXT))
                params.add(ReportParameter("2", reportSurveyId, "survey_id", surveyId.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("3", reportSurveyId, "values", values, ReportParameter.TEXT))

                Log.i("reportSurvey","${mVisitId},${surveyId},${values}")
                val report = GenericReport(reportSurveyId,
                        Login.getUserId(this).toString(),
                        "SurveyPrice",
                        "SurveyPrice: $mVisitId-(${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                        ApiClient.getInsertReportSurveyPrice(),
                        currentDate, Config.NO_CODE, currentUtc, params)


                reports.add(report)

            }


            //region add campaign posms to queue

            //endregion

            //region add report competitor to queue
            val competitors = daoSession.tReportCompetitorDao.queryBuilder()
                    .where(TReportCompetitorDao.Properties.VisitId.eq(mVisitId),
                            TReportCompetitorDao.Properties.IsDone.eq(Config.YES_CODE)).list()
            if(competitors.size > 0) {
                for (o in competitors){
                    val reportCompId = "COM${o.visitId}${o.activityTypeId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                    val params = ArrayList<ReportParameter>()
                    params.add(ReportParameter("1", reportCompId, "visit_id", o.visitId, ReportParameter.TEXT))
                    params.add(ReportParameter("2", reportCompId, "activity_type_id", o.activityTypeId.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("3", reportCompId, "product_id", o.productId.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("4", reportCompId, "description", o.description, ReportParameter.TEXT))
                    params.add(ReportParameter("5", reportCompId, "start_date", o.startDate.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("6", reportCompId, "end_date", o.endDate.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("7", reportCompId, "category_competitor_id", o.categoryId.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("8", reportCompId, "price", o.harga.toString(), ReportParameter.TEXT))
                    params.add(ReportParameter("9", reportCompId, "photo_file", o.photoPath, ReportParameter.FILE))
                    val report = GenericReport(reportCompId,
                            Login.getUserId(this).toString(),
                            "Competitor",
                            "Competitor: $mVisitId-${o.activityTypeId} (${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                            ApiClient.getInsertCompetitorUrl(),
                            currentDate, Config.NO_CODE, currentUtc, params)
                    reports.add(report)
                }
            }
            //endregion


        }



        val campaignPosms = daoSession.tReportCampaignDetailDao.queryBuilder()
                .where(TReportCampaignDetailDao.Properties.VisitId.eq(mVisitId),
                        TReportCampaignDetailDao.Properties.StoreId.eq(storeId),
                        TReportCampaignDetailDao.Properties.IsDone.eq(true)).list()


        //endregion

        if(campaignPosms.size > 0) {
            for (o in campaignPosms){
                val reportPicosId = "PicosDetail${o.visitId}${o.campaignId}${o.posmId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                Log.i("reportPicosId",reportPicosId.toString())

                val params = ArrayList<ReportParameter>()

                params.add(ReportParameter("1", reportPicosId, "visit_id", o.visitId, ReportParameter.TEXT))
                params.add(ReportParameter("2", reportPicosId, "campaign_id", o.campaignId.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("3", reportPicosId, "posm_id", o.posmId.toString(), ReportParameter.TEXT))
//                if(o.photoPath!=""){
//                    params.add(ReportParameter("4", reportId, "photo_file", o.photoPath, ReportParameter.FILE))
//                }

                params.add(ReportParameter("4", reportPicosId, "status_id", o.status.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("5", reportPicosId, "reason_id", o.alasan.toString(), ReportParameter.TEXT))
                if(o.photoPath!=""&& o.photoPath != null && File(o.photoPath).exists()){
                    params.add(ReportParameter("6", reportPicosId, "photo_file", o.photoPath, ReportParameter.FILE))
                }
                val reportDetail = GenericReport(reportPicosId,
                        Login.getUserId(this).toString(),
                        "PicosDetail",
                        "PicosDetail: $mVisitId-${o.campaignId}${o.posmId}(${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                        ApiClient.getInsertCampaignDetail(),
                        currentDate, Config.NO_CODE, currentUtc, params)
                reports.add(reportDetail)
            }
        }
//        val campaigns = daoSession.tReportCampaignHeaderDao.queryBuilder()
//                .where(TReportCampaignHeaderDao.Properties.VisitId.eq(mVisitId),
//                        TReportCampaignHeaderDao.Properties.IsDone.eq(true)).list()
        if(campaigns.size > 0) {
            for (x in campaigns){
                val reportId = "Picos${x.visitId}${x.campaignId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                Log.i("reportId",reportId.toString())

                val params = ArrayList<ReportParameter>()
                params.add(ReportParameter("1", reportId, "visit_id", x.visitId, ReportParameter.TEXT))
                params.add(ReportParameter("2", reportId, "campaign_id", x.campaignId.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("3", reportId, "is_available", x.isTersedia.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("4", reportId, "reason_id", x.reasonId.toString(), ReportParameter.TEXT))

                val reportPicos = GenericReport(reportId,
                        Login.getUserId(this).toString(),
                        "Picos",
                        "Picos: $mVisitId-${x.campaignId} (${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                        ApiClient.getInsertCampaignHeader(),
                        currentDate, Config.NO_CODE, currentUtc, params)
                reports.add(reportPicos)
            }
        }


        if(compliances.size > 0) {
            for (oo in compliances){
                val reportComplianceId = "Compliances${oo.visitId}${oo.campaignId}${oo.complianceId}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                Log.i("reportComplianceId",reportComplianceId.toString())

                val params = ArrayList<ReportParameter>()
                params.add(ReportParameter("1", reportComplianceId, "visit_id", oo.visitId, ReportParameter.TEXT))
                params.add(ReportParameter("2", reportComplianceId, "campaign_id", oo.campaignId.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("3", reportComplianceId, "compliance_id", oo.complianceId.toString(), ReportParameter.TEXT))
                params.add(ReportParameter("4", reportComplianceId, "is_comply", oo.isComply.toString(), ReportParameter.TEXT))

                val reportCompliance = GenericReport(reportComplianceId,
                        Login.getUserId(this).toString(),
                        "Compliance",
                        "Compliance: $mVisitId-${oo.campaignId}(${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                        ApiClient.getInsertCompliance(),
                        currentDate, Config.NO_CODE, currentUtc, params)
                reports.add(reportCompliance)

                val rnds = (0..100).random()

                val reportPhotoId = "CompliancesPhotos${oo.visitId}${oo.campaignId}${rnds}${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
                Log.i("reportPhotoId",reportPhotoId.toString())

                val paramsPhoto = ArrayList<ReportParameter>()
                paramsPhoto.add(ReportParameter("1", reportPhotoId, "visit_id", oo.visitId, ReportParameter.TEXT))
                paramsPhoto.add(ReportParameter("2", reportPhotoId, "campaign_id", oo.campaignId.toString(), ReportParameter.TEXT))
                paramsPhoto.add(ReportParameter("3", reportPhotoId, "compliance_id", oo.complianceId.toString(), ReportParameter.TEXT))
                if(oo.photoPath!=""&& oo.photoPath != null && File(oo.photoPath).exists()){
                    paramsPhoto.add(ReportParameter("4", reportPhotoId, "photo_file", oo.photoPath, ReportParameter.FILE))
                }


                val reportPhoto = GenericReport(reportPhotoId,
                        Login.getUserId(this).toString(),
                        "CompliancePhoto",
                        "CompliancePhoto: $mVisitId-${oo.campaignId}(${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                        ApiClient.getInsertCompliancePhoto(),
                        currentDate, Config.NO_CODE, currentUtc, paramsPhoto)
                reports.add(reportPhoto)


            }
        }

        if(Login.getEmployeeRoleId(this@CompetitorListActivity)==1||Login.getEmployeeRoleId(this@CompetitorListActivity)==2||Login.getEmployeeRoleId(this@CompetitorListActivity)==12 || Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
            //region add report visit out to queue
            val reportVisitId = "VOS$mVisitId${DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)}"
            val params = ArrayList<ReportParameter>()
            params.add(ReportParameter("1", reportVisitId, "visit_id", mVisitId!!, ReportParameter.TEXT))
            params.add(ReportParameter("2", reportVisitId, "check_out_datetime_utc",currentUtc.toString(), ReportParameter.TEXT))
            params.add(ReportParameter("3", reportVisitId, "check_out_datetime", currentDateTime, ReportParameter.TEXT))

            val rVisit = GenericReport(reportVisitId,
                    Login.getUserId(this).toString(), "CheckOut",
                    "CheckOut: $mVisitId (${DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong)})",
                    ApiClient.getUpdateVisitOutUrl(),
                    currentDate, Config.NO_CODE, currentUtc, params)
            reports.add(rVisit)
            //endregion

            //execute task

        }
        val task = TambahReportTask(this, reports)
        task.execute()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_competitor_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            back()
        }
        if (id == R.id.action_add) {
            val intent = Intent(this, CompetitorAddActivity::class.java)
            startActivity(intent)
        }
        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }


        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if(Login.getEmployeeRoleId(this)==12|| Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
            val menu1 = menu.findItem(R.id.action_dashboard)
            menu1.isVisible = false
            val menu2 = menu.findItem(R.id.action_add)
            menu2.isVisible = false
        }
        return true
    }

    private fun back() {
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "campaign")
        var intent = Intent(this, CampaignListActivity::class.java)
        if(Login.getEmployeeSurveyId(this@CompetitorListActivity)>0){
            SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "surveyprice")
            intent = Intent(this, SurveyPriceListActivity::class.java)
        }

        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        back()
    }
    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            if(Login.getEmployeeRoleId(this@CompetitorListActivity)==1 ||
                    Login.getEmployeeRoleId(this@CompetitorListActivity)==3 ||
                    Login.getEmployeeRoleId(this@CompetitorListActivity)==9 ||
                    Login.getEmployeeRoleId(this@CompetitorListActivity) ==10  ){
                SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "reseller")
                val intent = Intent(this, ResellerListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
            else{
                val storeId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
                val storeDao = daoSession.tStoreDao
                val store = storeDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(storeId)).limit(1).unique()
                store.isVisited = Config.YES_CODE
                storeDao.update(store)

                Login.clearSharedPrefVisitStore(this)
                val intent = Intent(this, StoreListActivity2::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()


            }

        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
