package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import com.iceteck.silicompressorr.SiliCompressor
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.adapters.DisplaysWeeklyAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.*
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import kotlinx.android.synthetic.main.activity_weekly_activity.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class WeeklyOperationActivity : AppCompatActivity(), Loadable {

    private lateinit var daoSession: DaoSession
//    private lateinit var localActivityTypeAdapter: ArrayAdapter<TDistributorArea>
    private lateinit var localWeeklyNameAdapter: ArrayAdapter<TWeekActivityName>
    private lateinit var mHeader: TReportWeeklyHeader
    internal var progressDialog: ProgressDialog? = null

    private var reportWeeklyActivity: MutableList<TReportWeeklyDetail> = ArrayList()

    private var mUserId = 0
    private var mRow=0
    private var mColumn=0
    private var displayHeaderId=""
    private var mDistributor=0L
    val requestCodeCamera=33




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weekly_activity)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_laporan_meeting_mingguan)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}"

        recognition_recycler.setFocusable(false)

        initData()
        initView()
//        initValuesToViews()
    }

    private fun initData() {
        mUserId=Login.getUserId(this)
        mRow=Login.getEmployeeRow2(this@WeeklyOperationActivity)
        mColumn=Login.getEmployeeColumn2(this@WeeklyOperationActivity)
        mDistributor=Login.getDistributor(this@WeeklyOperationActivity)

        daoSession = (application as GlobalApp).daoSession!!

//        localActivityTypeAdapter = ArrayAdapter(
//                this@WeeklyOperationActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tDistributorAreaDao.loadAll())
//        localActivityTypeAdapter.insert(TDistributorArea(null, 0, ""), 0)

        localWeeklyNameAdapter = ArrayAdapter(
                this@WeeklyOperationActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tWeekActivityNameDao.loadAll())
        localWeeklyNameAdapter.insert(TWeekActivityName(null, 0, ""), 0)


//        reportWeeklyActivity = daoSession.tReportWeeklyDetailDao.queryBuilder().where(TReportWeeklyDetailDao.Properties.UserId.eq(mUserId)).list()

//        if(reportWeeklyActivity.size>0){
//            mHeader=daoSession.tReportWeeklyHeaderDao.queryBuilder().where(TReportWeeklyHeaderDao.Properties.HeaderId.eq(reportWeeklyActivity.get(0).reportHeaderId)).limit(1).unique()
//        }

//        if (reportWeeklyActivity.isEmpty()) {


            displayHeaderId = "WO${mUserId}-${DateTime.now().toString("yyMMddHHmmssSS")}"
            var displayHeader=TReportWeeklyHeader()


            displayHeader.headerId=displayHeaderId
            displayHeader.userId=mUserId
            displayHeader.areaId=Login.getEmployeeArea(this@WeeklyOperationActivity)
            displayHeader.weeklyId=0
            displayHeader.distId=0L
            displayHeader.tanggal=""
            displayHeader.mekanisme=""
            displayHeader.tujuan=""
            displayHeader.result=""
            displayHeader.insight=""
            displayHeader.pesertaHadir=""
            displayHeader.photoRowSize=mRow
            displayHeader.photoColumnSize=mColumn




            daoSession.tReportWeeklyHeaderDao.insert(displayHeader)

            var col = 1
            var row = 1
            for (i in 1..(mColumn * mRow)) {
                val o = TReportWeeklyDetail()
                o.reportDetailId =  "${mUserId}." + row.toString() + col.toString() + "." + DateTime.now().toString("yMdHmsSSS")
                o.reportHeaderId = displayHeaderId
                o.userId = mUserId
                o.distId = 0
                o.photoPath = ""
                o.infoPhoto = ""
                o.columnNumber = col
                o.rowNumber = row
                o.leftPosition = GridPositionUtil.getLeftPosition(i, col)
                o.topPosition = GridPositionUtil.getTopPosition(i, mColumn, row)
                o.rightPosition = GridPositionUtil.getRightPosition(i, mColumn, col)
                o.bottomPosition = GridPositionUtil.getBottomPosition(i, mRow, mColumn, row)
                reportWeeklyActivity.add(o)
                daoSession.tReportWeeklyDetailDao.insert(o)
                if (col == mColumn) {
                    col = 0
                    row++
                }
                col++
            }
            reportWeeklyActivity = daoSession.tReportWeeklyDetailDao.queryBuilder().where(TReportWeeklyDetailDao.Properties.ReportHeaderId.eq(displayHeaderId)).list()
            if(reportWeeklyActivity.size>0){
                mHeader=daoSession.tReportWeeklyHeaderDao.queryBuilder().where(TReportWeeklyHeaderDao.Properties.HeaderId.eq(displayHeaderId)).limit(1).unique()//(reportWeeklyActivity.get(0).reportHeaderId)).limit(1).unique()
            }
//        }

        startCamera(requestCodeCamera)


    }

    private fun initView() {

//        spinDistributorType!!.setTitle("Pilih Distributor")
//        spinDistributorType!!.setPositiveButton("Tutup")

        spinActivityName!!.setTitle("Pilih Aktivitas")
        spinActivityName!!.setPositiveButton("Tutup")



        progressDialog = ProgressDialog(this)
        var mCalendar = Calendar.getInstance()
        var xCalendar = Calendar.getInstance()

        val dateStart = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            mCalendar.set(Calendar.YEAR, year)
            mCalendar.set(Calendar.MONTH, monthOfYear)
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            edPeriodeMulai.setText(sdf.format(mCalendar.getTime()))
        }

        edPeriodeMulai.setOnClickListener(View.OnClickListener {
            DatePickerDialog(
                    this@WeeklyOperationActivity, dateStart, mCalendar
                    .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                    mCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        })


        btnSimpan.setOnClickListener {
            validateForm()
        }
        
//        spinDistributorType.adapter = localActivityTypeAdapter

        spinActivityName.adapter = localWeeklyNameAdapter
        spinActivityName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val activityName = localWeeklyNameAdapter.getItem(position) as TWeekActivityName

                mHeader.weeklyId = activityName.weeklyNameId
//                mHeader.distributorName = distributor.distributorName


            }
        }

        refreshList()

    }
    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
//            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }



//    private fun initValuesToViews() {
//        if(mHeader!=null){
//            edActivityName.setText(mHeader.activityName)
//
//
//            if(mHeader!!.distId!=null){
//                if (mHeader!!.distId > 0) {
//                    val activityPosition = localActivityTypeAdapter.getPosition(daoSession.tDistributorAreaDao.queryBuilder().where(TDistributorAreaDao.Properties.DistributorId.eq(mHeader!!.distId)).limit(1).unique())
//                    spinDistributorType.setSelection(activityPosition)
//
//                }
//            }
//
//            edPeriodeMulai.setText(mHeader.tanggal)
//            edMekanism.setText(mHeader.mekanisme)
//            edPurpose.setText(mHeader.tujuan)
//            edResult.setText(mHeader.result)
//            edInsight.setText(mHeader.insight)
//            edPeserta.setText(mHeader.pesertaHadir)
//
//        }
//    }

  
    private fun validateForm() {
      
        if(!isValid()) {
            Toast.makeText(this, "Ambil foto activity terlebih dahulu", Toast.LENGTH_SHORT).show()
            return
        }

        if (!FormValidation.validateRequiredSpinner(this, spinActivityName, String.format(getString(R.string.info_msg_required_field_spinner), "Nama Aktivitas"))) {
            return
        }

//        if (!FormValidation.validateRequiredSpinner(this, spinDistributorType, String.format(getString(R.string.info_msg_required_field_spinner), "Tipe Distributor"))) {
//            return
//        }

        if (!FormValidation.validateRequiredText(this, edPeriodeMulai, getString(R.string.info_msg_required_field_text))) {
            return
        }

        if (!FormValidation.validateRequiredText(this, edMekanism, getString(R.string.info_msg_required_field_text))) {
            return
        }

        if (!FormValidation.validateRequiredText(this, edPurpose, getString(R.string.info_msg_required_field_text))) {
            return
        }

        if (!FormValidation.validateRequiredText(this, edResult, getString(R.string.info_msg_required_field_text))) {
            return
        }

        if (!FormValidation.validateRequiredText(this, edInsight, getString(R.string.info_msg_required_field_text))) {
            return
        }

        if (!FormValidation.validateRequiredText(this, edPeserta, getString(R.string.info_msg_required_field_text))) {
            return
        }
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun submitForm() {
//        val localTypeSelected=spinDistributorType.selectedItem as TDistributorArea
        val weekNameSelected=spinActivityName.selectedItem as TWeekActivityName

        mHeader.weeklyId=weekNameSelected.weeklyNameId
        mHeader.tanggal=edPeriodeMulai.text.toString()
        mHeader.distId=mDistributor
        mHeader.mekanisme=edMekanism.text.toString()
        mHeader.tujuan=edPurpose.text.toString()
        mHeader.result=edResult.text.toString()
        mHeader.insight=edInsight.text.toString()
        mHeader.pesertaHadir=edPeserta.text.toString()
        daoSession.tReportWeeklyHeaderDao.update(mHeader)

        if(reportWeeklyActivity.isNotEmpty()) {
            for (o in reportWeeklyActivity) {
                o.distId=mDistributor
                if(o.photoPath != "") {
                    daoSession.tReportWeeklyDetailDao.update(o)

                }
            }
        }

        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val datePath = DateTime.now().toString(Config.DATE_FORMAT_PATH)
        val reportWeeklyId = "WO" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val userId=Login.getUserId(this@WeeklyOperationActivity)

        val reportData = ArrayList<GenericReport>()
//
        var weeklyDetailValues=""
        var weeklyHeaderValues=""
//
        var weeklyHeaders=daoSession!!.tReportWeeklyHeaderDao.queryBuilder().where(TReportWeeklyHeaderDao.Properties.HeaderId.eq(displayHeaderId)).list()
        var weeklyDetails=daoSession!!.tReportWeeklyDetailDao.queryBuilder().where(TReportWeeklyDetailDao.Properties.ReportHeaderId.eq(displayHeaderId)).list()



        for (x in weeklyHeaders){
            weeklyHeaderValues+="${x.headerId},${x.userId},${x.weeklyId},${x.areaId},${x.distId},${x.tanggal},${x.mekanisme},${x.tujuan},${x.result},${x.insight},${x.pesertaHadir},${x.photoRowSize},${x.photoColumnSize};"
        }

        for (v in weeklyDetails){
            weeklyDetailValues+="${v.reportDetailId},${v.reportHeaderId},${v.userId},${v.distId},${v.rowNumber},${v.columnNumber};"
        }

        if (weeklyHeaderValues != "") {
            weeklyHeaderValues = weeklyHeaderValues.substring(0, weeklyHeaderValues.length - 1)
        }
        if (weeklyDetailValues != "") {
            weeklyDetailValues = weeklyDetailValues.substring(0, weeklyDetailValues.length - 1)
        }

        Log.i("param-valuesWeekDetail",weeklyDetailValues)
        Log.i("param-valuesWeekHeader",weeklyHeaderValues)
        Log.i("param-currentDate",currentDateTime)
        Log.i("param-datePath",datePath)



        var namaReportWeekly = "Report Data Weekly"
        val paramsWeekly = ArrayList<ReportParameter>()
        paramsWeekly.add(ReportParameter("1", reportWeeklyId, "header_values", weeklyHeaderValues, ReportParameter.TEXT))
        paramsWeekly.add(ReportParameter("2", reportWeeklyId, "detail_values", weeklyDetailValues, ReportParameter.TEXT))
        paramsWeekly.add(ReportParameter("3", reportWeeklyId, "created_date", currentDateTime, ReportParameter.TEXT))



        val reportWeekly = GenericReport(reportWeeklyId,
                Login.getUserId(this).toString(),
                namaReportWeekly,
                "$namaReportWeekly: " + userId.toString() + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getInsertReportWeeklyOperation(),
                currentDate, Config.NO_CODE, currentUtc, paramsWeekly)

        reportData.add(reportWeekly)


        for(m in weeklyDetails) {
            if (m.photoPath != null && !m.photoPath.equals("")) {

                if(File(m.photoPath).exists()){
                    Log.i("param-photoPath",m.photoPath)

                    var reportWeeklyIdPhoto = "PDW" + m.reportDetailId+"_${m.id}" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
                    var namaReport = "Report Photo Detail Weekly"
                    val paramsDetail = ArrayList<ReportParameter>()
                    paramsDetail.add(ReportParameter("1", reportWeeklyIdPhoto, "report_id", m.reportDetailId, ReportParameter.TEXT))
                    paramsDetail.add(ReportParameter("2", reportWeeklyIdPhoto, "report_name", "WEEKLY OPERATION", ReportParameter.TEXT))
                    paramsDetail.add(ReportParameter("3", reportWeeklyIdPhoto, "photo_row_number", m.rowNumber.toString(), ReportParameter.TEXT))
                    paramsDetail.add(ReportParameter("4", reportWeeklyIdPhoto, "photo_column_number", m.columnNumber.toString(), ReportParameter.TEXT))
                    paramsDetail.add(ReportParameter("5", reportWeeklyIdPhoto, "photo_file", m.photoPath, ReportParameter.FILE))
                    paramsDetail.add(ReportParameter("6", reportWeeklyIdPhoto, "created_date", datePath, ReportParameter.TEXT))

                    Log.i("report_detail",m.reportDetailId)

                    val reportPhoto = GenericReport(reportWeeklyIdPhoto,
                            Login.getUserId(this).toString(),
                            namaReport,
                            "$namaReport: " + userId.toString() + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                            ApiClient.getInsertWeeklyOperationDetailPhoto(),
                            currentDate, Config.NO_CODE, currentUtc, paramsDetail)
                    reportData.add(reportPhoto)
                }

            }
        }
////
        val task = TambahReportTask(this, reportData)
        task.execute()
        SharedPrefsUtils.setBooleanPreference(this@WeeklyOperationActivity, Config.KEY_CHECKED_REPORT_WEEKLY_REPORT, true)
//        finish()
    }

    private fun isValid() : Boolean {
        if (reportWeeklyActivity.isEmpty()) {
            return true
        }

        for (o in reportWeeklyActivity) {
            if (o.photoPath != "") {
                return true
            }
        }

        return false
    }
    

    private fun refreshList() {
        var displaysAdapter = DisplaysWeeklyAdapter(this, reportWeeklyActivity, R.layout.activity_recognition_grid_row)
        recognition_recycler.layoutManager = SpanningGridLayoutManager(this, mColumn, SpanningGridLayoutManager.VERTICAL, false)
        recognition_recycler.adapter = displaysAdapter
        displaysAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == 33) {
                try {
                    val path = data.getStringExtra("path")
                    val index = path.lastIndexOf("_")
                    val index2 = path.lastIndexOf(":")
                    val assId = path.substring(index + 1, index2)
                    val position = path.substring(index2 + 1, path.length).toInt()
                    for (o in reportWeeklyActivity) {
                        if (o.reportDetailId == assId) {
                            o.photoPath = path
                            refreshList()
                            return
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            else if (requestCode == 34) {
                Log.i("galeri", "galeri")
                try {
                    val selectedImage = data!!.data
//                    val infoPhoto = data.getStringExtra("InfoPhotoGaleri")

                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    val imgDecodableString = cursor.getString(columnIndex)
                    cursor.close()
                    val filePath = SiliCompressor.with(this).compress(imgDecodableString)
                    val assId =  SharedPrefsUtils.getStringPreference(this@WeeklyOperationActivity, Config.DETAIL_ID, "")

                    Log.i("photoDir", assId)
//
//                    val index = selectedImage.toString().lastIndexOf("_")
//                    val index2 = selectedImage.toString().lastIndexOf(":")
//                    val assId = selectedImage.toString().substring(index + 1, index2)
//                    val position = selectedImage.toString().substring(index2 + 1, selectedImage.toString().length).toInt()

                    for (o in reportWeeklyActivity) {
                        if (o.reportDetailId == assId) {
                            o.photoPath = filePath
//                            o.infoPhoto = ""
//                            Log.i("jedur",o.infoPhoto)
                            refreshList()
                            return
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "Terjadi kesalahan, ulangi pilih foto", Toast.LENGTH_LONG).show()
                }
            }
        }
        else  if (resultCode == Activity.RESULT_CANCELED) {
            try{
                Toast.makeText(this,"Pengambilan foto gagal, ulangi mengambil foto",Toast.LENGTH_SHORT).show()
            }
            catch (ex:Exception){
                ex.printStackTrace()
            }

        }
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog!!.setTitle(title)
        progressDialog!!.setMessage(message)
        progressDialog!!.setCancelable(false)
        if (show) {
            progressDialog!!.show()
            //selesai_button.setEnabled(false);
        } else {
            progressDialog!!.dismiss()
            // selesai_button.setEnabled(true);
        }
    }

    override fun setFinish(result: Boolean, message: String) {

        if (result) {
            finish()
        }

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()

    }
    
}
