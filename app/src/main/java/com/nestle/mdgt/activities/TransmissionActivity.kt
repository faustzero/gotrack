package com.nestle.mdgt.activities

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.ListView
import com.nestle.mdgt.R
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.DatabaseReport
import com.nestle.mdgt.reports.ReportUploader
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.ZippingTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import java.io.File
import java.util.*

class TransmissionActivity : AppCompatActivity(), Loadable {

    private lateinit var reciever: MyBroadcastReciever
    private lateinit var toSend: Array<String>
    private lateinit var sent: Array<String>

    private var adapterTerkirim: ArrayAdapter<String>? = null
    private var adapterBelumTerkirim: ArrayAdapter<String>? = null
    private var databaseReport: DatabaseReport? = null
    private var listViewtoBeSent: ListView? = null
    private var listViewSent: ListView? = null
    private var progressDialog: ProgressDialog? = null
    private val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transmission_history)
        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_transmission_history)
        supportActionBar!!.subtitle = Login.getUsername(this)

        init()
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(Intent.ACTION_SEND)
        reciever = MyBroadcastReciever(this)
        registerReceiver(reciever, filter)
        update()
    }

    internal fun init() {
        listViewSent = findViewById(R.id.transmission_listView_sent)
        listViewtoBeSent = findViewById(R.id.transmission_listView_tobesent)
        listViewSent?.emptyView = DataController.getEmptyView(this)
        listViewtoBeSent?.emptyView = DataController.getEmptyView(this)

        update()
    }

    fun update() {
        if (!isLoading) {
            val thread = Thread(Runnable {
                isLoading = true
                try {
                    databaseReport = DatabaseReport.getDatabase(applicationContext)

                    toSend = DatabaseReport.getDeskripsi(databaseReport?.reportToBeSent!!)
                    sent = DatabaseReport.getDeskripsi(databaseReport?.reportSent!!)

                    runOnUiThread {
                        if (adapterBelumTerkirim != null) {
                            adapterBelumTerkirim!!.clear()
                            adapterBelumTerkirim!!.addAll(*toSend)
                            adapterBelumTerkirim!!.notifyDataSetChanged()
                        } else {
                            val itemToSend = ArrayList(Arrays.asList(*toSend))
                            adapterBelumTerkirim = ArrayAdapter(this@TransmissionActivity, R.layout.activity_transmission_list_row, itemToSend)
                            listViewtoBeSent?.adapter = adapterBelumTerkirim
                        }

                        if (adapterTerkirim != null) {
                            adapterTerkirim!!.clear()
                            adapterTerkirim!!.addAll(*sent)
                            adapterTerkirim!!.notifyDataSetChanged()
                        } else {
                            val itemSent = ArrayList(Arrays.asList(*sent))
                            adapterTerkirim = ArrayAdapter(this@TransmissionActivity, R.layout.activity_transmission_list_row, itemSent)
                            listViewSent?.adapter = adapterTerkirim

                        }
                    }

                    Log.i("accepet reciever", "eksekusi reciever")

                } catch (e: Exception) {
                    e.printStackTrace()

                } finally {
                    isLoading = false
                }
            })

            thread.start()
        }
    }


    override fun onPause() {
        super.onPause()
        unregisterReceiver(reciever)

    }

    class MyBroadcastReciever : BroadcastReceiver {
        private var act: TransmissionActivity? = null

        constructor()

        constructor(activity: TransmissionActivity) {
            act = activity
        }

        override fun onReceive(context: Context, intent: Intent) {
            context.startService(Intent(context, ReportUploader::class.java))
            if (act != null)
                act!!.update()
        }

    }

    companion object {
        var isLoading: Boolean = false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_transmission, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
        }

        if (id == R.id.action_send_email) {
            sendEmail()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun sendEmail() {
        val sourcePath = StorageUtils.getDirectory(StorageUtils.DIRECTORY_ROOT)
        val outputPath = Environment.getExternalStorageDirectory().toString() + "/" + Login.getUsername(this) + "_nestlesmd$currentDateTime.zipa"
        val zippingTask = ZippingTask(this, sourcePath, outputPath)
        zippingTask.execute()
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            progressDialog!!.dismiss()
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        val mailto = arrayOf("nestle@pitjarus.com")
        EmailUtils.sendEmail(this, mailto, "Nestle SMD Folder - " + Login.getUsername(this), "", File(Environment.getExternalStorageDirectory(), Login.getUsername(this) + "_nestlesmd$currentDateTime.zipa"))
    }
}
