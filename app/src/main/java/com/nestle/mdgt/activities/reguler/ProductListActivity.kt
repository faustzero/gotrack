package com.nestle.mdgt.activities.reguler

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.MslsAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TReportMsl
import com.nestle.mdgt.database.TReportMslDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.mtoolbar.*

class ProductListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var txtSearch: EditText
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportMslDao
    private lateinit var items: MutableList<TReportMsl>
    private var adapter: MslsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_msl)
        supportActionBar!!.subtitle = "${Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportMslDao
        val productDao = daoSession.tProductDao

        items = itemDao.queryBuilder().where(TReportMslDao.Properties.VisitId.eq(mVisitId)).list()

        if (items.isEmpty()) {
            val products = productDao.queryBuilder().list()
            val productsSize = products.size
            if (productsSize > 0) {
                val objects = arrayOfNulls<TReportMsl>(productsSize)
                var o: TReportMsl
                for ((i, model) in products.withIndex()) {
                    o = TReportMsl()
                    o.visitId = mVisitId
                    o.storeId = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_ID, "")
                    o.surveyorId = Login.getUserId(this)
                    o.productId = model.productId
                    o.productCode = model.productCode
                    o.productName = model.productName
                    o.brandId = model.brandId
                    o.brandName = model.brandName
                    o.productCategoryId = model.productCategoryId
                    o.productCategoryName = model.productCategoryName
                    o.unitId1 = model.unitId1
                    o.unitName1 = model.unitName1
                    o.stock1 = 0
                    o.price1 = 0
                    o.unitId2 = model.unitId2
                    o.unitName2 = model.unitName2
                    o.stock2 = 0
                    o.price2 = 0
                    o.isDone = false
                    objects[i] = o
                }
                itemDao.insertInTx(objects.toList())
                this.items = itemDao.queryBuilder().where(TReportMslDao.Properties.VisitId.eq(mVisitId)).list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById(R.id.product_lblNoData)
        recycler = findViewById(R.id.product_recycler)
        txtSearch = findViewById(R.id.product_txtSearch)
        val btnLanjut = findViewById<Button>(R.id.product_btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }

        txtSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                searchProduct()
            }
            override fun afterTextChanged(editable: Editable) {
            }
        })
    }

    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun searchProduct() {
        items = if (txtSearch.text.toString().trim { it <= ' ' } == "") {
            itemDao.queryBuilder().where(TReportMslDao.Properties.VisitId.eq(mVisitId)).list()
        } else {
            val searchValue = "%" + txtSearch.text.toString().trim { it <= ' ' } + "%"
            itemDao.queryBuilder()
                    .where(TReportMslDao.Properties.VisitId.eq(mVisitId),
                            TReportMslDao.Properties.ProductName.like(searchValue)).list()
        }

        adapter = MslsAdapter(items, R.layout.activity_product_list_row, object : MslsAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportMsl) {
                val intent = Intent(this@ProductListActivity, ProductAddActivity::class.java)
                intent.putExtra("Id", item.id)
                startActivity(intent)
            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()

        lblNoData.visibility = View.GONE
        if (items.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }
    }

    private fun validateForm() {
        //region validation
//        if (!isAllItemsDone()) {
//            Toast.makeText(this, getString(R.string.info_msg_required_check_all_report_item, "sku"), Toast.LENGTH_SHORT).show()
//            return
//        }
        //endregion

        submitForm()
    }

    private fun submitForm() {
        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_MSL, true)
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "planogram")
        val intent = Intent(this, PlanogramListActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun isAllItemsDone(): Boolean {
        return if (items.isEmpty()) {
            true
        } else {
            itemDao.queryBuilder()
                    .where(TReportMslDao.Properties.VisitId.eq(mVisitId),
                            TReportMslDao.Properties.IsDone.eq(Config.YES_CODE))
                    .list().size == items.size
        }
    }

    public override fun onResume() {
        super.onResume()
        searchProduct()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_product_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }
}
