package com.nestle.mdgt.activities.reguler

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.SurveyPriceAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.NumberTextWatcherForThousand
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.activity_survey_list_row.*
import kotlinx.android.synthetic.main.mtoolbar.*

class SurveyPriceListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null
//    private var mStoreId: String? = null
    private var mSurveyorId: Int? = 0
//    private var mPlanogramRegionId: String? = null

    private var mId: Long = 0
    private lateinit var daoSession: DaoSession
    private var itemDao: TReportSurveyPriceDao? = null
    private var items: MutableList <TReportSurveyPrice>? = null
//    private var item: TReportSurveyPrice? = null
    private var adapter: SurveyPriceAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey_price)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_survey_price)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mSurveyorId = Login.getUserId(this)
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportSurveyPriceDao
        items = itemDao!!.queryBuilder().where(TReportSurveyPriceDao.Properties.VisitId.eq(mVisitId)).list()


        val surveyPriceDao = daoSession.tProductMSLDao
        if (items!!.isEmpty()) {
            val surveyPrices = surveyPriceDao.loadAll()
            Log.i("sizeMaster",surveyPrices.size.toString())

            val surverSize = surveyPrices.size
            if (surverSize > 0) {
                val objects = arrayOfNulls<TReportSurveyPrice>(surverSize)
                var o: TReportSurveyPrice
                for ((i, model) in surveyPrices.withIndex()) {
                    o = TReportSurveyPrice()
                    o.visitId = mVisitId
                    o.productId = model.productId
                    o.productName = model.productName
                    o.beli = -1.0
                    o.jual = -1.0
                    objects[i] = o
                }
                itemDao!!.insertInTx(objects.toList())
                items = itemDao!!.queryBuilder().where(TReportSurveyPriceDao.Properties.VisitId.eq(mVisitId)).list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById(R.id.survey_lblNoData)
        recycler = findViewById(R.id.survey_recycler)
        val btnLanjut = findViewById<Button>(R.id.survey_btnLanjut)
        btnLanjut.setOnClickListener { validateForm() }
    }

    private fun reloadList() {
        items = itemDao!!.queryBuilder().where(TReportSurveyPriceDao.Properties.VisitId.eq(mVisitId)).list()
        Log.i("sizeReport",items!!.size.toString())
        lblNoData.visibility = View.GONE
        if (items!!.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }

        adapter = SurveyPriceAdapter(items!!, R.layout.activity_survey_list_row)
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun validateForm() {
        //region validation
        if (!isAllItemsDone()) {
            Toast.makeText(this, getString(R.string.info_msg_required_check_all_report_item, "Harga Sku"), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion

        submitForm()
    }

    private fun submitForm() {
        for (o in items!!) {
            Log.i("ada",o.beli.toString())
            Log.i("ada",o.jual.toString())
            itemDao!!.update(o)
        }
        val intent = Intent(this, CompetitorListActivity::class.java)
        startActivity(intent)
        finish()
    }



    private fun isAllItemsDone(): Boolean {

        for (o in items!!){
            if (o.beli==-1.0 || o.jual==-1.0){
                return false
            }
        }
        return true
    }

    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    private fun back() {
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "campaign")
        val intent = Intent(this, CampaignListActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_activity_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            back()
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        back()
    }
}
