package com.nestle.mdgt.activities

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.EditText
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.nestle.mdgt.R
import com.nestle.mdgt.rests.ApiClient
import java.io.File

class FullImageIrInfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image_ir_info)
        val imageUrl = intent.getStringExtra("ImageUrl")
        val imageInfo = intent.getStringExtra("ImageInfo")
        val image = findViewById <PhotoView>(R.id.fullImage) as PhotoView
        val info = findViewById <EditText>(R.id.edInfoPhoto) as EditText

        if (File(imageUrl).exists()) {
            image.setImageBitmap(BitmapFactory.decodeFile(imageUrl))
            info.setText(imageInfo)
        } else {
            if(intent.getBooleanExtra("IsFullPath", false)) {
                Glide.with(this).asBitmap().load(imageUrl).into(image)
                info.setText(imageInfo)
            } else {
                Glide.with(this).asBitmap().load(ApiClient.hostUrl + imageUrl).into(image)
                info.setText(imageInfo)
            }
        }
    }
}
