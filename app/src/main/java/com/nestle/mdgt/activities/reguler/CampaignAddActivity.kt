package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.dhaval2404.imagepicker.ImagePicker
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.adapters.CampaignPosmAdapter
import com.nestle.mdgt.adapters.ComplianceAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.FormValidation
import com.nestle.mdgt.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_campaign_add.*
import kotlinx.android.synthetic.main.mtoolbar.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class CampaignAddActivity : AppCompatActivity() {
    private var mId: Long = 0

    private var mActionType: String = "simpan"

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportCampaignHeaderDao
    private lateinit var campaignPosmDao: TReportCampaignDetailDao
    private lateinit var items: MutableList<TReportCampaignDetail>
    private lateinit var compliances: MutableList<TReportCompliance>

    private var adapter: CampaignPosmAdapter? = null
    private var item: TReportCampaignHeader? = null
//    private var itemx: TReportCampaignDetail? = null
    private var mVisitId: String? = null
    private var tersedia=-1
    private var storeId:Int=0
    private var reasonId:Int=0
    private var mFilePath:String? = ""
    private var mAssId:String? = ""

    private lateinit var reasonAdapter: ArrayAdapter<TReasonCampaign>

    private lateinit var reasonPosmAdapter: ArrayAdapter<TReasonCampaignPosm>



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_campaign_add)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_tambah_planogram)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {
        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)

        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")

        mId = intent.getLongExtra("Id", 0)
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportCampaignHeaderDao
        campaignPosmDao = daoSession.tReportCampaignDetailDao
        item = itemDao.load(mId)

        items = campaignPosmDao.queryBuilder().where(TReportCampaignDetailDao.Properties.VisitId.eq(mVisitId),
                TReportCampaignDetailDao.Properties.CampaignId.eq(item!!.campaignId),
                TReportCampaignDetailDao.Properties.StoreId.eq(item!!.storeId)
                ).list()

        reasonAdapter = ArrayAdapter(this@CampaignAddActivity,
                android.R.layout.simple_spinner_dropdown_item, daoSession.tReasonCampaignDao.loadAll())
        reasonAdapter.insert(TReasonCampaign(null, 0, ""), 0)

        reasonPosmAdapter = ArrayAdapter(this@CampaignAddActivity,
                android.R.layout.simple_spinner_dropdown_item, daoSession.tReasonCampaignPosmDao.loadAll())
        reasonPosmAdapter.insert(TReasonCampaignPosm(null, 0, ""), 0)

        if (items.isEmpty()) {
            val campaignPosms = daoSession.tCampaignPosmDao.queryBuilder().where(TCampaignPosmDao.Properties.CampaignId.eq(item!!.campaignId)).list()
            val campaignPosmSize = campaignPosms.size
            if (campaignPosmSize > 0) {
                val objects = arrayOfNulls<TReportCampaignDetail>(campaignPosmSize)
                var o: TReportCampaignDetail
                for ((i, model) in campaignPosms.withIndex()) {
                    o = TReportCampaignDetail()
                    o.visitId = mVisitId
                    o.storeId = storeId
                    o.campaignId = model.campaignId
                    o.posmId = model.posmId
                    o.posmName = model.posmName
                    o.qrCode = model.qrCode
                    o.encode = model.encode
                    o.photoPath = ""
                    o.isDone = false
                    o.status = -1
                    o.alasan = 0
                    o.isQrMandatory = model.isQrMandatory

//                    if(Login.getEmployeeRoleId(this)==12 || Login.getEmployeeRoleId(this)==14 ){
                    if(model.isQrMandatory == 1){
                        o.isScanned=0
                    }
                    else{
                        o.isScanned=1
                    }
                    objects[i] = o
                }
                campaignPosmDao.insertInTx(objects.toList())
                this.items = campaignPosmDao.queryBuilder().where(TReportCampaignDetailDao.Properties.VisitId.eq(mVisitId),
                        TReportCampaignDetailDao.Properties.CampaignId.eq(item!!.campaignId),
                        TReportCampaignDetailDao.Properties.StoreId.eq(item!!.storeId)
                ).list()
            }
        }

        compliances = daoSession.tReportComplianceDao.queryBuilder()
                .where(TReportComplianceDao.Properties.VisitId.eq(mVisitId),
                        TReportComplianceDao.Properties.CampaignId.eq(item!!.campaignId)).list()


        Log.i("var-complianceSize",compliances.size.toString())


        if(compliances.isEmpty()) {
            var items = daoSession.tCampaignComplianceDao.queryBuilder()
                    .where(TCampaignComplianceDao.Properties.CampaignId.eq(item!!.campaignId))
                    .list()
            Log.i("var-complianceRawSize",items.size.toString())


            val itemsSize = items.size
            if (itemsSize > 0) {
                val arrcompliance = arrayOfNulls<TReportCompliance>(itemsSize)
                var compliance: TReportCompliance
                for ((i, model) in items.withIndex()) {

                    compliance = TReportCompliance()
                    compliance.visitId = mVisitId
                    compliance.campaignId = model.campaignId
                    compliance.complianceId = model.complianceId
                    compliance.complianceCode = model.complianceCode
                    compliance.compliance = model.compliance
                    compliance.photoPath = ""
                    compliance.isComply = -1
                    compliance.isDone = false
                    arrcompliance[i] = compliance
                }
                daoSession.tReportComplianceDao.insertInTx(arrcompliance.toList())

                compliances = daoSession.tReportComplianceDao.queryBuilder()
                        .where(TReportComplianceDao.Properties.VisitId.eq(mVisitId),
                                TReportComplianceDao.Properties.CampaignId.eq(item!!.campaignId)).list()

            }
        }
    }

    private fun initView() {
        if(Login.getEmployeeRoleId(this)==12|| Login.getEmployeeRoleId(this)==14){
            txtPicos.setText("Apakah tersedia salah satu posm ideal di toko?")
        }else if( Login.getEmployeeRoleId(this)==15){
            txtPicos.setText("Apakah tersedia salah satu posm MILO UHT di toko?")

        }
        spinReasonCampaign.adapter=reasonAdapter
        recyclerCompliance.visibility=View.GONE
        txtCompliance.visibility=View.GONE
        viewCompliance.visibility=View.GONE
        row_rgAvailable.setOnCheckedChangeListener { radioGroup, i ->
            val rb = radioGroup.findViewById<View>(i) as RadioButton?
            if (rb != null) {
                tersedia = rb.contentDescription.toString().toInt()
                if(tersedia==1){
                    spinReasonCampaign.visibility=View.GONE
                    txtReasonCampaign.visibility=View.GONE
                    recycler.visibility=View.VISIBLE

                    if(compliances.size>0){
                        recyclerCompliance.visibility=View.VISIBLE
                        txtCompliance.visibility=View.VISIBLE
                        viewCompliance.visibility=View.VISIBLE
                    }

                }
                else{
                    spinReasonCampaign.visibility=View.VISIBLE
                    txtReasonCampaign.visibility=View.VISIBLE
                    recycler.visibility=View.GONE

                    recyclerCompliance.visibility=View.GONE
                    txtCompliance.visibility=View.GONE
                    viewCompliance.visibility=View.GONE


                }
            }
        }

        btnSimpan.setOnClickListener {
            mActionType = "simpan"
            validateForm()
        }

    }

    private fun reloadList() {
//        items = campaignPosmDao.queryBuilder().where(TReportCampaignDetailDao.Properties.VisitId.eq(mVisitId),
//                TReportCampaignDetailDao.Properties.CampaignId.eq(item!!.campaignId)).list()

        adapter = CampaignPosmAdapter(daoSession,this@CampaignAddActivity,items,
                R.layout.activity_campaign_posm_list_row,reasonPosmAdapter, object : CampaignPosmAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportCampaignDetail) {

            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
//        picos_avaibility.text=itemx!!.posmName
    }

    private fun reloadListCompliance() {
        var complianceAdapter = ComplianceAdapter(compliances, R.layout.activity_comply_list_row, object : ComplianceAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportCompliance?) {

            }
        },this@CampaignAddActivity)
        recyclerCompliance.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerCompliance.adapter = complianceAdapter
        complianceAdapter.notifyDataSetChanged()
    }

    private fun initValuesToViews() {
        if (item != null) {
            lblCampaignName.text=item!!.campaignName
            lblPeriode.text="(${item!!.startDate})-(${item!!.endDate})"


        }
    }

    public override fun onResume() {
        super.onResume()
        reloadList()
        reloadListCompliance()
    }

    private fun validateForm() {
        if(tersedia==-1){
            Toast.makeText(this@CampaignAddActivity,"anda belum memilih status ketersediaan picos",Toast.LENGTH_SHORT).show()
            return
        }
        else if(tersedia==1){
            for (o in items){


//                if( !o.isDone){
//                    Toast.makeText(this@CampaignAddActivity,"anda belum foto semua posm",Toast.LENGTH_SHORT).show()
//                    return
//                }

                if( o.status == -1){
                    Toast.makeText(this@CampaignAddActivity,"anda belum memilih status posm",Toast.LENGTH_SHORT).show()
                    return
                }
                else{
                    if((o.photoPath == "" && o.status == 1)){
                        Toast.makeText(this@CampaignAddActivity,"anda belum foto semua posm",Toast.LENGTH_SHORT).show()
                        return
                    }
                    if(Login.getEmployeeRoleId(this)==12|| Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
                        if((o.alasan == 0 && o.status == 0)){
                            Toast.makeText(this@CampaignAddActivity,"anda belum mengisi reason posm",Toast.LENGTH_SHORT).show()
                            return
                        }
                    }

                }
            }


            for (o in compliances){


//                if( !o.isDone){
//                    Toast.makeText(this@CampaignAddActivity,"anda belum foto semua posm",Toast.LENGTH_SHORT).show()
//                    return
//                }

                if( o.isComply == -1){
                    Toast.makeText(this@CampaignAddActivity,"anda belum memilih status compliance picos",Toast.LENGTH_SHORT).show()
                    return
                }
                else{
                    if((o.photoPath == "" && o.isComply == 1)){
                        Toast.makeText(this@CampaignAddActivity,"anda belum foto compliance picos",Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            }


        }
        else{
            if (!FormValidation.validateRequiredSpinner(this, spinReasonCampaign, String.format(getString(R.string.info_msg_required_field_spinner), "Reason Picos"))) {
                return
            }
        }




        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        if(tersedia==1){
            for (o in items){
                o.isDone=true
                if(o.status==1){
                    o.alasan=-1
                }
                else{
                    if(o.alasan!=0){
                        o.photoPath=""
                    }
                }



                campaignPosmDao.update(o)
            }

            for (c in compliances){
                if(c.isComply==0){
                    c.photoPath=""
                }
                c.isDone=true

                daoSession.tReportComplianceDao.update(c)
            }
        }
        val selReason = spinReasonCampaign.selectedItem as TReasonCampaign
        if(selReason.reasonId>0){
            item!!.reasonId=selReason.reasonId
        }
        item!!.isTersedia=tersedia
        item!!.isDone=true
        itemDao.update(item)
        finish()
    }



    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && data != null) {
            Log.i("req_code",requestCode.toString())

            if (requestCode == 101) {
                try {
                    val path = data.getStringExtra("path")
                    val index = path.lastIndexOf("#")
                    val index2 = path.lastIndexOf("%")
                    val assId = path.substring(index + 1, index2)
                    val position = path.substring(index2 + 1, path.length).toInt()
                    Log.i("posmId",assId)
                    for (o in items) {

                        if (o.posmId == assId.toInt()&&o.campaignId==item!!.campaignId) {
                            o.photoPath = path
//                            o.isDone=true
                          //  campaignPosmDao.update(o)
                            reloadList()
                            return
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            if (requestCode == 102) {
                try {
                    val path = data.getStringExtra("path")
                    val index = path.lastIndexOf("#")
                    val index2 = path.lastIndexOf("%")
                    val assId = path.substring(index + 1, index2)
                    val position = path.substring(index2 + 1, path.length).toInt()
                    Log.i("posmId",assId)
                    for (o in compliances) {

                        if (o.complianceId == assId.toInt()&&o.campaignId==item!!.campaignId) {
                            o.photoPath = path
//                            o.isDone=true
                            //  campaignPosmDao.update(o)
                            reloadListCompliance()
                            return
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }else if(requestCode == 2404){

                try {
                    val selectedImage = data!!.data
                    ////                    val infoPhoto = data.getStringExtra("InfoPhotoGaleri")
//                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
//                    val cursor = contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
//                    cursor!!.moveToFirst()
//                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
//                    val imgDecodableString = cursor.getString(columnIndex)
//                    cursor.close()
//                    mFilePath = SiliCompressor.with(this).compress(imgDecodableString)

//                    Log.i("img_photo",mFilePath.toString())
//                    mFilePath = ImagePicker.getFilePath(data)
                    Log.i("img_photo2",ImagePicker.getFilePath(data).toString())
                    mFilePath = ImagePicker.getFilePath(data)
                    if(SharedPrefsUtils.getStringPreference(this@CampaignAddActivity, Config.COMPLIANCE_ID, "") != "x") {
                        mAssId = SharedPrefsUtils.getStringPreference(this@CampaignAddActivity, Config.COMPLIANCE_ID, "")
                        if (mFilePath != null && mAssId != null) {
                            for (o in compliances) {

                                if (o.complianceId == mAssId!!.toInt() && o.campaignId == item!!.campaignId) {
                                    o.photoPath = mFilePath
//                            o.isDone=true
                                    //  campaignPosmDao.update(o)
                                    reloadListCompliance()
                                    return
                                }
                            }
                        }
                    }
                    if(SharedPrefsUtils.getStringPreference(this@CampaignAddActivity, Config.POSM_ID, "") != "x") {
                        mAssId = SharedPrefsUtils.getStringPreference(this@CampaignAddActivity, Config.POSM_ID, "")
                        if (mFilePath != null && mAssId != null) {
                            for (o in items) {

                                if (o.posmId == mAssId!!.toInt()&&o.campaignId==item!!.campaignId) {
                                    o.photoPath = mFilePath
//                            o.isDone=true
                                    //  campaignPosmDao.update(o)
                                    reloadList()
                                    return
                                }
                            }
                        }
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "Terjadi kesalahan, ulangi pilih foto", Toast.LENGTH_LONG).show()
                }
            }
        }
    }



}
