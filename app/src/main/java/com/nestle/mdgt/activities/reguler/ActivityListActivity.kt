package com.nestle.mdgt.activities.reguler

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.ActivitiesAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TReportActivity
import com.nestle.mdgt.database.TReportActivityDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.mtoolbar.*

class ActivityListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportActivityDao
    private lateinit var items: MutableList<TReportActivity>
    private var adapter: ActivitiesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_picos)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportActivityDao
        val activityDao = daoSession.tActivityDao

        items = itemDao.queryBuilder().where(TReportActivityDao.Properties.VisitId.eq(mVisitId)).list()

        if (items.isEmpty()) {
            val activities = activityDao.queryBuilder().list()
            val activitiesSize = activities.size
            if (activitiesSize > 0) {
                val objects = arrayOfNulls<TReportActivity>(activitiesSize)
                var o: TReportActivity
                for ((i, model) in activities.withIndex()) {
                    o = TReportActivity()
                    o.visitId = mVisitId
                    o.storeId = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_ID, "")
                    o.surveyorId = Login.getUserId(this)
                    o.activityId = model.activityId
                    o.activityName = model.activityName
                    o.productCategoryId = model.productCategoryId
                    o.productCategoryName = model.productCategoryName
                    o.startDate = model.startDate
                    o.endDate = model.endDate
                    o.isAvailable = -1
                    o.photoPath = ""
                    o.isDone = false
                    objects[i] = o
                }
                itemDao.insertInTx(objects.toList())
                items = itemDao.queryBuilder().where(TReportActivityDao.Properties.VisitId.eq(mVisitId)).list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById(R.id.activity_lblNoData)
        recycler = findViewById(R.id.activity_recycler)
        val btnLanjut = findViewById<Button>(R.id.activity_btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }
    }

    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.infoFotoPopup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val text = layoutPopup.findViewById(R.id.infoFotoPopup_edText) as EditText
                val kamera = layoutPopup.findViewById(R.id.infoFotoPopup_btnCamera) as Button
                val galeri = layoutPopup.findViewById(R.id.infoFotoPopup_btnGalery) as Button


                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun reloadList() {
        items = itemDao.queryBuilder().where(TReportActivityDao.Properties.VisitId.eq(mVisitId)).list()
        lblNoData.visibility = View.GONE
        if (items.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }

        adapter = ActivitiesAdapter(this, items, R.layout.activity_activity_list_row)
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun validateForm() {
        //region validation
        if (!isAllItemsDone()) {
            Toast.makeText(this, getString(R.string.info_msg_required_check_all_report_item, "activity"), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion

        submitForm()
    }

    private fun submitForm() {
        for (o in items) {
            itemDao.update(o)
        }

        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_ACTIVITY, true)
//        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "competitor")

        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "reseller")


        val intent = Intent(this, ResellerListActivity::class.java)
        startActivity(intent)
    }

    private fun isAllItemsDone(): Boolean {
        if (items.isEmpty()) {
            return true
        }

        items
                .filter { it.isAvailable == -1 }
                .forEach { return false }

        return true
    }

    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    private fun back() {
        if (items != null) {
            for (o in items) {
                itemDao.refresh(o)
            }
        }
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "planogram")
        val intent = Intent(this, PlanogramListActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_activity_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            back()
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        back()
    }
}
