package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.activities.LoginActivity
import com.nestle.mdgt.activities.TransmissionActivity
import com.nestle.mdgt.adapters.CustomInfoWindowAdapter
import com.nestle.mdgt.adapters.MarketsAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TMarket
import com.nestle.mdgt.database.TMarketDao
import com.nestle.mdgt.database.TVisitMarket
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.models.MarketViewModel
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_market_list.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.*

open class MarketListActivity : AppCompatActivity(),
        GoogleMap.OnInfoWindowClickListener,
        OnMapReadyCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        Loadable {
    private val requestCodeTagLocation = 20

    private lateinit var daoSession: DaoSession
    private lateinit var marketDao: TMarketDao
    private lateinit var mMarkets: List<TMarket>

    private var mCurrentLocation: Location? = null
    private lateinit var mLocationManager: LocationManager
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mMap: GoogleMap? = null
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView
    private lateinit var marketsAdapter: MarketsAdapter
    private lateinit var marketViewModels: MutableList<MarketViewModel>

    private lateinit var mActionLocationType: String
    private lateinit var mActionSubmitType: String
    private var mMarketId: Int = 0
    private var mRegionId: Int = 0
    private lateinit var mVisitId: String
    private var radiusVerificationLimit: Int = 0

    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_market_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_daftar_pasar)
        supportActionBar!!.subtitle = Login.getUsername(this)

        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            val appPackageName = "com.google.android.gms"
            try {
                Toast.makeText(this, "Application need Google Play Services.\nGoogle Play Services not available or needs to be updated", Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/market/apps/details?id=" + appPackageName)))
            }

        }

        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mapFragment = supportFragmentManager.findFragmentById(R.id.market_map) as SupportMapFragment
        recycler = findViewById(R.id.market_recycler)

        initData()
        //initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mRegionId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_REGION_ID, 0)
        radiusVerificationLimit = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_ID, 0)
        daoSession = (application as GlobalApp).daoSession!!
        marketDao = daoSession.tMarketDao
        mMarkets = marketDao.queryBuilder().where(TMarketDao.Properties.RegionId.eq(mRegionId)).list()
        mActionLocationType = "CurrentLocation"
        progressDialog = ProgressDialog(this)
    }

    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = Config.INTERVAL
        mLocationRequest.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            market_lblLoading.visibility = View.GONE
            market_lblLoading.text = getString(R.string.label_mencari_lokasi)
            mActionLocationType = "CurrentLocation"
        }
    }

    override fun onLocationChanged(location: Location) {
        if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location
            if (LocationUtils.isFromMockProvider(this, location)) {
                stopLocationUpdates()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
            } else {
                when (mActionLocationType) {
                    "CurrentLocation" -> {
                        if (location.accuracy <= Config.ACCURACY_LIMIT) {
                            stopLocationUpdates()
                            mCurrentLocation = location
                            mapFragment.getMapAsync(this)
                        }
                    }
                    "UpdateLocation" -> {
                        if (location.accuracy <= Config.ACCURACY_SMALL_LIMIT) {
                            stopLocationUpdates()
                            mCurrentLocation = location
                            updateMarketLocationAndVisit(mMarketId, location.latitude, location.longitude)
                        }
                    }
                }
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        if (mMap != null) {
            mMap!!.clear()
        }
        mMap = map
        val peoplePoint = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
        var marketLocation: Location
        marketViewModels = ArrayList()

        for (o in mMarkets) {
            marketLocation = Location("MarketLocation")
            marketLocation.latitude = o.latitude
            marketLocation.longitude = o.longitude
            val distance = mCurrentLocation!!.distanceTo(marketLocation)

            val pinId: Float

            pinId = when {
                distance <= radiusVerificationLimit - 20 -> BitmapDescriptorFactory.HUE_GREEN
                distance <= radiusVerificationLimit -> BitmapDescriptorFactory.HUE_YELLOW
                else -> BitmapDescriptorFactory.HUE_RED
            }

            mMap!!.addMarker(MarkerOptions()
                    .position(LatLng(o.latitude, o.longitude))
                    .title(o.marketName)
                    .icon(BitmapDescriptorFactory.defaultMarker(pinId))
                    .snippet("${o.marketId};${o.marketCode};${o.address};${o.kecamatanName};$distance"))

            val marketViewModel = MarketViewModel()
            marketViewModel.market = o
            marketViewModel.distance = distance
            marketViewModels.add(marketViewModel)

        }

        marketsAdapter = MarketsAdapter(marketViewModels, R.layout.activity_market_list_row, object : MarketsAdapter.OnItemClickListener {
            override fun onItemClick(item: MarketViewModel) {
                if (item.market!!.isVisited == Config.NO_CODE) {
                    if (item.market!!.latitude == 0.toDouble() && item.market!!.longitude == 0.toDouble())  {
                        submitForm(item, false)
                    } else {
                        if (item.distance <= radiusVerificationLimit) {
                            submitForm(item, true)
                        } else {
                            Toast.makeText(this@MarketListActivity, "Anda berada diluar radius yang diperbolehkan untuk mengunjungi pasar", Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(this@MarketListActivity, item.market!!.marketName + " sudah Anda kunjungi", Toast.LENGTH_SHORT).show()
                }
            }
        })

        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = marketsAdapter

        mMap!!.addMarker(MarkerOptions()
                .position(peoplePoint)
                .title(Login.getEmployeeName(this@MarketListActivity))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_people_pin_36px))
                .snippet(""))

        mMap!!.addCircle(CircleOptions()
                .center(peoplePoint)
                .radius(radiusVerificationLimit.toDouble())
                .fillColor(resources.getColor(R.color.transparent_blue))
                .strokeColor(resources.getColor(R.color.piction_blue))
                .strokeWidth(5f))

        mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder()
                        .target(peoplePoint)
                        .zoom(14f)
                        .bearing(0f)
                        .tilt(0f)
                        .build()))

        mMap!!.setInfoWindowAdapter(CustomInfoWindowAdapter(this))
        mMap!!.setOnInfoWindowClickListener(this)
    }

    private fun submitForm(item: MarketViewModel, isAvailableMarketLocation: Boolean) {
        var message = getString(R.string.confirm_msg_visit_param, item.market!!.marketName)
        if (!isAvailableMarketLocation) {
            message = getString(R.string.confirm_msg_visit_update_lokasi_param, item.market!!.marketName)
        }

        val builder = android.app.AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(message)
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
            mMarketId = item.market!!.marketId
            if (!isAvailableMarketLocation) {
                mActionLocationType = "UpdateLocation"
                market_lblLoading.visibility = View.VISIBLE
                market_lblLoading.text = "Update lokasi ${item.market!!.marketName}. Mohon tunggu..."
                checkGPSPermission(requestCodeTagLocation)
            } else {
                visit(item.market!!.marketId)
            }
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun visit(marketId: Int) {
        mActionSubmitType = "Visit"
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val reportId = "VM" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        mVisitId = Helper.generateVisitIdMarket(Login.getUserId(this), marketId)

        //Insert visit
        val visitDao = daoSession.tVisitMarketDao
        val visit = TVisitMarket()
        visit.visitIdMarket = mVisitId
        visit.surveyorId = Login.getUserId(this)
        visit.marketId = marketId
        visit.isVisit = Config.YES_CODE
        visit.isCheckIn = Config.YES_CODE
        visit.reasonNoVisit = ""
        visit.startDateTimeUtc = currentUtc
        visit.endDateTimeUtc = currentUtc
        visit.startDateTime = currentDateTime
        visit.endDateTime = currentDateTime
        visitDao.insert(visit)

        //Add report visit to queue
        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "visit_id", visit.visitIdMarket, ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "market_id", visit.marketId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "surveyor_id", visit.surveyorId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("4", reportId, "is_visit", visit.isVisit.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("5", reportId, "is_checkin", visit.isCheckIn.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("6", reportId, "no_visit_reason", visit.reasonNoVisit, ReportParameter.TEXT))
        params.add(ReportParameter("7", reportId, "start_datetime_utc", visit.startDateTimeUtc.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("8", reportId, "end_datetime_utc", visit.endDateTimeUtc.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("9", reportId, "start_datetime", visit.startDateTime, ReportParameter.TEXT))
        params.add(ReportParameter("10", reportId, "end_datetime", visit.endDateTime, ReportParameter.TEXT))

        val report = GenericReport(reportId,
                Login.getUserId(this).toString(), "Visit Market",
                "Visit Market: " + mVisitId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getInsertVisitMarketUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)

        val task = TambahReportTask(this)
        task.execute(report)
    }

    private fun updateMarketLocationAndVisit(marketId: Int, latitude: Double, longitude: Double) {
        mActionSubmitType = "UpdateLocationAndVisit"
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        mVisitId = Helper.generateVisitIdMarket(Login.getUserId(this), marketId)

        //Add report update location to queue
        val reportId = "UML" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "market_id", marketId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "latitude", latitude.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "longitude", longitude.toString(), ReportParameter.TEXT))

        val reportUpdateLocation = GenericReport(reportId,
                Login.getUserId(this).toString(),
                "Update Market Location",
                "Update Market Location: " + marketId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getUpdateMarketLocationUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)

        //Add report visit to queue
        val visitDao = daoSession.tVisitMarketDao
        val visit = TVisitMarket()
        visit.visitIdMarket = mVisitId
        visit.surveyorId = Login.getUserId(this)
        visit.marketId = marketId
        visit.isVisit = Config.YES_CODE
        visit.isCheckIn = Config.YES_CODE
        visit.reasonNoVisit = ""
        visit.startDateTimeUtc = currentUtc
        visit.endDateTimeUtc = currentUtc
        visit.startDateTime = currentDateTime
        visit.endDateTime = currentDateTime
        visitDao.insert(visit)

        val reportIdVisit = "VM" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val paramVisits = ArrayList<ReportParameter>()
        paramVisits.add(ReportParameter("1", reportIdVisit, "visit_id", visit.visitIdMarket, ReportParameter.TEXT))
        paramVisits.add(ReportParameter("2", reportIdVisit, "market_id", visit.marketId.toString(), ReportParameter.TEXT))
        paramVisits.add(ReportParameter("3", reportIdVisit, "surveyor_id", visit.surveyorId.toString(), ReportParameter.TEXT))
        paramVisits.add(ReportParameter("4", reportIdVisit, "is_visit", visit.isVisit.toString(), ReportParameter.TEXT))
        paramVisits.add(ReportParameter("5", reportIdVisit, "is_checkin", visit.isCheckIn.toString(), ReportParameter.TEXT))
        paramVisits.add(ReportParameter("6", reportIdVisit, "no_visit_reason", visit.reasonNoVisit, ReportParameter.TEXT))
        paramVisits.add(ReportParameter("7", reportIdVisit, "start_datetime_utc", visit.startDateTimeUtc.toString(), ReportParameter.TEXT))
        paramVisits.add(ReportParameter("8", reportIdVisit, "end_datetime_utc", visit.endDateTimeUtc.toString(), ReportParameter.TEXT))
        paramVisits.add(ReportParameter("9", reportIdVisit, "start_datetime", visit.startDateTime, ReportParameter.TEXT))
        paramVisits.add(ReportParameter("10", reportIdVisit, "end_datetime", visit.endDateTime, ReportParameter.TEXT))

        val reportVisit = GenericReport(reportIdVisit,
                Login.getUserId(this).toString(),
                "Visit Market",
                "Visit Market: " + mVisitId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getInsertVisitMarketUrl(),
                currentDate, Config.NO_CODE, currentUtc, paramVisits)

        //add reports to task
        val task = TambahReportTask(this)
        task.execute(reportUpdateLocation, reportVisit)
    }

    override fun onInfoWindowClick(marker: Marker) {
        if (marker.title.trim { it <= ' ' } != Login.getEmployeeName(this@MarketListActivity)) {
            try {
                if (marker.snippet != "") {
                    val snippet = marker.snippet.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (java.lang.Float.parseFloat(snippet[4].trim { it <= ' ' }) <= radiusVerificationLimit) {
//                        val i = Intent(applicationContext, StoreListActivity::class.java)
//                        i.putExtra("MarketId", Integer.parseInt(snippet[0].trim { it <= ' ' }))
//                        startActivity(i)
                    } else {
                        Toast.makeText(this@MarketListActivity, "Anda berada diluar radius yang diperbolehkan untuk mengunjungi pasar", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: ArrayIndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {
        market_lblLoading.visibility = View.VISIBLE
        checkGPSPermission(requestCodeTagLocation)
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    private fun checkGPSPermission(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeTagLocation) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkGPSPermission(requestCodeTagLocation)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }

    private fun logout() {
        Thread({
            try {
                runOnUiThread {
                    setLoading(true, "Loading", "Logging Out...")
                }
                val imagePath = StorageUtils.getDirectory(StorageUtils.DIRECTORY_IMAGE)
                DataController.deleteFilesNotInReport(imagePath, applicationContext)

                runOnUiThread {
                    setLoading(false, "Loading", "Logging Out...")
                    Login.logout(this)
                    val iLogin = Intent(this, LoginActivity::class.java)
                    iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(iLogin)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_market_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_logout) {
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_msg_logout))
            builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> logout() }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }

        if (id == R.id.action_refresh) {
            if (mGoogleApiClient.isConnected) {
                mActionLocationType = "CurrentLocation"
                market_lblLoading.visibility = View.VISIBLE
                checkGPSPermission(requestCodeTagLocation)
            }
        }

        if (id == R.id.action_transmission) {
            val intent = Intent(this, TransmissionActivity::class.java)
            startActivity(intent)
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            val market = marketDao.queryBuilder().where(TMarketDao.Properties.MarketId.eq(mMarketId)).limit(1).unique()
            if (mActionSubmitType == "UpdateLocationAndVisit") {
                market.latitude = mCurrentLocation!!.latitude
                market.longitude = mCurrentLocation!!.longitude
                marketDao.update(market)
            }

            SharedPrefsUtils.setStringPreference(this, Config.KEY_VISIT_ID_MARKET, mVisitId)
            SharedPrefsUtils.setIntegerPreference(this, Config.KEY_MARKET_ID, market.marketId)
            SharedPrefsUtils.setStringPreference(this, Config.KEY_MARKET_NAME, market.marketName)

            //Intent
            val intent = Intent(this@MarketListActivity, StoreListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
