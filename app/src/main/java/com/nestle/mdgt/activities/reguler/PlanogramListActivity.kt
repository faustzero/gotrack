package com.nestle.mdgt.activities.reguler

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.adapters.PlanogramsAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TPlanogramDao
import com.nestle.mdgt.database.TReportPlanogram
import com.nestle.mdgt.database.TReportPlanogramDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.activity_planogram_list_row.*
import kotlinx.android.synthetic.main.mtoolbar.*
import java.security.cert.TrustAnchor

class PlanogramListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportPlanogramDao
    private lateinit var items: MutableList<TReportPlanogram>
    private var adapter: PlanogramsAdapter? = null
    private var storeId:Int=0
    private var storeName:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_planogram_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_list_display)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportPlanogramDao
        val planogramDao = daoSession.tPlanogramDao
        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        storeName= SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")!!

        items = itemDao.queryBuilder().where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId)).list()

        if (items.isEmpty()) {
            val planograms = planogramDao.queryBuilder().where(TPlanogramDao.Properties.StoreId.eq(storeId)).list()
            val planogramsSize = planograms.size
            Log.i("size plano",planogramsSize.toString())
            if (planogramsSize > 0) {
                val objects = arrayOfNulls<TReportPlanogram>(planogramsSize)
                var o: TReportPlanogram
                for ((i, model) in planograms.withIndex()) {
                    o = TReportPlanogram()
                    o.visitId = mVisitId
                    o.storeId = storeId.toString()
                    o.surveyorId = Login.getUserId(this)
                    o.planogramRegionId = model.planogramRegionId
                    o.storeId = model.storeId.toString()
                    o.storeName = model.storeName
                    o.planogramTypeId = model.planogramTypeId
                    o.planogramTypeName = model.planogramTypeName
                    o.productCategoryId = model.productCategoryId
                    o.productCategoryName = model.productCategoryName
                    o.guidelinePhotoPath = model.guidelinePhotoPath
                    o.isComply = -1
                    o.beforePhotoPath = ""
                    o.afterPhotoPath = ""
                    o.photoPath3 = ""
                    o.photoPath4 = ""
                    o.photoPath5 = ""
                    o.photoPath6 = ""
                    o.photoPath7 = ""
                    o.photoPath8 = ""
                    o.isDone = false
                    o.isPilih=false
                    o.isNew = Config.NO
                    o.isAllPosmsDone = false
                    o.isMsl = model.isMsl
                    objects[i] = o
                }
                itemDao.insertInTx(objects.toList())
                this.items = itemDao.queryBuilder().where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId)).list()
            }
        }
    }

    private fun initView() {
        lblNoData = findViewById(R.id.planogram_lblNoData)
        recycler = findViewById(R.id.planogram_recycler)
        val btnLanjut = findViewById<Button>(R.id.planogram_btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }
    }

    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post { mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun reloadList() {
        items = itemDao.queryBuilder().where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId)).list()
        lblNoData.visibility = View.GONE
        if (items.isEmpty()) {
            lblNoData.visibility = View.VISIBLE
        }

        adapter = PlanogramsAdapter(items, R.layout.activity_planogram_list_row, object : PlanogramsAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportPlanogram) {
                if(item.isDone){
                    Toast.makeText(this@PlanogramListActivity,"Display sudah dikerjakan",Toast.LENGTH_SHORT).show()
                }
                else{
                    val builder = android.app.AlertDialog.Builder(this@PlanogramListActivity)
                    builder.setTitle(getString(R.string.label_confirm))
                    builder.setMessage(getString(R.string.confirm_msg_yakin_planogram))
                    builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
                    val intent = Intent(this@PlanogramListActivity, PlanogramAddActivity::class.java)
                    intent.putExtra("Id", item.id)
                    intent.putExtra("mslName",item.planogramTypeName)
                    startActivity(intent)
                        }
                    builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss()
                    item.isDone=true
                    item.isPilih=false
                    itemDao.update(item)
                    reloadList()
                    }

                    val alertDialog = builder.create()
                    alertDialog.setCanceledOnTouchOutside(true)
                    alertDialog.show()
                }

            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    private fun validateForm() {
        //region validation
//        if (!isAllItemsDone()) {
//            Toast.makeText(this, getString(R.string.info_msg_required_check_all_report_item, "pajangan"), Toast.LENGTH_SHORT).show()
//            return
//        }
//        if (!isAllPosmItemsDone()) {
//            Toast.makeText(this, getString(R.string.info_msg_required_check_all_report_item, "posm dari planogram"), Toast.LENGTH_SHORT).show()
//            return
//        }
        //endregion

        submitForm()
    }

    private fun submitForm() {
        SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_PLANOGRAM, true)
        SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "campaign")
        val intent = Intent(this, CampaignListActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun isAllItemsDone(): Boolean {
        return if (items.isEmpty()) {
            true
        } else {
            itemDao.queryBuilder()
                    .where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId),
                            TReportPlanogramDao.Properties.IsDone.eq(Config.YES_CODE))
                    .list().size == items.size
        }
    }

    private fun isAllPosmItemsDone(): Boolean {
        return if (items.isEmpty()) {
            true
        } else {
            itemDao.queryBuilder()
                    .where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId),
                            TReportPlanogramDao.Properties.IsAllPosmsDone.eq(Config.YES_CODE))
                    .list().size == items.size
        }
    }

    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    private fun back() {
//        val intent = Intent(this, ProductListActivity::class.java)
//        startActivity(intent)
//        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_planogram_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            back()
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        back()
    }
}
