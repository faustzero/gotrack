package com.nestle.mdgt.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.hardware.Camera
import android.os.Bundle
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.Toast
import com.nestle.mdgt.R
import com.nestle.mdgt.utils.DateTimeUtils
import com.nestle.mdgt.utils.StorageUtils
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

class PhotoLandscapeActivity : Activity() {
    private lateinit var mSurfaceView: SurfaceView
    private lateinit var mWindowManager: WindowManager
    private lateinit var takePhoto: Button
    private lateinit var flash: CheckBox
    private lateinit var folder: String
    private lateinit var imgLeft: ImageView
    private lateinit var imgTop: ImageView
    private lateinit var imgRight: ImageView
    private lateinit var imgBottom: ImageView
    private var mFileName: String? = null
    internal var previewHolder: SurfaceHolder? = null
    internal var inPreview: Boolean = false
    internal var info: String? = null

    private var surfaceCallback: SurfaceHolder.Callback = object : SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder) {
            try {
                if (previewHolder != null)
                    camera!!.setPreviewDisplay(previewHolder)
                camera!!.startPreview()
            } catch (t: Throwable) {
                Toast.makeText(this@PhotoLandscapeActivity, t.message, Toast.LENGTH_LONG).show()
            }
        }

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            tes(width, height, holder)
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            if (camera != null)
                camera!!.stopPreview()
            inPreview = false
        }
    }

    private var callback: Camera.PictureCallback = Camera.PictureCallback { data, camera ->
        val path = "$folder/temp_$mFileName"
        val q: Bitmap? = BitmapFactory.decodeByteArray(data, 0, data.size)

        val matrix = Matrix()
        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            matrix.postRotate(-90f)
        } else {
            matrix.postRotate(0f)
        }
        val result: Bitmap? = Bitmap.createBitmap(q!!, 0, 0, q.width, q.height, matrix, true)
        try {
            val fos = FileOutputStream(File(path))
            result!!.compress(Bitmap.CompressFormat.JPEG, 85, fos)
            fos.flush()
            fos.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            val i = Intent()
            i.putExtra("path", path)
            i.putExtra("waktu", DateTimeUtils.waktuForReport)
            setResult(Activity.RESULT_OK, i)
            finish()
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        folder = StorageUtils.getDirectory(StorageUtils.DIRECTORY_IMAGE)
        inPreview = true
        mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        mFileName = intent.extras!!.getString("nama_file")
        info = intent.extras!!.getString("info")
        mCameraId = if(intent.extras!!.getInt("CameraFacing", Camera.CameraInfo.CAMERA_FACING_BACK) == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            Camera.CameraInfo.CAMERA_FACING_FRONT
        } else {
            Camera.CameraInfo.CAMERA_FACING_BACK
        }
        init()
    }

    private fun init() {
        setContentView(R.layout.activity_photo_landscape)

//        val pathLeft = intent.extras!!.getString("photo_path_left")
//        val pathTop = intent.extras!!.getString("photo_path_top")
//        val pathRight = intent.extras!!.getString("photo_path_right")
//        val pathBottom = intent.extras!!.getString("photo_path_bottom")

        mSurfaceView = findViewById(R.id.photoView)
        takePhoto = findViewById(R.id.takePhotoButton)
        flash = findViewById(R.id.flashPhotoCheck)
//        imgLeft = findViewById(R.id.photo_imgLeft)
//        imgTop = findViewById(R.id.photo_imgTop)
//        imgRight = findViewById(R.id.photo_imgRight)
//        imgBottom = findViewById(R.id.photo_imgBottom)

//        if(pathLeft != ""){
//            val origialBitmap = BitmapFactory.decodeFile(pathLeft)
//            val croppedBitmap = Bitmap.createBitmap(origialBitmap, (origialBitmap.width * 0.9).toInt(), 0, (origialBitmap.width * 0.1).toInt(), origialBitmap.height)
//            imgLeft.setImageBitmap(croppedBitmap)
//        }
//
//        if(pathTop != ""){
//            val origialBitmap = BitmapFactory.decodeFile(pathTop)
//            val croppedBitmap = Bitmap.createBitmap(origialBitmap, 0, (origialBitmap.height * 0.9).toInt(), origialBitmap.width, (origialBitmap.height * 0.1).toInt())
//            imgTop.setImageBitmap(croppedBitmap)
//        }
//
//        if(pathRight != ""){
//            val origialBitmap = BitmapFactory.decodeFile(pathRight)
//            val croppedBitmap = Bitmap.createBitmap(origialBitmap, 0, 0, (origialBitmap.width * 0.1).toInt(), origialBitmap.height)
//            imgRight.setImageBitmap(croppedBitmap)
//        }
//
//        if(pathBottom != ""){
//            val origialBitmap = BitmapFactory.decodeFile(pathBottom)
//            val croppedBitmap = Bitmap.createBitmap(origialBitmap, 0, 0, origialBitmap.width, (origialBitmap.height * 0.1).toInt())
//            imgBottom.setImageBitmap(croppedBitmap)
//        }

        flash.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val hasFlash = packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
                if (hasFlash) {
                    val p = camera!!.parameters
                    p.flashMode = Camera.Parameters.FLASH_MODE_ON
                    camera!!.parameters = p
                    camera!!.startPreview()
                }
            } else {
                val p = camera!!.parameters
                p.flashMode = Camera.Parameters.FLASH_MODE_OFF
                camera!!.parameters = p
                camera!!.startPreview()
            }
        }

        mSurfaceView.setOnClickListener {
            takePhoto.isEnabled = false
            mSurfaceView.isClickable = false
            try {
                camera!!.autoFocus { _, _ ->
                    takePhoto.isEnabled = true
                    mSurfaceView.isClickable = true
                }
            } catch (e: Exception){
                e.printStackTrace()
            }
        }

        takePhoto.setOnClickListener {
            takePhoto.isEnabled = false
            if (camera != null) {
                camera!!.takePicture(null, null, callback)
            }
        }

        try {
            releaseCameraAndPreview()
            camera = initCam()
            previewHolder = mSurfaceView.holder
            previewHolder!!.addCallback(surfaceCallback)
            previewHolder!!.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Tidak bisa membuka kamera. Pesan error: " + e.message, Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    private fun releaseCameraAndPreview() {
        //mPreview.setCamera(null);
        if (camera != null) {
            camera!!.release()
            camera = null
        }
    }

    internal fun tes(width: Int, height: Int, holder: SurfaceHolder) {
        if (inPreview) {
            camera!!.stopPreview()
            inPreview = false
        }
        val parameters = camera!!.parameters
        //	      Camera.Size size=getBestPreviewSize(width, height,parameters);

        val supportedPreviewsize = parameters.supportedPreviewSizes
        var bestPreviewSize: Camera.Size = parameters.previewSize
        val sizenow = bestPreviewSize.width + bestPreviewSize.height
        supportedPreviewsize
                .filter { it.width + it.height > sizenow }
                .forEach { bestPreviewSize = it }

        val orientation = mWindowManager.defaultDisplay.rotation
        val display = mWindowManager.defaultDisplay

//        if (display.rotation == Surface.ROTATION_0) {
//            parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height)
//            parameters.setRotation(Surface.ROTATION_0)
//            camera!!.setDisplayOrientation(90)
//        }
//
//        if (display.rotation == Surface.ROTATION_90) {
//            parameters.setPreviewSize(width, height)
//            parameters.setRotation(Surface.ROTATION_90)
//        }
//
//        if (display.rotation == Surface.ROTATION_180) {
//            parameters.setPreviewSize(height, width)
//            parameters.setRotation(Surface.ROTATION_180)
//        }
//
//        if (display.rotation == Surface.ROTATION_270) {
//            parameters.setPreviewSize(width, height)
//            parameters.setRotation(Surface.ROTATION_270)
//            camera!!.setDisplayOrientation(180)
//        }

        try {
            camera!!.parameters = parameters
            camera!!.setPreviewDisplay(holder)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        camera!!.startPreview()
        inPreview = true
    }

    private fun getBestPreviewSize(width: Int, height: Int, parameters: Camera.Parameters): Camera.Size? {
        var result: Camera.Size? = null

        for (size in parameters.supportedPreviewSizes) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size
                } else {
                    val resultArea = result.width * result.height
                    val newArea = size.width * size.height

                    if (newArea > resultArea) {
                        result = size
                    }
                }
            }
        }
        return result
    }

    public override fun onResume() {
        super.onResume()

        if (camera == null) {
            camera = initCam()
        }
        try {
            camera!!.setPreviewDisplay(previewHolder)
            camera!!.startPreview()
            inPreview = true
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    public override fun onStop() {
        super.onStop()
        if (camera != null)
            camera!!.release()
        inPreview = false
    }

    public override fun onPause() {
        super.onPause()
        if (inPreview) {
            camera!!.stopPreview()
            inPreview = false
        }

        if (camera != null)
            camera!!.release()
        camera = null
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (camera != null)
            camera!!.release()
    }

    companion object {
        internal var camera: Camera? = null
        internal var mCameraId: Int = 0
        internal fun initCam(): Camera {
            val c = Camera.open(mCameraId)
            val parameters = c.parameters
            val tes = parameters.supportedPictureSizes
            val thres = 1280 + 720 // 640 + 480 //1024 + 768;
            var s: Camera.Size? = null
            var gap = Integer.MAX_VALUE
            for (size in tes) {
                val temp = thres - (size.width + size.height)
                if (temp * temp < gap) {
                    s = size
                    gap = temp * temp
                }
            }
            parameters.setPictureSize(s!!.width, s.height)
            parameters.jpegQuality = 100
            parameters.jpegThumbnailQuality = 100
            c.parameters = parameters
            return c
        }
    }
}