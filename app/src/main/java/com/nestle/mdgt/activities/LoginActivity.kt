package com.nestle.mdgt.activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.reguler.*
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.models.LoginResponse
import com.nestle.mdgt.reports.DatabaseReport
import com.nestle.mdgt.reports.ReportUploader
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.rests.LoginApiInterface
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.util.*
import com.nestle.mdgt.services.NotifyWorker
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.nestle.mdgt.tasks.CheckUpdateTask
import com.nestle.mdgt.tasks.Loadable
import okhttp3.TlsVersion
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity(), Loadable {
    private lateinit var mAndroidId:String
    private lateinit var mAppVersion:String
    private lateinit var mDeviceData:String

    private var daoSession: DaoSession? = null
    internal lateinit var marketDao: TMarketDao
    internal lateinit var storeDao: TStoreDao
    internal lateinit var productDao: TProductDao
    internal lateinit var productCompetitorDao: TProductCompetitorDao
    internal lateinit var brandCompetitorDao: TBrandCompetitorDao
    internal lateinit var planogramDao: TPlanogramDao
    internal lateinit var posmDao: TPosmDao
    internal lateinit var activityDao: TActivityDao
    internal lateinit var activityTypeDao: TActivityTypeDao
    internal lateinit var planogramTypeDao: TPlanogramTypeDao
    internal lateinit var posmTypeDao: TPosmTypeDao
    internal lateinit var reasonNoVisitDao: TReasonNoVisitDao
    internal lateinit var storeTypeDao: TStoreTypeDao
    internal lateinit var distributorDao: TDistributorDao
    internal lateinit var distributorAreaDao: TDistributorAreaDao
    internal lateinit var areaDao: TAreaDao
    internal lateinit var kabupatenDao: TKabupatenDao
    internal lateinit var kecamatanDao: TKecamatanDao

    internal lateinit var nooRegionDao: TNooRegionDao
    internal lateinit var nooAreaDao: TNooAreaDao
    internal lateinit var nooDistributor: TNooDistributorDao

    internal lateinit var infoDao: TInfoDao
    internal lateinit var bannerDao: TBannerDao
    private lateinit var transmissionHistoryDao: TTransmissionHistoryDao
    private var progressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initData()
        initView()
    }

    private fun initData() {
        checkStoragePermission()
        mAndroidId = Helper.getAndroidId(this)
        mAppVersion = Helper.getVersion(this)
        mDeviceData = Helper.deviceData

        if (Login.isLoggedIn(this)) {
            val intent: Intent = if (!SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "").equals("")) {
                when (SharedPrefsUtils.getStringPreference(this, Config.KEY_IN_REPORT, "")) {
                    "planogram" -> Intent(this, PlanogramListActivity::class.java)
                    "campaign" -> Intent(this, CampaignListActivity::class.java)
                    "competitor" -> Intent(this, CompetitorListActivity::class.java)
                    "reseller" -> Intent(this, ResellerListActivity::class.java)
                    "surveyprice" -> Intent(this, SurveyPriceListActivity::class.java)


                    else ->Intent(this, PlanogramListActivity::class.java)
                }
            }
            else {
                when (SharedPrefsUtils.getStringPreference(this, Config.KEY_IN_REPORT, "")) {
                    "spv" -> Intent(this, MenuSpvActivity::class.java)

                    else ->Intent(this, StoreListActivity2::class.java)
                }

            }
            intent.putExtra("IsFromLogin", true)
            startActivity(intent)
            finish()
        }
    }

    private fun initView() {
        val appVersion = String.format(getString(R.string.label_app_version), mAppVersion, mAndroidId)
        login_txtAndroidId.text = appVersion

        login_btnLogin.setOnClickListener {
            val username = login_txtUsername.text.toString().trim()
            val password = login_txtPassword.text.toString().trim()
            if (username != "" && password != "") {
                val connectionDetectorUtil = ConnectionDetectorUtil(this@LoginActivity)
                if (connectionDetectorUtil.isConnectingToInternet) {
                    submitForm(username, password)
                } else {
                    Toast.makeText(applicationContext, getString(R.string.info_msg_no_network_connection), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(applicationContext, getString(R.string.info_msg_required_field_username_password), Toast.LENGTH_SHORT).show()
            }
        }


        checkUpdate.setOnClickListener{
//            Toast.makeText(this,"Versi aplikasi anda yg terbaru",Toast.LENGTH_SHORT).show()
            val connectionDetectorUtil = ConnectionDetectorUtil(this@LoginActivity)
            if (connectionDetectorUtil.isConnectingToInternet) {
                CekUpdate()
            } else {
                Toast.makeText(this, getString(R.string.info_msg_no_network_connection), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun CekUpdate() {

        val popUp = DataController.getPopup(this@LoginActivity, "Perhatian", "Aplikasi akan mengunduh versi terbaru jika ada dan akan menggunakan kuota internet apakah anda yakin melanjutkan update aplikasi?")
        popUp.setPositiveButton("Yakin") { dialogInterface, i ->
            val task = CheckUpdateTask(this@LoginActivity)
            task.execute()
        }

        popUp.setNegativeButton("Tidak") { dialogInterface, i -> dialogInterface.dismiss() }
        popUp.show()
    }

    private fun initDaoSession() {
        if ((application as GlobalApp).daoSession == null) {
            val database = StorageUtils.getDirectory(StorageUtils.DIRECTORY_DATABASE) + "/" + Config.DATABASE_NAME
            val helper = DaoMaster.DevOpenHelper(this, database)
            val db = helper.writableDb
            (application as GlobalApp).daoSession = DaoMaster(db).newSession()
        }
    }

    private fun setLogoutReminder() {
        try {
            // Jam 5 Sore
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.HOUR_OF_DAY, 17)
            val duration = TimeUnit.MILLISECONDS.toMinutes((calendar.timeInMillis - DateTimeUtils.waktuInLong))
            if (duration > 0) {
                val workTag = "LogoutReminderWork1"
                WorkManager.getInstance().cancelAllWorkByTag(workTag)
                val notificationWork = OneTimeWorkRequest.Builder(NotifyWorker::class.java)
                        .setInitialDelay(duration, TimeUnit.MINUTES)
                        .addTag(workTag)
                        .build()
                WorkManager.getInstance().enqueue(notificationWork)
            }

            // Jam 6 Sore
            val calendar2 = Calendar.getInstance()
            calendar2.set(Calendar.SECOND, 0)
            calendar2.set(Calendar.MINUTE, 0)
            calendar2.set(Calendar.HOUR_OF_DAY, 18)
            val duration2 = TimeUnit.MILLISECONDS.toMinutes((calendar2.timeInMillis - DateTimeUtils.waktuInLong))
            if (duration2 > 0) {
                val workTag = "LogoutReminderWork2"
                WorkManager.getInstance().cancelAllWorkByTag(workTag)
                val notificationWork2 = OneTimeWorkRequest.Builder(NotifyWorker::class.java)
                        .setInitialDelay(duration2, TimeUnit.MINUTES)
                        .addTag(workTag)
                        .build()
                WorkManager.getInstance().enqueue(notificationWork2)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun submitForm(username: String, password: String) {
        login_btnLogin.isEnabled = false
        login_progressBar.indeterminateDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        login_progressBar.visibility = View.VISIBLE
        daoSession = (application as GlobalApp).daoSession
        transmissionHistoryDao = daoSession!!.tTransmissionHistoryDao
        marketDao = daoSession!!.tMarketDao
        storeDao = daoSession!!.tStoreDao
        productDao = daoSession!!.tProductDao
        productCompetitorDao = daoSession!!.tProductCompetitorDao
        brandCompetitorDao = daoSession!!.tBrandCompetitorDao
        planogramDao = daoSession!!.tPlanogramDao
        posmDao = daoSession!!.tPosmDao
        activityDao = daoSession!!.tActivityDao
        activityTypeDao = daoSession!!.tActivityTypeDao
        planogramTypeDao = daoSession!!.tPlanogramTypeDao
        posmTypeDao = daoSession!!.tPosmTypeDao
        reasonNoVisitDao = daoSession!!.tReasonNoVisitDao
        storeTypeDao = daoSession!!.tStoreTypeDao
        distributorDao = daoSession!!.tDistributorDao
        areaDao = daoSession!!.tAreaDao
        kabupatenDao = daoSession!!.tKabupatenDao
        kecamatanDao = daoSession!!.tKecamatanDao
        infoDao = daoSession!!.tInfoDao
        bannerDao = daoSession!!.tBannerDao
        distributorAreaDao = daoSession!!.tDistributorAreaDao

        nooRegionDao = daoSession!!.tNooRegionDao
        nooAreaDao = daoSession!!.tNooAreaDao
        nooDistributor = daoSession!!.tNooDistributorDao

        transmissionHistoryDao.deleteAll()
        marketDao.deleteAll()
        storeDao.deleteAll()
        productDao.deleteAll()
        productCompetitorDao.deleteAll()
        brandCompetitorDao.deleteAll()
        planogramDao.deleteAll()
        posmDao.deleteAll()
        activityDao.deleteAll()
        activityTypeDao.deleteAll()
        planogramTypeDao.deleteAll()
        posmTypeDao.deleteAll()
        reasonNoVisitDao.deleteAll()
        storeTypeDao.deleteAll()
        distributorDao.deleteAll()
        areaDao.deleteAll()
        kabupatenDao.deleteAll()
        kecamatanDao.deleteAll()
        infoDao.deleteAll()
        bannerDao.deleteAll()
        distributorAreaDao.deleteAll()

        nooRegionDao.deleteAll()
        nooAreaDao.deleteAll()
        nooDistributor.deleteAll()

        daoSession!!.getTResellerDao().deleteAll()
        daoSession!!.getTResellerTypeDao().deleteAll()
        daoSession!!.tCampaignPosmDao.deleteAll()
        daoSession!!.tCampaignDao.deleteAll()
        daoSession!!.tReportCampaignHeaderDao.deleteAll()
        daoSession!!.tReportCampaignDetailDao.deleteAll()
        daoSession!!.tCategoryCompetitorDao.deleteAll()
        daoSession!!.tReasonCampaignDao.deleteAll()
        daoSession!!.tLocalActivityTypeDao.deleteAll()
        daoSession!!.tReportActivityHeaderDao.deleteAll()
        daoSession!!.tReportActivityDetailDao.deleteAll()
        daoSession!!.tReportSurveyPriceDao.deleteAll()
        daoSession!!.tProductMSLDao.deleteAll()
        daoSession!!.tReportWeeklyHeaderDao.deleteAll()
        daoSession!!.tReportWeeklyDetailDao.deleteAll()
        daoSession!!.tProductMSLRegionDao.deleteAll()
        daoSession!!.tReportProductPlanogramDao.deleteAll()
        daoSession!!.tDistributorStoreDao.deleteAll()
        daoSession!!.tLocalActivityNameDao.deleteAll()
        daoSession!!.tLocalAcitivityLocationDao.deleteAll()
        daoSession!!.tWeekActivityNameDao.deleteAll()
        daoSession!!.tRoleDao.deleteAll()

        daoSession!!.tCampaignComplianceDao.deleteAll()
        daoSession!!.tReasonCampaignPosmDao.deleteAll()
        daoSession!!.tReportComplianceDao.deleteAll()









        val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())

        val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
        val call = apiService.getLoginData(username, password, mAndroidId, mAppVersion, mDeviceData)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.isSuccessful) {
                    if (response.body().status == Config.STATUS_SUCCESS) {
                        try {
                            setLogoutReminder()
                            //User
                            val userAccount = response.body().userAccount
                            SharedPrefsUtils.clearPreferences(this@LoginActivity)
                            SharedPrefsUtils.setBooleanPreference(this@LoginActivity, Config.KEY_IS_LOGGED_IN, true)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ID, userAccount!!.surveyorId)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_NAME, userAccount.surveyorName)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROLE_ID, userAccount.surveyorTypeId)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROLE_NAME, userAccount.surveyorTypeName)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_USERNAME, userAccount.username)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_REGION_ID, userAccount.regionId)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_REGION_NAME, userAccount.regionName)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_ID, userAccount.radiusVerificationLimit)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_LOGIN_TIME, currentDateTimeString)
                            SharedPrefsUtils.setBooleanPreference(this@LoginActivity, Config.KEY_EMPLOYEE_FIRST_TIME_IN, true)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROW, userAccount.row)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROW2, userAccount.rowWor)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_COLUMN, userAccount.column)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_COLUMN2, userAccount.columnWor)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_SURVEY_ID, userAccount.surveyId)
                            SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_AREA_ID, userAccount.areaId)
                            SharedPrefsUtils.setLongPreference(this@LoginActivity, Config.KEY_DISTRIBUTOR_ID, userAccount.distributorId)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_DISTRIBUTOR_NAME, userAccount.distributorname)
                            SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.DETAIL_ID, "")


                            Log.i("WW1",userAccount.rowWor.toString())
                            Log.i("WW2",userAccount.columnWor.toString())



                            //Market
//                            if(response.body().markets != null) {
//                                val marketSize = response.body().markets!!.size
//                                if (marketSize > 0) {
//                                    val markets = arrayOfNulls<TMarket>(marketSize)
//                                    var market: TMarket
//                                    for ((i, model) in response.body().markets!!.withIndex()) {
//                                        market = TMarket()
//                                        market.marketId = model.marketId
//                                        market.marketCode = model.marketCode
//                                        market.marketName = model.marketName
//                                        market.address = model.address
//                                        market.kecamatanId = model.kecamatanId
//                                        market.kecamatanName = model.kecamatanName
//                                        market.kabupatenId = model.kabupatenId
//                                        market.kabupatenName = model.kabupatenName
//                                        market.areaId = model.areaId
//                                        market.areaName = model.areaName
//                                        market.regionId = model.regionId
//                                        market.regionName = model.regionName
//                                        market.latitude = model.latitude
//                                        market.longitude = model.longitude
//                                        market.isVisited = Config.NO_CODE
//                                        market.radiusVerificationLimit = model.radiusVerificationLimit
//                                        markets[i] = market
//                                    }
//                                    marketDao.insertInTx(markets.toList())
//                                }
//                            }
                            
                            //Store
                            if(response.body().stores != null) {
                                val storeSize = response.body().stores!!.size
                                if (storeSize > 0) {
                                    Log.i("store",response.body().stores!!.size.toString())
                                    val stores = arrayOfNulls<TStore>(storeSize)
                                    var store: TStore
                                    for ((i, model) in response.body().stores!!.withIndex()) {
                                        store = TStore()
                                        store.sId = model.sId
                                        store.marketId = model.marketId
                                        store.marketName = model.marketName
                                        store.storeId = model.storeId
                                        store.storeCode = model.storeCode
                                        store.storeName = model.storeName
                                        store.address = model.address
                                        store.phone = model.phone
                                        store.storeTypeId = model.storeTypeId
                                        store.storeTypeName = model.storeTypeName
                                        store.distributorId = model.distributorId
                                        store.distributorName = model.distributorName
                                        store.kecamatanId = model.kecamatanId
                                        store.kecamatanName = model.kecamatanName
                                        store.kabupatenId = model.kabupatenId
                                        store.kabupatenName = model.kabupatenName
                                        store.areaId = model.areaId
                                        store.areaName = model.areaName
                                        store.regionId = model.regionId
                                        store.regionName = model.regionName
                                        store.latitude = model.latitude
                                        store.longitude = model.longitude
                                        store.lastVisited = model.lastVisited
                                        store.lastVisitedUtc = model.lastVisitedUtc
                                        store.photoPath = model.photoPath
                                        store.isVisited = Config.NO_CODE
                                        store.radiusVerificationLimit = model.radiusVerificationLimit
                                        stores[i] = store
                                    }
                                    storeDao.insertInTx(stores.toList())
                                }
                            }

                            //Product
                            if(response.body().products != null) {
                                val productSize = response.body().products!!.size
                                if (productSize > 0) {
                                    val products = arrayOfNulls<TProduct>(productSize)
                                    var product: TProduct
                                    for ((i, model) in response.body().products!!.withIndex()) {
                                        product = TProduct()
                                        product.productRegionId = model.productRoleId
                                        product.regionId = model.roleId
                                        product.regionName = ""
                                        product.productId = model.productId
                                        product.productName = model.productName
                                        product.productCode = model.productCode
                                        product.produsenId = model.produsenId
                                        product.produsenName = model.produsenName
                                        product.brandId = model.brandId
                                        product.brandName = model.brandName
                                        product.productCategoryId = model.productCategoryId
                                        product.productCategoryName = model.productCategoryName
                                        product.unitId1 = model.unitId1
                                        product.unitName1 = model.unitName1
                                        product.price1 = model.price1
                                        product.unitId2 = model.unitId2
                                        product.unitName2 = model.unitName2
                                        product.price2 = model.price2
                                        product.isCompetitor = model.isCompetitor
                                        products[i] = product
                                    }
                                    productDao.insertInTx(products.toList())
                                }
                            }

                            //region Reseller
                            if (response.body().resellers != null) {
                                val resellerSize = response.body().resellers!!.size
                                if (resellerSize > 0) {
                                    var i = 0
                                    val resellers = arrayOfNulls<TReseller>(resellerSize)
                                    var reseller: TReseller
                                    for (model in response.body().resellers!!) {
                                        reseller = TReseller()
                                        reseller.storeId = model.getStoreId()
                                        reseller.storeCode = model.getStoreCode()
                                        reseller.storeName = model.getStoreName()
                                        reseller.surveyorId = model.getSurveyorId()
                                        reseller.surveyorNik = model.getSurveyorNik()
                                        reseller.surveyorName = model.getSurveyorName()
                                        reseller.resellerId = model.getResellerId()
                                        reseller.resellerName = model.getResellerName()
                                        reseller.resellerTypeId = model.getResellerTypeId()
                                        reseller.resellerTypeName = model.getResellerTypeName()
                                        reseller.radiusId = model.radiusId
                                        reseller.radiusName = model.radiusName
                                        reseller.phone = model.getPhone()
                                        reseller.address = model.getAddress()
                                        reseller.kabupatenId = model.getKabupatenId()
                                        reseller.kabupatenName = model.getKabupatenName()
                                        reseller.kecamatanId = model.getKecamatanId()
                                        reseller.kecamatanName = model.getKecamatanName()
                                        reseller.registerDate = if (model.getRegisterDate() == null) "" else model.getRegisterDate()
                                        reseller.latitude = model.getLatitude()
                                        reseller.longitude = model.getLongitude()
                                        reseller.isDonePenjualan = false
                                        reseller.isDonePullProgram = false
                                        reseller.isDoneUpdate = false
                                        resellers[i] = reseller
                                        i++
                                    }
                                    daoSession!!.tResellerDao.insertInTx(resellers.toList())
                                }
                            }
                            //endregion


                            //region Campaign
                            if (response.body().campaigns != null) {
                                val campaignSize = response.body().campaigns!!.size
                                if (campaignSize > 0) {
                                    var i = 0
                                    val campaigns = arrayOfNulls<TCampaign>(campaignSize)
                                    var campaign: TCampaign
                                    for (model in response.body().campaigns!!) {
                                        campaign = TCampaign()
                                        campaign.storeId=model.storeId
                                        campaign.campaignId=model.campaignId
                                        campaign.surveyorTypeId=model.surveyorTypeId
                                        campaign.campaignName=model.campaignName
                                        campaign.startDate=model.startDate
                                        campaign.endDate=model.endDate
                                        campaigns[i] = campaign
                                        i++
                                    }
                                    daoSession!!.tCampaignDao.insertInTx(campaigns.toList())
                                }
                            }
                            //endregion

                            //region Campaign Posm
                            if (response.body().campaignPosms != null) {
                                val campaignPosmSize = response.body().campaignPosms!!.size
                                if (campaignPosmSize > 0) {
                                    var i = 0
                                    val campaignPosms = arrayOfNulls<TCampaignPosm>(campaignPosmSize)
                                    var campaignPosm: TCampaignPosm
                                    for (model in response.body().campaignPosms!!) {
                                        campaignPosm = TCampaignPosm()
                                        campaignPosm.posmId=model.posmId
                                        campaignPosm.posmName=model.posmName
                                        campaignPosm.campaignId=model.campaignId
                                        campaignPosm.qrCode=model.qrCode
                                        campaignPosm.encode=model.encode
                                        campaignPosm.isQrMandatory=model.isQrMandatory

                                        campaignPosms[i] = campaignPosm

                                        i++
                                    }
                                    daoSession!!.tCampaignPosmDao.insertInTx(campaignPosms.toList())
                                }
                            }
                            //endregion

                            //region Reseller Type
                            if (response.body().resellerTypes != null) {
                                val resellerTypeSize = response.body().resellerTypes!!.size
                                if (resellerTypeSize > 0) {
                                    var i = 0
                                    val resellerTypes = arrayOfNulls<TResellerType>(resellerTypeSize)
                                    var resellerType: TResellerType
                                    for (model in response.body().resellerTypes!!) {
                                        resellerType = TResellerType()
                                        resellerType.resellerTypeId = model.getResellerTypeId()
                                        resellerType.resellerTypeName = model.getResellerTypeName()
                                        resellerTypes[i] = resellerType
                                        i++
                                    }
                                    daoSession!!.tResellerTypeDao.insertInTx(resellerTypes.toList())
                                }
                            }
                            //endregion


                            //ProductCompetitor
                            if(response.body().productCompetitors != null) {
                                val productCompetitorSize = response.body().productCompetitors!!.size
                                if (productCompetitorSize > 0) {
                                    val productCompetitors = arrayOfNulls<TProductCompetitor>(productCompetitorSize)
                                    var productCompetitor: TProductCompetitor
                                    for ((i, model) in response.body().productCompetitors!!.withIndex()) {
                                        productCompetitor = TProductCompetitor()
                                        productCompetitor.productId = model.productId
                                        productCompetitor.productName = model.productName
                                        productCompetitor.productCode = model.productCode
                                        productCompetitor.produsenId = model.produsenId
                                        productCompetitor.produsenName = model.produsenName
                                        productCompetitor.brandId = model.brandId
                                        productCompetitor.brandName = model.brandName
                                        productCompetitor.productCategoryId = model.productCategoryId
                                        productCompetitor.productCategoryName = model.productCategoryName
                                        productCompetitor.isCompetitor = model.isCompetitor
                                        productCompetitors[i] = productCompetitor
                                    }
                                    productCompetitorDao.insertInTx(productCompetitors.toList())
                                }
                            }

                            //BrandCompetitor
                            if(response.body().brandCompetitors != null) {
                                val brandCompetitorSize = response.body().brandCompetitors!!.size
                                if (brandCompetitorSize > 0) {
                                    val brandCompetitors = arrayOfNulls<TBrandCompetitor>(brandCompetitorSize)
                                    var brandCompetitor: TBrandCompetitor
                                    for ((i, model) in response.body().brandCompetitors!!.withIndex()) {
                                        brandCompetitor = TBrandCompetitor()
                                        brandCompetitor.brandId = model.brandId
                                        brandCompetitor.brandName = model.brandName
                                        brandCompetitor.productCategoryId = model.productCategoryId
                                        brandCompetitor.productCategoryName = model.productCategoryName
                                        brandCompetitor.isCompetitor = model.isCompetitor
                                        brandCompetitors[i] = brandCompetitor
                                    }
                                    brandCompetitorDao.insertInTx(brandCompetitors.toList())
                                }
                            }


                            //CategoryCompetitor
                            if(response.body().categoryCompetitors != null) {
                                val categoryCompetitorSize = response.body().categoryCompetitors!!.size
                                if (categoryCompetitorSize > 0) {
                                    val categoryCompetitors = arrayOfNulls<TCategoryCompetitor>(categoryCompetitorSize)
                                    var categoryCompetitor: TCategoryCompetitor
                                    for ((i, model) in response.body().categoryCompetitors!!.withIndex()) {
                                        categoryCompetitor = TCategoryCompetitor()
                                        categoryCompetitor.productCategoryId = model.productCategoryId
                                        categoryCompetitor.productCategoryName = model.productCategoryName
                                        categoryCompetitors[i] = categoryCompetitor
                                    }
                                    daoSession!!.tCategoryCompetitorDao.insertInTx(categoryCompetitors.toList())
                                }
                            }

                            //Planogram
                            if(response.body().planograms != null) {
                                val planogramSize = response.body().planograms!!.size
                                if (planogramSize > 0) {
                                    Log.i("planogramsize",response.body().planograms!!.size.toString())
                                    val planograms = arrayOfNulls<TPlanogram>(planogramSize)
                                    var planogram: TPlanogram
                                    for ((i, model) in response.body().planograms!!.withIndex()) {
                                        planogram = TPlanogram()
                                        planogram.planogramRegionId = model.planogramRegionId
                                        planogram.storeId = model.storeId
                                        planogram.storeName = model.storeName
                                        planogram.planogramTypeId = model.planogramTypeId
                                        planogram.planogramTypeName = model.planogramTypeName
                                        planogram.productCategoryId = model.productCategoryId
                                        planogram.productCategoryName = model.productCategoryName
                                        planogram.guidelinePhotoPath = model.guidelinePhotoPath
                                        planogram.isMsl = model.isMsl

                                        planograms[i] = planogram
                                    }
                                    planogramDao.insertInTx(planograms.toList())
                                }
                            }
                            
                            //Posm
                            if(response.body().posms != null) {
                                val posmSize = response.body().posms!!.size
                                if (posmSize > 0) {
                                    val posms = arrayOfNulls<TPosm>(posmSize)
                                    var posm: TPosm
                                    for ((i, model) in response.body().posms!!.withIndex()) {
                                        posm = TPosm()
                                        posm.posmPlanogramId = model.posmPlanogramId
                                        posm.planogramRegionId = model.planogramRegionId
                                        posm.planogramTypeId = model.planogramTypeId
                                        posm.planogramTypeName = model.planogramTypeName
                                        posm.posmTypeId = model.posmTypeId
                                        posm.posmTypeName = model.posmTypeName
                                        posms[i] = posm
                                    }
                                    posmDao.insertInTx(posms.toList())
                                }
                            }

                            //Activity
                            if(response.body().activities != null) {
                                val activitySize = response.body().activities!!.size
                                if (activitySize > 0) {
                                    val activitys = arrayOfNulls<TActivity>(activitySize)
                                    var activity: TActivity
                                    for ((i, model) in response.body().activities!!.withIndex()) {
                                        activity = TActivity()
                                        activity.activityId = model.activityId
                                        activity.activityName = model.activityName
                                        activity.productCategoryId = model.productCategoryId
                                        activity.productCategoryName = model.productCategoryName
                                        activity.startDate = model.startDate
                                        activity.endDate = model.endDate
                                        activitys[i] = activity
                                    }
                                    activityDao.insertInTx(activitys.toList())
                                }
                            }
                            
                            //ActivityType
                            if(response.body().activityTypes != null) {
                                val activityTypeSize = response.body().activityTypes!!.size
                                if (activityTypeSize > 0) {
                                    val activityTypes = arrayOfNulls<TActivityType>(activityTypeSize)
                                    var activityType: TActivityType
                                    for ((i, model) in response.body().activityTypes!!.withIndex()) {
                                        activityType = TActivityType()
                                        activityType.activityTypeId = model.activityTypeId
                                        activityType.activityTypeName = model.activityTypeName
                                        activityTypes[i] = activityType
                                    }
                                    activityTypeDao.insertInTx(activityTypes.toList())
                                }
                            }

                            //PlanogramType
                            if(response.body().planogramTypes != null) {
                                val planogramTypeSize = response.body().planogramTypes!!.size
                                if (planogramTypeSize > 0) {
                                    val planogramTypes = arrayOfNulls<TPlanogramType>(planogramTypeSize)
                                    var planogramType: TPlanogramType
                                    for ((i, model) in response.body().planogramTypes!!.withIndex()) {
                                        planogramType = TPlanogramType()
                                        planogramType.planogramTypeId = model.planogramTypeId
                                        planogramType.planogramTypeName = model.planogramTypeName
                                        planogramType.productCategoryId = model.productCategoryId
                                        planogramType.productCategoryName = model.productCategoryName
                                        planogramType.guidelinePhotoPath = model.guidelinePhotoPath
                                        planogramTypes[i] = planogramType
                                    }
                                    planogramTypeDao.insertInTx(planogramTypes.toList())
                                }
                            }
                            
                            //PosmType
                            if(response.body().posmTypes != null) {
                                val posmTypeSize = response.body().posmTypes!!.size
                                if (posmTypeSize > 0) {
                                    val posmTypes = arrayOfNulls<TPosmType>(posmTypeSize)
                                    var posmType: TPosmType
                                    for ((i, model) in response.body().posmTypes!!.withIndex()) {
                                        posmType = TPosmType()
                                        posmType.posmTypeId = model.posmTypeId
                                        posmType.posmTypeName = model.posmTypeName
                                        posmTypes[i] = posmType
                                    }
                                    posmTypeDao.insertInTx(posmTypes.toList())
                                }
                            }

                            //banners
                            if(response.body().banners != null) {
                                val bannerSize = response.body().banners!!.size
                                if (bannerSize > 0) {
                                    val bannerss = arrayOfNulls<TBanner>(bannerSize)
                                    var banner: TBanner
                                    for ((i, model) in response.body().banners!!.withIndex()) {
                                        banner = TBanner()
                                        banner.planogramTypeId = model.planogramTypeId
                                        banner.bannerId = model.bannerId
                                        banner.bannerName = model.bannerName
                                        banner.imagePath = model.imagePath
                                        banner.isLocalImage = model.isLocalImage
                                        banner.isActive = model.isActive
                                        bannerss[i] = banner
                                    }
                                    bannerDao.insertInTx(bannerss.toList())
                                }
                            }


                            //Planogram
                            if(response.body().compliances != null) {
                                val complianceSize = response.body().compliances!!.size
                                if (complianceSize > 0) {
                                    Log.i("planogramsize",response.body().compliances!!.size.toString())
                                    val compliances = arrayOfNulls<TCampaignCompliance>(complianceSize)
                                    var compliance: TCampaignCompliance
                                    for ((i, model) in response.body().compliances!!.withIndex()) {
                                        compliance = TCampaignCompliance()
                                        compliance.campaignId = model.campaignId
                                        compliance.complianceId = model.complianceId
                                        compliance.complianceCode = model.complianceCode
                                        compliance.compliance = model.compliance
                                        compliance.periodeStart = model.periodeStart
                                        compliance.periodeEnd = model.periodeEnd
                                        compliances[i] = compliance
                                    }
                                    daoSession!!.tCampaignComplianceDao.insertInTx(compliances.toList())
                                }
                            }
                            
                            //ReasonNoVisit
                            if(response.body().reasonNoVisits != null) {
                                val reasonNoVisitSize = response.body().reasonNoVisits!!.size
                                if (reasonNoVisitSize > 0) {
                                    val reasonNoVisits = arrayOfNulls<TReasonNoVisit>(reasonNoVisitSize)
                                    var reasonNoVisit: TReasonNoVisit
                                    for ((i, model) in response.body().reasonNoVisits!!.withIndex()) {
                                        reasonNoVisit = TReasonNoVisit()
                                        reasonNoVisit.reasonId = model.reasonId
                                        reasonNoVisit.reasonName = model.reason
                                        reasonNoVisits[i] = reasonNoVisit
                                    }
                                    reasonNoVisitDao.insertInTx(reasonNoVisits.toList())
                                }
                            }

                            //ReasonCampaign
                            if(response.body().reasonCampaigns != null) {
                                val reasonCampaignSize = response.body().reasonCampaigns!!.size
                                if (reasonCampaignSize > 0) {
                                    val reasonCampaigns = arrayOfNulls<TReasonCampaign>(reasonCampaignSize)
                                    var reasonCampaign: TReasonCampaign
                                    for ((i, model) in response.body().reasonCampaigns!!.withIndex()) {
                                        reasonCampaign = TReasonCampaign()
                                        reasonCampaign.reasonId = model.reasonId
                                        reasonCampaign.reasonName = model.reason
                                        reasonCampaigns[i] = reasonCampaign
                                    }
                                    daoSession!!.tReasonCampaignDao.insertInTx(reasonCampaigns.toList())
                                }
                            }

                            //ReasonCampaign
                            if(response.body().reasonCampaignPosm != null) {
                                val reasonCampaignPosmSize = response.body().reasonCampaignPosm!!.size
                                if (reasonCampaignPosmSize > 0) {
                                    val reasonCampaignPosms = arrayOfNulls<TReasonCampaignPosm>(reasonCampaignPosmSize)
                                    var reasonCampaignPosm: TReasonCampaignPosm
                                    for ((i, model) in response.body().reasonCampaignPosm!!.withIndex()) {
                                        reasonCampaignPosm = TReasonCampaignPosm()
                                        reasonCampaignPosm.reasonId = model.reasonId
                                        reasonCampaignPosm.reasonName = model.reason
                                        reasonCampaignPosms[i] = reasonCampaignPosm
                                    }
                                    daoSession!!.tReasonCampaignPosmDao.insertInTx(reasonCampaignPosms.toList())
                                }
                            }

                            //Store Type
                            if(response.body().storeTypes != null) {
                                val storeTypeSize = response.body().storeTypes!!.size
                                if (storeTypeSize > 0) {
                                    val storeTypes = arrayOfNulls<TStoreType>(storeTypeSize)
                                    var storeType: TStoreType
                                    for ((i, model) in response.body().storeTypes!!.withIndex()) {
                                        storeType = TStoreType()
                                        storeType.storeTypeId = model.storeTypeId
                                        storeType.storeTypeName = model.storeTypeName
                                        storeTypes[i] = storeType
                                    }
                                    storeTypeDao.insertInTx(storeTypes.toList())
                                }
                            }

                            //Distributor
                            if(response.body().distributors != null) {
                                val distributorSize = response.body().distributors!!.size
                                if (distributorSize > 0) {
                                    val distributors = arrayOfNulls<TDistributor>(distributorSize)
                                    var distributor: TDistributor
                                    for ((i, model) in response.body().distributors!!.withIndex()) {
                                        distributor = TDistributor()
                                        distributor.distributorId = model.distributorId
                                        distributor.distributorName = model.distributorName
                                        distributor.address = model.address
                                        distributors[i] = distributor
                                    }
                                    distributorDao.insertInTx(distributors.toList())
                                }
                            }

                            //Distributor area
                            if(response.body().distributorsAreas != null) {
                                val distributorAreaSize = response.body().distributorsAreas!!.size
                                if (distributorAreaSize > 0) {
                                    val distributorsareas = arrayOfNulls<TDistributorArea>(distributorAreaSize)
                                    var distributorarea: TDistributorArea
                                    for ((i, model) in response.body().distributorsAreas!!.withIndex()) {
                                        distributorarea = TDistributorArea()
                                        distributorarea.distributorId = model.distributorId
                                        distributorarea.distributorName = model.distributorName
                                        distributorsareas[i] = distributorarea
                                    }
                                    distributorAreaDao.insertInTx(distributorsareas.toList())
                                }
                            }

                            //Area
                            if(response.body().areas != null) {
                                val areaSize = response.body().areas!!.size
                                if (areaSize > 0) {
                                    val areas = arrayOfNulls<TArea>(areaSize)
                                    var area: TArea
                                    for ((i, model) in response.body().areas!!.withIndex()) {
                                        area = TArea()
                                        area.areaId = model.areaId
                                        area.areaName = model.areaName
                                        area.regionId = model.regionId
                                        areas[i] = area
                                    }
                                    areaDao.insertInTx(areas.toList())
                                }
                            }
                            
                            //Kabupaten
                            if(response.body().kabupatens != null) {
                                val kabupatenSize = response.body().kabupatens!!.size
                                if (kabupatenSize > 0) {
                                    val kabupatens = arrayOfNulls<TKabupaten>(kabupatenSize)
                                    var kabupaten: TKabupaten
                                    for ((i, model) in response.body().kabupatens!!.withIndex()) {
                                        kabupaten = TKabupaten()
                                        kabupaten.kabupatenId = model.kabupatenId
                                        kabupaten.kabupatenName = model.kabupatenName
                                        kabupaten.areaId = model.areaId
                                        kabupatens[i] = kabupaten
                                    }
                                    kabupatenDao.insertInTx(kabupatens.toList())
                                }
                            }
                            
                            //Kecamatan
                            if(response.body().kecamatans != null) {
                                val kecamatanSize = response.body().kecamatans!!.size
                                if (kecamatanSize > 0) {
                                    val kecamatans = arrayOfNulls<TKecamatan>(kecamatanSize)
                                    var kecamatan: TKecamatan
                                    for ((i, model) in response.body().kecamatans!!.withIndex()) {
                                        kecamatan = TKecamatan()
                                        kecamatan.kecamatanId = model.kecamatanId
                                        kecamatan.kecamatanName = model.kecamatanName
                                        kecamatan.kabupatenId = model.kabupatenId
                                        kecamatans[i] = kecamatan
                                    }
                                    kecamatanDao.insertInTx(kecamatans.toList())
                                }
                            }

                            //Info
                            if(response.body().infos != null) {
                                val infoSize = response.body().infos!!.size
                                if (infoSize > 0) {
                                    val infos = arrayOfNulls<TInfo>(infoSize)
                                    var info: TInfo
                                    for ((i, model) in response.body().infos!!.withIndex()) {
                                        info = TInfo()
                                        info.infoId = model.infoId
                                        info.infoName = model.infoName
                                        info.startDate = model.startDate
                                        info.endDate = model.endDate
                                        info.imagePath = model.imagePath
                                        infos[i] = info
                                    }
                                    infoDao.insertInTx(infos.toList())
                                }
                            }

                            //ActivityType
                            if(response.body().localActivitys != null) {
                                val LocalActivityTypeSize = response.body().localActivitys!!.size
                                if (LocalActivityTypeSize > 0) {
                                    val LocalActivityTypes = arrayOfNulls<TLocalActivityType>(LocalActivityTypeSize)
                                    var LocalActivityType: TLocalActivityType
                                    for ((i, model) in response.body().localActivitys!!.withIndex()) {
                                        LocalActivityType = TLocalActivityType()
                                        LocalActivityType.activityTypeId = model.activityTypeId
                                        LocalActivityType.activityName = model.activityTypeName
                                        LocalActivityTypes[i] = LocalActivityType
                                    }
                                    daoSession!!.tLocalActivityTypeDao.insertInTx(LocalActivityTypes.toList())

                                }
                            }


//                            ProductMsl
                            if(response.body().productMSLs != null) {
                                val productMSLSize = response.body().productMSLs!!.size
                                if (productMSLSize > 0) {
                                    val productMSLs = arrayOfNulls<TProductMSL>(productMSLSize)
                                    var productMSL: TProductMSL
                                    for ((i, model) in response.body().productMSLs!!.withIndex()) {
                                        productMSL = TProductMSL()
                                        productMSL.productId= model.productId
                                        productMSL.productCode = model.productCode
                                        productMSL.productName = model.productName
                                        productMSL.brandId = model.brandId
                                        productMSL.produsenId = model.produsenId
                                        productMSLs[i] = productMSL
                                    }
                                    daoSession!!.tProductMSLDao.insertInTx(productMSLs.toList())
                                }
                            }

                        //ProductMsl by Region
                            if(response.body().productMSLRegions != null) {
                                val productMSLRegionSize = response.body().productMSLRegions!!.size
                                if (productMSLRegionSize > 0) {
                                    val productMSLRegions = arrayOfNulls<TProductMSLRegion>(productMSLRegionSize)
                                    var productMSLRegion: TProductMSLRegion
                                    for ((i, model) in response.body().productMSLRegions!!.withIndex()) {
                                        productMSLRegion = TProductMSLRegion()
                                        productMSLRegion.productId= model.productId
                                        productMSLRegion.productName = model.productName
                                        productMSLRegion.isChecked = 0
                                        productMSLRegion.isChoosed = 0
                                        productMSLRegions[i] = productMSLRegion
                                    }
                                    daoSession!!.tProductMSLRegionDao.insertInTx(productMSLRegions.toList())
                                }
                            }

                            //Distributor Store
                            if(response.body().distributorsStore != null) {
                                val distributorStoreSize = response.body().distributorsStore!!.size
                                if (distributorStoreSize > 0) {
                                    Log.i("location disc",response.body().distributorsStore!!.size.toString())
                                    val distributorStores = arrayOfNulls<TDistributorStore>(distributorStoreSize)
                                    var distributorStore: TDistributorStore
                                    for ((i, model) in response.body().distributorsStore!!.withIndex()) {
                                        distributorStore = TDistributorStore()
                                        distributorStore.distributorId = model.distributorId
                                        distributorStore.storeId = model.storeId
                                        distributorStore.storeName = model.storeName
                                        distributorStore.locationId = model.locationId
                                        distributorStores[i] = distributorStore
                                    }
                                    daoSession!!.tDistributorStoreDao.insertInTx(distributorStores.toList())
                                }
                            }

                         //Local Activity Header
                            if(response.body().localHeaders != null) {
                                val localHeadersSize = response.body().localHeaders!!.size
                                if (localHeadersSize > 0) {
                                    val localHeaderss = arrayOfNulls<TReportActivityHeader>(localHeadersSize)
                                    var localHeader: TReportActivityHeader
                                    for ((i, model) in response.body().localHeaders!!.withIndex()) {
                                        localHeader = TReportActivityHeader()
                                        localHeader.headerId= model.reportHeaderId
                                        localHeader.userId = model.userId
                                        localHeader.activityId = model.activityNameId
                                        localHeader.activityName = model.activityName
                                        localHeader.periodeStart = model.periodeEnd
                                        localHeader.periodeStart = model.periodeStart
                                        localHeader.periodeEnd = model.periodeEnd
                                        localHeader.activityTypeId = model.activityTypeId
                                        localHeader.activityTypeName = model.activityTypeName
                                        localHeader.distributorId = model.distributorId
                                        localHeader.storeId = model.storeId
                                        localHeader.storeName = model.storeName
                                        localHeader.locationId = model.locationId
                                        localHeader.locationName = model.locationName
                                        localHeader.areaId = model.areaId
                                        localHeader.mekanisme= model.mekanisme
                                        localHeader.tujuan = model.tujuan
                                        localHeader.result = model.result
                                        localHeader.roleId = model.surveyorTypeId
                                        localHeader.userNames = model.namaUser
                                        localHeader.insight = model.insight
                                        localHeader.photoRowSize = model.photoRowSize
                                        localHeader.photoColumnSize = model.photoColumnSize
                                        localHeader.created = model.created
                                        localHeader.createdDate = model.createdDate
                                        localHeader.saved = 1
//                                        localHeader.is
                                        localHeaderss[i] = localHeader
                                    }
                                    daoSession!!.tReportActivityHeaderDao.insertInTx(localHeaderss.toList())
                                }
                            }

                             //Local Activity Detail
                            if(response.body().localDetails != null) {
                                val localDetailsSize = response.body().localDetails!!.size
                                if (localDetailsSize > 0) {
                                    val localDetails = arrayOfNulls<TReportActivityDetail>(localDetailsSize)
                                    var localDetail: TReportActivityDetail
                                    for ((i, model) in response.body().localDetails!!.withIndex()) {
                                        localDetail = TReportActivityDetail()
                                        localDetail.reportHeaderId= model.reportHeaderId
                                        localDetail.reportDetailId = model.reportDetailId
                                        localDetail.userId = model.userId
                                        localDetail.activityTypeId = model.activityTypeId
                                        localDetail.infoPhoto = model.info
                                        localDetail.rowNumber = model.photoRowNumber
                                        localDetail.columnNumber = model.photoColumnNumber
                                        localDetail.createdDate = model.createdDate
                                        localDetail.created = model.created
                                        localDetail.modified = ""
                                        localDetail.topPosition = model.topPosition
                                        localDetail.rightPosition = model.rightPosition
                                        localDetail.bottomPosition = model.bottomPosition
                                        localDetail.leftPosition = model.leftPosition
                                        if(model.photoPath!=null){
                                            localDetail.photoPath = model.photoPath
                                        }
                                        else{
                                            localDetail.photoPath = ""
                                        }
                                        localDetails[i] = localDetail
                                    }
                                    daoSession!!.tReportActivityDetailDao.insertInTx(localDetails.toList())
                                }
                            }

                            //Activity Type Name
                            if(response.body().localActivityNames != null) {
                                val localActivityNameSize = response.body().localActivityNames!!.size
                                if (localActivityNameSize > 0) {
                                    Log.i("location activiy",response.body().localActivityNames!!.size.toString())
                                    val localActivityNames = arrayOfNulls<TLocalActivityName>(localActivityNameSize)
                                    var localActivityName: TLocalActivityName
                                    for ((i, model) in response.body().localActivityNames!!.withIndex()) {
                                        localActivityName = TLocalActivityName()
                                        localActivityName.activityName = model.activityName
                                        localActivityName.activityNameId = model.activityNameId
                                        localActivityName.activityId = model.activityId
                                        localActivityNames[i] = localActivityName
                                    }
                                    daoSession!!.tLocalActivityNameDao.insertInTx(localActivityNames.toList())
                                }
                            }

                            if(response.body().localActivityLocation !=null) {
                                val localActivityLocationSize = response.body().localActivityLocation!!.size
                                if (localActivityLocationSize >0) {
                                    Log.i("location",response.body().localActivityLocation!!.size.toString())
                                    val localActivityLocations = arrayOfNulls<TLocalAcitivityLocation>(localActivityLocationSize)
                                    var localActivityLocation:TLocalAcitivityLocation
                                    for ((i, model) in response.body().localActivityLocation!!.withIndex()) {
                                        localActivityLocation = TLocalAcitivityLocation()
                                        localActivityLocation.locationId = model.locationId
                                        localActivityLocation.locationName = model.locationName
                                        localActivityLocations[i] = localActivityLocation
                                    }
                                    daoSession!!.tLocalAcitivityLocationDao.insertInTx(localActivityLocations.toList())

                                }
                            }

                            //Weekly Type Name
                            if(response.body().localActivityNamesWeekly != null) {
                                val localActivityNamesWeeklySize = response.body().localActivityNamesWeekly!!.size
                                if (localActivityNamesWeeklySize > 0) {
                                    val localActivityNamesWeeklys = arrayOfNulls<TWeekActivityName>(localActivityNamesWeeklySize)
                                    var localActivityNamesWeekly: TWeekActivityName
                                    for ((i, model) in response.body().localActivityNamesWeekly!!.withIndex()) {
                                        localActivityNamesWeekly = TWeekActivityName()
                                        localActivityNamesWeekly.weeklyName = model.activityName
                                        localActivityNamesWeekly.weeklyNameId = model.activityNameId
                                        localActivityNamesWeeklys[i] = localActivityNamesWeekly
                                    }
                                    daoSession!!.tWeekActivityNameDao.insertInTx(localActivityNamesWeeklys.toList())
                                }
                            }



                            //Role Type Name
                            if(response.body().roleName != null) {
                                val roleNameSize = response.body().roleName!!.size
                                if (roleNameSize > 0) {
                                    val roleNames = arrayOfNulls<TRole>(roleNameSize)
                                    var roles: TRole
                                    for ((i, model) in response.body().roleName!!.withIndex()) {
                                        roles = TRole()
                                        roles.roleId = model.RoleIds
                                        roles.roleName = model.RoleNames
                                        roleNames[i] = roles
                                    }
                                    daoSession!!.tRoleDao.insertInTx(roleNames.toList())
                                }
                            }

                            //Region
                            if(response.body().nooRegions != null) {
                                val regionSize = response.body().nooRegions!!.size
                                if (regionSize > 0) {
                                    val regions = arrayOfNulls<TNooRegion>(regionSize)
                                    var region: TNooRegion
                                    for ((i, model) in response.body().nooRegions!!.withIndex()) {
                                        region = TNooRegion()
                                        region.regionId = model.regionId
                                        region.regionName = model.regionName
                                        region.alias = model.alias
                                        regions[i] = region
                                    }
                                    nooRegionDao.insertInTx(regions.toList())
                                }
                            }
                            //Area
                            if(response.body().nooAreas != null) {
                                val areaSize = response.body().nooAreas!!.size
                                if (areaSize > 0) {
                                    val areas = arrayOfNulls<TNooArea>(areaSize)
                                    var area: TNooArea
                                    for ((i, model) in response.body().nooAreas!!.withIndex()) {
                                        area = TNooArea()
                                        area.areaId = model.areaId
                                        area.areaName = model.areaName
                                        area.regionId = model.regionId
                                        areas[i] = area
                                    }
                                    nooAreaDao.insertInTx(areas.toList())
                                }
                            }

                            //Distributor
                            if(response.body().nooDistributors != null) {
                                val kabupatenSize = response.body().nooDistributors!!.size
                                if (kabupatenSize > 0) {
                                    val distributors = arrayOfNulls<TNooDistributor>(kabupatenSize)
                                    var distributor: TNooDistributor
                                    for ((i, model) in response.body().nooDistributors!!.withIndex()) {
                                        distributor = TNooDistributor()
                                        distributor.distributorId = model.distributorId
                                        distributor.distributorName = model.distributorName
                                        distributor.areaId = model.areaId
                                        distributors[i] = distributor
                                    }
                                    nooDistributor.insertInTx(distributors.toList())
                                }
                            }



                            
                            var i = Intent(this@LoginActivity, DashboardActivity::class.java)
                            i.putExtra("IsFromLogin", true)
                            Log.i("roleId",Login.getEmployeeRoleId(this@LoginActivity).toString())

                            if(Login.getEmployeeRoleId(this@LoginActivity)==8){
                                SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_IN_REPORT, "spv")
                                i = Intent(this@LoginActivity, MenuSpvActivity::class.java)
                            }
                            else  if(Login.getEmployeeRoleId(this@LoginActivity)==12||Login.getEmployeeRoleId(this@LoginActivity)==13||Login.getEmployeeRoleId(this@LoginActivity)==14||Login.getEmployeeRoleId(this@LoginActivity)==15){
                                SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_IN_REPORT, "")
                                i = Intent(this@LoginActivity, StoreListActivity2::class.java)
                            }
                            startActivity(i)
                            finish()
                        } catch (e: Exception) {
                            e.printStackTrace()
                            Login.logout(this@LoginActivity)
                            val iLogin = Intent(this@LoginActivity, LoginActivity::class.java)
                            iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(iLogin)
                            finish()
                            Toast.makeText(this@LoginActivity, e.message, Toast.LENGTH_SHORT).show()
                        }

                    } else if (response.body().status.equals(Config.STATUS_FAILURE)) {
                        Toast.makeText(this@LoginActivity, response.body().message, Toast.LENGTH_SHORT).show()
                    }
                    login_progressBar.visibility = View.GONE
                    login_btnLogin.isEnabled = true
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                login_progressBar.visibility = View.GONE
                login_btnLogin.isEnabled = true
                Toast.makeText(this@LoginActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                StorageUtils.createDirectories()
                initDaoSession()
                DatabaseReport.Companion.getDatabase(this)
                val iService = Intent(applicationContext, ReportUploader::class.java)
                startService(iService)
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Storage permission is needed to write external storage.", Toast.LENGTH_SHORT).show()
                }
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), Config.REQUEST_CODE_WRITE_EXTERNAL_STORAGE)
            }
        } else {
            StorageUtils.createDirectories()
            DatabaseReport.Companion.getDatabase(this)
            val iService = Intent(applicationContext, ReportUploader::class.java)
            startService(iService)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == Config.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                StorageUtils.createDirectories()
                initDaoSession()
                DatabaseReport.Companion.getDatabase(this)
                val iService = Intent(applicationContext, ReportUploader::class.java)
                startService(iService)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm!!.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null) progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            progressDialog!!.dismiss()
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
        }
    }
}
