package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageActivity2
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoLandscapeActivity
import com.nestle.mdgt.adapters.ProductMslAdapter
import com.nestle.mdgt.adapters.ProductMslConfirmationAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_planogram_add.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime

class PlanogramAddActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener {


    private var mId: Long = 0
    private val requestCodeCamera = 10
    private var mPhotoPath11: String = ""
    private var mPhotoPath12: String = ""
    private var mPhotoPath13: String = ""
    private var mPhotoPath14: String = ""
    private var mPhotoPath21: String = ""
    private var mPhotoPath22: String = ""
    private var mPhotoPath23: String = ""
    private var mPhotoPath24: String = ""
    private var mPhotoType: Int = 0
    private var storeId:Int=0
    private var storeName:String=""
    private var isAvailablePhoto11: Boolean = false
    private var isAvailablePhoto12: Boolean = false
    private var isAvailablePhoto13: Boolean = false
    private var isAvailablePhoto14: Boolean = false
    private var isAvailablePhoto21: Boolean = false
    private var isAvailablePhoto22: Boolean = false
    private var isAvailablePhoto23: Boolean = false
    private var isAvailablePhoto24: Boolean = false
    private var mActionType: String = "simpan"

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportPlanogramDao
    private lateinit var planogramTypeDao: TPlanogramTypeDao
    private lateinit var mImageSlider: SliderLayout

    private var item: TReportPlanogram? = null

    private var planogramTypeAdapter: ArrayAdapter<TPlanogramType>? = null
    private lateinit var banners:List<TBanner>
    private lateinit var products:List<TProductMSLRegion>
    private var posisi=0
    private var namaMsl = ""
    private lateinit var items: MutableList<TReportProductPlanogram>




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_planogram_add)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_tambah_planogram)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {

        mId = intent.getLongExtra("Id", 0)
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportPlanogramDao
        planogramTypeDao = daoSession.tPlanogramTypeDao
        products=daoSession.tProductMSLRegionDao.queryBuilder().where(TProductMSLRegionDao.Properties.IsChecked.eq(0)).list()
        namaMsl = intent.getStringExtra("mslName")
        item = itemDao.load(mId)
        banners=daoSession.tBannerDao.queryBuilder().where(TBannerDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()

        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        storeName= SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")!!
        refreshProduct()


    }

    private fun initProducts(posisi:Int) {
//        try {
            products=daoSession.tProductMSLRegionDao.queryBuilder().where(TProductMSLRegionDao.Properties.IsChecked.eq(0)).list()
            Log.i("sizeProducts",daoSession.tProductMSLRegionDao.loadAll().size.toString())
            Log.i("sizeProducts2",products.size.toString())

            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
            val layoutPopup = inflater!!.inflate(R.layout.popup_products, findViewById(R.id.popupProducts))
            val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)

            val btnSimpan = layoutPopup.findViewById(R.id.popupBtnSimpan) as Button
            val popupRecycler = layoutPopup.findViewById(R.id.popupRecycler) as androidx.recyclerview.widget.RecyclerView
            if (products.size>0) {



                var productsAdapter = ProductMslAdapter(products, R.layout.activity_product_msl_list_row, object : ProductMslAdapter.OnItemClickListener {
                    override fun onItemClick(item: TProductMSLRegion) {

                    }
                })
                popupRecycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
                popupRecycler.adapter = productsAdapter
                productsAdapter.notifyDataSetChanged()

            }
            mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)


            btnSimpan.setOnClickListener {

                var reportPP: TReportProductPlanogram
                for (o in products) {
                    if(o.isChoosed==1){
                        Log.i("var-productId",o.productId.toString())
                        Log.i("var-position",posisi.toString())
                        reportPP = TReportProductPlanogram()
                        reportPP.productId= o.productId
                        reportPP.posisi = posisi
                        reportPP.planogramTypeId = item!!.planogramTypeId
                        reportPP.visitId = item!!.visitId
                        reportPP.planogramStoreId = item!!.planogramRegionId
                        reportPP.isListing=1



                        daoSession!!.tReportProductPlanogramDao.insert(reportPP)
                        o.isChecked=1
                        o.isChoosed=0
                        daoSession!!.tProductMSLRegionDao.update(o)

                    }
                }
                mPopupWindow.dismiss()

            }

//                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
//                        .apply(RequestOptions().fitCenter())
//                        .into(img)
//
//                desc.settings.javaScriptEnabled = false
//                desc.loadData(info.infoName,"text/html", null)



//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
    }

    private fun initConfirmation() {
        try {
            products=daoSession.tProductMSLRegionDao.queryBuilder().where(TProductMSLRegionDao.Properties.IsChecked.eq(0)).list()

            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layoutPopup = inflater.inflate(R.layout.popup_products_confirmation, findViewById(R.id.popupProducts))
            val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)

            val btnSimpan = layoutPopup.findViewById(R.id.popupBtnSimpan) as Button
            val btnClose = layoutPopup.findViewById(R.id.popupBtnClose) as ImageButton

            val popupRecycler = layoutPopup.findViewById(R.id.popupRecycler) as androidx.recyclerview.widget.RecyclerView
            if (products.size>0) {



                var confirmationAdapter = ProductMslConfirmationAdapter(products, R.layout.activity_product_msl_confirmation_row, object : ProductMslConfirmationAdapter.OnItemClickListener {
                    override fun onItemClick(item: TProductMSLRegion) {

                    }
                })
                popupRecycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
                popupRecycler.adapter = confirmationAdapter
                confirmationAdapter.notifyDataSetChanged()

            }


            btnSimpan.setOnClickListener {


                val builder = AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.label_confirm))
                builder.setMessage(getString(R.string.confirm_msg_save_pemanent))
                builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
                    submitForm()
                }
                builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
                val alertDialog = builder.create()
                alertDialog.setCanceledOnTouchOutside(false)
                alertDialog.show()
                mPopupWindow.dismiss()

            }

            btnClose.setOnClickListener { mPopupWindow.dismiss() }

//                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
//                        .apply(RequestOptions().fitCenter())
//                        .into(img)
//
//                desc.settings.javaScriptEnabled = false
//                desc.loadData(info.infoName,"text/html", null)

            mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun refreshProduct(){
        for(o in daoSession.tProductMSLRegionDao.loadAll()){
            o.isChecked=0
            o.isChoosed=0
            daoSession.tProductMSLRegionDao.update(o)
        }
    }

    private fun initView() {

        planogramTypeAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item,
                planogramTypeDao.queryBuilder().list())
        planogramTypeAdapter!!.insert(TPlanogramType(null, 0, "", 0, "", ""), 0)
        planogram_spinType.adapter = planogramTypeAdapter
        mImageSlider = findViewById(R.id.plano_imageSlider)
        mImageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom)


        if(item!!.isMsl==0){
            planogram_img21.visibility=View.GONE
            planogram_btnPhoto21.visibility=View.GONE
            planogram_img22.visibility=View.GONE
            planogram_btnPhoto22.visibility=View.GONE
            planogram_img23.visibility=View.GONE
            planogram_btnPhoto23.visibility=View.GONE
            planogram_img24.visibility=View.GONE
            planogram_btnPhoto24.visibility=View.GONE
        }

        planogram_btnSimpan.setOnClickListener {
            mActionType = "simpan"
            validateForm()
        }

        planogram_btnDaftarPosm.setOnClickListener {
            mActionType = "daftar posm"
            validateForm()
        }

        planogram_btnPhoto11.setOnClickListener {
            mPhotoType = requestCodeCamera + 11
            startCamera(mPhotoType)
        }
        planogram_btnPhoto12.setOnClickListener {
            mPhotoType = requestCodeCamera + 12
            startCamera(mPhotoType)
        }
        planogram_btnPhoto13.setOnClickListener {
            mPhotoType = requestCodeCamera + 13
            startCamera(mPhotoType)
        }
        planogram_btnPhoto14.setOnClickListener {
            mPhotoType = requestCodeCamera + 14
            startCamera(mPhotoType)
        }
        planogram_btnPhoto21.setOnClickListener {
            mPhotoType = requestCodeCamera + 21
            startCamera(mPhotoType)
        }
        planogram_btnPhoto22.setOnClickListener {
            mPhotoType = requestCodeCamera + 22
            startCamera(mPhotoType)
        }
        planogram_btnPhoto23.setOnClickListener {
            mPhotoType = requestCodeCamera + 23
            startCamera(mPhotoType)
        }
        planogram_btnPhoto24.setOnClickListener {
            mPhotoType = requestCodeCamera + 24
            startCamera(mPhotoType)
        }
        initBanner()
    }
    private fun initBanner(){
        for (o in banners){
            val imagePath = if(o.isLocalImage == 1) {
                "${ApiClient.hostUrl}${o.imagePath}"
            } else {
                o.imagePath
            }
            val sliderView = DefaultSliderView(this)
            sliderView.image(imagePath).scaleType = BaseSliderView.ScaleType.Fit
            sliderView.setOnSliderClickListener(this@PlanogramAddActivity)
            mImageSlider.addSlider(sliderView)
        }
    }

    private fun initValuesToViews() {

//        private fun reloadList() {
//            val popup = daoSession.tProductMSLRegionDao.queryBuilder().list()
//            lblNoData!!.visibility = View.GONE
//
//            if(popup.size==0){
//                lblNoData!!.visibility = View.VISIBLE
//            }
//
//            val adapter = ProductMslAdapter (popup, R.layout.activity_local_activity_list_row, object : ProductMslAdapter.OnItemClickListener {
//                override fun onItemClick(item: TReportActivityHeader) {
//                    val intent = Intent(this@PlanogramAddActivity, LocalAddActivity::class.java)
//                    intent.putExtra("Id", item.id)
//                    intent.putExtra("headerId",item.headerId)
//                    startActivity(intent)
//                }
//            })
//        pilihanPopup_recycle.layoutManager = LinearLayoutManager(this)
//        pilihanPopup_recycle.addItemDecoration(SimpleDividerItemDecoration(this))
//        pilihanPopup_recycle.adapter = adapter
//            adapter.notifyDataSetChanged()
//        }
        if (item != null) {
            supportActionBar!!.title = item!!.planogramTypeName
            Glide.with(this)
                    .asBitmap()
                    .load(ApiClient.hostUrl + item!!.guidelinePhotoPath)
                    .apply( RequestOptions().centerCrop())
                    .into(planogram_imgGuideline)

            planogram_imgGuideline.setOnClickListener {
                if (item!!.guidelinePhotoPath != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", item!!.guidelinePhotoPath)
                    startActivity(intent)
                }
            }
            //planogram_btnDaftarPosm.visibility = View.VISIBLE // di-comment karena posm menggunakan image recognition
            val planoTypePosition = planogramTypeAdapter!!.getPosition(planogramTypeDao.queryBuilder()
                    .where(TPlanogramTypeDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId))
                    .limit(1).unique())
            planogram_spinType.setSelection(planoTypePosition)
            planogram_spinType.isEnabled = false
            planogram_spinType.setBackgroundResource(R.drawable.form_spinner_style_disabled)

            if (item!!.isComply == Config.YES_CODE) {
                (planogram_rgComply.getChildAt(0) as RadioButton).isChecked = true
            } else if (item!!.isComply == Config.NO_CODE) {
                (planogram_rgComply.getChildAt(1) as RadioButton).isChecked = true
            }


            mPhotoPath11 = item!!.beforePhotoPath
            if (mPhotoPath11 != "") {
                planogram_img11.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img11.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath11))
                isAvailablePhoto11 = true
            } else {
                isAvailablePhoto11 = false
                mPhotoPath11 = ""
            }
            planogram_img11.setOnClickListener {
                if (mPhotoPath11 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath11)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }

            mPhotoPath12 = item!!.afterPhotoPath
            if (mPhotoPath12 != "") {
                planogram_img12.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img12.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath12))
                isAvailablePhoto12 = true
            } else {
                isAvailablePhoto12 = false
                mPhotoPath12 = ""
            }
            planogram_img12.setOnClickListener {
                if (mPhotoPath12 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath12)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }


            mPhotoPath21 = item!!.photoPath3
            if (mPhotoPath21 != "") {
                planogram_img21.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img21.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath21))
                isAvailablePhoto21 = true
            } else {
                isAvailablePhoto21 = false
                mPhotoPath21 = ""
            }
            planogram_img21.setOnClickListener {
                if (mPhotoPath21 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath21)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }

            mPhotoPath22 = item!!.photoPath4
            if (mPhotoPath22 != "") {
                planogram_img22.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img22.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath22))
                isAvailablePhoto22 = true
            } else {
                isAvailablePhoto22 = false
                mPhotoPath22 = ""
            }
            planogram_img22.setOnClickListener {
                if (mPhotoPath22 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath22)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }


            mPhotoPath13 = item!!.photoPath5
            if (mPhotoPath13 != "") {
                planogram_img13.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img13.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath13))
                isAvailablePhoto13 = true
            } else {
                isAvailablePhoto13 = false
                mPhotoPath13 = ""
            }
            planogram_img13.setOnClickListener {
                if (mPhotoPath13 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath13)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }


            mPhotoPath14 = item!!.photoPath6
            if (mPhotoPath14 != "") {
                planogram_img14.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img14.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath14))
                isAvailablePhoto14 = true
            } else {
                isAvailablePhoto14 = false
                mPhotoPath14 = ""
            }
            planogram_img14.setOnClickListener {
                if (mPhotoPath14 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath14)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }

            mPhotoPath23 = item!!.photoPath7
            if (mPhotoPath23 != "") {
                planogram_img23.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img23.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath23))
                isAvailablePhoto23 = true
            } else {
                isAvailablePhoto23 = false
                mPhotoPath23 = ""
            }
            planogram_img23.setOnClickListener {
                if (mPhotoPath23 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath23)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }

            mPhotoPath24 = item!!.photoPath8
            if (mPhotoPath24 != "") {
                planogram_img24.scaleType = ImageView.ScaleType.FIT_XY
                planogram_img24.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath24))
                isAvailablePhoto24 = true
            } else {
                isAvailablePhoto24 = false
                mPhotoPath24 = ""
            }
            planogram_img24.setOnClickListener {
                if (mPhotoPath24 != "") {
                    val intent = Intent(this, FullImageActivity::class.java)
                    intent.putExtra("ImageUrl", mPhotoPath24)
                    intent.putExtra("Rotation", 90)
                    startActivity(intent)
                }
            }


            if(item!!.isAllPosmsDone){
                planogram_btnDaftarPosm.text = getString(R.string.label_daftar_posm_checked)
            }
        }
    }

    private fun validateForm() {
        //region Validation
        if(item == null) {
            if (!FormValidation.validateRequiredSpinner(this, planogram_spinType, String.format(getString(R.string.info_msg_required_field_spinner), "tipe pajangan"))) {
                return
            }
            val planogramTypeSelected = planogram_spinType.selectedItem as TPlanogramType

            if(isExistPlanogram(planogramTypeSelected.planogramTypeId, storeId)) {
                Toast.makeText(this, "${planogramTypeSelected.planogramTypeName} sudah ada, silahkan pilih tipe pajangan lainnya", Toast.LENGTH_SHORT).show()
                return
            }
        }
//        if (planogram_rgComply.checkedRadioButtonId == -1) {
//            Toast.makeText(this, String.format(getString(R.string.info_msg_required_param, "Pertanyaan \"Apakah comply?\"")), Toast.LENGTH_SHORT).show()
//            return
//        }
        if (!isAvailablePhoto11) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "pajangan"), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion
        if(namaMsl=="MSL") {
            initConfirmation()
        }
        else{
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_msg_save_pemanent))
            builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
                var reportPP: TReportProductPlanogram
                for (o in products) {
                    if(o.isChoosed==0){
                        reportPP = TReportProductPlanogram()
                        reportPP.productId= o.productId
                        reportPP.posisi = 0
                        reportPP.planogramTypeId = item!!.planogramTypeId
                        reportPP.visitId = item!!.visitId
                        reportPP.planogramStoreId = item!!.planogramRegionId
                        reportPP.isListing=0



                        daoSession!!.tReportProductPlanogramDao.insert(reportPP)

                    }
                }
                refreshProduct()
                submitForm()

            }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }

    }

    private fun submitForm() {
        val compliance = -1 //Integer.parseInt(findViewById<View>(planogram_rgComply.checkedRadioButtonId).contentDescription.toString())
        items=daoSession.tReportProductPlanogramDao.queryBuilder()
                .where(TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                        TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                        TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
        for (o in items){
            Log.i("var-posisi",o.posisi.toString())
            Log.i("var-productId",o.productId.toString())
//            Log.i("var-visitId",o.visitId.toString())
//            Log.i("var-planoId",o.planogramTypeId.toString())
//            Log.i("var-planoStoreId",o.planogramStoreId.toString())
            Log.i("var-isListing",o.isListing.toString())

        }
        if (item == null) {
            val planogramTypeSelected = planogram_spinType.selectedItem as TPlanogramType
            val regionId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_REGION_ID, 0)

            if (!isExistPlanogram(planogramTypeSelected.planogramTypeId, regionId)) {
                item = TReportPlanogram()
                item!!.visitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
                item!!.surveyorId = Login.getUserId(this)
                item!!.planogramRegionId = Helper.generatePlanogramId(planogramTypeSelected.planogramTypeId, regionId)
                item!!.storeId = storeId.toString()
                item!!.storeName = storeName//SharedPrefsUtils.getStringPreference(this, Config.KEY_EMPLOYEE_REGION_NAME, "")
                item!!.planogramTypeId = planogramTypeSelected.planogramTypeId
                item!!.planogramTypeName = planogramTypeSelected.planogramTypeName
                item!!.productCategoryId = planogramTypeSelected.productCategoryId
                item!!.productCategoryName = planogramTypeSelected.productCategoryName
                item!!.isComply = compliance
                item!!.beforePhotoPath = mPhotoPath11
                item!!.afterPhotoPath = mPhotoPath12
                item!!.photoPath3 = mPhotoPath21
                item!!.photoPath4 = mPhotoPath22
                item!!.photoPath5 = mPhotoPath13
                item!!.photoPath6 = mPhotoPath14
                item!!.photoPath7 = mPhotoPath23
                item!!.photoPath8 = mPhotoPath24
                item!!.isNew = Config.YES
                item!!.isDone = true
                item!!.isPilih =true
                item!!.isAllPosmsDone = false
                itemDao.insert(item)
            }
        } else {
            item!!.isComply = compliance
            item!!.beforePhotoPath = mPhotoPath11
            item!!.afterPhotoPath = mPhotoPath12
            item!!.photoPath3 = mPhotoPath21
            item!!.photoPath4 = mPhotoPath22
            item!!.photoPath5 = mPhotoPath13
            item!!.photoPath6 = mPhotoPath14
            item!!.photoPath7 = mPhotoPath23
            item!!.photoPath8 = mPhotoPath24
            item!!.isDone = true
            item!!.isPilih = true
            itemDao.update(item)
        }

        if (mActionType == "daftar posm") {
            SharedPrefsUtils.setStringPreference(this, Config.KEY_PLANOGRAM_REGION_ID, item!!.planogramRegionId)
            SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, "posm")
            val intent = Intent(this, PosmListActivity::class.java)
            startActivity(intent)
        }

        finish()
    }

    private fun isExistPlanogram(planogramTypeId: Int, storeId: Int): Boolean {
        return daoSession.tPlanogramDao.queryBuilder().where(TPlanogramDao.Properties.PlanogramTypeId.eq(planogramTypeId),
                TPlanogramDao.Properties.StoreId.eq(storeId)).list().size > 0

    }

    private fun callIntentCamera() {
        val intent = Intent(this, PhotoLandscapeActivity::class.java)
        intent.putExtra("nama_file", "planogram_" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))

        if(mPhotoType == requestCodeCamera + 11) {
            intent.putExtra("photo_path_left", "")
            intent.putExtra("photo_path_top", "")
            intent.putExtra("photo_path_right", mPhotoPath12)
            intent.putExtra("photo_path_bottom", mPhotoPath21)
        }
        if(mPhotoType == requestCodeCamera + 12) {
            intent.putExtra("photo_path_left", mPhotoPath11)
            intent.putExtra("photo_path_top", "")
            intent.putExtra("photo_path_right", mPhotoPath13)
            intent.putExtra("photo_path_bottom", mPhotoPath22)
        }
        if(mPhotoType == requestCodeCamera + 13) {
            intent.putExtra("photo_path_left", mPhotoPath12)
            intent.putExtra("photo_path_top", "")
            intent.putExtra("photo_path_right", mPhotoPath14)
            intent.putExtra("photo_path_bottom", mPhotoPath23)
        }
        if(mPhotoType == requestCodeCamera + 14) {
            intent.putExtra("photo_path_left", mPhotoPath13)
            intent.putExtra("photo_path_top", "")
            intent.putExtra("photo_path_right", "")
            intent.putExtra("photo_path_bottom", mPhotoPath24)
        }
        if(mPhotoType == requestCodeCamera + 21) {
            intent.putExtra("photo_path_left", "")
            intent.putExtra("photo_path_top", mPhotoPath11)
            intent.putExtra("photo_path_right", mPhotoPath22)
            intent.putExtra("photo_path_bottom", "")
        }
        if(mPhotoType == requestCodeCamera + 22) {
            intent.putExtra("photo_path_left", mPhotoPath21)
            intent.putExtra("photo_path_top", mPhotoPath12)
            intent.putExtra("photo_path_right", mPhotoPath23)
            intent.putExtra("photo_path_bottom", "")
        }
        if(mPhotoType == requestCodeCamera + 23) {
            intent.putExtra("photo_path_left", mPhotoPath22)
            intent.putExtra("photo_path_top", mPhotoPath13)
            intent.putExtra("photo_path_right", mPhotoPath24)
            intent.putExtra("photo_path_bottom", "")
        }
        if(mPhotoType == requestCodeCamera + 24) {
            intent.putExtra("photo_path_left", mPhotoPath23)
            intent.putExtra("photo_path_top", mPhotoPath14)
            intent.putExtra("photo_path_right", "")
            intent.putExtra("photo_path_bottom", "")
        }

        startActivityForResult(intent, mPhotoType)
    }

    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
            callIntentCamera()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent ?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == (requestCodeCamera + 11) && resultCode == Activity.RESULT_OK) {
                mPhotoPath11 = data?.extras!!.getString("path")
                planogram_img11.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath11))
                planogram_img11.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto11 = true

                posisi=1
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }


            }
            if (requestCode == (requestCodeCamera + 12) && resultCode == Activity.RESULT_OK) {
                mPhotoPath12 = data?.extras!!.getString("path")
                planogram_img12.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath12))
                planogram_img12.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto12 = true
                posisi=2
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }

            }
            if (requestCode == (requestCodeCamera + 13) && resultCode == Activity.RESULT_OK) {
                mPhotoPath13 = data?.extras!!.getString("path")
                planogram_img13.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath13))
                planogram_img13.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto13 = true
                posisi=3
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }
            }
            if (requestCode == (requestCodeCamera + 14) && resultCode == Activity.RESULT_OK) {
                mPhotoPath14 = data?.extras!!.getString("path")
                planogram_img14.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath14))
                planogram_img14.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto14 = true
                posisi=4
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }
            }
            if (requestCode == (requestCodeCamera + 21) && resultCode == Activity.RESULT_OK) {
                mPhotoPath21 = data?.extras!!.getString("path")
                planogram_img21.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath21))
                planogram_img21.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto21 = true
                posisi=5
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }
            }
            if (requestCode == (requestCodeCamera + 22) && resultCode == Activity.RESULT_OK) {
                mPhotoPath22 = data?.extras!!.getString("path")
                planogram_img22.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath22))
                planogram_img22.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto22 = true
                posisi=6
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }
            }
            if (requestCode == (requestCodeCamera + 23) && resultCode == Activity.RESULT_OK) {
                mPhotoPath23 = data?.extras!!.getString("path")
                planogram_img23.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath23))
                planogram_img23.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto23 = true
                posisi=7
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }
            }
            if (requestCode == (requestCodeCamera + 24) && resultCode == Activity.RESULT_OK) {
                mPhotoPath24 = data?.extras!!.getString("path")
                planogram_img24.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath24))
                planogram_img24.scaleType = ImageView.ScaleType.FIT_XY
                isAvailablePhoto24 = true
                posisi=8
                items=daoSession.tReportProductPlanogramDao.queryBuilder()
                        .where(TReportProductPlanogramDao.Properties.Posisi.eq(posisi),
                                TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                                TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId),
                                TReportProductPlanogramDao.Properties.PlanogramTypeId.eq(item!!.planogramTypeId)).list()
                for (i in items){
                    var product=daoSession.tProductMSLRegionDao.queryBuilder()
                            .where(TProductMSLRegionDao.Properties.ProductId.eq(i.productId)).limit(1).unique()
                    product.isChoosed=0
                    product.isChecked=0
                    daoSession.tProductMSLRegionDao.update(product)
                }
                daoSession.tReportProductPlanogramDao.deleteInTx(items)
                if(namaMsl=="MSL") {
                    initProducts(posisi)
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            isAvailablePhoto11 = false
            isAvailablePhoto12 = false
            isAvailablePhoto13 = false
            isAvailablePhoto14 = false
            isAvailablePhoto21 = false
            isAvailablePhoto22 = false
            isAvailablePhoto23 = false
            isAvailablePhoto24 = false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
            items=daoSession.tReportProductPlanogramDao.queryBuilder()
                    .where(TReportProductPlanogramDao.Properties.VisitId.eq(item!!.visitId),
                            TReportProductPlanogramDao.Properties.PlanogramStoreId.eq(item!!.planogramRegionId)).list()
            daoSession.tReportProductPlanogramDao.deleteInTx(items)

            refreshProduct()
            finish()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSliderClick(slider: BaseSliderView?) {
//        if (slider!!.url != "") {
            val intent = Intent(this, FullImageActivity2::class.java)
            intent.putExtra("ImageUrl", slider!!.url)
            startActivity(intent)
//        }
    }

    override fun onBackPressed() {
        discardConfirmation()
    }
}
