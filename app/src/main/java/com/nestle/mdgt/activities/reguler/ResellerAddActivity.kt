package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import com.toptoche.searchablespinnerlibrary.SearchableSpinner

import org.joda.time.DateTime
import org.joda.time.LocalDate

import java.util.ArrayList
import kotlinx.android.synthetic.main.activity_reseller_add.*


class ResellerAddActivity : AppCompatActivity(),
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        Loadable {
    private var spinResellerType: Spinner? = null
    private var spinKabupaten: SearchableSpinner? = null
    private var spinKecamatan: SearchableSpinner? = null
    private var txtResellerName: EditText? = null
    private var txtPhone: EditText? = null
    private var txtAddress: EditText? = null
    private var isAvailableLocation = false
    private var requestLocationCount:Int=0



    private var mAreaId: Int = 0

    private var daoSession: DaoSession? = null
    private var resellerDao: TResellerDao? = null
    private var kabupatenDao: TKabupatenDao? = null
    private var kecamatanDao: TKecamatanDao? = null
    private var resellerTypeDao: TResellerTypeDao? = null
    private var kabupatenAdapter: ArrayAdapter<TKabupaten>? = null
    private var kecamatanAdapter: ArrayAdapter<TKecamatan>? = null
    private var resellerTypeAdapter: ArrayAdapter<TResellerType>? = null
    internal var progressDialog: ProgressDialog? = null
    private var radiusId=1
    val REQUEST_CODE_TAG_LOCATION = 20


    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mCurrentLocation: Location? = null
    private val mStoreLocation = Location("StoreLocation")
    private var radiusVerificationLimit: Int = 0

    private var radiusOk=false
    private var mStoreId: Int = 0
    private lateinit var mStore:TStore
    private var radiusName=""





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reseller_add)

        val subTitle = Login.getUsername(this) + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setTitle(getString(R.string.label_tambah_reseller))
        supportActionBar!!.setSubtitle(subTitle)

        initLocationRequest()

        initData()
        initView()

    }

    private fun initData() {
        mAreaId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_AREA_ID, 0)
        radiusId= intent.getIntExtra("radiusId",1)
        radiusName=intent.getStringExtra("radiusName")
        daoSession = (application as GlobalApp).daoSession
        mStoreId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID,0)
        mStore = daoSession!!.tStoreDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(mStoreId)).limit(1).unique()
        resellerDao = daoSession!!.tResellerDao
        kabupatenDao = daoSession!!.tKabupatenDao
        kecamatanDao = daoSession!!.tKecamatanDao
        resellerTypeDao = daoSession!!.tResellerTypeDao

        radiusVerificationLimit = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_ID, 0)
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager



    }

    private fun initView() {
        val btnSimpan = findViewById<View>(R.id.resellerAdd_btSimpan) as Button
        txtResellerName = findViewById<View>(R.id.resellerAdd_txtResellerName) as EditText
        txtPhone = findViewById<View>(R.id.resellerAdd_txtPhone) as EditText
        txtAddress = findViewById<View>(R.id.resellerAdd_txtAddress) as EditText
        spinKabupaten = findViewById<View>(R.id.resellerAdd_spinKabupaten) as SearchableSpinner
        spinKecamatan = findViewById<View>(R.id.resellerAdd_spinKecamatan) as SearchableSpinner
        spinResellerType = findViewById<View>(R.id.resellerAdd_spinResellerType) as Spinner

        if(radiusId==2){
            resellerbtnLocation.visibility=View.GONE
        }
        else{
            resellerbtnLocation.visibility=View.VISIBLE
        }

        resellerbtnLocation.setOnClickListener {
            startLocationUpdates(REQUEST_CODE_TAG_LOCATION)
        }

        progressDialog = ProgressDialog(this)


        spinKabupaten!!.setTitle("Pilih Kabupaten")
        spinKabupaten!!.setPositiveButton("Tutup")
        spinKecamatan!!.setTitle("Pilih Kecamatan")
        spinKecamatan!!.setPositiveButton("Tutup")

        resellerTypeAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item,
                resellerTypeDao!!.queryBuilder().orderAsc(TResellerTypeDao.Properties.ResellerTypeName).list())
        resellerTypeAdapter!!.insert(TResellerType(null, 0, ""), 0)
        spinResellerType!!.adapter = resellerTypeAdapter

        kabupatenAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item,
                kabupatenDao!!.queryBuilder().orderAsc(TKabupatenDao.Properties.KabupatenName).list())
        kabupatenAdapter!!.insert(TKabupaten(null, 0, "", 0), 0)
        spinKabupaten!!.adapter = kabupatenAdapter

        spinKabupaten!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                val kabupaten = kabupatenAdapter!!.getItem(i)
                kecamatanAdapter = ArrayAdapter(this@ResellerAddActivity, android.R.layout.simple_spinner_dropdown_item,
                        kecamatanDao!!.queryBuilder()
                                .where(TKecamatanDao.Properties.KabupatenId.eq(kabupaten!!.kabupatenId))
                                .orderAsc(TKecamatanDao.Properties.KecamatanName).list())
                kecamatanAdapter!!.insert(TKecamatan(null, 0, "", 0), 0)
                spinKecamatan!!.adapter = kecamatanAdapter
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        btnSimpan.setOnClickListener { btnSimpanClicked() }
    }

    private fun btnSimpanClicked() {
        //region Validation
        if(radiusId==1){
            if(!isAvailableLocation){
                Toast.makeText(this@ResellerAddActivity,"Anda belum tag lokasi reseller",Toast.LENGTH_SHORT).show()
                return
            }

//            if(!radiusOk){
//                Toast.makeText(this@ResellerAddActivity,"Lokasi reseller terlalu jauh dari toko",Toast.LENGTH_SHORT).show()
//                return
//            }
        }



        if (!FormValidation.validateRequiredText(this, txtResellerName!!, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, txtPhone!!, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, txtAddress!!, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, spinResellerType!!, String.format(getString(R.string.info_msg_required_field_spinner), "tipe reseller"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, spinKabupaten!!, String.format(getString(R.string.info_msg_required_field_spinner), "kabupaten"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, spinKecamatan!!, String.format(getString(R.string.info_msg_required_field_spinner), "kecamatan"))) {
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        val storeId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        val storeCode = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_CODE, "")
        val storeName = SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")
        val surveyorId = Login.getUserId(this)
        val surveyorNik = Login.getUserId(this)
        val surveyorName = Login.getEmployeeName(this)
        val kabupatenSelected = spinKabupaten!!.selectedItem as TKabupaten
        val kecamatanSelected = spinKecamatan!!.selectedItem as TKecamatan
        val resellerTypeSelected = spinResellerType!!.selectedItem as TResellerType
        val resellerId = Helper.generateResellerId(storeId, resellerTypeSelected.resellerTypeId, kecamatanSelected.kecamatanId)

        val reseller = TReseller()
        reseller.storeId = storeId
        reseller.storeCode = storeCode
        reseller.storeName = storeName
        reseller.surveyorId = surveyorId
        reseller.surveyorNik = surveyorNik.toString()
        reseller.surveyorName = surveyorName
        reseller.resellerId = resellerId
        reseller.resellerName = txtResellerName!!.text.toString().trim { it <= ' ' }
        reseller.resellerTypeId = resellerTypeSelected.resellerTypeId
        reseller.resellerTypeName = resellerTypeSelected.resellerTypeName
        reseller.radiusId = radiusId
        reseller.radiusName = radiusName
        reseller.phone = txtPhone!!.text.toString().trim { it <= ' ' }
        reseller.address = txtAddress!!.text.toString().trim { it <= ' ' }
        reseller.kabupatenId = kabupatenSelected.kabupatenId!!
        reseller.kabupatenName = kabupatenSelected.kabupatenName
        reseller.kecamatanId = kecamatanSelected.kecamatanId!!
        reseller.kecamatanName = kecamatanSelected.kecamatanName
        reseller.registerDate = DateTime.now().toString("yyyy-MM-dd")
        reseller.latitude = 0.0
        reseller.longitude = 0.0
        reseller.isDonePenjualan = false
        reseller.isDonePullProgram = false
        resellerDao!!.insert(reseller)

        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val listReport = ArrayList<GenericReport>()

        val id_report = DataController.waktuForReport + resellerId + "Reseller Add"//+category.id_category+"-"+c.id_product_heinz+"-"+c.competitor_id;
        val parameters = ArrayList<ReportParameter>()
        parameters.add(ReportParameter("1", id_report, "reseller_id", resellerId, ReportParameter.TEXT))
        parameters.add(ReportParameter("2", id_report, "reseller_name", txtResellerName!!.text.toString().trim { it <= ' ' }, ReportParameter.TEXT))
        parameters.add(ReportParameter("3", id_report, "reseller_type_id", resellerTypeSelected.resellerTypeId.toString(), ReportParameter.TEXT))
        parameters.add(ReportParameter("4", id_report, "phone", txtPhone!!.text.toString().trim { it <= ' ' }, ReportParameter.TEXT))
        parameters.add(ReportParameter("5", id_report, "address", txtAddress!!.text.toString().trim { it <= ' ' }, ReportParameter.TEXT))
        parameters.add(ReportParameter("6", id_report, "kecamatan_id", kecamatanSelected.kecamatanId.toString(), ReportParameter.TEXT))
        parameters.add(ReportParameter("7", id_report, "register_date", currentDate, ReportParameter.TEXT))
        parameters.add(ReportParameter("8", id_report, "store_id", storeId.toString(), ReportParameter.TEXT))
        parameters.add(ReportParameter("9", id_report, "surveyor_id", surveyorId.toString(), ReportParameter.TEXT))
        parameters.add(ReportParameter("10", id_report, "radius_id", radiusId.toString(), ReportParameter.TEXT))
        if(mCurrentLocation!=null){
            parameters.add(ReportParameter("11", id_report, "longitude", mCurrentLocation!!.longitude.toString(), ReportParameter.TEXT))
            parameters.add(ReportParameter("12", id_report, "latitude", mCurrentLocation!!.latitude.toString(), ReportParameter.TEXT))
        }




        val report = GenericReport(id_report,
                Login.getEmployeeRoleId(this@ResellerAddActivity).toString(),
                "Report Reseller Add",
                "Report Reseller Add : " + resellerId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getInsertReseller(),
                currentDate, Config.NO_CODE, currentUtc, parameters)
        listReport.add(report)
        val tambahReportTask = TambahReportTask(this, listReport)

        tambahReportTask.execute()


        //        TambahReportTask task =new  TambahReportTask(this, listReport);
        //        task.execute();

        // add report reseller to queue
        //BACKGROUND SENDING VERSI 1.0
        //        ReportResellerAdd report = new ReportResellerAdd();
        //        report.model = reseller;
        //        report.reportDate = DateTimeUtils.transformFullTime(DateTimeUtils.getWaktuInLong());
        //        ReportSender sender = ReportSender.loadLogReport();
        //        sender.addReport(report);

        finish()
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

    private fun initLocationRequest() {
        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            try {
                Toast.makeText(this, getString(R.string.info_msg_google_play_service_not_available), Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_market_google_play_service))))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_store_google_play_service))))
            }

        }
        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    private fun startLocationUpdates(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    try {
                        mLocationManager!!.removeTestProvider(LocationManager.GPS_PROVIDER)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    resellerbtnLocation.isEnabled = false
                    isAvailableLocation = false
                    lblLoading.visibility = View.VISIBLE
                    resellerbtnLocation.setImageResource(R.drawable.ic_my_location_grey_24dp)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                resellerbtnLocation.isEnabled = false
                isAvailableLocation = false
                lblLoading.text = getString(R.string.info_msg_search_location)
                lblLoading.visibility = View.VISIBLE
                resellerbtnLocation.setImageResource(R.drawable.ic_my_location_grey_24dp)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = Config.INTERVAL
        mLocationRequest!!.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            resellerbtnLocation.isEnabled = true
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            if (!isAvailableLocation) {
                lblLoading.visibility = View.GONE
                resellerbtnLocation.setImageResource(R.drawable.ic_my_location_white_24dp)
                 resellerbtnLocation.isEnabled = false
            }
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }


    override fun onLocationChanged(location: Location) {
        if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location
            if (LocationUtils.isFromMockProvider(this, location)) {
                stopLocationUpdates()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                resellerbtnLocation.isEnabled = true
                Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
            } else {

                    if (location.accuracy <= Config.ACCURACY_SMALL_LIMIT) {
                        if (requestLocationCount == Config.REQUEST_LOCATION_LIMIT) {
                            stopLocationUpdates()
                            isAvailableLocation = true
                            resellerbtnLocation.isEnabled = true
                            resellerbtnLocation.setImageResource(R.drawable.ic_my_location_white_24dp)
                            lblLoading.setText(getString(R.string.label_klik_untuk_verfikasi_lokasi))
                        } else {
                            mStoreLocation.latitude = mStore!!.latitude
                            mStoreLocation.longitude = mStore!!.longitude
                            val distance = location.distanceTo(mStoreLocation)
                            lblLoading.setText("Jarak anda dengan store " + Helper.convertMeterToKm(distance))
                            resellerbtnLocation.setImageResource(R.drawable.ic_my_location_white_24dp)
                            isAvailableLocation = true
                            requestLocationCount++
                            resellerbtnLocation.isEnabled = true
                            if (distance <= radiusVerificationLimit) {
                                Log.i("distance",distance.toString())
                                stopLocationUpdates()
                                mCurrentLocation = location
                                radiusOk=true
                                resellerbtnLocation.isEnabled = true
                                resellerbtnLocation.setImageResource(R.drawable.ic_my_location_white_24dp)

                                lblLoading.setText("Jarak anda dengan store " + Helper.convertMeterToKm(distance))
                                lblLoading.visibility=View.VISIBLE
                                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                            }
                        }
                    }


            }
        }
    }

    public override fun onResume() {
        super.onResume()
        resellerbtnLocation.isEnabled = true

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    public override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient!!.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        if (requestCode == REQUEST_CODE_TAG_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates(REQUEST_CODE_TAG_LOCATION)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }




    override fun setLoading(show: Boolean, title: String, message: String) {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog!!.setTitle(title)
        progressDialog!!.setMessage(message)
        progressDialog!!.setCancelable(false)
        if (show) {
            progressDialog!!.show()
            //selesai_button.setEnabled(false);
        } else {
            progressDialog!!.dismiss()
            // selesai_button.setEnabled(true);
        }
    }

    override fun setFinish(result: Boolean, message: String) {

        // selesai_button.setEnabled(true);
        if (result) {

        }

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()

    }
}
