//package com.pitjarus.garudafood.activities
//
//import android.Manifest
//import android.app.Activity
//import android.app.ProgressDialog
//import android.content.Context
//import android.content.Intent
//import android.content.pm.PackageManager
//import android.graphics.BitmapFactory
//import android.hardware.Camera
//import android.os.Build
//import android.os.Bundle
//import android.support.v7.app.AppCompatActivity
//import android.view.*
//import android.widget.*
//import com.pitjarus.garudafood.GlobalApp
//import com.pitjarus.garudafood.R
//import com.pitjarus.garudafood.database.DaoSession
//import com.pitjarus.garudafood.database.TAbsensi
//import com.pitjarus.garudafood.models.Login
//import com.pitjarus.garudafood.reports.GenericReport
//import com.pitjarus.garudafood.reports.ReportParameter
//import com.pitjarus.garudafood.rests.ApiClient
//import com.pitjarus.garudafood.tasks.Loadable
//import com.pitjarus.garudafood.tasks.TambahReportTask
//import com.pitjarus.garudafood.utils.Config
//import com.pitjarus.garudafood.utils.DateTimeUtils
//import com.pitjarus.garudafood.utils.SharedPrefsUtils
//import kotlinx.android.synthetic.main.activity_login_absensi.*
//import org.joda.time.DateTime
//import java.util.*
//
//class AbsensiActivity : AppCompatActivity(), Loadable {
//
//    companion object {
//        const val REQUEST_CODE_CAMERA = 108
//    }
//
//    private lateinit var absensi: TAbsensi
//    private lateinit var daoSession: DaoSession
//    private var progressDialog: ProgressDialog? = null
//    private var mPhotoType: Int = 0
//    private var mPhotoPromoPath: String? = null
//
//
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_login_absensi)
//
//        initData()
//        initView()
////        initValuesToViews()
//    }
//
//    private fun initData() {
//        daoSession = (application as GlobalApp).daoSession!!
//        absensi = TAbsensi()
//        absensi.photoPath=mPhotoPromoPath
//    }
//
//    private fun initView() {
//        absensi_lblSurveyorName.text = Login.getEmployeeName(this)
//        absensi_lblTanggal.text = DateTime.now().toString("EEEE, dd MMMM yyyy")
//
//
//        val onClickListener = View.OnClickListener { view -> validateForm(view.id) }
//        btnMasuk.setOnClickListener(onClickListener)
//        btnSakit.setOnClickListener(onClickListener)
//        btnIzin.setOnClickListener(onClickListener)
//        btnStockOpname.setOnClickListener(onClickListener)
//        btnPerbantuan.setOnClickListener(onClickListener)
//        btnTukarOff.setOnClickListener(onClickListener)
//        btnCuti.setOnClickListener(onClickListener)
//
//
//        btnPhoto1.setOnClickListener {
//            mPhotoType = AbsensiActivity.REQUEST_CODE_CAMERA
//            startCamera(mPhotoType)
//        }
//
//        imgAbsen1.setOnClickListener {
//            if (mPhotoPromoPath != null) {
//                val intent = Intent(this, FullImageActivity::class.java)
//                intent.putExtra("ImageUrl", mPhotoPromoPath)
//                startActivity(intent)
//            }
//        }
//    }
//    private fun validateForm(id: Int) {
//        when (id) {
//            R.id.btnMasuk -> {
//                val builder = android.app.AlertDialog.Builder(this)
//                builder.setTitle(getString(R.string.label_confirm))
//                builder.setMessage(getString(R.string.confirm_msg_absen))
//                builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm(Config.ABSEN_MASUK,"Masuk") }
//                builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
//                val alertDialog = builder.create()
//                alertDialog.setCanceledOnTouchOutside(false)
//                alertDialog.show()
//            }
//            R.id.btnSakit -> {
//                RemarksPopup(Config.ABSEN_SAKIT,Config.ABSEN_SAKIT_TEXT)
//            }
//            R.id.btnStockOpname -> {
//                RemarksPopup(Config.ABSEN_STOCK_OPNAME,Config.ABSEN_STOCK_OPNAME_TEXT)
//            }
//            R.id.btnPerbantuan -> {
//                RemarksPopup(Config.ABSEN_PERBANTUAN,Config.ABSEN_PERBANTUAN_TEXT)
//            }
//            R.id.btnCuti -> {
//                RemarksPopup(Config.ABSEN_CUTI,Config.ABSEN_CUTI_TEXT)
//            }
//            R.id.btnIzin -> {
//                SpinnerRemarksPopup(Config.ABSEN_IZIN,Config.ABSEN_IZIN_TEXT)
//            }
//            R.id.btnTukarOff -> {
//                RemarksPopup(Config.ABSEN_TUKAR_OFF,Config.ABSEN_TUKAR_OFF_TEXT)
//            }
//        }
//    }
//
//    private fun RemarksPopup(absensiType: String, AbsensiText: String) {
//        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        val layoutPopup = inflater.inflate(R.layout.activity_absensi_foto_popup, findViewById(R.id.popup_absenOff))
//        val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
//        val lblTitle = layoutPopup.findViewById(R.id.popup_lblTitle) as TextView
//        val btnOk = layoutPopup.findViewById(R.id.popup_btnOk) as Button
//        val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as Button
//        val spinnerAbsen = layoutPopup.findViewById(R.id.spinnerAbsen) as Spinner
//
//        lblTitle.text = "Keterangan $AbsensiText"
//        spinnerAbsen.visibility=View.GONE
//
//        btnOk.setOnClickListener {
//                submitForm(absensiType,AbsensiText)
//                mPopupWindow.dismiss()
//        }
//        btnClose.setOnClickListener { mPopupWindow.dismiss() }
//        mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)
//
//
//    }
//
//    private fun SpinnerRemarksPopup(absensiType: String,AbsensiText: String) {
//        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        val layoutPopup = inflater.inflate(R.layout.activity_absensi_foto_popup, findViewById(R.id.popup_absenOff))
//        val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
//        val lblTitle = layoutPopup.findViewById(R.id.popup_lblTitle) as TextView
//        val btnOk = layoutPopup.findViewById(R.id.popup_btnOk) as Button
//        val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as Button
//        val spinnerAbsen = layoutPopup.findViewById(R.id.spinnerAbsen) as Spinner
//
//        lblTitle.text = "Keterangan $AbsensiText"
//
//        val spinnerCountShoesArrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, resources.getStringArray(R.array.arr_absen))
//        spinnerAbsen.adapter = spinnerCountShoesArrayAdapter
//
//        btnOk.setOnClickListener {
//            if(spinnerAbsen.getSelectedItem().toString()=="Pengurusan SIM"){
//                absensi.absensiSubType = "1"
//            }
//            if(spinnerAbsen.getSelectedItem().toString()=="Duka Cita Saudara Kandung, Orang Tua dan Mertua"){
//                absensi.absensiSubType = "2"
//            }
//            if(spinnerAbsen.getSelectedItem().toString()=="Lainnya"){
//                absensi.absensiSubType = "3"
//            }
//
//            if(spinnerAbsen.getSelectedItem().toString()==""){
//                Toast.makeText(this, "Pilih izin terlebih dahulu.", Toast.LENGTH_SHORT).show()
//            }
//            else{
//                submitForm(absensiType,AbsensiText)
//                mPopupWindow.dismiss()
//            }
//
//        }
//
//        btnClose.setOnClickListener { mPopupWindow.dismiss() }
//        mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)
//    }
//
//    private fun submitForm(absensiType: String,AbsensiText: String) {
//
//        absensi.surveyorId = Login.getUserId(this)
//        absensi.absensiDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
//        absensi.absensiDateTimeUtc = DateTimeUtils.currentUtc
//        absensi.absensiType = absensiType
//        absensi.photoPath = mPhotoPromoPath
//
//
//        val currentUtc = absensi.absensiDateTimeUtc
//        val currentDate = DateTime.now().toString("yyyy-MM-dd")
//        val reportId = "ABS" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
//
//        val params = ArrayList<ReportParameter>()
//        params.add(ReportParameter("1", reportId, "surveyor_id", absensi.surveyorId.toString(), ReportParameter.TEXT))
//        params.add(ReportParameter("2", reportId, "attendance_type_id", absensi.absensiType, ReportParameter.TEXT))
//        if(absensi.absensiSubType!=null){
//            params.add(ReportParameter("3", reportId, "attendance_sub_type_id", absensi.absensiSubType, ReportParameter.TEXT))
//        }
//        else if(absensi.absensiSubType==null){
//            params.add(ReportParameter("3", reportId, "attendance_sub_type_id", "0", ReportParameter.TEXT))
//        }
//        params.add(ReportParameter("4", reportId, "notes", "0", ReportParameter.TEXT))
//        params.add(ReportParameter("5", reportId, "attendance_datetime", absensi.absensiDateTime.toString(), ReportParameter.TEXT))
//        params.add(ReportParameter("6", reportId, "attendance_datetime_utc", absensi.absensiDateTimeUtc.toString(), ReportParameter.TEXT))
//
//        if(mPhotoPromoPath!=null){
//        params.add(ReportParameter("7", reportId, "photo_file", absensi.photoPath, ReportParameter.FILE))
//        }
//        else if(mPhotoPromoPath==null){
//            params.add(ReportParameter("7", reportId, "photo_file", "", ReportParameter.TEXT))
//        }
//
//        val report = GenericReport(reportId,
//                Login.getUserId(this).toString(),
//                "Absensi",
//                "Absensi: " + AbsensiText + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
//                ApiClient.getInsertAbsensiUrl(),
//                currentDate, Config.NO_CODE, currentUtc, params)
//
//        val task = TambahReportTask(this)
//        task.execute(report)
//    }
//
//    private fun callIntentCamera() {
//        val intent = Intent(this, PhotoActivity::class.java)
//        intent.putExtra("nama_file", "promo_" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))
//        intent.putExtra("CameraFacing", Camera.CameraInfo.CAMERA_FACING_BACK)
//        startActivityForResult(intent, mPhotoType)
//    }
//
//    private fun startCamera(requestCamera: Int) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
//                callIntentCamera()
//            } else {
//                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
//                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
//                }
//
//                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
//            }
//        } else {
//            callIntentCamera()
//        }
//    }
//
//    private fun logout() {
//        Login.logout(this)
//        val iLogin = Intent(this, LoginActivity::class.java)
//        iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        startActivity(iLogin)
//    }
//
////    override fun onCreateOptionsMenu(menu: Menu): Boolean {
////        val inflater = menuInflater
////        inflater.inflate(R.menu.menu_absensi, menu)
////
////        return true
////    }
//
////    override fun onOptionsItemSelected(item: MenuItem): Boolean {
////        val id = item.itemId
////
////        if (id == R.id.action_logout) {
////            val builder = android.app.AlertDialog.Builder(this)
////            builder.setTitle(getString(R.string.label_confirm))
////            builder.setMessage(getString(R.string.confirm_msg_logout))
////            builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> logout() }
////            builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
////            val alertDialog = builder.create()
////            alertDialog.setCanceledOnTouchOutside(false)
////            alertDialog.show()
////        }
////
////
////        return super.onOptionsItemSelected(item)
////    }
//
//    override fun setLoading(show: Boolean, title: String, message: String) {
//        try {
//            if (progressDialog == null) progressDialog = ProgressDialog(this)
//            progressDialog!!.setTitle(title)
//            progressDialog!!.setMessage(message)
//            progressDialog!!.setCancelable(false)
//            if (show) {
//                progressDialog!!.show()
//            } else {
//                progressDialog!!.dismiss()
//            }
//        } catch (e: Exception) {
//            progressDialog!!.dismiss()
//            e.printStackTrace()
//        }
//    }
//
//    override fun setFinish(result: Boolean, message: String) {
//        if (result) {
//            daoSession.tAbsensiDao.insert(absensi)
//            when (absensi.absensiType) {
//                Config.ABSEN_MASUK -> {
//                    SharedPrefsUtils.setIntegerPreference(this, Config.KEY_IS_ABSENT, 1)
//                    val intent = Intent(this, StoreListActivity::class.java)
//                    startActivity(intent)
//                }
//                Config.ABSEN_SAKIT, Config.ABSEN_IZIN, Config.ABSEN_CUTI, Config.ABSEN_PERBANTUAN, Config.ABSEN_STOCK_OPNAME, Config.ABSEN_TUKAR_OFF -> logout()
//            }
//            finish()
//        }
//        //Toast.makeText(this, message, Toast.LENGTH_LONG).show()
//    }
//
//    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        try {
//            super.onActivityResult(requestCode, resultCode, data)
//            if (requestCode == REQUEST_CODE_CAMERA && resultCode == Activity.RESULT_OK) {
//                mPhotoPromoPath = data?.extras!!.getString("path")
//                imgAbsen1.setImageBitmap(BitmapFactory.decodeFile(mPhotoPromoPath))
//                imgAbsen1.scaleType = ImageView.ScaleType.CENTER_CROP
////                isAvailablePhotoPromo = true
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
////            isAvailablePhotoPromo = false
//        }
//    }
//}
