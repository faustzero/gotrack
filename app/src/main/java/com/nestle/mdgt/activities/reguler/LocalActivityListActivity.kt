package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.adapters.LocalAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.rests.LoginApiInterface
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.mtoolbar.*

class LocalActivityListActivity : AppCompatActivity() {
    private lateinit var lblNoData: TextView
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView

    private var mVisitId: String? = null
    private var mUserId: Int? = null

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportActivityHeaderDao
    private lateinit var items: MutableList<TReportActivityHeader>
    private lateinit var local: MutableList<TReportActivityHeader>
//    private lateinit var mHeader: List<TReportActivityHeader>
    private var displayHeader = TReportActivityHeader()
//    private var adapter: LocalAdapter? = null
    private var progressDialog: ProgressDialog? = null
    private var surveyId:Int=0
    private var storeId:Int=0
//    private var isSaved="0"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_activity_list)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_daftar_activity_tracking)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}"

        initData()
        initView()
        if(intent.getBooleanExtra("IsFromLogin", false)) {
            initPopupInfo()
        }
    }

    private fun initData() {
        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
//        val id = intent.getLongExtra("Id", 0)
        surveyId=Login.getEmployeeSurveyId(this)
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mUserId =Login.getUserId(this)
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportActivityHeaderDao
        items = itemDao.queryBuilder().where(TReportActivityHeaderDao.Properties.UserId.eq(mUserId)).list()
        progressDialog = ProgressDialog(this)
        local = daoSession.tReportActivityHeaderDao.queryBuilder().where(TReportActivityHeaderDao.Properties.UserId.eq(mUserId),(TReportActivityHeaderDao.Properties.Saved.eq(1))).list()
//        mHeader = itemDao.loadAll()

        Log.i("data_local",local.size.toString())
//        planograms = daoSession.tReportPlanogramDao.queryBuilder()
//                .where(TReportPlanogramDao.Properties.VisitId.eq(mVisitId),
//                        TReportPlanogramDao.Properties.IsDone.eq(Config.YES_CODE)).list()
//
//        campaigns = daoSession.tReportCampaignHeaderDao.queryBuilder()
//                .where(TReportCampaignHeaderDao.Properties.VisitId.eq(mVisitId),
//                        TReportCampaignHeaderDao.Properties.IsDone.eq(true)).list()
    }

    private fun initView() {
        lblNoData = findViewById(R.id.local_lblNoData)
        recycler = findViewById(R.id.local_recycler)

        val btnLanjut = findViewById<Button>(R.id.local_btnLanjut)

        btnLanjut.setOnClickListener { validateForm() }
    }


    private fun initPopupInfo() {
        try {
            val info = daoSession.tInfoDao.queryBuilder().limit(1).unique()
            if (info != null) {
                val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val layoutPopup = inflater.inflate(R.layout.popup_reminder_info, findViewById(R.id.popup_reminder_info))
                val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
                val btnClose = layoutPopup.findViewById(R.id.popup_btnClose) as ImageButton
                val img = layoutPopup.findViewById(R.id.popup_imgInfo) as ImageView
                val desc = layoutPopup.findViewById(R.id.popup_lblDescription) as WebView

                Glide.with(inflater.context).load(ApiClient.hostUrl + info.imagePath)
                        .apply(RequestOptions().fitCenter())
                        .into(img)

                desc.settings.javaScriptEnabled = false
                desc.loadData(info.infoName,"text/html", null)

                btnClose.setOnClickListener { mPopupWindow.dismiss() }
                layoutPopup.post({ mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0) })
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun reloadList() {
        local = daoSession.tReportActivityHeaderDao.queryBuilder().where(TReportActivityHeaderDao.Properties.UserId.eq(mUserId),
                (TReportActivityHeaderDao.Properties.Saved.eq(1)),(TReportActivityHeaderDao.Properties.IsDeleted.eq(false))).list()
        lblNoData!!.visibility = View.GONE
        if(local.size==0){
            lblNoData!!.visibility = View.VISIBLE
        }

        val adapter = LocalAdapter(daoSession,this@LocalActivityListActivity,local,R.layout.activity_local_activity_list_row, object : LocalAdapter.OnItemClickListener {
            override fun onItemClick(item: TReportActivityHeader) {
                val intent = Intent(this@LocalActivityListActivity, LocalAddActivity::class.java)
                intent.putExtra("Id", item.id)
                intent.putExtra("headerId",item.headerId)
                startActivity(intent)

            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = adapter
        adapter.notifyDataSetChanged()

    }
//    private fun deleteList () {
//
//        val builder = AlertDialog.Builder(this )
//        builder.setTitle("Confirmation")
//        builder.setMessage("Are you sure want to delete ?")
//        builder.setPositiveButton("YES") { _, i ->
//            Log.i("log1","cari data")
//            val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
//            val call  = apiService.deleteList()
//            Log.i("delete","cari data delete")
//
//        }
//    }


    private fun validateForm() {

        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, i ->
            submitForm()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {

        SharedPrefsUtils.setBooleanPreference(this@LocalActivityListActivity, Config.KEY_CHECKED_REPORT_LOCAL_ACTIVITY, true)
        finish()

    }

    public override fun onResume() {
        super.onResume()
        reloadList()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_local_activity_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }
        if (id == R.id.action_add) {

            val intent = Intent(this, LocalAddActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_back))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
    discardConfirmation()
    }
}
