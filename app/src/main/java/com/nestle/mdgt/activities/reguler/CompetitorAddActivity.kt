package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoActivity
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.FormValidation
import com.nestle.mdgt.utils.NumberTextWatcherForThousand
import com.nestle.mdgt.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_competitor_add.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class CompetitorAddActivity : AppCompatActivity() {
    private var mId: Long = 0
    private val requestCodeCamera = 10
    private var mPhotoPath: String? = null
    private var mPhotoType: Int = 0
    private var isAvailablePhoto: Boolean = false

    private lateinit var daoSession: DaoSession
    private lateinit var itemDao: TReportCompetitorDao
    private lateinit var categoryDao: TCategoryCompetitorDao
    private lateinit var productDao: TProductCompetitorDao
    private lateinit var activityDao: TActivityTypeDao

    private var item: TReportCompetitor? = null

    private var competitorCategoryAdapter: ArrayAdapter<TCategoryCompetitor>? = null
    private var competitorProductAdapter: ArrayAdapter<TProductCompetitor>? = null
    private var competitorTypeAdapter: ArrayAdapter<TActivityType>? = null
    private lateinit var mCalendar: Calendar
    private var storeId:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_competitor_add)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_tambah_competitor)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "")}"

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {
        storeId= SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        mId = intent.getLongExtra("Id", 0)
        daoSession = (application as GlobalApp).daoSession!!
        itemDao = daoSession.tReportCompetitorDao
        categoryDao = daoSession.tCategoryCompetitorDao
        productDao = daoSession.tProductCompetitorDao
        activityDao = daoSession.tActivityTypeDao
        item = itemDao.load(mId)
    }

    private fun initView() {
        competitor_edPrice.addTextChangedListener(NumberTextWatcherForThousand(competitor_edPrice))


        competitorCategoryAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, categoryDao.queryBuilder().list())
        competitorCategoryAdapter!!.insert(TCategoryCompetitor(null, 0, ""), 0)
        competitor_spinCategory.adapter = competitorCategoryAdapter


        competitorTypeAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, activityDao.queryBuilder().list())
        competitorTypeAdapter!!.insert(TActivityType(null, 0, ""), 0)
        competitor_spinActivity.adapter = competitorTypeAdapter


        competitor_spinCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val categoryComp = competitorCategoryAdapter!!.getItem(position) as TCategoryCompetitor
                var productsComp=productDao.queryBuilder().where(TProductCompetitorDao.Properties.ProductCategoryId.eq(categoryComp.productCategoryId)).list()

                competitorProductAdapter = ArrayAdapter(this@CompetitorAddActivity, android.R.layout.simple_spinner_dropdown_item, productsComp)
                competitorProductAdapter!!.insert(TProductCompetitor(null, 0, "","", 0, "", 0,"",0,"",0), 0)
                competitor_spinProduct.adapter = competitorProductAdapter

                if(item!=null){
                    if(item!!.productId>0){
                        val productPosition = competitorProductAdapter!!.getPosition(productDao.queryBuilder()
                                .where(TProductCompetitorDao.Properties.ProductId.eq(item!!.productId))
                                .limit(1).unique())
                        competitor_spinProduct.setSelection(productPosition)
                        competitor_spinProduct.isEnabled = false
                        competitor_spinProduct.setBackgroundResource(R.drawable.form_spinner_style_disabled)

                    }
                }

            }
        }

        competitor_imgPhoto.setOnClickListener {
            if (mPhotoPath != null) {
                val intent = Intent(this, FullImageActivity::class.java)
                intent.putExtra("ImageUrl", mPhotoPath)
                startActivity(intent)
            }
        }

        competitor_btnPhoto.setOnClickListener {
            mPhotoType = requestCodeCamera
            startCamera(mPhotoType)
        }

        mCalendar = Calendar.getInstance()
        val dateMulai = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            mCalendar.run {
                set(Calendar.YEAR, year)
                set(Calendar.MONTH, monthOfYear)
                set(Calendar.DAY_OF_MONTH, dayOfMonth)
            }
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
//            val sdf2 = SimpleDateFormat("dd MMMM yyyy", Locale.US)
            competitor_edPeriodeMulai.setText(sdf.format(mCalendar.time))
//            competitor_edPeriodeMulai.tag = sdf.format((mCalendar.time))
        }
        competitor_edPeriodeMulai.setOnClickListener({
            DatePickerDialog(this, dateMulai, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)).show()
        })

        mCalendar = Calendar.getInstance()
        val dateSelesai = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            mCalendar.run {
                set(Calendar.YEAR, year)
                set(Calendar.MONTH, monthOfYear)
                set(Calendar.DAY_OF_MONTH, dayOfMonth)
            }
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
//            val sdf2 = SimpleDateFormat("dd MMMM yyyy", Locale.US)
            competitor_edPeriodeSelesai.setText(sdf.format(mCalendar.time))
//            competitor_edPeriodeSelesai.tag = sdf.format((mCalendar.time))
        }
        competitor_edPeriodeSelesai.setOnClickListener({
            DatePickerDialog(this, dateSelesai, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)).show()
        })

        competitor_btnSimpan.setOnClickListener { validateForm() }
    }

    private fun initValuesToViews() {
        if (item != null) {
            supportActionBar!!.title = item!!.activityTypeName
            val categoryPosition = competitorCategoryAdapter!!.getPosition(categoryDao.queryBuilder()
                    .where(TCategoryCompetitorDao.Properties.ProductCategoryId.eq(item!!.categoryId))
                    .limit(1).unique())
            competitor_spinCategory.setSelection(categoryPosition)
            competitor_spinCategory.isEnabled = false
            competitor_spinCategory.setBackgroundResource(R.drawable.form_spinner_style_disabled)

//            val productPosition = competitorProductAdapter!!.getPosition(productDao.queryBuilder()
//                    .where(TProductCompetitorDao.Properties.ProductId.eq(item!!.productId))
//                    .limit(1).unique())
//            competitor_spinProduct.setSelection(productPosition)
//            competitor_spinProduct.isEnabled = false
//            competitor_spinProduct.setBackgroundResource(R.drawable.form_spinner_style_disabled)

            val activityTypePosition = competitorTypeAdapter!!.getPosition(activityDao.queryBuilder()
                    .where(TActivityTypeDao.Properties.ActivityTypeId.eq(item!!.activityTypeId))
                    .limit(1).unique())
            competitor_spinActivity.setSelection(activityTypePosition)
            competitor_spinActivity.isEnabled = false
            competitor_spinActivity.setBackgroundResource(R.drawable.form_spinner_style_disabled)

            competitor_edDeskripsi.setText(item!!.description)
            competitor_edPeriodeMulai.setText(item!!.startDate)
            competitor_edPeriodeSelesai.setText(item!!.endDate)

            mPhotoPath = item!!.photoPath
            val file = File(mPhotoPath)
            if (file.exists()) {
                competitor_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath))
                competitor_imgPhoto.scaleType = ImageView.ScaleType.CENTER_CROP
                isAvailablePhoto = true
            }

            competitor_edPrice.setText(item!!.harga.toString())

        }
    }

    private fun validateForm() {
        //region Validation
        if (!isAvailablePhoto) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "competitor"), Toast.LENGTH_SHORT).show()
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, competitor_spinCategory, String.format(getString(R.string.info_msg_required_field_spinner), "category"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, competitor_spinProduct, String.format(getString(R.string.info_msg_required_field_spinner), "SKU"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, competitor_spinActivity, String.format(getString(R.string.info_msg_required_field_spinner), "activity"))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, competitor_edPeriodeMulai, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, competitor_edPeriodeSelesai, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, competitor_edDeskripsi, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, competitor_edPrice, getString(R.string.info_msg_required_field_text))) {
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        if (item == null) {
            val competitorCategorySelected = competitor_spinCategory.selectedItem as TCategoryCompetitor
            val competitorProductSelected = competitor_spinProduct.selectedItem as TProductCompetitor
            val competitorActivitySelected = competitor_spinActivity.selectedItem as TActivityType
            var price=competitor_edPrice.text.toString().replace(",".toRegex(), "")


            item = TReportCompetitor()
            item!!.visitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
            item!!.storeId = storeId.toString()
            item!!.surveyorId = Login.getUserId(this)
            item!!.activityTypeId = competitorActivitySelected.activityTypeId
            item!!.activityTypeName = competitorActivitySelected.activityTypeName
            item!!.productId = competitorProductSelected.productId
            item!!.productName = competitorProductSelected.productName
            item!!.categoryId = competitorCategorySelected.productCategoryId
            item!!.categoryName = competitorCategorySelected.productCategoryName
            item!!.startDate = competitor_edPeriodeMulai.text.toString()
            item!!.endDate = competitor_edPeriodeSelesai.text.toString()
            item!!.description = competitor_edDeskripsi.text.toString()
            item!!.photoPath = mPhotoPath
            item!!.harga = price.toInt()
            item!!.isDone = true
            itemDao.insert(item)
        } else {
            var price=competitor_edPrice.text.toString().replace(",".toRegex(), "")

            item!!.startDate = competitor_edPeriodeMulai.text.toString()
            item!!.endDate = competitor_edPeriodeSelesai.text.toString()
            item!!.description = competitor_edDeskripsi.text.toString()
            item!!.photoPath = mPhotoPath
            item!!.harga = price.toInt()
            item!!.isDone = true
            itemDao.update(item)
        }
        finish()
    }

    private fun callIntentCamera() {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("nama_file", "competitor" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))
        startActivityForResult(intent, mPhotoType)
    }

    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
            callIntentCamera()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent ?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == requestCodeCamera && resultCode == Activity.RESULT_OK) {
                mPhotoPath = data?.extras!!.getString("path")
                competitor_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath))
                competitor_imgPhoto.scaleType = ImageView.ScaleType.CENTER_CROP
                isAvailablePhoto = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isAvailablePhoto = false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }
}

