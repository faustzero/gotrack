package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.nestle.mdgt.utils.DateTimeUtils
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.DashboardActivity
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoActivity
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_store_detail.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.ArrayList

class StoreDetailActivity : AppCompatActivity(),
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        Loadable {

    private val requestCodeCamera = 10
    private val requestCodeTagLocation = 20

    private var progressDialog: ProgressDialog? = null

    private var mPhotoStorePath: String? = null
    private var mPhotoType: Int = 0
    private var isAvailablePhotoStore: Boolean = false
    private var isAvailableStoreLocation:Boolean = false
    private var isAvailableLocation = false
    private var mActionType: String? = null
    private var mVisitIdMarket:String? = null
    private var mVisitId:String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var visitDao: TVisitStoreDao
    private lateinit var storeDao: TStoreDao
    private lateinit var reasonDao: TReasonNoVisitDao
    private lateinit var mStore: TStore

    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mCurrentLocation: Location? = null
    private val mStoreLocation = Location("StoreLocation")
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_detail)
        initLocationRequest()
        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_detail_store)
        supportActionBar!!.subtitle = "${Login.getUsername(this)}/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}"

        initData()
        initView()
    }

    private fun initData() {
        val storeId = intent.getStringExtra("StoreId")
        mVisitIdMarket = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_MARKET, "")
        daoSession = (application as GlobalApp).daoSession!!
        storeDao = daoSession.tStoreDao
        reasonDao = daoSession.tReasonNoVisitDao
        mStore = storeDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(storeId)).limit(1).unique()
        progressDialog = ProgressDialog(this)
        isAvailableLocation = true
        isAvailableStoreLocation = !(mStore.latitude == 0.toDouble() || mStore.longitude == 0.toDouble())
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private fun initView() {
        store_lblStoreName.text = mStore.storeName
        store_lblAddress.text = mStore.address
        store_lblArea.text = "${mStore.areaName}, ${mStore.regionName}"
        if (isAvailableStoreLocation) {
            store_lblLocation.text = "${mStore.latitude}, ${mStore.longitude}"
        } else {
            store_lblLocation.text = "Lokasi(latitude, longitude) tidak tersedia"
            store_layoutTagLocation.visibility = View.VISIBLE
            isAvailableLocation = false
        }
        store_lblTelp.text = mStore.phone
        store_lblStoreType.text = mStore.storeTypeName

        store_imgStore.setOnClickListener {
            if (mPhotoStorePath != null) {
                val intent = Intent(this, FullImageActivity::class.java)
                intent.putExtra("ImageUrl", mPhotoStorePath)
                startActivity(intent)
            }
        }

        store_btnStore.setOnClickListener {
            mPhotoType = requestCodeCamera
            startCamera(mPhotoType)
        }

        store_imgLocation.setOnClickListener {
            startLocationUpdates(requestCodeTagLocation)
        }

        store_btnVisit.setOnClickListener {
            validateForm("visit")
        }
        store_btnNoVisit.setOnClickListener {
            validateForm("novisit")
        }
    }

    private fun validateForm(actionType: String) {
        if (actionType == "visit") {
            //region Validation
            if (!isAvailablePhotoStore) {
                Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "toko"), Toast.LENGTH_SHORT).show()
                return
            }
            if (!isAvailableLocation) {
                Toast.makeText(this, String.format(getString(R.string.info_msg_required_update_location)), Toast.LENGTH_SHORT).show()
                return
            }
            //endregion

            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_msg_visit))
            builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
                mActionType = "visit"
                submitForm("visit", "")
            }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } else {
            if (!isAvailablePhotoStore) {
                Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "toko"), Toast.LENGTH_SHORT).show()
                return
            }
            if (!isAvailableLocation) {
                Toast.makeText(this, String.format(getString(R.string.info_msg_required_update_location)), Toast.LENGTH_SHORT).show()
                return
            }
            selectReason()
        }
    }

    private fun submitForm(actionType: String, reasonNoVisit: String) {
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val reportId = "VIN" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        mVisitId = Helper.generateVisitIdStore(Login.getUserId(this), mStore.storeId)
        var namaReport = "Visit"
        var isVisit = 1
        if(actionType == "novisit") {
            isVisit = 0
            namaReport = "No Visit"
        }

        //Insert visit
        visitDao = daoSession.tVisitStoreDao
        val visit = TVisitStore()
        visit.visitIdStore = mVisitId
        visit.visitIdMarket = mVisitIdMarket
        visit.surveyorId = Login.getUserId(this)
        visit.storeId = mStore.storeId
        visit.isVisit = isVisit
        visit.isCheckIn = Config.YES_CODE
        visit.reasonNoVisit = reasonNoVisit
        visit.startDateTimeUtc = currentUtc
        visit.endDateTimeUtc = currentUtc
        visit.startDateTime = currentDateTime
        visit.endDateTime = currentDateTime
        visit.photoPath = mPhotoStorePath
        visitDao.insert(visit)

        //Add report visit to queue
        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "visit_id", visit.visitIdStore, ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "visit_id_market", visit.visitIdMarket, ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "store_id", visit.storeId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("4", reportId, "surveyor_id", visit.surveyorId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("5", reportId, "is_visit", visit.isVisit.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("6", reportId, "is_checkin", visit.isCheckIn.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("7", reportId, "no_visit_reason", visit.reasonNoVisit, ReportParameter.TEXT))
        params.add(ReportParameter("8", reportId, "start_datetime_utc", visit.startDateTimeUtc.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("9", reportId, "end_datetime_utc", visit.endDateTimeUtc.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("10", reportId, "start_datetime", visit.startDateTime, ReportParameter.TEXT))
        params.add(ReportParameter("11", reportId, "end_datetime", visit.endDateTime, ReportParameter.TEXT))
        params.add(ReportParameter("12", reportId, "photo_file", mPhotoStorePath!!, ReportParameter.FILE))

        val report = GenericReport(reportId,
                Login.getUserId(this).toString(),
                namaReport,
                "$namaReport: " + mVisitId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getInsertVisitUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)

        val task = TambahReportTask(this)
        task.execute(report)
    }

    private fun updateStoreLocation(storeId: String, latitude: Double, longitude: Double) {
        mActionType = "update location"
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val reportId = "USL" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)

        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "store_id", storeId, ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "latitude", latitude.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "longitude", longitude.toString(), ReportParameter.TEXT))

        val report = GenericReport(reportId,
                Login.getUserId(this).toString(),
                "Update Store Location",
                "Update Store Location: " + storeId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getUpdateStoreLocationUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)

        val task = TambahReportTask(this)
        task.execute(report)
    }

    private fun selectReason() {
        val reasons = reasonDao.loadAll()
        val items = arrayOfNulls<String>(reasons.size + 1)
        var i = 0
        reasons.forEach{
            items[i] = it.reasonName
            i++
        }
        items[reasons.size] = "Cancel"

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Pilih alasan")
        builder.setItems(items) { dialog, item ->
            dialog.dismiss()
            if(items[item] != "Cancel") {
                mActionType = "novisit"
                submitForm("novisit", items[item]!!)
            }
        }
        builder.show()
    }

    private fun callIntentCamera() {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("nama_file", "store_" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))
        startActivityForResult(intent, mPhotoType)
    }

    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
            callIntentCamera()
        }
    }

    private fun initLocationRequest() {
        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            try {
                Toast.makeText(this, getString(R.string.info_msg_google_play_service_not_available), Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_market_google_play_service))))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_store_google_play_service))))
            }

        }
        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    private fun startLocationUpdates(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    try {
                        mLocationManager!!.removeTestProvider(LocationManager.GPS_PROVIDER)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    store_imgLocation.isEnabled = false
                    isAvailableLocation = false
                    store_lblUpdateLokasi.text = getString(R.string.info_msg_search_location)
                    store_imgLocation.setImageResource(R.drawable.ic_my_location_grey_128dp)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                store_imgLocation.isEnabled = false
                isAvailableLocation = false
                store_lblUpdateLokasi.text = getString(R.string.info_msg_search_location)
                store_imgLocation.setImageResource(R.drawable.ic_my_location_grey_128dp)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = Config.INTERVAL
        mLocationRequest!!.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            store_imgLocation.isEnabled = true
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            if (!isAvailableLocation) {
                store_lblUpdateLokasi.text = getString(R.string.label_update_lokasi)
                store_imgLocation.setImageResource(R.drawable.ic_my_location_black_128dp)
            }
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }

    override fun onLocationChanged(location: Location) {
        if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location
            if (LocationUtils.isFromMockProvider(this, location)) {
                stopLocationUpdates()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                store_imgLocation.isEnabled = true
                Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
            } else {
                if (location.accuracy <= Config.ACCURACY_SMALL_LIMIT) {
                    stopLocationUpdates()
                    mCurrentLocation = location
                    isAvailableLocation = true
                    isAvailableStoreLocation = true
                    store_imgLocation.isEnabled = true
                    store_imgLocation.setImageResource(R.drawable.ic_check_orange_128dp)
                    store_lblLocation.text = "${location.latitude}, ${location.longitude}"

                    mStore.latitude = location.latitude
                    mStore.longitude = location.longitude
                    storeDao.update(mStore)
                    updateStoreLocation(mStore.storeId, mStore.latitude, mStore.longitude)

                    mStoreLocation.latitude = mStore.latitude
                    mStoreLocation.longitude = mStore.longitude
                    val distance = location.distanceTo(mStoreLocation)
                    store_lblUpdateLokasi.text = "Jarak anda dengan\ntoko " + Helper.convertMeterToKm(distance)
                    window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                }
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    public override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient!!.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent ?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == requestCodeCamera && resultCode == Activity.RESULT_OK) {
                mPhotoStorePath = data?.extras!!.getString("path")
                store_imgStore.setImageBitmap(BitmapFactory.decodeFile(mPhotoStorePath))
                store_imgStore.scaleType = ImageView.ScaleType.CENTER
                isAvailablePhotoStore = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isAvailablePhotoStore = false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

        if (requestCode == requestCodeTagLocation) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates(requestCodeTagLocation)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_store_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
        }

        if (id == R.id.action_dashboard) {
            val intent = Intent(this, DashboardActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            if (mActionType == "visit") {
                if (!isAvailableStoreLocation) {
                    mStore.latitude = mCurrentLocation!!.latitude
                    mStore.longitude = mCurrentLocation!!.longitude
                    storeDao.update(mStore)
                }
                Login.clearSharedPrefVisitStore(this)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_VISIT_ID_STORE, mVisitId!!)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_STORE_ID, mStore.storeId)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_STORE_NAME, mStore.storeName)

                val intent = Intent(this, ProductListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }

            if (mActionType == "novisit") {
                mStore.isVisited = Config.YES_CODE
                storeDao.update(mStore)
                finish()
            }
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
