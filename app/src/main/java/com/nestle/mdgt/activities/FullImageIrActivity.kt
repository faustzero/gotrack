package com.nestle.mdgt.activities

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.nestle.mdgt.R
import com.nestle.mdgt.rests.ApiClient
import java.io.File

class FullImageIrActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image_ir)
        val imageUrl = intent.getStringExtra("ImageUrl")
        val image = findViewById <PhotoView>(R.id.fullImage) as PhotoView

        if (File(imageUrl).exists()) {
            image.setImageBitmap(BitmapFactory.decodeFile(imageUrl))
        } else {
            if(intent.getBooleanExtra("IsFullPath", false)) {
                Glide.with(this).asBitmap().load(imageUrl).into(image)
            } else {
                Glide.with(this).asBitmap().load(ApiClient.hostUrl + imageUrl).into(image)
            }
        }
    }
}
