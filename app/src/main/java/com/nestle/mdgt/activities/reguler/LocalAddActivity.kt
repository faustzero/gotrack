package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import com.bumptech.glide.Glide
import com.dsm.mediapicker.MediaPicker
import com.github.dhaval2404.imagepicker.ImagePicker
import com.iceteck.silicompressorr.SiliCompressor
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.adapters.DisplaysAdapter
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.*
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TLocalActivityType
import com.nestle.mdgt.database.TReportActivityDetail
import com.nestle.mdgt.database.TReportActivityHeader
import com.nestle.mdgt.database.TRole
import com.nestle.mdgt.models.ActivityLocation
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import kotlinx.android.synthetic.main.activity_local_activity.*
import kotlinx.android.synthetic.main.activity_local_activity_list_row.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class LocalAddActivity : AppCompatActivity(), Loadable {

    private lateinit var daoSession: DaoSession
    private lateinit var localActivityTypeAdapter: ArrayAdapter<TLocalActivityType>
    private lateinit var mHeader: TReportActivityHeader
    private lateinit var storeAdapter: ArrayAdapter<TDistributorStore>
    private lateinit var localActivityNameAdapter: ArrayAdapter<TLocalActivityName>
    private lateinit var roleNameAdapter: ArrayAdapter<TRole>
    private lateinit var localActivityLocation: ArrayAdapter<TLocalAcitivityLocation>
    private var local: TReportActivityHeader? = null
    internal var progressDialog: ProgressDialog? = null
    private var reportLocalActivity: MutableList<TReportActivityDetail> = ArrayList()
    private val REQUEST_CODE_SELECT_FILE_FROM_GALLERY = 102
    private var mPhotoGeneralPath: String? = null
    private var id=0L
    private var mUserId = 0
    private var mRow=0
    private var mColumn=0
    private var mDistributor=0L
    private var mnamaDistributor:String? = ""
    private var mFilePath:String? = ""
    private var mAssId:String? = ""
    val requestCodeCamera=101
    private var displayHeaderId=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_activity)

        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_activity_tracking)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}"

        recognition_recycler.setFocusable(false)

        initData()
        initView()
        initValuesToViews()
    }

    private fun initData() {
        startCamera(requestCodeCamera)
        mUserId = Login.getUserId(this)
        mRow = Login.getEmployeeRow(this@LocalAddActivity)
        mColumn = Login.getEmployeeColumn(this@LocalAddActivity)
        mDistributor = Login.getDistributor(this@LocalAddActivity)
        mnamaDistributor = Login.getNameDistributor(this@LocalAddActivity)

        Log.i("namadistr",mnamaDistributor)
        id = intent.getLongExtra("Id", 0)
        daoSession = (application as GlobalApp).daoSession!!
        local = daoSession.tReportActivityHeaderDao.load(id)

        Log.i("dataDistri",mDistributor.toString())

        localActivityTypeAdapter = ArrayAdapter(
                this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tLocalActivityTypeDao.loadAll())
        localActivityTypeAdapter.insert(TLocalActivityType(null, 0, ""), 0)


        roleNameAdapter = ArrayAdapter(
                this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tRoleDao.loadAll())
        roleNameAdapter.insert(TRole(null, 0, ""), 0)


//        distributorAdapter = ArrayAdapter(this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tDistributorAreaDao.loadAll())
////        distributorAdapter.insert(TDistributorArea(null,0,""), 0)

        storeAdapter = ArrayAdapter(this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tDistributorStoreDao.loadAll())
                daoSession.tDistributorStoreDao.loadAll())

        storeAdapter.insert(TDistributorStore(null,0,"",0,0), 0)
        var galery = intent.getStringExtra("galery")

        localActivityLocation = ArrayAdapter(
                this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
                daoSession.tLocalAcitivityLocationDao.loadAll())
        localActivityLocation.insert(TLocalAcitivityLocation(null,0,""),0)

//        localActivityNameAdapter = ArrayAdapter(
//                this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tLocalActivityNameDao.loadAll())
////                daoSession.tLocalActivityNameDao.queryBuilder().where(TLocalActivityNameDao.Properties.ActivityId.eq(mHeader.activityTypeId)).list())
//        localActivityNameAdapter.insert(TLocalActivityName(null, 0, "",0), 0)
//
//        storeAdapter = ArrayAdapter(this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                        daoSession.tDistributorStoreDao.loadAll())
////                daoSession.tDistributorStoreDao.queryBuilder().where(TDistributorStoreDao.Properties.LocationId.eq(mHeader.locationId)).list())
//        storeAdapter.insert(TDistributorStore(null,0,"",0,0), 0)


        if (id > 0 ) {
//
            displayHeaderId=intent.getStringExtra("headerId")
            Log.i("EXISTINGG","dor")
            reportLocalActivity = daoSession.tReportActivityDetailDao.queryBuilder().where(TReportActivityDetailDao.Properties.ReportHeaderId.eq(displayHeaderId)).list()
            var reportLocalActivitys = daoSession.tReportActivityDetailDao.queryBuilder().where(TReportActivityDetailDao.Properties.ReportHeaderId.eq(displayHeaderId)).limit(1).unique()

            if (reportLocalActivity.size > 0) {
                mHeader = daoSession.tReportActivityHeaderDao.queryBuilder().where(TReportActivityHeaderDao.Properties.HeaderId.eq(displayHeaderId)).limit(1).unique()

                Log.i("poto1",mHeader.photoRowSize.toString())
                Log.i("poto2",mHeader.photoColumnSize.toString())
                Log.i("poto3",reportLocalActivitys.rowNumber.toString())
                Log.i("poto4",reportLocalActivitys.columnNumber.toString())
            }
        }

        else if(id < 1){
            if (reportLocalActivity.isEmpty()) {

                displayHeaderId = "LA${mUserId}-${DateTime.now().toString("yyMMddHHmmssSS")}"
                var displayHeader = TReportActivityHeader()
                displayHeader.headerId = displayHeaderId
                displayHeader.userId = mUserId
                displayHeader.areaId = Login.getEmployeeArea(this@LocalAddActivity)
                displayHeader.activityName = ""
                displayHeader.activityId = 0
                displayHeader.periodeStart = ""
                displayHeader.periodeEnd = ""
                displayHeader.locationId = 0
                displayHeader.locationName = ""
                displayHeader.activityTypeId = 0
                displayHeader.activityTypeName = ""
                displayHeader.storeId = 0
                displayHeader.storeName = ""
                displayHeader.roleId = 0
                displayHeader.userNames = ""
                displayHeader.mekanisme = ""
                displayHeader.tujuan = ""
                displayHeader.result = ""
                displayHeader.insight = ""
                displayHeader.photoRowSize = mRow
                displayHeader.photoColumnSize = mColumn
                displayHeader.saved = 0
                displayHeader.created = ""
                displayHeader.createdDate = ""
                daoSession.tReportActivityHeaderDao.insert(displayHeader)

                var col = 1
                var row = 1
                for (i in 1..(mColumn * mRow)) {
                    val o = TReportActivityDetail()
                    o.reportDetailId = "${mUserId}." + row.toString() + col.toString() + "." + DateTime.now().toString("yMdHmsSSS")
                    o.reportHeaderId = displayHeaderId
                    o.userId = mUserId
                    o.activityTypeId = 0
                    o.photoPath = ""
                    o.infoPhoto = ""
                    o.columnNumber = col
                    o.rowNumber = row
                    o.leftPosition = GridPositionUtil.getLeftPosition(i, col)
                    o.topPosition = GridPositionUtil.getTopPosition(i, mColumn, row)
                    o.rightPosition = GridPositionUtil.getRightPosition(i, mColumn, col)
                    o.bottomPosition = GridPositionUtil.getBottomPosition(i, mRow, mColumn, row)
                    o.created = ""
                    o.createdDate = ""
                    o.modified = ""
                    reportLocalActivity.add(o)
                    daoSession.tReportActivityDetailDao.insert(o)
                    if (col == mColumn) {
                        col = 0
                        row++
                    }
                    col++
                }
                reportLocalActivity = daoSession.tReportActivityDetailDao.queryBuilder().where(TReportActivityDetailDao.Properties.ReportHeaderId.eq(displayHeaderId)).list()
                if (reportLocalActivity.size > 0) {
                    mHeader = daoSession.tReportActivityHeaderDao.queryBuilder().where(TReportActivityHeaderDao.Properties.HeaderId.eq(displayHeaderId)).limit(1).unique()
                }
            }

        }



    }

    private fun initView() {

        spinDistributor!!.setTitle("Pilih Distributor")
        spinDistributor!!.setPositiveButton("Tutup")

        spinStore!!.setTitle("Pilih Store")
        spinStore!!.setPositiveButton("Tutup")

        spinActivityName!!.setTitle("Pilih Aktivitas")
        spinActivityName!!.setPositiveButton("Tutup")

        spinLocation!!.setTitle("Pilih Location")
        spinLocation!!.setPositiveButton("Tutup")

        edDistributor.setText(mnamaDistributor)
        Log.i("nama d",mnamaDistributor)
        progressDialog = ProgressDialog(this)
        var mCalendar = Calendar.getInstance()
        var xCalendar = Calendar.getInstance()

        val dateStart = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            mCalendar.set(Calendar.YEAR, year)
            mCalendar.set(Calendar.MONTH, monthOfYear)
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            edPeriodeMulai.setText(sdf.format(mCalendar.getTime()))
        }

//        val dateEnd = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//            xCalendar.set(Calendar.YEAR, year)
//            xCalendar.set(Calendar.MONTH, monthOfYear)
//            xCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//            val myFormat = "yyyy-MM-dd"
//            val sdf = SimpleDateFormat(myFormat, Locale.US)
//            edPeriodeSelesai.setText(sdf.format(xCalendar.getTime()))
//        }
        edPeriodeMulai.setOnClickListener(View.OnClickListener {
            DatePickerDialog(
                    this@LocalAddActivity, dateStart, mCalendar
                    .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                    mCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        })

//        edPeriodeSelesai.setOnClickListener(View.OnClickListener {
//            DatePickerDialog(
//                    this@LocalAddActivity, dateEnd, mCalendar
//                    .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
//                    mCalendar.get(Calendar.DAY_OF_MONTH)
//            ).show()
//        })

        spinAktivityType.adapter = localActivityTypeAdapter
        spinAktivityType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val activity = localActivityTypeAdapter.getItem(position) as TLocalActivityType

                mHeader.activityTypeId = activity.activityTypeId
//                mHeader.distributorName = distributor.distributorName

                localActivityNameAdapter = ArrayAdapter(
                        this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tLocalActivityNameDao.loadAll())
                        daoSession.tLocalActivityNameDao.queryBuilder().where(TLocalActivityNameDao.Properties.ActivityId.eq(mHeader.activityTypeId)).list())
                localActivityNameAdapter.insert(TLocalActivityName(null, 0, "",0), 0)


                spinActivityName.adapter = localActivityNameAdapter
                spinActivityName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {}

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val activityName = localActivityNameAdapter.getItem(position) as TLocalActivityName

                        mHeader.activityId = activityName.activityNameId
                    }
                }
                if(mHeader != null) {
                    if (mHeader!!.activityId > 0) {
                        val activityNamePosition = localActivityNameAdapter.getPosition(daoSession.tLocalActivityNameDao.queryBuilder().where(TLocalActivityNameDao.Properties.ActivityNameId.eq(mHeader.activityId)).limit(1).unique())
                        spinActivityName.setSelection(activityNamePosition)
                    }
                }
            }
        }


        spinRoleType.adapter = roleNameAdapter
        spinRoleType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val roles = roleNameAdapter.getItem(position) as TRole

                mHeader.roleId = roles.roleId
//                mHeader.distributorName = distributor.distributorName



            }
        }


        spinLocation.adapter = localActivityLocation
        spinLocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val ActivityLocation = localActivityLocation.getItem(position) as TLocalAcitivityLocation
                mHeader.locationId = ActivityLocation.locationId

            }
        }

//        storeAdapter = ArrayAdapter(this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
////                        daoSession.tDistributorStoreDao.loadAll())
//                daoSession.tDistributorStoreDao.queryBuilder().where(TDistributorStoreDao.Properties.DistributorId.eq(mHeader.distributorId)).list())
//        storeAdapter.insert(TDistributorStore(null,0,"",0,0), 0)

        spinStore.adapter = storeAdapter
        spinStore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val store = storeAdapter.getItem(position) as TDistributorStore
                mHeader.storeId=store.storeId
            }
        }

        btnSimpan.setOnClickListener {
            validateForm()
        }

//        spinDistributor.adapter = distributorAdapter
//        spinDistributor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//            }
//
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                val distributor = distributorAdapter.getItem(position) as TDistributorArea
//
//                mHeader.distributorId = distributor.distributorId
//
//                storeAdapter = ArrayAdapter(this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tDistributorStoreDao.queryBuilder().where(TDistributorStoreDao.Properties.DistributorId.eq(mDistributor)).list())
//
//
//                storeAdapter.insert(TDistributorStore(null,0,"",0), 0)
//                spinStore.adapter = storeAdapter
//
//                if (id.toInt() != 0) {
//                    if (mHeader!!.distributorId > 0) {
//                        val store = storeAdapter.getPosition(daoSession.tDistributorStoreDao.queryBuilder().
//                                where(TDistributorStoreDao.Properties.DistributorId.eq(mDistributor)).
//                                limit(1).unique()
//                        )
//                        spinStore.setSelection(store)
//                    }
//                }
//            }
//        }


        refreshList()

    }
    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
//            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }

    private fun initValuesToViews() {
        val localActivityNameAdapter1 = ArrayAdapter(
                this@LocalAddActivity, android.R.layout.simple_spinner_dropdown_item,
//                daoSession.tLocalActivityNameDao.loadAll())
                daoSession.tLocalActivityNameDao.queryBuilder().where(TLocalActivityNameDao.Properties.ActivityId.eq(mHeader.activityTypeId)).list())
        localActivityNameAdapter1.insert(TLocalActivityName(null, 0, "",0), 0)



        if(mHeader!=null){
            edPeriodeMulai.setText(mHeader.periodeStart)
//            edPeriodeSelesai.setText(mHeader.periodeEnd)
//            edLocation.setText(mHeader.locationId)

            if(mHeader!!.activityTypeId!=null){
                if (mHeader!!.activityTypeId > 0) {
                    val activityPosition = localActivityTypeAdapter.getPosition(daoSession.tLocalActivityTypeDao.queryBuilder().where(TLocalActivityTypeDao.Properties.ActivityTypeId.eq(mHeader!!.activityTypeId)).limit(1).unique())
                    spinAktivityType.setSelection(activityPosition)

//                    val activityNamePosition = localActivityNameAdapter!!.getPosition(daoSession.tLocalActivityNameDao.queryBuilder().
//                            where(TLocalActivityNameDao.Properties.ActivityNameId.eq(mHeader!!.activityId)).
//                            limit(1).unique()
//                    )
//                    spinActivityName.setSelection(activityNamePosition)

                }
            }



            if(mHeader!!.roleId!=null){
                if (mHeader!!.roleId > 0) {
                    val rolePosition = roleNameAdapter.getPosition(daoSession.tRoleDao.queryBuilder().where(TRoleDao.Properties.RoleId.eq(mHeader!!.roleId)).limit(1).unique())
                    spinRoleType.setSelection(rolePosition)

                }
            }


            if(mHeader!!.locationName!=null){
                if(mHeader!!.locationId>0) {
                    val activityLocation = localActivityLocation.getPosition(daoSession.tLocalAcitivityLocationDao.queryBuilder().where(TLocalAcitivityLocationDao.Properties.LocationId.eq(mHeader.locationId)).limit(1).unique())
                    spinLocation.setSelection(activityLocation)
                    Log.i("test 4",activityLocation.toString())
                }
            }

            if(mHeader!!.storeId!=null){
                if (mHeader!!.storeId > 0) {
                    val storeNamePosition = storeAdapter.getPosition(daoSession.tDistributorStoreDao.queryBuilder().where(TDistributorStoreDao.Properties.StoreId.eq(mHeader!!.storeId)).limit(1).unique())
                    spinStore.setSelection(storeNamePosition)

                }
            }

            edMekanism.setText(mHeader.mekanisme)
            edPurpose.setText(mHeader.tujuan)
            edResult.setText(mHeader.result)
            edInsight.setText(mHeader.insight)
            edNamaUser.setText(mHeader.userNames)
            if (id > 0){
//                edMekanism.isEnabled=false
//                edMekanism.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
//                edPurpose.isEnabled=false
//                edPurpose.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
//                edResult.isEnabled=false
//                edResult.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
//                edInsight.isEnabled=false
//                edInsight.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
//                spinActivityName.isEnabled=false
//                spinActivityName.background = ContextCompat.getDrawable(this, R.drawable.form_spinner_style_disabled)
//                edPeriodeMulai.isEnabled=false
//                edPeriodeMulai.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
////                edPeriodeSelesai.isEnabled=false
////                edPeriodeSelesai.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
//                edLocation.isEnabled=false
//                edLocation.background = ContextCompat.getDrawable(this, R.drawable.form_edit_text_style_disabled)
//                spinAktivityType.isEnabled=false
//                spinAktivityType.background = ContextCompat.getDrawable(this, R.drawable.form_spinner_style_disabled)
//                spinDistributor.isEnabled=false
//                spinDistributor.background = ContextCompat.getDrawable(this, R.drawable.form_spinner_style_disabled)
//                spinStore.isEnabled=false
//                spinStore.background = ContextCompat.getDrawable(this, R.drawable.form_spinner_style_disabled)
//                edDistributor.background = ContextCompat.getDrawable(this,R.drawable.form_edit_text_style_disabled)
                btnSimpan.text = "UPDATE"
            }

        }
    }


    private fun validateForm() {

        if(!isValid()) {
            Toast.makeText(this, "Ambil foto activity terlebih dahulu", Toast.LENGTH_SHORT).show()
            return
        }

        if (!FormValidation.validateRequiredSpinner(this, spinRoleType, String.format(getString(R.string.info_msg_required_field_spinner), "Role"))) {
            return
        }

        if (!FormValidation.validateRequiredText(this, edNamaUser, getString(R.string.info_msg_required_field_text))) {
            return
        }

        if (!FormValidation.validateRequiredSpinner(this, spinAktivityType, String.format(getString(R.string.info_msg_required_field_spinner), "Tipe Aktivitas"))) {
            return
        }
//        if (!FormValidation.validateRequiredSpinner(this, spinDistributor, String.format(getString(R.string.info_msg_required_field_spinner), "Tipe Distributor"))) {
//            return
//        }
        if (!FormValidation.validateRequiredSpinner(this, spinLocation, String.format(getString(R.string.info_msg_required_field_spinner), "Lokasi"))) {
            return
        }

        if (!FormValidation.validateRequiredSpinner(this, spinStore, String.format(getString(R.string.info_msg_required_field_spinner), "Tipe Store"))) {
            return
        }



        if (!FormValidation.validateRequiredSpinner(this, spinActivityName, String.format(getString(R.string.info_msg_required_field_spinner), "Nama Aktivitas"))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, edPeriodeMulai, getString(R.string.info_msg_required_field_text))) {
            return
        }
//        if (!FormValidation.validateRequiredText(this, edPeriodeSelesai, getString(R.string.info_msg_required_field_text))) {
//            return
//        }
//        if (!FormValidation.validateRequiredText(this, edLocation, getString(R.string.info_msg_required_field_text))) {
//            return
//        }

//        if (!FormValidation.validateRequiredText(this, edMekanism, getString(R.string.info_msg_required_field_text))) {
//            return
//        }
//
//        if (!FormValidation.validateRequiredText(this, edPurpose, getString(R.string.info_msg_required_field_text))) {
//            return
//        }
//
//        if (!FormValidation.validateRequiredText(this, edResult, getString(R.string.info_msg_required_field_text))) {
//            return
//        }
//
//        if (!FormValidation.validateRequiredText(this, edInsight, getString(R.string.info_msg_required_field_text))) {
//            return
//        }


        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun submitForm() {
        val localTypeSelected=spinAktivityType.selectedItem as TLocalActivityType
//        val distributorTypeSelected=spinDistributor.selectedItem as TDistributorArea
        val roleTypeSelected=spinRoleType.selectedItem as TRole
        val storeTypeSelected=spinStore.selectedItem as TDistributorStore
        val nameTypeSelected=spinActivityName.selectedItem as TLocalActivityName
        val locationSelected = spinLocation.selectedItem as TLocalAcitivityLocation
        var headers=daoSession!!.tReportActivityHeaderDao.queryBuilder().where(TReportActivityHeaderDao.Properties.HeaderId.eq(mHeader.headerId)).list()
        var details=daoSession!!.tReportActivityDetailDao.queryBuilder().where(TReportActivityDetailDao.Properties.ReportHeaderId.eq(mHeader.headerId)).list()
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val datePath = DateTime.now().toString(Config.DATE_FORMAT_PATH)
        val reportId = "LA" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val reportData = ArrayList<GenericReport>()
        var displayHeaderValues=""
        var displayDetailValues=""

            Log.i("test","tes1")
            mHeader.activityName = nameTypeSelected.activityName
            mHeader.activityId = nameTypeSelected.activityNameId
            mHeader.periodeStart = edPeriodeMulai.text.toString()
            mHeader.periodeEnd = ""
            mHeader.locationName = locationSelected.locationName
            mHeader.locationId = locationSelected.locationId
            mHeader.activityTypeId = localTypeSelected.activityTypeId
            mHeader.activityTypeName = localTypeSelected.activityName
            mHeader.distributorId = mDistributor
            mHeader.storeId = storeTypeSelected.storeId
            mHeader.storeName = storeTypeSelected.storeName
            mHeader.mekanisme = edMekanism.text.toString()
            mHeader.tujuan = edPurpose.text.toString()
            mHeader.result = edResult.text.toString()
            mHeader.insight = edInsight.text.toString()
            mHeader.userNames = edNamaUser.text.toString()
            mHeader.roleId = roleTypeSelected.roleId

            mHeader.saved = 1
            daoSession.tReportActivityHeaderDao.update(mHeader)
            Log.i("mHeader1",mHeader.activityId.toString())
            Log.i("mHeader2",mHeader.distributorId.toString())
            Log.i("mHeader3",mHeader.storeId.toString())
            Log.i("mHeader4",mHeader.distributorId.toString())
            if (reportLocalActivity.isNotEmpty()) {
                for (o in reportLocalActivity) {
                    o.activityTypeId = localTypeSelected.activityTypeId
                    if (o.photoPath != "") {
                        daoSession.tReportActivityDetailDao.update(o)

                    }
                }
            }


            for (y in headers) {
                displayHeaderValues += "${y.headerId}^${y.userId}^${y.activityId}^${y.periodeStart}^${y.periodeEnd}^${y.locationId}^${y.activityTypeId}" +
                        "^${y.areaId}^${y.mekanisme}^${y.tujuan}^${y.result}^${y.insight}^${y.storeId}^${y.distributorId}^${y.photoRowSize}^${y.photoColumnSize}^" +
                        "${y.roleId}^" +
                        "${y.userNames};"
            }

            for (z in details) {
                displayDetailValues += "${z.reportDetailId},${z.reportHeaderId},${z.userId},${z.activityTypeId},${z.infoPhoto},${z.rowNumber},${z.columnNumber};"
            }

            if (displayHeaderValues != "") {
                displayHeaderValues = displayHeaderValues.substring(0, displayHeaderValues.length - 1)
            }
            if (displayDetailValues != "") {
                displayDetailValues = displayDetailValues.substring(0, displayDetailValues.length - 1)
            }

            var namaReport = "Report Data"
            val params = ArrayList<ReportParameter>()
            params.add(ReportParameter("1", reportId, "header_values", displayHeaderValues, ReportParameter.TEXT))
            params.add(ReportParameter("2", reportId, "detail_values", displayDetailValues, ReportParameter.TEXT))
            params.add(ReportParameter("3", reportId, "created_date", currentDateTime, ReportParameter.TEXT))


            val report = GenericReport(reportId,
                    Login.getUserId(this).toString(),
                    namaReport,
                    "$namaReport: " + mUserId.toString() + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                    ApiClient.getInsertGeneralReport(),
                    currentDate, Config.NO_CODE, currentUtc, params)

            reportData.add(report)
/*
            for (y in details) {
                if (y.photoPath != null && !y.photoPath.equals("")) {

                    if (File(y.photoPath).exists()) {
                        Log.i("param-photoPath", y.photoPath)

                        var reportIdPhoto = "PD" + y.reportDetailId + "_${y.id}" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
                        var namaReport = "Report Photo Detail"
                        val paramsDetail = ArrayList<ReportParameter>()
                        paramsDetail.add(ReportParameter("1", reportIdPhoto, "report_id", y.reportDetailId, ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("2", reportIdPhoto, "report_name", "LOCAL ACTIVITY DETAIL", ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("3", reportIdPhoto, "photo_row_number", y.rowNumber.toString(), ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("4", reportIdPhoto, "photo_column_number", y.columnNumber.toString(), ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("5", reportIdPhoto, "photo_file", y.photoPath, ReportParameter.FILE))
                        paramsDetail.add(ReportParameter("6", reportIdPhoto, "created_date", datePath, ReportParameter.TEXT))


                        val reportPhoto = GenericReport(reportIdPhoto,
                                Login.getUserId(this).toString(),
                                namaReport,
                                "$namaReport: " + mUserId.toString() + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                                ApiClient.getupdateActivityDetailPhoto(),
                                currentDate, Config.NO_CODE, currentUtc, paramsDetail)
                        reportData.add(reportPhoto)
                    }

                }
            }
*/

            for (y in details) {
                if (y.photoPath != null && !y.photoPath.equals("")) {

                    if (File(y.photoPath).exists()) {
                        Log.i("param-photoPath", y.photoPath)
                        Log.i("param-photoInfo", y.infoPhoto)

                        var reportIdPhoto = "PD" + y.reportDetailId + "_${y.id}" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
                        var namaReportPhoto = "Report Photo Detail"
                        val paramsDetail = ArrayList<ReportParameter>()
                        paramsDetail.add(ReportParameter("1", reportIdPhoto, "report_id", y.reportDetailId, ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("2", reportIdPhoto, "report_name", "LOCAL ACTIVITY DETAIL", ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("3", reportIdPhoto, "photo_row_number", y.rowNumber.toString(), ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("4", reportIdPhoto, "photo_column_number", y.columnNumber.toString(), ReportParameter.TEXT))
                        paramsDetail.add(ReportParameter("5", reportIdPhoto, "photo_file", y.photoPath, ReportParameter.FILE))
                        paramsDetail.add(ReportParameter("6", reportIdPhoto, "created_date", datePath, ReportParameter.TEXT))


                        val reportPhoto = GenericReport(reportIdPhoto,
                                Login.getUserId(this).toString(),
                                namaReport,
                                "$namaReport: " + mUserId.toString() + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                                ApiClient.getupdateActivityDetailPhoto(),
                                currentDate, Config.NO_CODE, currentUtc, paramsDetail)
                        reportData.add(reportPhoto)
                    }

                }
            }


        Log.i("succ",displayHeaderValues.toString())
        val task = TambahReportTask(this, reportData)
        task.execute()
    }

    private fun isValid() : Boolean {
        if (reportLocalActivity.isEmpty()) {
            return true
        }

        for (o in reportLocalActivity) {
            if (o.photoPath != "") {
                return true
            }
        }

        return false
    }

    private fun refreshList() {
        var displaysAdapter = DisplaysAdapter(this, reportLocalActivity, R.layout.activity_recognition_grid_row)
        recognition_recycler.layoutManager = SpanningGridLayoutManager(this, mColumn, SpanningGridLayoutManager.VERTICAL, false)
        recognition_recycler.adapter = displaysAdapter
        displaysAdapter.notifyDataSetChanged()
    }


//    private fun galleryIntent() {
//        val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//        startActivityForResult(galleryIntent, REQUEST_CODE_SELECT_FILE_FROM_GALLERY)
//    }

    private fun popupInfo(detailId:String,pathPhoto:String){
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layoutPopup = inflater.inflate(R.layout.popup_summary_info_foto2, findViewById(R.id.popup_summary_infoFoto2))
        val mPopupWindow = PopupWindow(layoutPopup, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true)
        val lblTitle = layoutPopup.findViewById(R.id.infoFotoPopup_lblHeader) as TextView
        val ImgView = layoutPopup.findViewById(R.id.IdFoto) as ImageView
        val editText = layoutPopup.findViewById(R.id.infoFotoPopup_edText) as EditText
        val btnOk = layoutPopup.findViewById(R.id.btnSimpan) as Button
        val btnClose = layoutPopup.findViewById(R.id.infoFotoPopup_btnClose) as ImageButton

        editText.requestFocus()
        ImgView.setImageBitmap(BitmapFactory.decodeFile(pathPhoto))
        btnClose.setOnClickListener { mPopupWindow.dismiss() }
        mPopupWindow.showAtLocation(layoutPopup, Gravity.CENTER, 0, 0)

//        Log.i("log_path", path)

        btnOk.setOnClickListener {
            if(editText.text.toString()!="") {
//                var dataDetail=daoSession.tReportActivityDetailDao.queryBuilder().where(
//                        TReportActivityDetailDao.Properties.ReportDetailId.eq(detailId)).unique()
//
//                dataDetail.photoPath=pathPhoto
//                daoSession.tReportActivityDetailDao.update(dataDetail) (cara 1)

                for (o in reportLocalActivity) {
                    if (o.reportDetailId == detailId) {
                        o.photoPath = pathPhoto
                        o.infoPhoto = editText.text.toString()

                    }
                }
                refreshList()
                mPopupWindow.dismiss()
            }
            else {
                Toast.makeText(this, "Isi info foto terlebih dahulu", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == 101) {
                try {
                    val path = data?.getStringExtra("path")
                    Log.i("infoPhoto", path)
                    val index = path!!.lastIndexOf("_")
                    val index2 = path!!.lastIndexOf(":")
                    val assId = path!!.substring(index + 1, index2)
                    val position = path!!.substring(index2 + 1, path.length).toInt()

                    popupInfo(assId,path!!)


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                try {
                    val selectedImage = data!!.data
    ////                    val infoPhoto = data.getStringExtra("InfoPhotoGaleri")
//                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
//                    val cursor = contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
//                    cursor!!.moveToFirst()
//                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
//                    val imgDecodableString = cursor.getString(columnIndex)
//                    cursor.close()
//                    mFilePath = SiliCompressor.with(this).compress(imgDecodableString)

//                    Log.i("img_photo",mFilePath.toString())
//                    mFilePath = ImagePicker.getFilePath(data)
                    Log.i("img_photo2",ImagePicker.getFilePath(data).toString())
                    mFilePath = ImagePicker.getFilePath(data)
                    mAssId = SharedPrefsUtils.getStringPreference(this@LocalAddActivity, Config.DETAIL_ID, "")
                    if(mFilePath!=null && mAssId!=null){
                        popupInfo(mAssId!!,mFilePath!!)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "Terjadi kesalahan, ulangi pilih foto", Toast.LENGTH_LONG).show()
                }
            }

        } else if (resultCode == Activity.RESULT_CANCELED || data == null) {
            try {
                Toast.makeText(this, "Pengambilan foto gagal, ulangi mengambil foto", Toast.LENGTH_SHORT).show()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


        }

    override fun setLoading(show: Boolean, title: String, message: String) {
        if (progressDialog == null)
            progressDialog = ProgressDialog(this)
        progressDialog!!.setTitle(title)
        progressDialog!!.setMessage(message)
        progressDialog!!.setCancelable(false)
        if (show) {
            progressDialog!!.show()
            //selesai_button.setEnabled(false);
        } else {
            progressDialog!!.dismiss()
            // selesai_button.setEnabled(true);
        }    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            finish()
        }

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->

            if (id.toInt()==0){
                daoSession.tReportActivityHeaderDao.refresh(mHeader)
//            daoSession.tReportActivityDetailDao.refresh(TReportActivityDetail())
            }

            finish()
        }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

}
