package com.nestle.mdgt.activities.reguler

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.adapters.PullProgramsAdapter
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TReportPullProgram
import com.nestle.mdgt.database.TReportPullProgramDao
import com.nestle.mdgt.database.TReseller
import com.nestle.mdgt.database.TResellerDao
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils
import com.nestle.mdgt.utils.SimpleDividerItemDecoration

class PullProgramListActivity : AppCompatActivity() {
    private var lblNoData: TextView? = null
    private var mVisitId: String? = null
    private var mResellerId: String? = null

    private var daoSession: DaoSession? = null
    private var reportPullProgramDao: TReportPullProgramDao? = null
    private var reportPullPrograms: MutableList<TReportPullProgram>? = null
    private var pullprogramsAdapter: PullProgramsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_program_list)

        val subTitle = Login.getUsername(this) + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_NAME, "") + "/" + SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_NAME, "")
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_pull_program)
        supportActionBar!!.subtitle = subTitle

        initData()
        initView()
    }

    private fun initData() {
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID_STORE, "")
        mResellerId = SharedPrefsUtils.getStringPreference(this, Config.KEY_RESELLER_ID, "")
        daoSession = (application as GlobalApp).daoSession
        reportPullProgramDao = daoSession!!.tReportPullProgramDao
        reportPullPrograms = reportPullProgramDao!!.queryBuilder()
                .where(TReportPullProgramDao.Properties.VisitId.eq(mVisitId),
                        TReportPullProgramDao.Properties.ResellerId.eq(mResellerId)).list()
    }

    private fun initView() {
        val btnAdd = findViewById<View>(R.id.pullprogramList_fabAdd) as FloatingActionButton
        lblNoData = findViewById<View>(R.id.pullprogramList_lblNoData) as TextView
        val btnSelesai = findViewById<View>(R.id.pullprogramList_btnSelesai) as Button
        val recycler = findViewById<View>(R.id.pullprogramList_recycler) as androidx.recyclerview.widget.RecyclerView

        lblNoData!!.visibility = View.GONE
        if (reportPullPrograms!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }

        pullprogramsAdapter = PullProgramsAdapter(reportPullPrograms, R.layout.activity_pullprogram_list_row, PullProgramsAdapter.OnItemClickListener { item ->
            val intent = Intent(this@PullProgramListActivity, PullProgramAddActivity::class.java)
            intent.putExtra("Id", item.id)
            startActivity(intent)
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.addItemDecoration(SimpleDividerItemDecoration(this))
        recycler.adapter = pullprogramsAdapter

        btnAdd.setOnClickListener {
            val intent = Intent(this@PullProgramListActivity, PullProgramAddActivity::class.java)
            startActivity(intent)
        }

        btnSelesai.setOnClickListener { btnSelesaiClicked() }
    }

    private fun btnSelesaiClicked() {
        //region validation
        if (!validateForm()) {
            Toast.makeText(this, getString(R.string.info_msg_required_fields), Toast.LENGTH_SHORT).show()
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_finish))
        builder.setPositiveButton(getString(R.string.label_yes)) { dialogInterface, i -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        //SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_RESELLER_PULLPROGGRAM, true);
        //region Update reseller
        val resellerDao = daoSession!!.tResellerDao
        val reseller = resellerDao.queryBuilder().where(TResellerDao.Properties.ResellerId.eq(mResellerId)).limit(1).unique()
        reseller.isDonePullProgram = true
        daoSession!!.update(reseller)
        //endregion
        finish()
    }

    private fun validateForm(): Boolean {
        return if (reportPullPrograms!!.size == 0) {
            true
        } else true

    }

    public override fun onResume() {
        super.onResume()
        reportPullPrograms!!.clear()
        reportPullPrograms!!.addAll(reportPullProgramDao!!.queryBuilder()
                .where(TReportPullProgramDao.Properties.VisitId.eq(mVisitId),
                        TReportPullProgramDao.Properties.ResellerId.eq(mResellerId))
                .list())
        pullprogramsAdapter!!.notifyDataSetChanged()
        lblNoData!!.visibility = View.GONE
        if (reportPullPrograms!!.size == 0) {
            lblNoData!!.visibility = View.VISIBLE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
