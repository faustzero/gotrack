package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.LoginActivity
import com.nestle.mdgt.activities.TransmissionActivity
import com.nestle.mdgt.adapters.CustomInfoWindowAdapter
import com.nestle.mdgt.adapters.StoresAdapter2
import com.nestle.mdgt.database.DaoSession
import com.nestle.mdgt.database.TResellerDao
import com.nestle.mdgt.database.TStore
import com.nestle.mdgt.database.TStoreDao
import com.nestle.mdgt.models.*
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.rests.DashboardApiInterface
import com.nestle.mdgt.rests.LoginApiInterface
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_store_list2.*
import kotlinx.android.synthetic.main.content_dashboard.*
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StoreListActivity2 : AppCompatActivity(),
    OnMapReadyCallback,
    LocationListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
        Loadable {
    private val requestCodeTagLocation = 20

    private lateinit var daoSession: DaoSession
    private lateinit var mStores: List<TStore>

    private var mCurrentLocation: Location? = null
    private lateinit var mLocationManager: LocationManager
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mMap: GoogleMap? = null
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView
    private lateinit var storesAdapter: StoresAdapter2
    private lateinit var storeViewModels: MutableList<StoreViewModel>
    private var radiusVerificationLimit: Int = 0
    private var progressDialog: ProgressDialog? = null
    private lateinit var mKunjungan : MutableList<TStore>
    private lateinit var mBelumKunjungan: MutableList<TStore>
    private var surveyorId = 0
    internal lateinit var mDrawer: MultiDirectionSlidingDrawer
    internal lateinit var mCurrentTimeWeather: TextView
    private var TC = 0
    private var TN = 0
    private var AD = 0
    private var mCuacaTemp : String = ""
    private var mCuaca : String =""
    private var mTimeHit : String =""
    private var mCuacaId : Int = 0
    private var mNextCuacaTemp : String = ""
    private var mNextCuaca : String =""
    private var mNextTimeHit : String =""
    private var mNextCuacaId : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_list2)

        setSupportActionBar(mtoolbar as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_store_list)

        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            val appPackageName = getString(R.string.label_google_package_name)
            try {
                Toast.makeText(this, "Application need Google Play Services.\nGoogle Play Services not available or needs to be updated", Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("store://details?id=$appPackageName")))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }
        }

        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mapFragment = supportFragmentManager.findFragmentById(R.id.store_map) as SupportMapFragment
        recycler = findViewById(R.id.store_recycler)

        initData()
        initView()
    }

    private fun initData() {
        surveyorId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ID, 0)
        radiusVerificationLimit = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_ID, 0)
        daoSession = (application as GlobalApp).daoSession!!
        mStores = daoSession.tStoreDao.queryBuilder().list()
        progressDialog = ProgressDialog(this)
        storeViewModels = ArrayList()
    }

    private fun initView() {
        mDrawer = findViewById(R.id.drawer) as MultiDirectionSlidingDrawer
        mCurrentTimeWeather = findViewById(R.id.currentTimeWeather) as TextView

        store_btnSearch.setOnClickListener {
            searchStores()
        }
        mDrawer.setOnDrawerOpenListener {
            if(mCurrentLocation?.longitude !=null || mCurrentLocation?.latitude !=null) {

                drawer.isClickable = true
                handle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_more_white_24dp)
                val curretDate = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
                mKunjungan = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.IsVisited.eq(1)).list()
                mBelumKunjungan = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.IsVisited.eq(0)).list()
                mCurrentTimeWeather.text = curretDate
                dashboard_lblTotalStore4.text = mStores.size.toString()
                dashboard_lblTotalStore5.text = mKunjungan.size.toString()
                dashboard_lblTotalStore6.text = mBelumKunjungan.size.toString()
                Log.i("TN",TN.toString())
                Log.i("TC",TC.toString())
                Log.i("datacek",AD.toString())
                if(Login.getEmployeeRoleId(this)==13){
                    var resellersOut = daoSession.tResellerDao!!.queryBuilder()
                            .where(TResellerDao.Properties.RadiusId.eq(1))
                            .list()

                    var resellersIn = daoSession.tResellerDao!!.queryBuilder()
                            .where(TResellerDao.Properties.RadiusId.eq(2))
                            .list()

                    txtResellerInstore.setText("${resellersIn.size}")
                    txtResellerOutstore.setText("${resellersOut.size}")


                    lblReseller.visibility=View.VISIBLE
                    txtReseller.visibility=View.VISIBLE
                    lblResellerInstore.visibility=View.VISIBLE
                    txtResellerInstore.visibility=View.VISIBLE
                    lblResellerOutstore.visibility=View.VISIBLE
                    txtResellerOutstore.visibility=View.VISIBLE

                    lblSelling.visibility=View.VISIBLE
                    txtSelling.visibility=View.VISIBLE
                    lblActualResellerInstore.visibility=View.VISIBLE
                    txtActualResellerInstore.visibility=View.VISIBLE
                    lblActualResellerOutstore.visibility=View.VISIBLE
                    txtActualResellerOutstorea.visibility=View.VISIBLE


                    lbl_penjualan.visibility=View.GONE
                    dashboard_lblTotalPenjualan.visibility=View.GONE
                    progressBarPenjualan.visibility=View.GONE

                    lbl_reportToko.visibility=View.GONE
                    dashboard_lblTotalReportToko.visibility=View.GONE
                    progressBarToko.visibility=View.GONE
                }
                else{
                    lblReseller.visibility=View.GONE
                    txtReseller.visibility=View.GONE
                    lblResellerInstore.visibility=View.GONE
                    txtResellerInstore.visibility=View.GONE
                    lblResellerOutstore.visibility=View.GONE
                    txtResellerOutstore.visibility=View.GONE

                    lblSelling.visibility=View.GONE
                    txtSelling.visibility=View.GONE
                    lblActualResellerInstore.visibility=View.GONE
                    txtActualResellerInstore.visibility=View.GONE
                    lblActualResellerOutstore.visibility=View.GONE
                    txtActualResellerOutstorea.visibility=View.GONE

                    lbl_reportToko.visibility=View.VISIBLE
                    dashboard_lblTotalReportToko.visibility=View.VISIBLE
                    if(Login.getEmployeeRoleId(this)==12 || Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
                        lbl_reportToko.setText("Toko berdisplay")
                        lbl_penjualan.visibility=View.GONE
                        dashboard_lblTotalPenjualan.visibility=View.GONE

                        progressBarPenjualan.visibility=View.GONE
                        lblTokoPicos.visibility=View.VISIBLE
                        txtTokoPicos.visibility=View.VISIBLE
                    }
                }



                if (TC<1 || TN<1){
                    getAchievement()
                    getWeather()
                    getNextWeather()

                }
                else if (cekSelisihWaktu2Jam()){

                    getAchievement()
                    getWeather()
                    getNextWeather()

                }
                else{

                }
            }
            else
            {
                Toast.makeText(this@StoreListActivity2,"Harap menunggu, GPS sedang melakukan kalibrasi",Toast.LENGTH_SHORT).show()

            }

            mDrawer.setOnDrawerCloseListener {
                handle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_less_white_24dp)
                drawer.isClickable = false
            }
        }

        if(Login.getEmployeeRoleId(this)==12 || Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
            fabAdd.show()
        }
        else{
            fabAdd.hide()
        }

        fabAdd.setOnClickListener{
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_noo))
            builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
                val connectionDetectorUtil = ConnectionDetectorUtil(this@StoreListActivity2)
                if (connectionDetectorUtil.isConnectingToInternet) {
                    val intent = Intent(this@StoreListActivity2, NooAddActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(applicationContext, getString(R.string.info_msg_no_network_connection), Toast.LENGTH_SHORT).show()
                }

            }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()

        }
    }

    private fun searchStores() {
        if (mCurrentLocation != null) {
            if (storeViewModels.isNotEmpty()) {
                val list = storeViewModels
                        .filter { it.store!!.storeName.toLowerCase().contains(store_txtSearch.text.toString().toLowerCase()) }
                        .sortedBy { it.distance }
                storesAdapter = StoresAdapter2(list, R.layout.activity_store_list_row2, radiusVerificationLimit, object : StoresAdapter2.OnItemClickListener {
                    override fun onItemClick(item: StoreViewModel) {
                        if (item.store!!.isVisited == Config.NO_CODE) {
                            if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                                val intent = Intent(this@StoreListActivity2, StoreDetailActivity2::class.java)
                                Log.i("store_id",item.store!!.storeId)
                                intent.putExtra("StoreId", item.store!!.storeId)
                                intent.putExtra("locationStatus", 0)
                                startActivity(intent)
                            } else {
                                if (item.distance <= radiusVerificationLimit) {
                                    val intent = Intent(this@StoreListActivity2, StoreDetailActivity2::class.java)
                                    Log.i("store_id",item.store!!.storeId)
                                    intent.putExtra("StoreId", item.store!!.storeId)
                                    intent.putExtra("locationStatus", 1)
                                    startActivity(intent)
                                }
                                else {
                                    val intent = Intent(this@StoreListActivity2, StoreDetailActivity2::class.java)
                                    Log.i("store_id",item.store!!.storeId)
                                    intent.putExtra("StoreId", item.store!!.storeId)
                                    intent.putExtra("locationStatus", 0)
                                    startActivity(intent)
                                }
                            }
                        } else {
                            Toast.makeText(this@StoreListActivity2, item.store!!.storeName + " sudah Anda kunjungi", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
                recycler.adapter = storesAdapter
                storesAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = Config.INTERVAL
        mLocationRequest.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            store_lblLoading.visibility = View.GONE
            store_lblLoading.text = getString(R.string.label_mencari_lokasi)
        }
    }

    override fun onLocationChanged(location: Location) {
        if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location
            if (LocationUtils.isFromMockProvider(this, location)) {
                stopLocationUpdates()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
            } else {
                if (location.accuracy <= Config.ACCURACY_LIMIT) {
                    stopLocationUpdates()
                    mCurrentLocation = location
                    mapFragment.getMapAsync(this)
                }
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        if (mMap != null) {
            mMap!!.clear()
        }
        mMap = map
        val peoplePoint = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
        var storeLocation: Location

        storeViewModels = ArrayList()
        for (o in mStores) {
            storeLocation = Location("StoreLocation")
            storeLocation.latitude = o.latitude
            storeLocation.longitude = o.longitude
            val distance = mCurrentLocation!!.distanceTo(storeLocation)
            val pinId: Float
            pinId = when {
                distance <= radiusVerificationLimit - 20 -> BitmapDescriptorFactory.HUE_GREEN
                distance <= radiusVerificationLimit -> BitmapDescriptorFactory.HUE_YELLOW
                else -> BitmapDescriptorFactory.HUE_RED
            }

            mMap!!.addMarker(MarkerOptions()
                .position(LatLng(o.latitude, o.longitude))
                .title(o.storeName)
                .icon(BitmapDescriptorFactory.defaultMarker(pinId))
                .snippet("${o.storeId};${o.storeCode};${o.address};${o.areaName};$distance"))
            val storeViewModel = StoreViewModel()
            storeViewModel.store = o
            storeViewModel.distance = distance
            storeViewModels.add(storeViewModel)
        }
        storesAdapter = StoresAdapter2(storeViewModels.sortedBy { it.distance }, R.layout.activity_store_list_row2, radiusVerificationLimit, object : StoresAdapter2.OnItemClickListener {
            override fun onItemClick(item: StoreViewModel) {
                if (item.store!!.isVisited == Config.NO_CODE) {
                    if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                        val intent = Intent(this@StoreListActivity2, StoreDetailActivity2::class.java)
                        Log.i("store_id",item.store!!.storeId)
                        intent.putExtra("StoreId", item.store!!.storeId)
                        intent.putExtra("locationStatus", 0)
                        startActivity(intent)
                    } else {
                        if (item.distance <= radiusVerificationLimit) {
                            val intent = Intent(this@StoreListActivity2, StoreDetailActivity2::class.java)
                            Log.i("store_id",item.store!!.storeId)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 1)
                            startActivity(intent)
                        }
                        else {
                            val intent = Intent(this@StoreListActivity2, StoreDetailActivity2::class.java)
                            Log.i("store_id",item.store!!.storeId)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 0)
                            startActivity(intent)
                        }
                    }
                } else {
                    Toast.makeText(this@StoreListActivity2, item.store!!.storeName + " sudah Anda kunjungi", Toast.LENGTH_SHORT).show();
                }
            }
        })
        recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycler.adapter = storesAdapter
        storesAdapter.notifyDataSetChanged()

        mMap!!.addMarker(MarkerOptions()
            .position(peoplePoint)
            .title(Login.getEmployeeName(this))
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_people_pin_36px))
            .snippet(""))

        mMap!!.addCircle(CircleOptions()
            .center(peoplePoint)
            .radius(radiusVerificationLimit.toDouble())
            .fillColor(resources.getColor(R.color.transparent_blue))
            .strokeColor(resources.getColor(R.color.piction_blue))
            .strokeWidth(5f))

        mMap!!.moveCamera(CameraUpdateFactory.newCameraPosition(
            CameraPosition.Builder()
                .target(peoplePoint)
                .zoom(15f)
                .bearing(0f)
                .tilt(40f)
                .build()))

        mMap!!.setInfoWindowAdapter(CustomInfoWindowAdapter(this))
    }

    public override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {
        store_lblLoading.visibility = View.VISIBLE
        checkGPSPermission(requestCodeTagLocation)
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    private fun checkGPSPermission(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeTagLocation) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkGPSPermission(requestCodeTagLocation)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }

    private fun logout() {
        Thread {
            try {
                runOnUiThread {
                    setLoading(true, "Loading", "Logging Out")
                }
                Login.logout(this)
                val imagePath = StorageUtils.getDirectory(StorageUtils.DIRECTORY_IMAGE)
                DataController.deleteFilesNotInReport(imagePath, this)

                runOnUiThread {
                    setLoading(false, "Loading", "Logging Out")
                    val iLogin = Intent(this, LoginActivity::class.java)
                    iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(iLogin)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }.start()
    }

    private fun isValidForm(): Boolean {
//        for(c in mStores) {
//            if(c.isVisited!=1) {
//                return false
//            }
//        }
        return true
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_store_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_logout) {
            if(isValidForm()){
                val builder = android.app.AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.label_confirm))
                builder.setMessage(getString(R.string.confirm_msg_logout))
                builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> logout() }
                builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
                val alertDialog = builder.create()
                alertDialog.setCanceledOnTouchOutside(false)
                alertDialog.show()
            }
            else{
                Toast.makeText(this, "Anda harus mengunjungi semua toko", Toast.LENGTH_SHORT).show()
            }



        }

        if (id == R.id.action_refresh) {
            store_lblLoading.visibility = View.VISIBLE
            mStores = daoSession.tStoreDao.queryBuilder().list()
            checkGPSPermission(requestCodeTagLocation)
        }

        if (id == R.id.action_transmission) {
            val intent = Intent(this, TransmissionActivity::class.java)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun cekSelisihWaktu3Jam(dt_txt: String): Boolean {
        val datevisitin = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val datevisitout = dt_txt
        Log.i("visitIn12",datevisitin)
        Log.i("visitOut12",datevisitout)
        try {
            val date1 = format.parse(datevisitin)
            val date2 = format.parse(datevisitout)
            val selisihwaktu = DateTimeUtils.SubtractingDate(date1, date2)
            //long selisihwaktu[] = {0,0,4};
            Log.i("selisih hari", selisihwaktu[0].toString())
            Log.i("selisih jam", selisihwaktu[1].toString())
            Log.i("selisih menit", selisihwaktu[2].toString())
            Log.i("date1", datevisitin)
            Log.i("date2", datevisitout)
            return if (selisihwaktu[0] > 0) {
                false
            } else {
                selisihwaktu[1] >= 3
            }//return true;
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return true
    }

    private fun cekSelisihWaktu2Jam(): Boolean {
        val datevisitin = mTimeHit.toString()
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val datevisitout = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
        Log.i("visitin",datevisitin)
        Log.i("visitout",datevisitout)
        try {
            val date1 = format.parse(datevisitin)
            val date2 = format.parse(datevisitout)
            val selisihwaktu = DateTimeUtils.SubtractingDate(date1, date2)
            //long selisihwaktu[] = {0,0,4};
            Log.i("selisih hari", selisihwaktu[0].toString())
            Log.i("selisih jam", selisihwaktu[1].toString())
            Log.i("selisih menit", selisihwaktu[2].toString())
            Log.i("date1", date1.toString())
            Log.i("date2", date2.toString())
            return if (selisihwaktu[0] >= 1) {
                true
            } else {
                selisihwaktu[1] >= 2

            }//return true;

        } catch (e: ParseException) {
            e.printStackTrace()
        }
        Log.i("mega","ulol4")
        return true
    }


    private fun getWeather() {
        progressBarCurrentWeather.visibility = View.VISIBLE
        val apiService = ApiClient.client2!!.create(LoginApiInterface::class.java)
        val call = apiService.getWeather(mCurrentLocation!!.latitude,mCurrentLocation!!.longitude,"65ca8214adfddd5f749006df53827c4e","metric")
        call.enqueue(object : retrofit2.Callback<WeatherResponse> {
            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                try {
                    if (response.isSuccessful) {
                        Log.i("teest1",response.isSuccessful.toString())
                        Log.i("teest2",response.code().toString())
                        mCuacaTemp = response.body().main!!.temp.toString()+"°C"
                        mCuaca = response.body().weathers!!.get(0).info
                        mTimeHit = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
                        mCuacaId = response.body().weathers!!.get(0).id
                        currentWeatherTemp.text = mCuacaTemp
                        currentWeatherStatus.text = mCuaca
                        Log.i("teest3",mCuacaId.toString())

                        changePictureCurrentWeather()
                        TC=1

                    }
                    progressBarCurrentWeather.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                try {
                    progressBarCurrentWeather.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity2, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getNextWeather() {
        progressBarNextWeather.visibility = View.VISIBLE
        val apiService = ApiClient.client2!!.create(LoginApiInterface::class.java)
        val call = apiService.getWeatherNext(mCurrentLocation!!.latitude,mCurrentLocation!!.longitude,"65ca8214adfddd5f749006df53827c4e","metric")
        call.enqueue(object : retrofit2.Callback<ListWeather> {
            override fun onResponse(call: Call<ListWeather>, response: Response<ListWeather>) {
                try {
                    if (response.isSuccessful) {
                        Log.i("getnext",response.isSuccessful.toString())
                        for (o in response.body().list!!) {

                            if(cekSelisihWaktu3Jam(o.dt_txt)) {
                                mNextCuaca = o.weathers!!.get(0).info
                                mNextCuacaTemp = o.main!!.temp.toString() + "°C"
                                mNextTimeHit = o.dt_txt
                                mNextCuacaId = o.weathers!!.get(0).id

                                Log.i("cuacaid", mNextCuacaId.toString())

                                break

                            }
                        }
                        predictWeatherTemp.text=mNextCuacaTemp
                        predictWeatherStatus.text=mNextCuaca

                        changePictureNextWeather()
                        TN=1
                    }
                    progressBarNextWeather.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<ListWeather>, t: Throwable) {
                try {
                    progressBarNextWeather.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity2, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAchievement() {
        try {
            if(Login.getEmployeeRoleId(this)==13){
                progressBarToko.visibility = View.GONE
                progressBarPenjualan.visibility = View.GONE
            }
            else if(Login.getEmployeeRoleId(this)==12 || Login.getEmployeeRoleId(this)==14 || Login.getEmployeeRoleId(this)==15){
                progressBarToko.visibility = View.VISIBLE
                progressBarPenjualan.visibility = View.GONE
                progressBarPicos.visibility = View.VISIBLE
            }
            else{
                progressBarToko.visibility = View.VISIBLE
                progressBarPenjualan.visibility = View.VISIBLE
                progressBarPicos.visibility = View.GONE
            }
            progressBarKunjungan3.visibility = View.VISIBLE
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
            val apiService = ApiClient.client!!.create(DashboardApiInterface::class.java)
            val call = apiService.getDailyAchievement(date, surveyorId)
            call.enqueue(object : retrofit2.Callback<DailyDashboardResponse> {
                override fun onResponse(call: Call<DailyDashboardResponse>, response: Response<DailyDashboardResponse>) {
                    if (response.isSuccessful) {
                        if (response.body().status == Config.STATUS_SUCCESS) {
                            try {
                                val ach = response.body().dailyAchievement
                                dashboard_lblTotalStore7.text = ach!!.actualVisit
                                dashboard_lblTotalPenjualan.text = ach.selling
                                dashboard_lblTotalReportToko.text = ach.planogramStore
                                txtActualResellerInstore.text = ach.instoreSelling
                                txtActualResellerOutstorea.text = ach.outstoreSelling
                                txtTokoPicos.text = ach.picos

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                    progressBarKunjungan3.visibility = View.GONE
                    progressBarToko.visibility = View.GONE
                    progressBarPenjualan.visibility = View.GONE
                    progressBarPicos.visibility = View.GONE
                }

                override fun onFailure(call: Call<DailyDashboardResponse>, t: Throwable) {
                    progressBarKunjungan3.visibility = View.GONE
                    progressBarToko.visibility = View.GONE
                    progressBarPenjualan.visibility = View.GONE
                    progressBarPicos.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity2, t.message, Toast.LENGTH_SHORT).show()
                }
            })
        } catch (e: Exception) {
            progressBarKunjungan3.visibility = View.GONE
            progressBarToko.visibility = View.GONE
            progressBarPenjualan.visibility = View.GONE
            progressBarPicos.visibility = View.GONE
            e.printStackTrace()
        }
    }

    private fun changePictureCurrentWeather(){

        if(mCuacaId >= 801 && mCuacaId <=820){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_berawan))
            currentWeatherStatus.text="Berawan"
        }
        else if(mCuacaId >= 500 && mCuacaId <=540){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            currentWeatherStatus.text="Hujan"
        }
        else if(mCuacaId == 800){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_cerah))
            currentWeatherStatus.text="Cerah"
        }
        else if(mCuacaId >= 300 && mCuacaId <=330){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_gerimis))
            currentWeatherStatus.text="Gerimis"
        }
        else if(mCuacaId >= 200 && mCuacaId <=240){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_storm))
            currentWeatherStatus.text="Badai"
        }
        else if(mCuacaId >= 701 && mCuacaId <=790){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_haze))
            currentWeatherStatus.text="Kabut"
        }
        else if(mCuacaId >= 600 && mCuacaId <=630){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            currentWeatherStatus.text="Hujan"
        }
    }

    private fun changePictureNextWeather(){

        if(mNextCuacaId >= 801 && mNextCuacaId <=820){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_berawan))
            predictWeatherStatus.text="Berawan"
        }
        else if(mNextCuacaId >= 500 && mNextCuacaId <=540){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            predictWeatherStatus.text="Hujan"
        }
        else if(mNextCuacaId == 800){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_cerah))
            predictWeatherStatus.text="Cerah"
        }
        else if(mNextCuacaId >= 300 && mNextCuacaId <=330){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_gerimis))
            predictWeatherStatus.text="Gerimis"
        }
        else if(mNextCuacaId >= 200 && mNextCuacaId <=240){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_storm))
            predictWeatherStatus.text="Badai"
        }
        else if(mNextCuacaId >= 701 && mNextCuacaId <=790){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_haze))
            predictWeatherStatus.text="Kabut"
        }
        else if(mNextCuacaId >= 600 && mNextCuacaId <=630){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            predictWeatherStatus.text="Hujan"
        }
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null) progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            progressDialog!!.dismiss()
            e.printStackTrace()
        }
    }
    override fun onBackPressed() {
        if(mDrawer.isShown) {
            if( mDrawer.isOpened)
                mDrawer.animateClose()
        }
        else {
            super.onBackPressed()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
    }
}
