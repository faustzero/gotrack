package com.nestle.mdgt.activities.reguler

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.nestle.mdgt.GlobalApp
import com.nestle.mdgt.R
import com.nestle.mdgt.activities.FullImageActivity
import com.nestle.mdgt.activities.PhotoActivity
import com.nestle.mdgt.database.*
import com.nestle.mdgt.models.Login
import com.nestle.mdgt.reports.GenericReport
import com.nestle.mdgt.reports.ReportParameter
import com.nestle.mdgt.rests.ApiClient
import com.nestle.mdgt.tasks.Loadable
import com.nestle.mdgt.tasks.TambahReportTask
import com.nestle.mdgt.utils.*
import kotlinx.android.synthetic.main.activity_store_add.*
import kotlinx.android.synthetic.main.mtoolbar.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.*

class StoreAddActivity : AppCompatActivity()
        , LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        Loadable {
    private val requestCodeCamera = 10
    private val requestCodeTagLocation = 20
    private var mPhotoPath: String? = null
    private var mPhotoType: Int = 0
    private var isAvailablePhoto: Boolean = false
    private var isAvailableLocation = false
    private lateinit var mStoreId: String
    private lateinit var mStoreCode: String

    private lateinit var daoSession: DaoSession
    private lateinit var mMarket: TMarket
    private lateinit var storeDao: TStoreDao
    private lateinit var areaDao: TAreaDao
    private lateinit var distributorDao: TDistributorDao
    private lateinit var storeTypeDao: TStoreTypeDao

    private var areaAdapter: ArrayAdapter<TArea>? = null
    private var distributorAdapter: ArrayAdapter<TDistributor>? = null
    private var storeTypeAdapter: ArrayAdapter<TStoreType>? = null

    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mCurrentLocation: Location? = null
    private var progressDialog: ProgressDialog? = null
    private lateinit var marketLocation: Location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_add)
        initLocationRequest()
        setSupportActionBar(mtoolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_tambah_store)
        supportActionBar!!.subtitle = "${ Login.getUsername(this)}" +
                "/${SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")}"

        initData()
        initView()
    }

    private fun initData() {
        daoSession = (application as GlobalApp).daoSession!!
        storeDao = daoSession.tStoreDao
        areaDao = daoSession.tAreaDao
        distributorDao = daoSession.tDistributorDao
        storeTypeDao = daoSession.tStoreTypeDao
        progressDialog = ProgressDialog(this)
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val mMarketId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_MARKET_ID, 0)
        //val mMarketName = SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")!!

        mMarket = daoSession.tMarketDao.queryBuilder().where(TMarketDao.Properties.MarketId.eq(mMarketId)).limit(1).unique()
        marketLocation = Location("MarketLocation")
        marketLocation.latitude = mMarket.latitude
        marketLocation.longitude = mMarket.longitude
        //marketRadiusVerificationLimit = market.radiusVerificationLimit
    }

    private fun initView() {
        distributorAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, distributorDao.queryBuilder().list())
        distributorAdapter!!.insert(TDistributor(null, "","", ""), 0)
        store_spinDistributor.adapter = distributorAdapter

        storeTypeAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, storeTypeDao.queryBuilder().list())
        storeTypeAdapter!!.insert(TStoreType(null, 0, ""), 0)
        store_spinStoreType.adapter = storeTypeAdapter

        areaAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, areaDao.queryBuilder().list())
        areaAdapter!!.insert(TArea(null, 0, "", 0), 0)
        store_spinArea.adapter = areaAdapter

        store_spinArea.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                hideKeyboard()
                val area = areaAdapter!!.getItem(i)
                val kabupatenAdapter = ArrayAdapter(this@StoreAddActivity, android.R.layout.simple_spinner_dropdown_item,
                        daoSession.tKabupatenDao.queryBuilder()
                                .where(TKabupatenDao.Properties.AreaId.eq(area!!.areaId))
                                .orderAsc(TKabupatenDao.Properties.KabupatenName).list())
                kabupatenAdapter.insert(TKabupaten(null, 0, "", 0), 0)
                store_spinKabupaten.adapter = kabupatenAdapter

                //Kabupaten
                store_spinKabupaten.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                        val kabupaten = kabupatenAdapter.getItem(i)
                        val kecamatanAdapter = ArrayAdapter(this@StoreAddActivity, android.R.layout.simple_spinner_dropdown_item,
                                daoSession.tKecamatanDao.queryBuilder()
                                        .where(TKecamatanDao.Properties.KabupatenId.eq(kabupaten!!.kabupatenId))
                                        .orderAsc(TKecamatanDao.Properties.KecamatanName).list())
                        kecamatanAdapter.insert(TKecamatan(null, 0, "", 0), 0)
                        store_spinKecamatan.adapter = kecamatanAdapter
                    }
                    override fun onNothingSelected(adapterView: AdapterView<*>) {
                    }
                }
            }
            override fun onNothingSelected(adapterView: AdapterView<*>) {
            }
        }

        store_imgPhoto.setOnClickListener {
            if (mPhotoPath != null) {
                val intent = Intent(this, FullImageActivity::class.java)
                intent.putExtra("ImageUrl", mPhotoPath)
                startActivity(intent)
            }
        }

        store_btnPhoto.setOnClickListener {
            mPhotoType = requestCodeCamera
            startCamera(mPhotoType)
        }

        store_btnSimpan.setOnClickListener { validateForm() }

        store_btnLocation.setOnClickListener {
            startLocationUpdates(requestCodeTagLocation)
        }
    }

    private fun validateForm() {
        //region Validation
        if (!isAvailablePhoto) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "toko"), Toast.LENGTH_SHORT).show()
            return
        }
        if (!isAvailableLocation) {
            Toast.makeText(this, String.format(getString(R.string.info_msg_required_update_location)), Toast.LENGTH_SHORT).show()
            return
        }
        if (!FormValidation.validateRequiredText(this, store_txtStoreName, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, store_txtPhone, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, store_spinStoreType, String.format(getString(R.string.info_msg_required_field_spinner), "tipe toko"))) {
            return
        }
        if (!FormValidation.validateRequiredText(this, store_txtAddress, getString(R.string.info_msg_required_field_text))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, store_spinArea, String.format(getString(R.string.info_msg_required_field_spinner), "area"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, store_spinKabupaten, String.format(getString(R.string.info_msg_required_field_spinner), "kabupaten"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, store_spinKecamatan, String.format(getString(R.string.info_msg_required_field_spinner), "kecamatan"))) {
            return
        }
        if (!FormValidation.validateRequiredSpinner(this, store_spinDistributor, String.format(getString(R.string.info_msg_required_field_spinner), "distributor"))) {
            return
        }
        //endregion

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_save))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> submitForm() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun submitForm() {
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val kecamatanSelected = store_spinKecamatan.selectedItem as TKecamatan
        val distributorSelected = store_spinDistributor.selectedItem as TDistributor
        val storeTypeSelected = store_spinStoreType.selectedItem as TStoreType
        mStoreId = Helper.generateStoreId(mMarket.marketId, storeTypeSelected.storeTypeId, kecamatanSelected.kecamatanId)
        mStoreCode = Helper.generateStoreCode(mMarket.marketId, storeTypeSelected.storeTypeId, kecamatanSelected.kecamatanId)

        //Add report store to queue
        val reportId = "SIN" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val params = ArrayList<ReportParameter>()
        params.add(ReportParameter("1", reportId, "store_id", mStoreId, ReportParameter.TEXT))
        params.add(ReportParameter("2", reportId, "store_code", mStoreCode, ReportParameter.TEXT))
        params.add(ReportParameter("3", reportId, "store_name", store_txtStoreName.text.toString().trim(), ReportParameter.TEXT))
        params.add(ReportParameter("4", reportId, "store_type_id", storeTypeSelected.storeTypeId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("5", reportId, "phone", store_txtPhone.text.toString().trim(), ReportParameter.TEXT))
        params.add(ReportParameter("6", reportId, "address", store_txtAddress.text.toString().trim(), ReportParameter.TEXT))
        params.add(ReportParameter("7", reportId, "kecamatan_id", kecamatanSelected.kecamatanId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("8", reportId, "distributor_id", distributorSelected.distributorId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("9", reportId, "latitude", mCurrentLocation!!.latitude.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("10", reportId, "longitude", mCurrentLocation!!.longitude.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("11", reportId, "register_date", currentDateTime.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("12", reportId, "market_id", mMarket.marketId.toString(), ReportParameter.TEXT))
        params.add(ReportParameter("13", reportId, "surveyor_id", Login.getUserId(this).toString(), ReportParameter.TEXT))
        params.add(ReportParameter("14", reportId, "photo_file", mPhotoPath!!, ReportParameter.FILE))

        val report = GenericReport(reportId,
                Login.getUserId(this).toString(),
                "Store Add",
                "Store Add: " + mStoreId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
                ApiClient.getInsertStoreUrl(),
                currentDate, Config.NO_CODE, currentUtc, params)
        val task = TambahReportTask(this)
        task.execute(report)
    }

    private fun callIntentCamera() {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("nama_file", "storeadd" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))
        startActivityForResult(intent, mPhotoType)
    }

    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
            callIntentCamera()
        }
    }


    private fun initLocationRequest() {
        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            try {
                Toast.makeText(this, getString(R.string.info_msg_google_play_service_not_available), Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_market_google_play_service))))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_store_google_play_service))))
            }
        }
        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    private fun startLocationUpdates(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    try {
                        mLocationManager!!.removeTestProvider(LocationManager.GPS_PROVIDER)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    store_btnLocation.isEnabled = false
                    isAvailableLocation = false
                    store_lblUpdateLokasi.text = getString(R.string.info_msg_search_location)
                    store_lblUpdateLokasi.visibility = View.VISIBLE
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                store_btnLocation.isEnabled = false
                isAvailableLocation = false
                store_lblUpdateLokasi.text = getString(R.string.info_msg_search_location)
                store_lblUpdateLokasi.visibility = View.VISIBLE
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = Config.INTERVAL
        mLocationRequest!!.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            store_btnLocation.isEnabled = true
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            if (!isAvailableLocation) {
                store_lblUpdateLokasi.text = getString(R.string.label_mencari_lokasi)
                store_lblUpdateLokasi.visibility = View.GONE
            }
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }

    override fun onLocationChanged(location: Location) {
        if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location
            if (LocationUtils.isFromMockProvider(this, location)) {
                stopLocationUpdates()
                store_btnLocation.isEnabled = true
                store_lblUpdateLokasi.visibility = View.GONE
                Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
            } else {
                if (location.accuracy <= Config.ACCURACY_SMALL_LIMIT) {
                    stopLocationUpdates()

                    val distance = mCurrentLocation!!.distanceTo(marketLocation)
                    mCurrentLocation = location
                    window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    store_btnLocation.isEnabled = true

                    if (distance <= mMarket.radiusVerificationLimit) {
                        isAvailableLocation = true
                        store_lblUpdateLokasi.text = "Lokasi: ${location.latitude}, ${location.longitude}"
                        store_lblUpdateLokasi.visibility = View.VISIBLE
                    } else {
                        isAvailableLocation = false
                        store_lblUpdateLokasi.visibility = View.GONE
                        Toast.makeText(this, "Anda berada diluar radius pasar yang diperbolehkan untuk menambahkan toko", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    public override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient!!.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent ?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == requestCodeCamera && resultCode == Activity.RESULT_OK) {
                mPhotoPath = data?.extras!!.getString("path")
                store_imgPhoto.setImageBitmap(BitmapFactory.decodeFile(mPhotoPath))
                store_imgPhoto.scaleType = ImageView.ScaleType.CENTER_CROP
                isAvailablePhoto = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isAvailablePhoto = false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeCamera) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

        if (requestCode == requestCodeTagLocation) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates(requestCodeTagLocation)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun discardConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.label_confirm))
        builder.setMessage(getString(R.string.confirm_msg_discard))
        builder.setPositiveButton(getString(R.string.label_yes)) { _, _ -> finish() }
        builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            discardConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        discardConfirmation()
    }

    override fun setLoading(show: Boolean, title: String, message: String) {
        try {
            if (progressDialog == null)
                progressDialog = ProgressDialog(this)
            progressDialog!!.setTitle(title)
            progressDialog!!.setMessage(message)
            progressDialog!!.setCancelable(false)
            if (show) {
                progressDialog!!.show()
            } else {
                progressDialog!!.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setFinish(result: Boolean, message: String) {
        if (result) {
            val areaSelected = store_spinArea.selectedItem as TArea
            val kabupatenSelected = store_spinKabupaten.selectedItem as TKabupaten
            val kecamatanSelected = store_spinKecamatan.selectedItem as TKecamatan
            val distributorSelected = store_spinDistributor.selectedItem as TDistributor
            val storeTypeSelected = store_spinStoreType.selectedItem as TStoreType
            val marketId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_MARKET_ID, 0)

            val o = TStore()
            o.marketId = marketId
            o.marketName = SharedPrefsUtils.getStringPreference(this, Config.KEY_MARKET_NAME, "")
            o.storeId = mStoreId
            o.storeCode = mStoreCode
            o.storeName = store_txtStoreName.text.toString().trim()
            o.storeTypeId = storeTypeSelected.storeTypeId
            o.storeTypeName = storeTypeSelected.storeTypeName
            o.address = store_txtPhone.text.toString().trim()
            o.phone = store_txtPhone.text.toString().trim()
            o.regionId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_REGION_ID, 0)
            o.regionName = SharedPrefsUtils.getStringPreference(this, Config.KEY_EMPLOYEE_REGION_NAME, "")
            o.areaId = areaSelected.areaId
            o.areaName = areaSelected.areaName
            o.kabupatenId = kabupatenSelected.kabupatenId
            o.kabupatenName = kabupatenSelected.kabupatenName
            o.kecamatanId = kecamatanSelected.kecamatanId
            o.kecamatanName = kecamatanSelected.kecamatanName
            o.distributorId = distributorSelected.distributorId
            o.distributorName = distributorSelected.distributorName
            o.latitude = mCurrentLocation!!.latitude
            o.longitude = mCurrentLocation!!.longitude
            o.lastVisited = ""
            o.lastVisitedUtc = 0
            o.radiusVerificationLimit = 300
            o.isVisited = Config.NO_CODE
            storeDao.insert(o)

            finish()
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun hideKeyboard() {
        try {
            val view = this.currentFocus
            if (view != null) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

