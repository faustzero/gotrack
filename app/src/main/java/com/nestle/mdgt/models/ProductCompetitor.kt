package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ProductCompetitor {
    @SerializedName("product_id")
    var productId: Int = 0

    @SerializedName("product_code")
    var productCode: String = ""

    @SerializedName("product_name")
    var productName: String = ""

    @SerializedName("product_category_id")
    var productCategoryId: Int = 0

    @SerializedName("product_category_name")
    var productCategoryName: String = ""

    @SerializedName("brand_id")
    var brandId: Int = 0

    @SerializedName("brand_name")
    var brandName: String = ""

    @SerializedName("produsen_id")
    var produsenId: Int = 0

    @SerializedName("produsen_name")
    var produsenName: String = ""

    @SerializedName("is_competitor")
    var isCompetitor: Int = 0
}
