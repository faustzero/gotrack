package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class LocalActivityHeader {
    @SerializedName("report_header_id")
    var reportHeaderId: String = ""

    @SerializedName("user_id")
    var userId: Int = 0

    @SerializedName("activity_name")
    var activityName: String = ""

    @SerializedName("activity_name_id")
    var activityNameId: Int = 0

    @SerializedName("periode_start")
    var periodeStart: String = ""

    @SerializedName("periode_end")
    var periodeEnd: String = ""

    @SerializedName("location_name")
    var locationName: String = ""

    @SerializedName("lokasi")
    var locationId: Int = 0

    @SerializedName("activity_type_id")
    var activityTypeId: Int = 0

    @SerializedName("activity_type_name")
    var activityTypeName: String = ""

    @SerializedName("distributor_id")
    var distributorId: Long = 0L

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("area_id")
    var areaId: Int = 0

    @SerializedName("mekanisme")
    var mekanisme: String = ""

    @SerializedName("tujuan")
    var tujuan: String = ""

    @SerializedName("result")
    var result: String = ""

    @SerializedName("insight")
    var insight: String = ""

    @SerializedName("photo_row_size")
    var photoRowSize: Int = 0

    @SerializedName("photo_column_size")
    var photoColumnSize: Int = 0

    @SerializedName("created_date")
    var createdDate: String = ""

    @SerializedName("created")
    var created: String = ""


    @SerializedName("surveyor_type_id")
    var surveyorTypeId: Int = 0


    @SerializedName("nama_user")
    var namaUser: String = ""

}