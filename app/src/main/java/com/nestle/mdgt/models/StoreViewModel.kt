package com.nestle.mdgt.models

import com.nestle.mdgt.database.TStore

class StoreViewModel {
    var store: TStore? = null
    var distance: Float = 0.toFloat()
}
