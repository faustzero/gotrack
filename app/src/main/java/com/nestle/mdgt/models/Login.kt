package com.nestle.mdgt.models

import android.content.Context
import com.nestle.mdgt.utils.Config
import com.nestle.mdgt.utils.SharedPrefsUtils

object Login {
    fun logout(context: Context) {
        SharedPrefsUtils.clearPreferences(context)
    }

    fun clearSharedPrefVisitMarket(context: Context) {
        SharedPrefsUtils.setStringPreference(context, Config.KEY_VISIT_ID_MARKET, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_VISIT_ID_STORE, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_STORE_ID, 0)
        SharedPrefsUtils.setStringPreference(context, Config.KEY_STORE_CODE, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_STORE_NAME, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_MARKET_ID, 0)
        SharedPrefsUtils.setStringPreference(context, Config.KEY_MARKET_NAME, "")
    }

    fun clearSharedPrefVisitStore(context: Context) {
        SharedPrefsUtils.setStringPreference(context, Config.KEY_VISIT_ID_STORE, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_STORE_ID, 0)
        SharedPrefsUtils.setStringPreference(context, Config.KEY_STORE_CODE, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_STORE_NAME, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_PLANOGRAM_REGION_ID, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_IN_REPORT, "")
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_MSL, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_PLANOGRAM, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_POSM, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_TOPUP, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_ACTIVITY, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_COMPETITOR, false)
    }

    fun checkoutReseller(context: Context) {
        SharedPrefsUtils.setStringPreference(context, Config.KEY_RESELLER_ID, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_RESELLER_NAME, 0)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_IN_REPORT_RESELLER, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_RESELLER_PENJUALAN, false)
        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_RESELLER_PULLPROGGRAM, false)
    }

    fun isLoggedIn(context: Context): Boolean =
            SharedPrefsUtils.getBooleanPreference(context, Config.KEY_IS_LOGGED_IN, false)

    fun getUserId(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_ID, 0)

    fun getUsername(context: Context): String? =
            SharedPrefsUtils.getStringPreference(context, Config.KEY_EMPLOYEE_USERNAME, "NULL")

    fun getEmployeeName(context: Context): String? =
            SharedPrefsUtils.getStringPreference(context, Config.KEY_EMPLOYEE_NAME, "NULL")

    fun getEmployeeRoleName(context: Context): String? =
            SharedPrefsUtils.getStringPreference(context, Config.KEY_EMPLOYEE_ROLE_NAME, "NULL")

    fun getEmployeeRoleId(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_ROLE_ID, 0)

    fun getEmployeeSurveyId(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_SURVEY_ID, 0)

    fun getEmployeeRow(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_ROW, 0)

    fun getEmployeeColumn(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_COLUMN, 0)

    fun getEmployeeRow2(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_ROW2, 0)

    fun getEmployeeColumn2(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_COLUMN2, 0)

    fun getEmployeeArea(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_AREA_ID, 0)

    fun getDistributor(context: Context): Long =
            SharedPrefsUtils.getLongPreference(context, Config.KEY_DISTRIBUTOR_ID, 0L)

    fun getNameDistributor(context: Context): String? =
            SharedPrefsUtils.getStringPreference(context, Config.KEY_DISTRIBUTOR_NAME,"NULL")

}