package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Banners {
    @SerializedName("planogram_type_id")
    var planogramTypeId: Int = 0

    @SerializedName("banner_id")
    var bannerId: Int = 0

    @SerializedName("banner_name")
    var bannerName: String = ""

    @SerializedName("image_path")
    var imagePath: String = ""

    @SerializedName("is_local_image")
    var isLocalImage: Int = 0

    @SerializedName("is_active")
    var isActive: Int = 0
}
