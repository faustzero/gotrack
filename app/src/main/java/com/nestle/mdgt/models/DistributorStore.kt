package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class DistributorStore {
    @SerializedName("distributor_id")
    var distributorId: Int = 0

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("location_id")
    var locationId: Int = 0
}