package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class StoreType {
    @SerializedName("store_type_id")
    var storeTypeId: Int = 0

    @SerializedName("store_type_name")
    var storeTypeName: String = ""
}