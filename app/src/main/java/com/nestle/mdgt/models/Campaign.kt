package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Campaign {

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("campaign_id")
    var campaignId: Int = 0

    @SerializedName("surveyor_type_id")
    var surveyorTypeId: Int = 0

    @SerializedName("campaign_name")
    var campaignName: String = ""

    @SerializedName("start_date")
    var startDate: String = ""

    @SerializedName("end_date")
    var endDate: String = ""
}