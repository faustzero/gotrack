package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Distributor {
    @SerializedName("distributor_id")
    var distributorId: String = ""

    @SerializedName("distributor_name")
    var distributorName: String = ""

    @SerializedName("address")
    var address: String = ""
}