package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ProductMslRegion {
    @SerializedName("product_id")
    var productId: Int = 0

    @SerializedName("product_name")
    var productName: String = ""

}