package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class LocalActivityDetail {
    @SerializedName("report_detail_id")
    var reportDetailId: String = ""

    @SerializedName("report_header_id")
    var reportHeaderId: String = ""

    @SerializedName("user_id")
    var userId: Int = 0

    @SerializedName("activity_type_id")
    var activityTypeId: Int = 0

    @SerializedName("info")
    var info: String = ""

    @SerializedName("photo_row_number")
    var photoRowNumber: Int = 0

    @SerializedName("photo_column_number")
    var photoColumnNumber: Int = 0

    @SerializedName("created_date")
    var createdDate: String = ""

    @SerializedName("created")
    var created: String = ""

    @SerializedName("modified")
    var modified: String = ""

    @SerializedName("photo_path")
    var photoPath: String? = ""

    @SerializedName("top_path")
    var topPosition: Int = 0

    @SerializedName("right_path")
    var rightPosition: Int = 0

    @SerializedName("bottom_path")
    var bottomPosition: Int = 0

    @SerializedName("left_path")
    var leftPosition: Int = 0

}