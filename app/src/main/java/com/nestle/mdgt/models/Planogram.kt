package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Planogram {
    @SerializedName("planogram_store_id")
    var planogramRegionId: String = ""

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("planogram_type_id")
    var planogramTypeId: Int = 0

    @SerializedName("planogram_type_name")
    var planogramTypeName: String = ""

    @SerializedName("product_category_id")
    var productCategoryId: Int = 0

    @SerializedName("product_category_name")
    var productCategoryName: String = ""

    @SerializedName("guideline_photo_path")
    var guidelinePhotoPath: String = ""

    @SerializedName("is_msl")
    var isMsl: Int = 0
}
