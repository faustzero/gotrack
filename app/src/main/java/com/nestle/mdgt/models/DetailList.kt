package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class DetailList {

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
