package com.nestle.mdgt.models

import com.nestle.mdgt.database.TMarket

class MarketViewModel {
    var market: TMarket? = null
    var distance: Float = 0.toFloat()
}
