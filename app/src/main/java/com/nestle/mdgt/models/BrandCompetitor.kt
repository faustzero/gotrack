package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class BrandCompetitor {
    @SerializedName("brand_id")
    var brandId: Int = 0

    @SerializedName("brand_name")
    var brandName: String = ""

    @SerializedName("product_category_id")
    var productCategoryId: Int = 0

    @SerializedName("product_category_name")
    var productCategoryName: String = ""


    @SerializedName("is_competitor")
    var isCompetitor: Int = 0
}
