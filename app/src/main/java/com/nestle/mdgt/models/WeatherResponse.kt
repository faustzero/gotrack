package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class WeatherResponse {
    @SerializedName("weather")
    val weathers: List<Weather>? = null

    @SerializedName("main")
    val main: Main? = null


}
