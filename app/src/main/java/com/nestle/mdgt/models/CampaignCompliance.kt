package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class CampaignCompliance {

    @SerializedName("campaign_id")
    var campaignId: Int = 0

    @SerializedName("compliance_id")
    var complianceId: Int = 0

    @SerializedName("compliance_code")
    var complianceCode: String = ""

    @SerializedName("compliance")
    var compliance: String = ""

    @SerializedName("periode_start")
    var periodeStart: String = ""

    @SerializedName("periode_end")
    var periodeEnd: String = ""

}