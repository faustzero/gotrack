package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class DailyDashboardResponse {
    @SerializedName("daily_achievement")
    var dailyAchievement: DailyAchievement? = null

    @SerializedName("status")
    var status: String = ""

}