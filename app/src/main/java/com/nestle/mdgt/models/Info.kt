package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Info {
    @SerializedName("info_id")
    var infoId: Int = 0

    @SerializedName("info_name")
    var infoName: String = ""

    @SerializedName("start_date")
    var startDate: String = ""

    @SerializedName("end_date")
    var endDate: String = ""

    @SerializedName("image_path")
    var imagePath: String = ""
}