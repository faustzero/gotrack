package com.nestle.mdgt.models;

import com.google.gson.annotations.SerializedName;

public class ResellerType {
    @SerializedName("reseller_type_id")
    private int resellerTypeId;

    @SerializedName("reseller_type_name")
    private String resellerTypeName;

    public int getResellerTypeId() {
        return resellerTypeId;
    }

    public void setResellerTypeId(int resellerTypeId) {
        this.resellerTypeId = resellerTypeId;
    }

    public String getResellerTypeName() {
        return resellerTypeName;
    }

    public void setResellerTypeName(String resellerTypeName) {
        this.resellerTypeName = resellerTypeName;
    }
}
