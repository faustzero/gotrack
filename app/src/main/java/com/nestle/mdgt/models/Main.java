package com.nestle.mdgt.models;

import com.google.gson.annotations.SerializedName;

public class Main {
    @SerializedName("temp")
    public Double temp;

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }
}
