package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class RoleName {
    @SerializedName("surveyor_type_id")
    var RoleIds: Int = 0

    @SerializedName("surveyor_type_name")
    var RoleNames: String = ""


}