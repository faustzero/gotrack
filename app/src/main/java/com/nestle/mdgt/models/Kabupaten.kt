package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Kabupaten {
    @SerializedName("kabupaten_id")
    var kabupatenId: Int = 0

    @SerializedName("kabupaten_name")
    var kabupatenName: String = ""

    @SerializedName("area_id")
    var areaId: Int = 0
}