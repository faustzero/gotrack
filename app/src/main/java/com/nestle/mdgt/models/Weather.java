package com.nestle.mdgt.models;

import com.google.gson.annotations.SerializedName;

public class Weather {
    @SerializedName("id")
    public int id;

    @SerializedName("main")
    public String info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
