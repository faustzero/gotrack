package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class CampaignPosm {

    @SerializedName("posm_id")
    var posmId: Int = 0

    @SerializedName("posm_name")
    var posmName: String = ""

    @SerializedName("campaign_id")
    var campaignId: Int = 0

    @SerializedName("qr_code")
    var qrCode: String = ""

    @SerializedName("encode")
    var encode: String = ""

    @SerializedName("is_qr_mandatory")
    var isQrMandatory: Int = 0

}