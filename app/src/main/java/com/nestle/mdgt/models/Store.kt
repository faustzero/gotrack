package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Store {
    @SerializedName("id")
    var sId: Int = 0

    @SerializedName("market_id")
    var marketId: Int = 0

    @SerializedName("market_name")
    var marketName: String = ""

    @SerializedName("store_id")
    var storeId: String = ""

    @SerializedName("store_code")
    var storeCode: String = ""

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("address")
    var address: String = ""

    @SerializedName("phone")
    var phone: String = ""

    @SerializedName("store_type_id")
    var storeTypeId: Int = 0

    @SerializedName("store_type_name")
    var storeTypeName: String = ""

    @SerializedName("distributor_id")
    var distributorId: String = ""

    @SerializedName("distributor_name")
    var distributorName: String = ""

    @SerializedName("kecamatan_id")
    var kecamatanId: Int = 0

    @SerializedName("kecamatan_name")
    var kecamatanName: String = ""

    @SerializedName("kabupaten_id")
    var kabupatenId: Int = 0

    @SerializedName("kabupaten_name")
    var kabupatenName: String = ""

    @SerializedName("area_id")
    var areaId: Int = 0

    @SerializedName("area_name")
    var areaName: String = ""

    @SerializedName("region_id")
    var regionId: Int = 0

    @SerializedName("region_name")
    var regionName: String = ""

    @SerializedName("latitude")
    var latitude: Double = 0.toDouble()

    @SerializedName("longitude")
    var longitude: Double = 0.toDouble()

    @SerializedName("last_visited")
    var lastVisited: String = ""

    @SerializedName("last_visited_utc")
    var lastVisitedUtc: Int = 0

    @SerializedName("photo_path")
    var photoPath: String = ""

    @SerializedName("radius_verification_limit")
    var radiusVerificationLimit: Int = 0
}
