package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class DailyAchievement {
    @SerializedName("jml_selling")
    var selling: String = "-"

    @SerializedName("actual")
    var actualVisit: String = "-"

    @SerializedName("target")
    var planKunjungan: String = "-"

    @SerializedName("persen_visit")
    var persenVisit: String = "-"

    @SerializedName("jml_planogram")
    var planogramStore: String = "-"

    @SerializedName("picos")
    var picos: String = "-"


    @SerializedName("instore_selling")
    var instoreSelling: String = "-"


    @SerializedName("outstore_selling")
    var outstoreSelling: String = "-"

}