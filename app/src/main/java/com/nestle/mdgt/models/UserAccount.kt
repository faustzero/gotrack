package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class UserAccount {
    @SerializedName("nik")
    var nik: String = ""

    @SerializedName("username")
    var username: String = ""

    @SerializedName("surveyor_id")
    var surveyorId: Int = 0

    @SerializedName("surveyor_name")
    var surveyorName: String = ""

    @SerializedName("surveyor_type_id")
    var surveyorTypeId: Int = 0

    @SerializedName("surveyor_type_name")
    var surveyorTypeName: String = ""

    @SerializedName("area_id")
    var areaId: Int = 0

    @SerializedName("area_name")
    var areaName: String = ""

    @SerializedName("region_id")
    var regionId: Int = 0

    @SerializedName("region_name")
    var regionName: String = ""

    @SerializedName("radius_verification_limit")
    var radiusVerificationLimit: Int = 0

    @SerializedName("survey_id")
    var surveyId: Int = 0

    @SerializedName("row")
    var row: Int = 0

    @SerializedName("row_wor")
    var rowWor: Int = 0

    @SerializedName("column")
    var column: Int = 0

    @SerializedName("column_wor")
    var columnWor: Int = 0

    @SerializedName("distributor_id")
    var distributorId: Long = 0L

    @SerializedName("distributor_name")
    var distributorname: String = ""

}