package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class PosmType {
    @SerializedName("posm_type_id")
    var posmTypeId: Int = 0

    @SerializedName("posm_type_name")
    var posmTypeName: String = ""
}