package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Region {
    @SerializedName("region_id")
    var regionId: Int = 0

    @SerializedName("region_name")
    var regionName: String = ""

    @SerializedName("region_name_alias")
    var alias: String = ""
}