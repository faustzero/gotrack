package com.nestle.mdgt.models;

import com.google.gson.annotations.SerializedName;

public class Reseller {
    @SerializedName("store_id")
    private int storeId;

    @SerializedName("store_code")
    private String storeCode;

    @SerializedName("store_name")
    private String storeName;

    @SerializedName("surveyor_id")
    private int surveyorId;

    @SerializedName("surveyor_nik")
    private String surveyorNik;

    @SerializedName("surveyor_name")
    private String surveyorName;

    @SerializedName("reseller_id")
    private String resellerId;

    @SerializedName("reseller_name")
    private String resellerName;

    @SerializedName("reseller_type_id")
    private int resellerTypeId;

    @SerializedName("reseller_type_name")
    private String resellerTypeName;

    @SerializedName("radius_id")
    private int radiusId;

    @SerializedName("radius_name")
    private String radiusName;

    @SerializedName("phone")
    private String phone;

    @SerializedName("address")
    private String address;

    @SerializedName("kabupaten_id")
    private int kabupatenId;

    @SerializedName("kabupaten_name")
    private String kabupatenName;

    @SerializedName("kecamatan_id")
    private int kecamatanId;

    @SerializedName("kecamatan_name")
    private String kecamatanName;

    @SerializedName("tanggal_daftar")
    private String registerDate;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getResellerId() {
        return resellerId;
    }

    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    public String getResellerName() {
        return resellerName;
    }

    public void setResellerName(String resellerName) {
        this.resellerName = resellerName;
    }

    public int getResellerTypeId() {
        return resellerTypeId;
    }

    public void setResellerTypeId(int resellerTypeId) {
        this.resellerTypeId = resellerTypeId;
    }

    public String getResellerTypeName() {
        return resellerTypeName;
    }

    public void setResellerTypeName(String resellerTypeName) {
        this.resellerTypeName = resellerTypeName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getKabupatenId() {
        return kabupatenId;
    }

    public void setKabupatenId(int kabupatenId) {
        this.kabupatenId = kabupatenId;
    }

    public String getKabupatenName() {
        return kabupatenName;
    }

    public void setKabupatenName(String kabupatenName) {
        this.kabupatenName = kabupatenName;
    }

    public int getKecamatanId() {
        return kecamatanId;
    }

    public void setKecamatanId(int kecamatanId) {
        this.kecamatanId = kecamatanId;
    }

    public String getKecamatanName() {
        return kecamatanName;
    }

    public void setKecamatanName(String kecamatanName) {
        this.kecamatanName = kecamatanName;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public int getSurveyorId() {
        return surveyorId;
    }

    public void setSurveyorId(int surveyorId) {
        this.surveyorId = surveyorId;
    }

    public String getSurveyorNik() {
        return surveyorNik;
    }

    public void setSurveyorNik(String surveyorNik) {
        this.surveyorNik = surveyorNik;
    }

    public String getSurveyorName() {
        return surveyorName;
    }

    public void setSurveyorName(String surveyorName) {
        this.surveyorName = surveyorName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getRadiusId() {
        return radiusId;
    }

    public void setRadiusId(int radiusId) {
        this.radiusId = radiusId;
    }

    public String getRadiusName() {
        return radiusName;
    }

    public void setRadiusName(String radiusName) {
        this.radiusName = radiusName;
    }
}
