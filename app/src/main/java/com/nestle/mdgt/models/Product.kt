package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Product {

    @SerializedName("product_role_id")
    var productRoleId: Int = 0

    @SerializedName("surveyor_type_id")
    var roleId: Int = 0

    @SerializedName("product_id")
    var productId: Int = 0

    @SerializedName("product_code")
    var productCode: String = ""

    @SerializedName("product_name")
    var productName: String = ""

    @SerializedName("product_category_id")
    var productCategoryId: Int = 0

    @SerializedName("product_category_name")
    var productCategoryName: String = ""

    @SerializedName("brand_id")
    var brandId: Int = 0

    @SerializedName("brand_name")
    var brandName: String = ""

    @SerializedName("produsen_id")
    var produsenId: Int = 0

    @SerializedName("produsen_name")
    var produsenName: String = ""

    @SerializedName("is_competitor")
    var isCompetitor: Int = 0

    @SerializedName("unit_id1")
    var unitId1: Int = 0

    @SerializedName("unit_name1")
    var unitName1: String = ""

    @SerializedName("price1")
    var price1: Int = 0

    @SerializedName("unit_id2")
    var unitId2: Int = 0

    @SerializedName("unit_name2")
    var unitName2: String = ""

    @SerializedName("price2")
    var price2: Int = 0
}
