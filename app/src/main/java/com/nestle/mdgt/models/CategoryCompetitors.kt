package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class CategoryCompetitors {

    @SerializedName("product_category_id")
    var productCategoryId: Int = 0

    @SerializedName("product_category_name")
    var productCategoryName: String = ""
}