package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Kecamatan {
    @SerializedName("kecamatan_id")
    var kecamatanId: Int = 0

    @SerializedName("kecamatan_name")
    var kecamatanName: String = ""

    @SerializedName("kabupaten_id")
    var kabupatenId: Int = 0
}