package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class LocalActivityType {
    @SerializedName("activity_id")
    var activityTypeId: Int = 0

    @SerializedName("activity_name")
    var activityTypeName: String = ""
}