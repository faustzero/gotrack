package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ProductMsl {
    @SerializedName("product_id")
    var productId: Int = 0

    @SerializedName("product_code")
    var productCode: String = ""

    @SerializedName("product_name")
    var productName: String = ""

    @SerializedName("brand_id")
    var brandId: Int = 0

    @SerializedName("produsen_id")
    var produsenId: Int = 0

}