package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Market {
    @SerializedName("market_id")
    var marketId: Int = 0

    @SerializedName("market_code")
    var marketCode: String = ""

    @SerializedName("market_name")
    var marketName: String = ""

    @SerializedName("address")
    var address: String = ""

    @SerializedName("kecamatan_id")
    var kecamatanId: Int = 0

    @SerializedName("kecamatan_name")
    var kecamatanName: String = ""

    @SerializedName("kabupaten_id")
    var kabupatenId: Int = 0

    @SerializedName("kabupaten_name")
    var kabupatenName: String = ""

    @SerializedName("area_id")
    var areaId: Int = 0

    @SerializedName("area_name")
    var areaName: String = ""

    @SerializedName("region_id")
    var regionId: Int = 0

    @SerializedName("region_name")
    var regionName: String = ""

    @SerializedName("latitude")
    var latitude: Double = 0.toDouble()

    @SerializedName("longitude")
    var longitude: Double = 0.toDouble()

    @SerializedName("radius_verification_limit")
    var radiusVerificationLimit: Int = 0
}
