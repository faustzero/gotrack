package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ReasonCampaign {
    @SerializedName("reason_id")
    var reasonId: Int = 0

    @SerializedName("reason_name")
    var reason: String? = null
}
