package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class DistributorArea {
    @SerializedName("distributor_id")
    var distributorId: Int = 0

    @SerializedName("distributor_name")
    var distributorName: String = ""

    @SerializedName("area_id")
    var areaId: Int = 0

}