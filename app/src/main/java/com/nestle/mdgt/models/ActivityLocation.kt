package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ActivityLocation {
    @SerializedName("location_id")
    var locationId: Int = 0

    @SerializedName("location_name")
    var locationName: String = ""
}