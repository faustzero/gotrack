package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Activity {
    @SerializedName("activity_id")
    var activityId: Int = 0

    @SerializedName("activity_name")
    var activityName: String = ""

    @SerializedName("product_category_id")
    var productCategoryId: Int = 0

    @SerializedName("product_category_name")
    var productCategoryName: String = ""

    @SerializedName("start_date")
    var startDate: String = ""

    @SerializedName("end_date")
    var endDate: String = ""
}