package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ActivityName {
    @SerializedName("activity_name_id")
    var activityNameId: Int = 0

    @SerializedName("activity_name")
    var activityName: String = ""

    @SerializedName("activity_id")
    var activityId: Int = 0
}