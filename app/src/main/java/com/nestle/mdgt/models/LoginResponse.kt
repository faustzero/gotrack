package com.nestle.mdgt.models

import android.graphics.Region
import com.google.gson.annotations.SerializedName
import com.nestle.mdgt.database.TReasonCampaignPosm

class LoginResponse {
    @SerializedName("surveyor")
    var userAccount: UserAccount? = null
//
//    @SerializedName("markets")
//    var markets: List<Market>? = null

    @SerializedName("stores")
    var stores: List<Store>? = null

    @SerializedName("products_by_role")
    var products: List<Product>? = null

    @SerializedName("resellers")
    var resellers: List<Reseller>? = null

    @SerializedName("campaigns")
    var campaigns: List<Campaign>? = null

    @SerializedName("campaign_posms")
    var campaignPosms: List<CampaignPosm>? = null

    @SerializedName("campaign_compliances")
    var compliances: List<CampaignCompliance>? = null

    @SerializedName("reseller_types")
    var resellerTypes: List<ResellerType>? = null

    @SerializedName("product_competitors")
    var productCompetitors: List<ProductCompetitor>? = null

    @SerializedName("brand_competitors")
    var brandCompetitors: List<BrandCompetitor>? = null

    @SerializedName("planograms_by_store")
    var planograms: List<Planogram>? = null

    @SerializedName("reasons_campaigns")
    var reasonCampaigns: List<ReasonCampaign>? = null

    @SerializedName("reasons_campaign_posm")
    var reasonCampaignPosm: List<ReasonPosmCampaign>? = null

    @SerializedName("category_competitor")
    var categoryCompetitors: List<CategoryCompetitors>? = null

    @SerializedName("posms")
    var posms: List<Posm>? = null

    @SerializedName("activities")
    var activities: List<Activity>? = null

    @SerializedName("planogram_types")
    var planogramTypes: List<PlanogramType>? = null

    @SerializedName("posm_types")
    var posmTypes: List<PosmType>? = null

    @SerializedName("competitor_activity_types")
    var activityTypes: List<ActivityType>? = null

    @SerializedName("reasons")
    var reasonNoVisits: List<ReasonNoVisit>? = null

    @SerializedName("store_types")
    var storeTypes: List<StoreType>? = null

    @SerializedName("distributors")
    var distributors: List<Distributor>? = null

    @SerializedName("distributors_area")
    var distributorsAreas: List<DistributorArea>? = null

    @SerializedName("distributors_store")
    var distributorsStore: List<DistributorStore>? = null

    @SerializedName("areas")
    var areas: List<Area>? = null

    @SerializedName("kabupatens")
    var kabupatens: List<Kabupaten>? = null

    @SerializedName("kecamatans")
    var kecamatans: List<Kecamatan>? = null

    @SerializedName("infos")
    var infos: List<Info>? = null

    @SerializedName("banners")
    var banners: List<Banners>? = null

    @SerializedName("local_activitys")
    var localActivitys: List<LocalActivityType>? = null

    @SerializedName("product_msl")
    var productMSLs: List<ProductMsl>? = null

    @SerializedName("product_msl_region")
    var productMSLRegions: List<ProductMslRegion>? = null

    @SerializedName("local_activity_headers")
    var localHeaders: List<LocalActivityHeader>? = null

    @SerializedName("local_activitys_details")
    var localDetails: List<LocalActivityDetail>? = null

    @SerializedName("local_activity_names")
    var localActivityNames: List<ActivityName>? = null

    @SerializedName("local_activity_location")
    var localActivityLocation: List<ActivityLocation>? = null

    @SerializedName("local_activity_names_weekly")
    var localActivityNamesWeekly: List<ActivityName>? = null

    @SerializedName("surveyor_type_id")
    var roleName: List<RoleName>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

    @SerializedName("noo_regions")
    var nooRegions: List<com.nestle.mdgt.models.Region>? = null

    @SerializedName("noo_areas")
    var nooAreas: List<Area>? = null

    @SerializedName("noo_distributor")
    var nooDistributors: List<DistributorArea>? = null



}
