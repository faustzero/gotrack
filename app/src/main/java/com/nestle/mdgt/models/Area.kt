package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Area {
    @SerializedName("area_id")
    var areaId: Int = 0

    @SerializedName("area_name")
    var areaName: String = ""

    @SerializedName("region_id")
    var regionId: Int = 0
}