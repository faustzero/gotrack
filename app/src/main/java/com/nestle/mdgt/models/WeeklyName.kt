package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class WeeklyName {
    @SerializedName("activity_name_id")
    var activityNameId: Int = 0

    @SerializedName("activity_name")
    var activityName: String = ""
}