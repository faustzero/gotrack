package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class NooDataResponse {

    @SerializedName("store")
    var store: Store? = null

    @SerializedName("campaigns")
    var campaigns: List<Campaign>? = null

    @SerializedName("campaign_posms")
    var campaignPosms: List<CampaignPosm>? = null

    @SerializedName("planograms_by_store")
    var planograms: List<Planogram>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""


}
