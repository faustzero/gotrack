package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class Posm {
    @SerializedName("posm_planogram_id")
    var posmPlanogramId: String = ""

    @SerializedName("planogram_region_id")
    var planogramRegionId: String = ""

    @SerializedName("planogram_type_id")
    var planogramTypeId: Int = 0

    @SerializedName("planogram_type_name")
    var planogramTypeName: String = ""

    @SerializedName("posm_type_id")
    var posmTypeId: Int = 0

    @SerializedName("posm_type_name")
    var posmTypeName: String = ""

}