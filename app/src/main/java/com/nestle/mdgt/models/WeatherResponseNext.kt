package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class WeatherResponseNext {
    @SerializedName("weather")
    val weathers: List<Weather>? = null

    @SerializedName("main")
    val main: Main? = null

    @SerializedName("dt_txt")
    val dt_txt = ""
}
