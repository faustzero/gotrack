package com.nestle.mdgt.models

import com.google.gson.annotations.SerializedName

class ReportResponse {
    @SerializedName("status")
    var status: String? = null

    @SerializedName("message")
    var message: String? = null
}
