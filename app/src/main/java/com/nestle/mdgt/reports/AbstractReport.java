package com.nestle.mdgt.reports;

import java.io.Serializable;

public interface AbstractReport extends Serializable {
	boolean sendReport();

	String getReturn();

	String getDescription();
}
