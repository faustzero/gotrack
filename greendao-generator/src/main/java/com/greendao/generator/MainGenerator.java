package com.greendao.generator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MainGenerator {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.nestle.mdgt.database");

        //region TransmissionHistory Entity
        Entity transHistory = schema.addEntity("TTransmissionHistory");
        transHistory.addIdProperty();
        transHistory.addStringProperty("Description");
        //endregion

        //region Info
        Entity info = schema.addEntity("TInfo");
        info.addIdProperty();
        info.addIntProperty("InfoId");
        info.addStringProperty("InfoName");
        info.addStringProperty("StartDate");
        info.addStringProperty("EndDate");
        info.addStringProperty("ImagePath");
        //endregion
        
        //region Reason No Visit Entity
        Entity reason = schema.addEntity("TReasonNoVisit");
        reason.addIdProperty();
        reason.addIntProperty("ReasonId").notNull();
        reason.addStringProperty("ReasonName").notNull();
        //endregion

        //region Competitor Activity Type
        Entity activityType = schema.addEntity("TActivityType");
        activityType.addIdProperty();
        activityType.addIntProperty("ActivityTypeId");
        activityType.addStringProperty("ActivityTypeName");
        //endregion

        //region Store Type
        Entity storeType = schema.addEntity("TStoreType");
        storeType.addIdProperty();
        storeType.addIntProperty("StoreTypeId");
        storeType.addStringProperty("StoreTypeName");
        //endregion
        
        //region Distributor
        Entity distributor = schema.addEntity("TDistributor");
        distributor.addIdProperty();
        distributor.addStringProperty("DistributorId");
        distributor.addStringProperty("DistributorName");
        distributor.addStringProperty("Address");
        //endregion

        //region Area
        Entity area = schema.addEntity("TArea");
        area.addIdProperty();
        area.addIntProperty("AreaId");
        area.addStringProperty("AreaName");
        area.addIntProperty("RegionId");
        //endregion
        
        //region Kabupaten
        Entity kabupaten = schema.addEntity("TKabupaten");
        kabupaten.addIdProperty();
        kabupaten.addIntProperty("KabupatenId");
        kabupaten.addStringProperty("KabupatenName");
        kabupaten.addIntProperty("AreaId");
        //endregion
        
        //region Kecamatan
        Entity kecamatan = schema.addEntity("TKecamatan");
        kecamatan.addIdProperty();
        kecamatan.addIntProperty("KecamatanId");
        kecamatan.addStringProperty("KecamatanName");
        kecamatan.addIntProperty("KabupatenId");
        //endregion

        //region Planogram Type
        Entity planogramType = schema.addEntity("TPlanogramType");
        planogramType.addIdProperty();
        planogramType.addIntProperty("PlanogramTypeId");
        planogramType.addStringProperty("PlanogramTypeName");
        planogramType.addIntProperty("ProductCategoryId");
        planogramType.addStringProperty("ProductCategoryName");
        planogramType.addStringProperty("GuidelinePhotoPath");
        //endregion

        //region Posm Type
        Entity posmType = schema.addEntity("TPosmType");
        posmType.addIdProperty();
        posmType.addIntProperty("PosmTypeId");
        posmType.addStringProperty("PosmTypeName");
        //endregion

        //region Activity Entity
        Entity activity = schema.addEntity("TActivity");
        activity.addIdProperty();
        activity.addIntProperty("ActivityId").notNull();
        activity.addStringProperty("ActivityName").notNull();
        activity.addIntProperty("ProductCategoryId").notNull();
        activity.addStringProperty("ProductCategoryName").notNull();
        activity.addStringProperty("StartDate").notNull();
        activity.addStringProperty("EndDate").notNull();
        //endregion

        //region Market Entity
        Entity market = schema.addEntity("TMarket");
        market.addIdProperty();
        market.addIntProperty("MarketId").notNull();
        market.addStringProperty("MarketCode").notNull();
        market.addStringProperty("MarketName").notNull();
        market.addStringProperty("Address").notNull();
        market.addIntProperty("KecamatanId").notNull();
        market.addStringProperty("KecamatanName").notNull();
        market.addIntProperty("KabupatenId").notNull();
        market.addStringProperty("KabupatenName").notNull();
        market.addIntProperty("AreaId").notNull();
        market.addStringProperty("AreaName").notNull();
        market.addIntProperty("RegionId").notNull();
        market.addStringProperty("RegionName").notNull();
        market.addDoubleProperty("Latitude");
        market.addDoubleProperty("Longitude");
        market.addIntProperty("IsVisited").notNull();
        market.addIntProperty("RadiusVerificationLimit").notNull();
        //endregion

        //region Unit Entity
        Entity unit = schema.addEntity("TUnit");
        unit.addIdProperty();
        unit.addIntProperty("ProductId").notNull();
        unit.addStringProperty("ProductCode").notNull();
        unit.addStringProperty("ProductName").notNull();
        unit.addIntProperty("UnitId").notNull();
        unit.addStringProperty("UnitName").notNull();
        unit.addStringProperty("Description").notNull();
        unit.addIntProperty("Price").notNull();
        //endregion

        //region Store Entity
        Entity store = schema.addEntity("TStore");
        store.addIdProperty();
        store.addIntProperty("sId").notNull();
        store.addIntProperty("MarketId").notNull();
        store.addStringProperty("MarketName").notNull();
        store.addStringProperty("StoreId").notNull();
        store.addStringProperty("StoreCode").notNull();
        store.addStringProperty("StoreName").notNull();
        store.addIntProperty("StoreTypeId").notNull();
        store.addStringProperty("StoreTypeName").notNull();
        store.addStringProperty("Address").notNull();
        store.addStringProperty("Phone").notNull();
        store.addIntProperty("KecamatanId").notNull();
        store.addStringProperty("KecamatanName").notNull();
        store.addIntProperty("KabupatenId").notNull();
        store.addStringProperty("KabupatenName").notNull();
        store.addIntProperty("AreaId").notNull();
        store.addStringProperty("AreaName").notNull();
        store.addIntProperty("RegionId").notNull();
        store.addStringProperty("RegionName").notNull();
        store.addStringProperty("DistributorId").notNull();
        store.addStringProperty("DistributorName").notNull();
        store.addDoubleProperty("Latitude");
        store.addDoubleProperty("Longitude");
        store.addStringProperty("PhotoPath");
        store.addIntProperty("LastVisitedUtc");
        store.addStringProperty("LastVisited");
        store.addIntProperty("IsVisited").notNull();
        store.addIntProperty("RadiusVerificationLimit").notNull();
        //endregion

        //region Product Entity
        Entity product = schema.addEntity("TProduct");
        product.addIdProperty();
        product.addIntProperty("ProductRegionId").notNull();
        product.addIntProperty("RegionId").notNull();
        product.addStringProperty("RegionName").notNull();
        product.addIntProperty("ProductId").notNull();
        product.addStringProperty("ProductCode").notNull();
        product.addStringProperty("ProductName").notNull();
        product.addIntProperty("ProductCategoryId").notNull();
        product.addStringProperty("ProductCategoryName").notNull();
        product.addIntProperty("BrandId").notNull();
        product.addStringProperty("BrandName").notNull();
        product.addIntProperty("ProdusenId").notNull();
        product.addStringProperty("ProdusenName").notNull();
        product.addIntProperty("IsCompetitor").notNull();
        product.addIntProperty("UnitId1").notNull();
        product.addStringProperty("UnitName1").notNull();
        product.addIntProperty("Price1").notNull();
        product.addIntProperty("UnitId2").notNull();
        product.addStringProperty("UnitName2").notNull();
        product.addIntProperty("Price2").notNull();
        //endregion

        //region Product Competitor Entity
        Entity productCompetitor = schema.addEntity("TProductCompetitor");
        productCompetitor.addIdProperty();
        productCompetitor.addIntProperty("ProductId").notNull();
        productCompetitor.addStringProperty("ProductCode").notNull();
        productCompetitor.addStringProperty("ProductName").notNull();
        productCompetitor.addIntProperty("ProductCategoryId").notNull();
        productCompetitor.addStringProperty("ProductCategoryName").notNull();
        productCompetitor.addIntProperty("BrandId").notNull();
        productCompetitor.addStringProperty("BrandName").notNull();
        productCompetitor.addIntProperty("ProdusenId").notNull();
        productCompetitor.addStringProperty("ProdusenName").notNull();
        productCompetitor.addIntProperty("IsCompetitor").notNull();
        //endregion

        //region Brand Competitor Entity
        Entity brandCompetitor = schema.addEntity("TBrandCompetitor");
        brandCompetitor.addIdProperty();
        brandCompetitor.addIntProperty("BrandId").notNull();
        brandCompetitor.addStringProperty("BrandName").notNull();
        brandCompetitor.addIntProperty("ProductCategoryId").notNull();
        brandCompetitor.addStringProperty("ProductCategoryName").notNull();
        brandCompetitor.addIntProperty("IsCompetitor").notNull();
        //endregion

        //region Planogram Entity
        Entity planogram = schema.addEntity("TPlanogram");
        planogram.addIdProperty();
        planogram.addStringProperty("PlanogramRegionId").notNull();
        planogram.addIntProperty("StoreId").notNull();
        planogram.addStringProperty("StoreName").notNull();
        planogram.addIntProperty("PlanogramTypeId").notNull();
        planogram.addStringProperty("PlanogramTypeName").notNull();
        planogram.addIntProperty("ProductCategoryId").notNull();
        planogram.addStringProperty("ProductCategoryName").notNull();
        planogram.addStringProperty("GuidelinePhotoPath");
        planogram.addIntProperty("IsMsl");

        //endregion

        //region Posm Entity
        Entity posm = schema.addEntity("TPosm");
        posm.addIdProperty();
        posm.addStringProperty("PosmPlanogramId").notNull();
        posm.addStringProperty("PlanogramRegionId").notNull();
        posm.addIntProperty("PlanogramTypeId").notNull();
        posm.addStringProperty("PlanogramTypeName").notNull();
        posm.addIntProperty("PosmTypeId").notNull();
        posm.addStringProperty("PosmTypeName").notNull();
        //endregion

        //region Visit Market Entity
        Entity visitMarket = schema.addEntity("TVisitMarket");
        visitMarket.addIdProperty();
        visitMarket.addStringProperty("VisitIdMarket");
        visitMarket.addIntProperty("SurveyorId");
        visitMarket.addIntProperty("MarketId");
        visitMarket.addIntProperty("IsVisit");
        visitMarket.addIntProperty("IsCheckIn");
        visitMarket.addStringProperty("ReasonNoVisit");
        visitMarket.addLongProperty("StartDateTimeUtc");
        visitMarket.addLongProperty("EndDateTimeUtc");
        visitMarket.addStringProperty("StartDateTime");
        visitMarket.addStringProperty("EndDateTime");
        //endregion

        //region Visit Store Entity
        Entity visitStore = schema.addEntity("TVisitStore");
        visitStore.addIdProperty();
        visitStore.addStringProperty("VisitIdStore");
        visitStore.addStringProperty("VisitIdMarket");
        visitStore.addIntProperty("SurveyorId");
        visitStore.addStringProperty("StoreId");
        visitStore.addIntProperty("IsVisit");
        visitStore.addIntProperty("IsCheckIn");
        visitStore.addStringProperty("ReasonNoVisit");
        visitStore.addLongProperty("StartDateTimeUtc");
        visitStore.addLongProperty("EndDateTimeUtc");
        visitStore.addStringProperty("StartDateTime");
        visitStore.addStringProperty("EndDateTime");
        visitStore.addStringProperty("PhotoPath");
        //endregion

        //region Report MSL Entity
        Entity msl = schema.addEntity("TReportMsl");
        msl.addIdProperty();
        msl.addStringProperty("VisitId");
        msl.addIntProperty("SurveyorId");
        msl.addStringProperty("StoreId");
        msl.addIntProperty("ProductId");
        msl.addStringProperty("ProductCode");
        msl.addStringProperty("ProductName");
        msl.addIntProperty("ProductCategoryId");
        msl.addStringProperty("ProductCategoryName");
        msl.addIntProperty("BrandId");
        msl.addStringProperty("BrandName");
        msl.addIntProperty("UnitId1");
        msl.addStringProperty("UnitName1");
        msl.addIntProperty("Stock1");
        msl.addIntProperty("Price1");
        msl.addIntProperty("UnitId2");
        msl.addStringProperty("UnitName2");
        msl.addIntProperty("Stock2");
        msl.addIntProperty("Price2");
        msl.addBooleanProperty("IsDone");
        //endregion

        //region Reseller Entity
        Entity reseller = schema.addEntity("TReseller");
        reseller.addIdProperty();
        reseller.addIntProperty("StoreId").notNull();
        reseller.addStringProperty("StoreCode").notNull();
        reseller.addStringProperty("StoreName").notNull();
        reseller.addIntProperty("SurveyorId").notNull();
        reseller.addStringProperty("SurveyorNik").notNull();
        reseller.addStringProperty("SurveyorName").notNull();
        reseller.addStringProperty("ResellerId").notNull();
        reseller.addStringProperty("ResellerName").notNull();
        reseller.addIntProperty("ResellerTypeId").notNull();
        reseller.addStringProperty("ResellerTypeName").notNull();
        reseller.addIntProperty("RadiusId").notNull();
        reseller.addStringProperty("RadiusName").notNull();
        reseller.addStringProperty("Phone").notNull();
        reseller.addStringProperty("Address").notNull();
        reseller.addIntProperty("KabupatenId").notNull();
        reseller.addStringProperty("KabupatenName").notNull();
        reseller.addIntProperty("KecamatanId").notNull();
        reseller.addStringProperty("KecamatanName").notNull();
        reseller.addStringProperty("RegisterDate").notNull();
        reseller.addDoubleProperty("Latitude");
        reseller.addDoubleProperty("Longitude");
        reseller.addBooleanProperty("IsDonePullProgram").notNull();
        reseller.addBooleanProperty("IsDonePenjualan").notNull();
        reseller.addBooleanProperty("IsDoneLocation").notNull();
        reseller.addBooleanProperty("IsDoneUpdate").notNull();
        //endregion

        //region Reseller Type Entity
        Entity resellerType = schema.addEntity("TResellerType");
        resellerType.addIdProperty();
        resellerType.addIntProperty("ResellerTypeId").notNull();
        resellerType.addStringProperty("ResellerTypeName").notNull();

        Entity campaign = schema.addEntity("TCampaign");
        campaign.addIdProperty();
        campaign.addIntProperty("StoreId").notNull();
        campaign.addIntProperty("CampaignId").notNull();
        campaign.addIntProperty("SurveyorTypeId").notNull();
        campaign.addStringProperty("CampaignName").notNull();
        campaign.addStringProperty("startDate").notNull();
        campaign.addStringProperty("EndDate").notNull();
        //endregion

        Entity campaignPosm = schema.addEntity("TCampaignPosm");
        campaignPosm.addIdProperty();
        campaignPosm.addIntProperty("PosmId").notNull();
        campaignPosm.addStringProperty("PosmName").notNull();
        campaignPosm.addIntProperty("CampaignId").notNull();
        campaignPosm.addStringProperty("QrCode").notNull();
        campaignPosm.addStringProperty("Encode");
        campaignPosm.addIntProperty("IsQrMandatory").notNull();

        //endregion

        Entity rCampaignHeader = schema.addEntity("TReportCampaignHeader");
        rCampaignHeader.addIdProperty();
        rCampaignHeader.addStringProperty("VisitId");
        rCampaignHeader.addIntProperty("StoreId").notNull();
        rCampaignHeader.addIntProperty("CampaignId").notNull();
        rCampaignHeader.addStringProperty("CampaignName").notNull();
        rCampaignHeader.addStringProperty("startDate").notNull();
        rCampaignHeader.addStringProperty("EndDate").notNull();
        rCampaignHeader.addIntProperty("IsTersedia");
        rCampaignHeader.addIntProperty("ReasonId").notNull();
        rCampaignHeader.addBooleanProperty("IsDone");
        //endregion

        Entity rCampaignDetail = schema.addEntity("TReportCampaignDetail");
        rCampaignDetail.addIdProperty();
        rCampaignDetail.addStringProperty("VisitId");
        rCampaignDetail.addIntProperty("StoreId").notNull();
        rCampaignDetail.addIntProperty("CampaignId").notNull();
        rCampaignDetail.addIntProperty("PosmId").notNull();
        rCampaignDetail.addStringProperty("PosmName").notNull();
        rCampaignDetail.addStringProperty("PhotoPath").notNull();
        rCampaignDetail.addBooleanProperty("IsDone");
        rCampaignDetail.addIntProperty("Status");
        rCampaignDetail.addIntProperty("Alasan");
        rCampaignDetail.addIntProperty("IsScanned");
        rCampaignDetail.addStringProperty("QrCode");
        rCampaignDetail.addStringProperty("Encode");
        rCampaignDetail.addIntProperty("IsQrMandatory");


        //endregion

        //region Report Topup Entity
        Entity topup = schema.addEntity("TReportTopup");
        topup.addIdProperty();
        topup.addStringProperty("VisitId");
        topup.addIntProperty("SurveyorId");
        topup.addStringProperty("StoreId");
        topup.addIntProperty("ProductId");
        topup.addStringProperty("ProductCode");
        topup.addStringProperty("ProductName");
        topup.addIntProperty("ProductCategoryId");
        topup.addStringProperty("ProductCategoryName");
        topup.addIntProperty("BrandId");
        topup.addStringProperty("BrandName");
        topup.addIntProperty("UnitId1");
        topup.addStringProperty("UnitName1");
        topup.addIntProperty("Quantity1");
        topup.addIntProperty("Price1");
        topup.addIntProperty("UnitId2");
        topup.addStringProperty("UnitName2");
        topup.addIntProperty("Quantity2");
        topup.addIntProperty("Price2");
        topup.addBooleanProperty("IsDone");
        //endregion

        Entity categoryCompetitor = schema.addEntity("TCategoryCompetitor");
        categoryCompetitor.addIdProperty();
        categoryCompetitor.addIntProperty("ProductCategoryId").notNull();
        categoryCompetitor.addStringProperty("ProductCategoryName").notNull();
        //endregion


        //region Report Pull Program Entity
        Entity banner = schema.addEntity("TBanner");
        banner.addIdProperty();
        banner.addIntProperty("PlanogramTypeId").notNull();
        banner.addIntProperty("BannerId").notNull();
        banner.addStringProperty("BannerName").notNull();
        banner.addStringProperty("ImagePath").notNull();
        banner.addIntProperty("IsLocalImage").notNull();
        banner.addIntProperty("isActive").notNull();
        //endregion

        //region Report Planogram Entity
        Entity rPlanogram = schema.addEntity("TReportPlanogram");
        rPlanogram.addIdProperty();
        rPlanogram.addStringProperty("VisitId");
        rPlanogram.addIntProperty("SurveyorId");
        rPlanogram.addStringProperty("StoreId");
        rPlanogram.addStringProperty("PlanogramRegionId");
        rPlanogram.addStringProperty("StoreName");
        rPlanogram.addIntProperty("PlanogramTypeId");
        rPlanogram.addStringProperty("PlanogramTypeName");
        rPlanogram.addIntProperty("ProductCategoryId");
        rPlanogram.addStringProperty("ProductCategoryName");
        rPlanogram.addStringProperty("GuidelinePhotoPath");
        rPlanogram.addIntProperty("IsComply");
        rPlanogram.addStringProperty("IsNew");
        rPlanogram.addStringProperty("BeforePhotoPath");
        rPlanogram.addStringProperty("AfterPhotoPath");
        rPlanogram.addStringProperty("PhotoPath3");
        rPlanogram.addStringProperty("PhotoPath4");
        rPlanogram.addStringProperty("PhotoPath5");
        rPlanogram.addStringProperty("PhotoPath6");
        rPlanogram.addStringProperty("PhotoPath7");
        rPlanogram.addStringProperty("PhotoPath8");
        rPlanogram.addBooleanProperty("IsDone");
        rPlanogram.addBooleanProperty("IsAllPosmsDone");
        rPlanogram.addIntProperty("IsMsl");
        rPlanogram.addBooleanProperty("isPilih");

        //endregion

        //region Report Penjualan Entity
        Entity penjualan = schema.addEntity("TReportPenjualan");
        penjualan.addIdProperty();
        penjualan.addStringProperty("VisitId").notNull();
        penjualan.addStringProperty("ResellerId").notNull();
        penjualan.addStringProperty("ResellerName").notNull();
        penjualan.addIntProperty("BrandId").notNull();
        penjualan.addStringProperty("BrandName").notNull();
        penjualan.addIntProperty("ProductId").notNull();
        penjualan.addStringProperty("ProductName").notNull();
        penjualan.addIntProperty("UnitId1").notNull();
        penjualan.addStringProperty("UnitName1").notNull();
        penjualan.addIntProperty("Quantity1").notNull();
        penjualan.addIntProperty("Price1").notNull();
        penjualan.addIntProperty("UnitId2").notNull();
        penjualan.addStringProperty("UnitName2").notNull();
        penjualan.addIntProperty("Quantity2").notNull();
        penjualan.addIntProperty("Price2").notNull();
        //endregion

        //region Report Posm Entity
        Entity rPosm = schema.addEntity("TReportPosm");
        rPosm.addIdProperty();
        rPosm.addStringProperty("VisitId");
        rPosm.addIntProperty("SurveyorId");
        rPosm.addStringProperty("StoreId");
        rPosm.addStringProperty("PosmPlanogramId");
        rPosm.addStringProperty("PlanogramRegionId");
        rPosm.addIntProperty("PlanogramTypeId");
        rPosm.addStringProperty("PlanogramTypeName");
        rPosm.addIntProperty("PosmTypeId");
        rPosm.addStringProperty("PosmTypeName");
        rPosm.addIntProperty("Ketersediaan");
        rPosm.addIntProperty("Penempatan");
        rPosm.addIntProperty("Kondisi");
        rPosm.addStringProperty("IsNew");
        rPosm.addStringProperty("PhotoPath");
        rPosm.addBooleanProperty("IsDone");
        //endregion

        //region Report Activity Entity
        Entity rActivity = schema.addEntity("TReportActivity");
        rActivity.addIdProperty();
        rActivity.addStringProperty("VisitId");
        rActivity.addIntProperty("SurveyorId");
        rActivity.addStringProperty("StoreId");
        rActivity.addIntProperty("ActivityId").notNull();
        rActivity.addStringProperty("ActivityName").notNull();
        rActivity.addIntProperty("ProductCategoryId").notNull();
        rActivity.addStringProperty("ProductCategoryName").notNull();
        rActivity.addStringProperty("StartDate").notNull();
        rActivity.addStringProperty("EndDate").notNull();
        rActivity.addIntProperty("IsAvailable");
        rActivity.addStringProperty("PhotoPath");
        rActivity.addBooleanProperty("IsDone");
        //endregion

        //region Report Competitor Entity
        Entity rCompetitor = schema.addEntity("TReportCompetitor");
        rCompetitor.addIdProperty();
        rCompetitor.addStringProperty("VisitId");
        rCompetitor.addIntProperty("SurveyorId");
        rCompetitor.addStringProperty("StoreId");
        rCompetitor.addIntProperty("ActivityTypeId").notNull();
        rCompetitor.addStringProperty("ActivityTypeName").notNull();
        rCompetitor.addIntProperty("Harga").notNull();
        rCompetitor.addIntProperty("ProductId").notNull();
        rCompetitor.addStringProperty("ProductName").notNull();
        rCompetitor.addIntProperty("CategoryId").notNull();
        rCompetitor.addStringProperty("CategoryName").notNull();
        rCompetitor.addStringProperty("StartDate").notNull();
        rCompetitor.addStringProperty("EndDate").notNull();
        rCompetitor.addStringProperty("Description");
        rCompetitor.addStringProperty("PhotoPath");
        rCompetitor.addBooleanProperty("IsDone");
        //endregion

        //region Reason Planogram Entity
        Entity reasonP = schema.addEntity("TReasonCampaign");
        reasonP.addIdProperty();
        reasonP.addIntProperty("ReasonId").notNull();
        reasonP.addStringProperty("ReasonName").notNull();
        //endregion

        //region Report Pull Program Entity
        Entity pullProgram = schema.addEntity("TReportPullProgram");
        pullProgram.addIdProperty();
        pullProgram.addStringProperty("VisitId").notNull();
        pullProgram.addStringProperty("ResellerId").notNull();
        pullProgram.addStringProperty("ResellerName").notNull();
        pullProgram.addStringProperty("StickerName").notNull();
        pullProgram.addIntProperty("Quantity").notNull();
        //endregion

        //ReportHeader Entity
        Entity rActivityHeader = schema.addEntity("TReportActivityHeader");
        rActivityHeader.addIdProperty();
        rActivityHeader.addStringProperty("HeaderId").notNull();
        rActivityHeader.addIntProperty("UserId").notNull();
        rActivityHeader.addStringProperty("ActivityName").notNull();
        rActivityHeader.addIntProperty("ActivityId").notNull();
        rActivityHeader.addStringProperty("PeriodeStart").notNull();
        rActivityHeader.addStringProperty("PeriodeEnd").notNull();
        rActivityHeader.addStringProperty("LocationName").notNull();
        rActivityHeader.addIntProperty("LocationId").notNull();
        rActivityHeader.addIntProperty("ActivityTypeId").notNull();
        rActivityHeader.addStringProperty("ActivityTypeName").notNull();
        rActivityHeader.addIntProperty("AreaId").notNull();
        rActivityHeader.addStringProperty("Mekanisme").notNull();
        rActivityHeader.addStringProperty("Tujuan").notNull();
        rActivityHeader.addStringProperty("Result").notNull();
        rActivityHeader.addStringProperty("Insight").notNull();
        rActivityHeader.addIntProperty("PhotoRowSize").notNull();
        rActivityHeader.addIntProperty("PhotoColumnSize").notNull();
        rActivityHeader.addStringProperty("CreatedDate").notNull();
        rActivityHeader.addStringProperty("Created").notNull();
        rActivityHeader.addIntProperty("Saved").notNull();
        rActivityHeader.addLongProperty("DistributorId").notNull();
        rActivityHeader.addIntProperty("StoreId").notNull();
        rActivityHeader.addStringProperty("StoreName").notNull();
        rActivityHeader.addIntProperty("RoleId").notNull();
//        rActivityHeader.addStringProperty("RoleName").notNull();
        rActivityHeader.addStringProperty("UserNames").notNull();
        rActivityHeader.addBooleanProperty("IsDeleted").notNull();

        //ReportDetail Entity
        Entity rDisplayDetail = schema.addEntity("TReportActivityDetail");
        rDisplayDetail.addIdProperty();
        rDisplayDetail.addStringProperty("ReportDetailId").notNull();
        rDisplayDetail.addStringProperty("ReportHeaderId").notNull();
        rDisplayDetail.addIntProperty("ActivityTypeId").notNull();
        rDisplayDetail.addIntProperty("UserId").notNull();
        rDisplayDetail.addStringProperty("PhotoPath").notNull();
        rDisplayDetail.addIntProperty("ColumnNumber").notNull();
        rDisplayDetail.addIntProperty("RowNumber").notNull();
        rDisplayDetail.addIntProperty("LeftPosition").notNull();
        rDisplayDetail.addIntProperty("TopPosition").notNull();
        rDisplayDetail.addIntProperty("RightPosition").notNull();
        rDisplayDetail.addIntProperty("BottomPosition").notNull();
        rDisplayDetail.addStringProperty("InfoPhoto").notNull();
        rDisplayDetail.addStringProperty("CreatedDate").notNull();
        rDisplayDetail.addStringProperty("Created").notNull();
        rDisplayDetail.addStringProperty("modified").notNull();


        //region ActivityName Type Entity
        Entity rLocalActivity = schema.addEntity("TLocalActivityType");
        rLocalActivity.addIdProperty();
        rLocalActivity.addIntProperty("ActivityTypeId").notNull();
        rLocalActivity.addStringProperty("ActivityName").notNull();
        //endregion

        //region Report Survey Entity
        Entity rSurveyPrice = schema.addEntity("TReportSurveyPrice");
        rSurveyPrice.addIdProperty();
        rSurveyPrice.addStringProperty("VisitId");
        rSurveyPrice.addIntProperty("ProductId").notNull();
        rSurveyPrice.addStringProperty("ProductName").notNull();
        rSurveyPrice.addDoubleProperty("Jual").notNull();
        rSurveyPrice.addDoubleProperty("Beli").notNull();
        //endregion

        //region Product MSL Price Entity
        Entity productMSL = schema.addEntity("TProductMSL");
        productMSL.addIdProperty();
        productMSL.addIntProperty("ProductId").notNull();
        productMSL.addStringProperty("ProductCode").notNull();
        productMSL.addStringProperty("ProductName").notNull();
        productMSL.addIntProperty("BrandId").notNull();
        productMSL.addIntProperty("ProdusenId").notNull();
        //endregion

        //region Product MSL Region Entity
        Entity productMSLRegion = schema.addEntity("TProductMSLRegion");
        productMSLRegion.addIdProperty();
        productMSLRegion.addIntProperty("ProductId").notNull();
        productMSLRegion.addStringProperty("ProductName").notNull();
        productMSLRegion.addIntProperty("isChoosed").notNull();
        productMSLRegion.addIntProperty("isChecked").notNull();
        //endregion

        //ReportHeader Entity
        Entity rWeeklyReportHeader = schema.addEntity("TReportWeeklyHeader");
        rWeeklyReportHeader.addIdProperty();
        rWeeklyReportHeader.addStringProperty("HeaderId").notNull();
        rWeeklyReportHeader.addIntProperty("UserId").notNull();
        rWeeklyReportHeader.addIntProperty("WeeklyId").notNull();
        rWeeklyReportHeader.addIntProperty("AreaId").notNull();
        rWeeklyReportHeader.addLongProperty("DistId").notNull();
        rWeeklyReportHeader.addStringProperty("Tanggal").notNull();
        rWeeklyReportHeader.addStringProperty("Mekanisme").notNull();
        rWeeklyReportHeader.addStringProperty("Tujuan").notNull();
        rWeeklyReportHeader.addStringProperty("Result").notNull();
        rWeeklyReportHeader.addStringProperty("Insight").notNull();
        rWeeklyReportHeader.addStringProperty("PesertaHadir").notNull();
        rWeeklyReportHeader.addIntProperty("PhotoRowSize").notNull();
        rWeeklyReportHeader.addIntProperty("PhotoColumnSize").notNull();


        //ReportDetail Entity
        Entity rWeeklyReportDetail = schema.addEntity("TReportWeeklyDetail");
        rWeeklyReportDetail.addIdProperty();
        rWeeklyReportDetail.addStringProperty("ReportDetailId").notNull();
        rWeeklyReportDetail.addStringProperty("ReportHeaderId").notNull();
        rWeeklyReportDetail.addLongProperty("DistId").notNull();
        rWeeklyReportDetail.addIntProperty("UserId").notNull();
        rWeeklyReportDetail.addStringProperty("PhotoPath").notNull();
        rWeeklyReportDetail.addStringProperty("InfoPhoto").notNull();
        rWeeklyReportDetail.addIntProperty("ColumnNumber").notNull();
        rWeeklyReportDetail.addIntProperty("RowNumber").notNull();
        rWeeklyReportDetail.addIntProperty("LeftPosition").notNull();
        rWeeklyReportDetail.addIntProperty("TopPosition").notNull();
        rWeeklyReportDetail.addIntProperty("RightPosition").notNull();
        rWeeklyReportDetail.addIntProperty("BottomPosition").notNull();


        //region Distributor
        Entity distributorArea = schema.addEntity("TDistributorArea");
        distributorArea.addIdProperty();
        distributorArea.addIntProperty("DistributorId").notNull();
        distributorArea.addStringProperty("DistributorName").notNull();
        //endregion

        //region Distributor
        Entity reportPP = schema.addEntity("TReportProductPlanogram");
        reportPP.addIdProperty();
        reportPP.addStringProperty("VisitId").notNull();
        reportPP.addIntProperty("Posisi").notNull();
        reportPP.addIntProperty("ProductId").notNull();
        reportPP.addIntProperty("PlanogramTypeId").notNull();
        reportPP.addStringProperty("PlanogramStoreId").notNull();
        reportPP.addIntProperty("isListing").notNull();

        //region Distributor Store
        Entity distributorStore = schema.addEntity("TDistributorStore");
        distributorStore.addIdProperty();
        distributorStore.addIntProperty("DistributorId").notNull();
        distributorStore.addStringProperty("StoreName").notNull();
        distributorStore.addIntProperty("StoreId").notNull();
        distributorStore.addIntProperty("locationId").notNull();
        //endregion

        //region Activity Name Entity
        Entity rLocalActivityName = schema.addEntity("TLocalActivityName");
        rLocalActivityName.addIdProperty();
        rLocalActivityName.addIntProperty("ActivityNameId").notNull();
        rLocalActivityName.addStringProperty("ActivityName").notNull();
        rLocalActivityName.addIntProperty("activityId").notNull();
        //endregion

        //region Activity Location Name
        Entity rLocalActivityLocation = schema.addEntity("TLocalAcitivityLocation");
        rLocalActivityLocation.addIdProperty();
        rLocalActivityLocation.addIntProperty("LocationId").notNull();
        rLocalActivityLocation.addStringProperty("LocationName").notNull();
        //endregion


        //region Weekly Name Entity
        Entity rWeeklyActivityName = schema.addEntity("TWeekActivityName");
        rWeeklyActivityName.addIdProperty();
        rWeeklyActivityName.addIntProperty("WeeklyNameId").notNull();
        rWeeklyActivityName.addStringProperty("WeeklyName").notNull();
        //endregion

        //endregion

        //region Role User Type
        Entity roleType = schema.addEntity("TRole");
        roleType.addIdProperty();
        roleType.addIntProperty("RoleId").notNull();
        roleType.addStringProperty("RoleName").notNull();
        //endregion

        //region
        Entity nooRegion = schema.addEntity("TNooRegion");
        nooRegion.addIdProperty();
        nooRegion.addIntProperty("RegionId");
        nooRegion.addStringProperty("RegionName");
        nooRegion.addStringProperty("Alias");
        //endregion

        //region Area
        Entity nooArea = schema.addEntity("TNooArea");
        nooArea.addIdProperty();
        nooArea.addIntProperty("AreaId");
        nooArea.addStringProperty("AreaName");
        nooArea.addIntProperty("RegionId");
        //endregion

        //region Distributor
        Entity nooDistributor = schema.addEntity("TNooDistributor");
        nooDistributor.addIdProperty();
        nooDistributor.addIntProperty("DistributorId");
        nooDistributor.addStringProperty("DistributorName");
        nooDistributor.addIntProperty("AreaId");
        //endregion

        //region Compliance
        Entity campaignCompliance = schema.addEntity("TCampaignCompliance");
        campaignCompliance.addIdProperty();
        campaignCompliance.addIntProperty("CampaignId");
        campaignCompliance.addIntProperty("ComplianceId");
        campaignCompliance.addStringProperty("ComplianceCode");
        campaignCompliance.addStringProperty("Compliance");
        campaignCompliance.addStringProperty("PeriodeStart");
        campaignCompliance.addStringProperty("PeriodeEnd");
        //endregion

        //region Reason Posm Entity
        Entity reasonCampaignPosm = schema.addEntity("TReasonCampaignPosm");
        reasonCampaignPosm.addIdProperty();
        reasonCampaignPosm.addIntProperty("ReasonId").notNull();
        reasonCampaignPosm.addStringProperty("ReasonName").notNull();
        //endregion

        Entity rCompliance = schema.addEntity("TReportCompliance");
        rCompliance.addIdProperty();
        rCompliance.addStringProperty("VisitId");
        rCompliance.addIntProperty("CampaignId").notNull();
        rCompliance.addIntProperty("ComplianceId").notNull();
        rCompliance.addStringProperty("ComplianceCode").notNull();
        rCompliance.addStringProperty("Compliance").notNull();
        rCompliance.addIntProperty("IsComply").notNull();
        rCompliance.addStringProperty("PhotoPath").notNull();
        rCompliance.addBooleanProperty("IsDone");


        new DaoGenerator().generateAll(schema, "../app/src/main/java");
    }
}